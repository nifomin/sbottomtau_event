FROM atlas/analysisbase:21.2.96 AS buildstage
ADD --chown=atlas . /sbottomtau_event/
WORKDIR /sbottomtau_event/
RUN bash -c 'source /home/atlas/release_setup.sh && \
    rm -rf file/ && \
    rm -rf .git && \
    mkdir run build && \
    cd build && \
    cmake ../source && \
    make && ls -lt'

FROM atlas/analysisbase:21.2.96 AS deploystage
WORKDIR /sbottomtau_event/
COPY --from=0 --chown=atlas /sbottomtau_event/ /sbottomtau_event/

# && \
#source x86_64-centos7-gcc8-opt/setup.sh && \
#cd ../run && \
#ls -lt ../ && \
#test_submit --inputPath=../file --baselineNtuple --bjetTrig && \
#../control/./process.sh ../file output.root'
#ls -lt submitDir/data-miniVectorNtuple_SbotPlateau && \
#normalise_samples submitDir/data-miniVectorNtuple_SbotPlateau/mc16_13TeV.436531.MGPy8EG_A14N23LO_BB_onestepN2hN1_850_180_50_1tau.deriv.DAOD_SUSY3.e6967_a875_r9364_p3712.root test.root 25000 && \
#add_bjet_trig_sf test.root && \
#flatten_ntuples test.root flat_signal.root'
