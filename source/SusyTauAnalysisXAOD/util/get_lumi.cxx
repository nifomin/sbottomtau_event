#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <TTree.h>
#include <TFile.h>

#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"

// SUSYTools
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "SUSYTools/SUSYCrossSection.h"

#include <utility>
#include <map>
#include <set>
#include <fstream>

#include <cstdlib>

int main( int argc, char* argv[] ) {
    if ( argc < 1 )
	return 1;
    
    std::cerr << "Processing..." << std::endl;
    
    //Get old file, old tree and set top branch address
    TFile *file = new TFile(argv[1]);
    TTree *tree = (TTree*)file->Get("NOMINAL");
    Long64_t nentries = tree->GetEntries();
    
    std::set<std::pair<int, int > > lb_count;
    
    int lb, rn;
    tree->SetBranchAddress("LumiBlock",&lb);
    tree->SetBranchAddress("RunNumber",&rn);
    
    for (Long64_t i=0;i<nentries; i++) {
	tree->GetEntry(i);

	if ( i % 100000 == 0 )
	    std::cerr << i << " / " << nentries << std::endl; 
	
	if ( lb_count.count( std::pair<int,int>( rn, lb ) ) )
	{
	    continue;
	}
	else
	{
	    lb_count.insert( std::pair<int,int>( rn, lb ) );
	}
    }
    
    TFile * lumi = new TFile((std::string( std::getenv("ROOTCOREBIN") ) + "/data/SusyTauAnalysisXAOD/prw/ilumicalc_histograms_None_276262-284484.root").c_str());
    TTree * lumitree = (TTree*)lumi->Get("LumiMetaData");
    
    float lumi_lb;
    unsigned int run,lumiblock;
    float total_lumi = 0;
    
    lumitree->SetBranchAddress("RunNbr", &run);
    lumitree->SetBranchAddress("LBStart", &lumiblock);
    lumitree->SetBranchAddress("IntLumi", &lumi_lb);
    
    
    for ( int i = 0; i < lumitree->GetEntries(); i++ )
    {
	lumitree->GetEntry(i);
	if ( lb_count.count( std::pair<int,int>( run, lumiblock ) ) )
	    total_lumi += lumi_lb;
    }
    std::cout << total_lumi / 1000000 << std::endl;
    return 0;
}
