#include <stdio.h>
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"
#include "TH1F.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TVirtualPad.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TPad.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include <map>
#include <iterator>
#include "TLatex.h"
#include "TIterator.h"
#include "TKey.h"
#include "TBranch.h"
#include "util/leaves.h"
#include "TEfficiency.h"
#include "TGraphAsymmErrors.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"

using namespace std;

int main( int argc, char* argv[] ) {

//std::string cdi_file_path = "/afs/cern.ch/work/m/mstamenk/public/CDI-Giovanni/files/2017-21-13TeV-MC16-CDI-Online-20190118.root";
std::string cdi_file_path = "/sbottomtau_event/grls/2017-21-13TeV-MC16-CDI-Online-20190118.root";
 std::string offlineWP = "77", onlineWP = "60"; 

BTaggingEfficiencyTool* offlineSFtool;
BTaggingEfficiencyTool* conditionalSFtool;
 offlineSFtool = new BTaggingEfficiencyTool("offlineSFtool");
  offlineSFtool->setProperty("ScaleFactorFileName", cdi_file_path);
  offlineSFtool->setProperty("JetAuthor", "AntiKt4EMTopoJets");
  offlineSFtool->setProperty("TaggerName", "MV2c10");
  offlineSFtool->setProperty("OperatingPoint", "FixedCutBEff_"+offlineWP);
  offlineSFtool->initialize();


  conditionalSFtool = new BTaggingEfficiencyTool("conditionalSFtool");
  conditionalSFtool->setProperty("ScaleFactorFileName", cdi_file_path);
  conditionalSFtool->setProperty("JetAuthor", "AntiKt4EMTopoJets");
  conditionalSFtool->setProperty("TaggerName", "CombinedOnlineMV2c20OfflineMV2c10"+offlineWP);
  conditionalSFtool->setProperty("OperatingPoint", "FixedCutBEff_"+onlineWP);
//  conditionalSFtool->setProperty("SystematicsStrategy", "Envelope"); 
  conditionalSFtool->initialize();

        CP::SystematicSet systs = conditionalSFtool->affectingSystematics();
           std::vector<CP::SystematicSet> sets;
           for (auto syst : systs) {
             CP::SystematicSet myset;
             myset.insert(syst);
             sets.push_back(myset);
           }


std::string samplename;
std::string susytag;
bool isSUSY5 = false;

if(argc > 3){std::cout<<"Too many arguments. Exiting."<<std::endl; return 0;}

else if (argc ==2)
{
samplename = argv[1];
if(susytag=="susy5"){isSUSY5 = true;}
}
else if (argc ==3)
{
samplename = argv[1];
susytag = argv[2];
if(susytag=="susy5"){isSUSY5 = true;}
}

else if (argc < 2){std::cout<<"Please provide input file as arguments. Exiting."<<std::endl; return 0;}


std::cout<<samplename<<std::endl;


TFile *bjeteff16 = new TFile("/sbottomtau_event/grls/BJetTriggerEfficiencies-00-02-01.root");

//TFile *bjeteff17 = new TFile("/disk/atlas3/users/nifomin/sbottom_trig/BJetTriggerEfficiencies-Data2017-GRL_RenamedGraphs_03Feb2019.root");
TFile *bjeteff17 = new TFile("/sbottomtau_event/grls/BJetTriggerEfficiencies-Data2017-410470-09May2019.root");


TGraphAsymmErrors *bjetcorr17 = (TGraphAsymmErrors*)bjeteff17->Get("g_SF_Off77_match_hlt50_jetPt");
TGraphAsymmErrors *bjetcorr16 = (TGraphAsymmErrors*)bjeteff16->Get("g_Eff_Event_leadingJet_jetEta");
TGraphAsymmErrors *bjetcorr162 = (TGraphAsymmErrors*)bjeteff16->Get("g_SF_offJets77_match_hlt_match_hlt60_jetPt");









TFile *f = new TFile(samplename.c_str(), "update");

std::vector< std::string > processed_keys;

TIter next(f->GetListOfKeys());
TKey *key;

while ((key=(TKey*)next()))
{

if ( key->GetClassName() != std::string("TTree") ) {continue;}
if ( key->GetName() == std::string("signal_metadata") || key->GetName() == std::string("signal_metadata_CBK") || key->GetName() == std::string("skim_metadata")){continue;}



TTree *tree = (TTree*)f->Get(key->GetName());

if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
      continue;
else processed_keys.push_back( key->GetName() );

std::cout<< key->GetName() << std::endl;



std::vector<float> *jet_pt = 0;
tree->SetBranchAddress("jet_pt", &jet_pt);
std::vector<float> *jet_eta = 0;
tree->SetBranchAddress("jet_eta", &jet_eta);
std::vector<float> *jet_mv2c10 = 0;
tree->SetBranchAddress("jet_mv2c10", &jet_mv2c10);
std::vector<float> *jet_onlineBtag = 0;
tree->SetBranchAddress("jet_onlineBtag", &jet_onlineBtag);
std::vector<int> *jet_isBjetMatched = 0;
tree->SetBranchAddress("jet_isBjetMatched", &jet_isBjetMatched);
std::vector<int> *jet_isBjet = 0;
tree->SetBranchAddress("jet_isBjet", &jet_isBjet);
int treatAsYear;
tree->SetBranchAddress("treatAsYear", &treatAsYear);
std::vector<int> *jet_HadronTruthID = 0;
tree->SetBranchAddress("jet_hadronTruthID", &jet_HadronTruthID);


float bjet_trig_sf;
TBranch *bsf = tree->Branch("bjet_trig_sf",&bjet_trig_sf,"bjet_trig_sf/F");
float bjet_trig_sf_up;
TBranch *bsf_up = tree->Branch("bjet_trig_sf_up",&bjet_trig_sf_up,"bjet_trig_sf_up/F");
float bjet_trig_sf_down;
TBranch *bsf_down = tree->Branch("bjet_trig_sf_down",&bjet_trig_sf_down,"bjet_trig_sf_down/F");

float bjet_trig_sf2;
TBranch *bsf2 = tree->Branch("bjet_trig_sf2",&bjet_trig_sf2,"bjet_trig_sf2/F");


double trig_sf, trig_sf_up, trig_sf_down;

for(int i = 0;i<tree->GetEntries();i++)
{
tree->GetEntry(i);

int nbjet = 0;
double bjetptmatch = 0;
bjet_trig_sf = 1.;
for(unsigned int j = 0;j<jet_pt->size();j++)
{
if(jet_pt->at(j) > 100000 && jet_isBjet->at(j) == 1 && (jet_isBjetMatched->at(j)  || isSUSY5))
{
if(jet_pt->at(j) > bjetptmatch){bjetptmatch = jet_pt->at(j);}//std::cout<<"matched bjet"<<std::endl;}
nbjet++;
}
}

double bjetpt = 0;
bjet_trig_sf = 1.;
bjet_trig_sf2 = 1.;
trig_sf = 1.;
trig_sf_up = 1.;
trig_sf_down = 1.;
for(unsigned int j = 0;j<jet_pt->size();j++)
{
if(jet_pt->at(j) > 20000 && jet_isBjet->at(j) == 1)
{
if(jet_pt->at(j) > bjetpt){bjetpt = jet_pt->at(j);}//std::cout<<"matched bjet"<<std::endl;}
}
}

//std::cout<<"bjetpt "<<bjetpt<<std::endl;
//std::cout<<bjetcorr17->Eval(bjetpt/1000.)<<std::endl;

if(treatAsYear == 2015 || treatAsYear == 2016)
{
for(unsigned int j = 0;j<jet_pt->size();j++)
{
if(jet_pt->at(j) > 100000 && jet_isBjet->at(j) == 1 && (jet_isBjetMatched->at(j) || isSUSY5))
{
  float offlineSF = 1, offlineMCeff = 1;
  Analysis::CalibrationDataVariables vars_jet;
  vars_jet.jetPt = jet_pt->at(j);
  vars_jet.jetEta = jet_eta->at(j);
  vars_jet.jetTagWeight = jet_mv2c10->at(j);
  offlineSFtool->applySystematicVariation(CP::SystematicSet());
  offlineSFtool->getScaleFactor(jet_HadronTruthID->at(j), vars_jet, offlineSF);
  offlineSFtool->getMCEfficiency(jet_HadronTruthID->at(j), vars_jet, offlineMCeff);
//std::cout<<offlineSF<<std::endl;
//std::cout<<offlineMCeff<<std::endl;
float envelope = 0;
float conditionalSF = 1, conditionalMCeff = 1;
 bool isB = jet_HadronTruthID->at(j) == 5;  
  bool isPt35 = jet_pt->at(j) > 35000.;  
 bool isOnlineMatched = jet_onlineBtag->at(j) > -1.5;
 if ( isB && isPt35 && isOnlineMatched ) {
    vars_jet.jetTagWeight = jet_onlineBtag->at(j);
 conditionalSFtool->applySystematicVariation(CP::SystematicSet());       
 conditionalSFtool->getScaleFactor(jet_HadronTruthID->at(j), vars_jet, conditionalSF);    
 conditionalSFtool->getScaleFactor(jet_HadronTruthID->at(j), vars_jet, conditionalMCeff); 

//  CP::SystematicSet recommendedSystematicsList = conditionalSFtool->affectingSystematics();
// for (CP::SystematicVariation sysvar : recommendedSystematicsList) printf("%s\n",sysvar.name().c_str());


//  std::vector<CP::SystematicSet> variedSFs; variedSFs.resize(sets.size());
           for (unsigned int var = 0; var < sets.size(); ++var) {
float conditionalSF2 = 1;
conditionalSFtool->applySystematicVariation(sets[var]);
conditionalSFtool->getScaleFactor(jet_HadronTruthID->at(j), vars_jet, conditionalSF2);
envelope = envelope + pow((conditionalSF - conditionalSF2),2);
}

envelope = envelope/(sets.size()-1.);
envelope = sqrt(envelope);
}
float combinedSF = offlineSF*conditionalSF; 
float combinedSF_up = offlineSF*(conditionalSF+envelope);
float combinedSF_down = offlineSF*(conditionalSF-envelope);

//float combinedMCeff = offlineMCeff*conditionalMCeff;   
// float combinedIneffSF = ( 1 - combinedMCeff*combinedSF) / ( 1 - combinedMCeff );

trig_sf=trig_sf*combinedSF;
trig_sf_up=trig_sf_up*combinedSF_up;
trig_sf_down=trig_sf_down*combinedSF_down;
}


}


bjet_trig_sf = trig_sf;bjet_trig_sf_up = trig_sf_up;bjet_trig_sf_down = trig_sf_down;
}
//std::cout<<trig_sf<<std::endl;


if(bjetptmatch > 100000)
{
if(treatAsYear == 2015 || treatAsYear == 2016){bjet_trig_sf2 = bjetcorr162->Eval(bjetptmatch/1000.) * bjetcorr16->Eval(jet_eta->at(0));}
else if (treatAsYear == 2018){bjet_trig_sf2 = 1;}
}
else if(bjetptmatch < 100000)
{
if(treatAsYear == 2015 || treatAsYear == 2016){bjet_trig_sf2 =1;}
else if (treatAsYear == 2018){bjet_trig_sf2 = 1;}
}


double x,y;
float x1,y1;
//x=0;y=0;x1=0;y1=0;

double presf = 0;
double presf_up = 0;
double presf_down = 0;



for(unsigned int j = 0;j<jet_pt->size();j++)
{
if(jet_pt->at(j) > 100000 && jet_isBjet->at(j) == 1 && (jet_isBjetMatched->at(j) || isSUSY5))
{
presf = 1;presf_up = 1;presf_down = 1;

if(treatAsYear == 2017) //{bjet_trig_sf = bjetcorr17->Eval(bjetpt/1000.);}
{
for(int i = 0; i < bjetcorr17->GetN();i++)
{
y1 = bjetcorr17->GetErrorY(i);
x1 = bjetcorr17->GetErrorX(i);
bjetcorr17->GetPoint(i,x,y);
//std::cout<<y-y1<<"  "<<y<<y+y1<<"  "<<std::endl;
if(jet_pt->at(j)/1000. > (x-x1) && jet_pt->at(j)/1000. < (x+x1))
{
presf = presf * (1-y);
presf_up = presf_up * (1 - (y+y1));
presf_down = presf_down * (1 - (y-y1));
}


}
}


if(treatAsYear == 2018)
{
for(int i = 0; i < bjetcorr17->GetN();i++)
{
y1 = bjetcorr17->GetErrorY(i);
x1 = bjetcorr17->GetErrorX(i);
bjetcorr17->GetPoint(i,x,y);
//std::cout<<y-y1<<"  "<<y<<"  "<<y+y1<<std::endl;
if(jet_pt->at(j)/1000. > (x-x1) && jet_pt->at(j)/1000. < (x+x1))
{
presf = presf * (1-y);
presf_up = presf_up * (1 - (y+y1));
presf_down = presf_down * (1 - (y-y1));
}


//bjet_trig_sf = bjetcorr17->Eval(bjetpt/1000.);


}
}


}
}

if(treatAsYear == 2018 || treatAsYear == 2017)
{
bjet_trig_sf = 1. - presf;
bjet_trig_sf_up = 1. - presf_up;
bjet_trig_sf_down = 1. - presf_down;
}
//if(isSUSY5){bjet_trig_sf = 1;}
//std::cout<<bjet_trig_sf<<std::endl;
bsf->Fill();
bsf_up->Fill();
bsf_down->Fill();
bsf2->Fill();

}
//tree->Print();
tree->Write();
}

f->Delete();
return 0;

}
