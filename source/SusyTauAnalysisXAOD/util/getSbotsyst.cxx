#include <TSystem.h>
#include <TTree.h>
#include <TKey.h>
#include <TFile.h>
#include <TCut.h>
#include <utility>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include "util/leaves.h"
#include <algorithm>

using namespace std;

// NNPDF30_NNLO has 100 PDF error sets
static const int Npdf = 100;

void FillSumw(TreeLeavesFlat* leaves, vector<string> region, vector<int> index, std::map<int, vector<double> > sumw_CBK, double** sumw)
{

  // don't process samples that don't have LHE3 weights
  if(leaves->mcEventWeights->size() < Npdf) return;


  bool triggercuts 	 = leaves->JetTileVeto == 0 && leaves->TauTileVeto == 0 && leaves->JetTileVetoLBA52 == 0 && leaves->TauTileVetoLBA52 == 0 && leaves->IsMETTrigPassed > 0 && leaves->met > 180000 && leaves->jet_1_pt > 120000 && leaves->jet_n > 1 && leaves->jet_1_isBadTight == 0 && leaves->jet_2_pt > 25000;
  bool qcdcuts		 = leaves->jet_1_delPhiMet > 0.4 && leaves->jet_2_delPhiMet > 0.4;
  bool antiqcdcuts	 = (leaves->jet_1_delPhiMet < 0.4 || leaves->jet_2_delPhiMet < 0.4);
  //bool qcdcutscr	 = qcdcuts && leaves->jet_1_delPhiMet < (3.14159-0.2);
  bool truecrcut 	 = leaves->tau_1_mtMet < 80000;
  bool onetaucut         = leaves->tau_n == 1 && leaves->tau_1_JetBDTMedium == true;
  bool twotaucut         = leaves->tau_n > 1 && leaves->tau_1_JetBDTMedium == true && leaves->tau_2_JetBDTMedium == true;
  bool cruppercut        = leaves->ht<800000 && leaves->met < 300000;  
  bool leptoncut  	 = leaves->mu_n == 1;
  bool leptonvetocut	 = leaves->mu_n == 0;
  bool fakecrcut 	 = leaves->mu_1_mtMet < 100000;
  bool shapeBaselineCuts = twotaucut && triggercuts && qcdcuts && leaves->jet_n > 2 && leaves->sumMT > 150000 && leaves->ht > 800000;



  double weight = leaves->lumiWeight;

  // WARNING !!! DO WE APPLY OTHER WEIGHTS??
  // there can be correlations between PDF/scale and the pt/HT regime...
  // some weights are clearly not needed, like PRW, JVT (totally uncorrelated)
  weight *= leaves->tau_weight;
  weight *= leaves->bjet_weight;
  // just to cross check the pre-fit yields, don't need to use these for theory uncertainty calculation
  weight *= leaves->pileupweight;
  weight *= leaves->jvt_weight;

 //need to add more regions here, TODO 
  for(uint i=0;i<region.size();i++) {
    
    bool pass = false;
    
    
    if(region[i]=="Nominal"){pass=true;} 
    else if(region[i]=="KinTauCR"){pass = leaves->IsBjetTrigPassed == 1 && leaves->ht < 700000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.4 && leaves->jet_2_delPhiMet > 0.4 && leaves->mu_n>=1 && leaves->jet_n >=3 && leaves->jet_n_btag == 2 && leaves->tau_n == 0 && leaves->met > 160000 && leaves->JetTileVeto==0 && leaves->JetTileVetoLBA52==0 && leaves->TauTileVeto==0 && leaves->TauTileVetoLBA52==0 && leaves->jet_1_isBadTight==0 && leaves->passBjetGRL;}
    else if(region[i]=="TrueTauCR"){pass = leaves->IsBjetTrigPassed == 1 && leaves->ht < 700000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.4 && leaves->jet_2_delPhiMet > 0.4 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 2 && leaves->tau_n == 1 && leaves->met > 160000 && leaves->JetTileVeto==0 && leaves->JetTileVetoLBA52==0 && leaves->TauTileVeto==0 && leaves->TauTileVetoLBA52==0  && leaves->jet_1_isBadTight==0 && leaves->passBjetGRL && leaves->tau_1_mtMet <80000;}
    else if(region[i]=="FakeTauCR"){pass = leaves->IsBjetTrigPassed == 1 && leaves->ht < 700000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.4 && leaves->jet_2_delPhiMet > 0.4 && leaves->mu_n>=1 && leaves->jet_n >=2 && leaves->tau_n >= 1 && leaves->met > 160000 && leaves->taumu_angles > 1.0 && leaves->JetTileVeto==0 && leaves->JetTileVetoLBA52==0 && leaves->TauTileVeto==0 && leaves->TauTileVetoLBA52==0  && leaves->jet_1_isBadTight==0 && leaves->passBjetGRL;}
    else if(region[i]=="BtagCR"){pass = leaves->IsBjetTrigPassed == 1 && leaves->ht < 700000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.4 && leaves->jet_2_delPhiMet > 0.4 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 1 && leaves->tau_n == 1 && leaves->met > 160000 && leaves->JetTileVeto==0 && leaves->JetTileVetoLBA52==0 && leaves->TauTileVeto==0 && leaves->TauTileVetoLBA52==0  && leaves->jet_1_isBadTight==0 && leaves->passBjetGRL;}

    else{continue;}      
    



    if(pass==true) {

      // nominal also should be mcEventWeights[0]
      sumw[i][0] += weight*leaves->mcEventWeight;

      if(leaves->mcEventWeight != leaves->mcEventWeights->at(0))
	cout << "PROBLEM WITH NOMINAL WEIGHT" << endl;

      // weight systematics
      for(uint j=1;j<index.size();j++) {
	if(sumw_CBK[leaves->SampleID][index[j]] == 0.) 
	  cout << "Empty CBK for SampleID " << leaves->SampleID << " weight index " << index[j] << endl;
	else 
{
	  // normalize w.r.t sumw_CBK of the systematics variation
if(index[j]==193){sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]] * leaves->mcEventWeights->at(index[j-2]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-2]] / leaves->mcEventWeight;}
else if(index[j]==194){sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]]* leaves->mcEventWeights->at(index[j-6]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-6]] / leaves->mcEventWeight;}
else{sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]];}
}
      }
    }

  }
}


int main( int argc, char* argv[] ) {

  if ( argc !=2  )
    return 1;


  TFile *file = new TFile(argv[1],"READ");  

//TODO add more regions here
  vector<string> region;
  region.push_back("Nominal");
  region.push_back("KinTauCR");
  region.push_back("TrueTauCR");
  region.push_back("FakeTauCR");
  region.push_back("BtagCR");

  const uint Nregion = region.size();

  // indices of weight systematic variation, with rel21/mc16 the strategy changes a bit, updating indices
  vector<int> index;
  // nominal
  index.push_back(0);
  // PDF weights
  for(uint j=0;j<Npdf;j++) index.push_back(15+j);
  // alpha_s down is now apparently isr:muRfac=10_fsr:muRfac=20 
  index.push_back(199);
  // alpha_s up is now apparently isr:muRfac=10_fsr:muRfac=05
  index.push_back(198);
  // MUR0.5_MUF0.5
  index.push_back(5);
  // MUR0.5_MUF1
  index.push_back(4);
  // MUR1_MUF0.5
  index.push_back(2);
  // MUR1_MUF2
  index.push_back(1);
  // MUR2_MUF1
  index.push_back(3);
  // MUR2_MUF2
  index.push_back(6);
  // Var3cUp
  index.push_back(193);
  // Var3cDown
  index.push_back(194);
  // 1 nominal, Npdf(=100) PDF variations, 2 alpha_s variations, 6 scale variations 
  const uint Nsyst = index.size();

  double** sumw = new double*[Nregion];
  // shall we compute sumw2 to keep track of high-weight events?

  for(uint i=0;i<Nregion;i++) {
    sumw[i] = new double[Nsyst];
    for(uint j=0;j<Nsyst;j++) sumw[i][j] = 0.;
  }


  // rescale sumw[i] to sumw[0] using CutBookKeepers
  std::map<int, vector<double> > sumw_CBK;
  
  TTree * metadata = (TTree*) file->Get("skim_metadata");
  if( metadata )
    {
      vector<double>* EventWeightSumVect = 0;
      int SampleID;
      metadata->SetBranchAddress("SampleID", &SampleID);
      metadata->SetBranchAddress("EventWeightSumVect", &EventWeightSumVect);
      for ( int i = 0; i < metadata->GetEntries(); i++ )
	{
	  metadata->GetEntry( i );

	  if(sumw_CBK[SampleID].size()==0) {
	    for(uint j=0;j<EventWeightSumVect->size();j++)
	      sumw_CBK[SampleID].push_back(EventWeightSumVect->at(j));
	  }
	  else {
	    if(sumw_CBK[SampleID].size() != EventWeightSumVect->size()) {
	      cout << "Inconsistent vector size: " << sumw_CBK[SampleID].size() << " vs " << EventWeightSumVect->size() << ". Aborting..." << endl;
	      return 1;
	    }
	    for(uint j=0;j<EventWeightSumVect->size();j++)
	      sumw_CBK[SampleID][j] += EventWeightSumVect->at(j);
	  }
	}
    }
  else {
    cout << "Could not find metadata tree. Aborting..." << endl;
    return 1;
  }

  TIter next(file->GetListOfKeys());
  TKey *key;
  
  vector< string > processed_keys;
  
  while ((key=(TKey*)next()))
    {
      if ( key->GetClassName() != std::string("TTree") || key->GetName() != std::string("NOMINAL") )
	continue;
      
      // process trees only once
      if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
	continue;
      else processed_keys.push_back( key->GetName() );
      
      cout << "processing tree " << key->GetName() << endl;

      TTree *tree = (TTree*)file->Get( key->GetName() );

      // load flat ntuple structure from flattener
      TreeLeavesFlat * leaves = new TreeLeavesFlat();

      // set branch address for needed variables
      tree->SetBranchAddress("taumu_angles", &leaves->taumu_angles);
      tree->SetBranchAddress("bjet_1_pt", &leaves->bjet_1_pt);
      tree->SetBranchAddress("bjet_2_pt", &leaves->bjet_2_pt);
      tree->SetBranchAddress("jet_n_btag", &leaves->jet_n_btag);
      tree->SetBranchAddress("passBjetGRL", &leaves->passBjetGRL);
      tree->SetBranchAddress("IsBjetTrigPassed", &leaves->IsBjetTrigPassed);
      tree->SetBranchAddress("IsMETTrigPassed", &leaves->IsMETTrigPassed);
      tree->SetBranchAddress("tau_n", &leaves->tau_n);
      tree->SetBranchAddress("tau_1_pt", &leaves->tau_1_pt);
      tree->SetBranchAddress("tau_1_JetBDTMedium", &leaves->tau_1_JetBDTMedium);
      tree->SetBranchAddress("tau_1_mtMet", &leaves->tau_1_mtMet);
      tree->SetBranchAddress("tau_1_delPhiMet", &leaves->tau_1_delPhiMet);
      tree->SetBranchAddress("tau_1_charge", &leaves->tau_1_charge);
      tree->SetBranchAddress("tau_2_pt", &leaves->tau_2_pt);
      tree->SetBranchAddress("tau_2_JetBDTMedium", &leaves->tau_2_JetBDTMedium);
      tree->SetBranchAddress("tau_2_mtMet", &leaves->tau_2_mtMet);
      tree->SetBranchAddress("tau_2_delPhiMet", &leaves->tau_2_delPhiMet);
      tree->SetBranchAddress("tau_2_charge", &leaves->tau_2_charge);
      tree->SetBranchAddress("jet_n", &leaves->jet_n);
      tree->SetBranchAddress("jet_n_btag", &leaves->jet_n_btag);
      tree->SetBranchAddress("jet_1_pt", &leaves->jet_1_pt);
      tree->SetBranchAddress("jet_1_isBadTight", &leaves->jet_1_isBadTight);
      tree->SetBranchAddress("jet_1_delPhiMet", &leaves->jet_1_delPhiMet);
      tree->SetBranchAddress("jet_2_pt", &leaves->jet_2_pt);
      tree->SetBranchAddress("jet_2_delPhiMet", &leaves->jet_2_delPhiMet);
      tree->SetBranchAddress("mu_n", &leaves->mu_n);
      tree->SetBranchAddress("mu_1_mtMet", &leaves->mu_1_mtMet);
      tree->SetBranchAddress("met", &leaves->met);
      tree->SetBranchAddress("meff", &leaves->meff);
      tree->SetBranchAddress("ht", &leaves->ht);
      tree->SetBranchAddress("sumMT", &leaves->sumMT);
      tree->SetBranchAddress("sumMTJet", &leaves->sumMTJet);
      tree->SetBranchAddress("sumMTTauJet", &leaves->sumMTTauJet);
      tree->SetBranchAddress("Mt2", &leaves->Mt2);
      tree->SetBranchAddress("Mtaumu", &leaves->Mtaumu);
      tree->SetBranchAddress("JetTileVeto", &leaves->JetTileVeto);
      tree->SetBranchAddress("TauTileVeto", &leaves->TauTileVeto);
      tree->SetBranchAddress("JetTileVetoLBA52", &leaves->JetTileVetoLBA52);
      tree->SetBranchAddress("TauTileVetoLBA52", &leaves->TauTileVetoLBA52);
      tree->SetBranchAddress("lumiweight", &leaves->lumiWeight);
      tree->SetBranchAddress("tau_medium_weight", &leaves->tau_medium_weight);
      tree->SetBranchAddress("tau_weight", &leaves->tau_weight);
      tree->SetBranchAddress("bjet_weight", &leaves->bjet_weight);
      tree->SetBranchAddress("pileupweight" , &leaves->pileupweight);
      tree->SetBranchAddress("jvt_weight" , &leaves->jvt_weight);
      tree->SetBranchAddress("mcEventWeights", &leaves->mcEventWeights);
      tree->SetBranchAddress("mcEventWeight", &leaves->mcEventWeight);
      tree->SetBranchAddress("SampleID", &leaves->SampleID);


      Long64_t nentries = tree->GetEntries();
    
      for (Long64_t i=0;i<nentries; i++) {

	if (i>0 && i%100000 == 0) cout << i << " / " << nentries << endl;

	tree->GetEntry(i);
	
	FillSumw(leaves,region,index,sumw_CBK,sumw);
      }

    }


  // perform the actual PDF+alpha_s and scale uncertainty calculation
  // NNPDF seems to have a symmetric error prescription (RMS estimator)
  // mc16 - prescription (somewhat) changed. Alpha_s and scale are somewhat different now?
  // radLow is now derived from nominal samples, radHigh from alternative samples.
  // we compute both, need to select them based on what hdamp samples were produced with. Nominal hdamp = 1.5, alternative hdamp = 3.0
  // this also means tot error is kind of useless
  double* PDF_err = new double[Nregion];
  
  for(uint i=0;i<Nregion;i++) {

    cout << "region " << region[i] << " sumw[0] " << sumw[i][0] << endl;

    PDF_err[i] = 0.; 

    for(uint j=1;j<=Npdf;j++) {
      PDF_err[i] += pow( sumw[i][j]-sumw[i][0], 2.);
    }    
    PDF_err[i] /= (Npdf-1);
    PDF_err[i] = sqrt(PDF_err[i]);
    
    double alphas_err = ( sumw[i][Npdf+2] - sumw[i][Npdf+1] )/2.;

    double scale_min = sumw[i][0];
    double scale_max = sumw[i][0];
    for(uint j=Npdf+3;j<=Npdf+8;j++) {
      if(sumw[i][j] > scale_max) scale_max = sumw[i][j];
      if(sumw[i][j] < scale_min) scale_min = sumw[i][j];
    }

    double scale_err = ( scale_max - scale_min )/2.;
    
    double radLow_err =  sumw[i][Npdf+10] / sumw[i][0];
    double radHigh_err = sumw[i][Npdf+9] / sumw[i][0];

    double err_tot = sqrt( pow(PDF_err[i],2.) + pow(alphas_err,2.) + pow(scale_err,2.) );
    
    cout << "PDF " << PDF_err[i]/sumw[i][0] << " alpha_s " << alphas_err/sumw[i][0] << " radLow " <<radLow_err  << " radHigh " << radHigh_err << endl << endl ;
  }

  // need to decide which output format we want for HistFitter
  
  
  
  return 0;
}

