#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"

// SUSYTools
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "SUSYTools/SUSYCrossSection.h"

#include <map>
#include <fstream>

int main( int argc, char* argv[] ) {

    // Take the submit directory from the input if provided:
    std::string submitDir = "submitDir";
    
    
    if ( ! SusyTauGlobals::s_config.ParseCommandLine( argc, argv ) )
	return 0;
    
    auto config_list = SusyTauGlobals::s_config.GetOptions();
    
    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;

    if ( ! config_list.count( "inputPath" ) )
    {
	std::cout << "Please provide an input path to read from" << std::endl;
	return 0;
    }
    
    const char* inputFilePath = gSystem->ExpandPathName ( config_list["inputPath"].as<std::string>().c_str() ); 
    SH::DiskListLocal list (inputFilePath);
    
    SH::ScanDir()
	.sampleDepth(0)
	.samplePostfix("_tid*_*")
	.scan( sh, list );
    
    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );
    // Add metadata for derivation and fullsim
    SusyTauGlobals::s_config.ConfigureSamples( sh );
    
    int pdgid1 = 0;
    int pdgid2 = 0;
    std::map<int, std::map<int, int> > counter;

    std::ofstream process_counter;
    process_counter.open(  std::string(getenv("ROOTCOREBIN")) +"/data/SusyTauAnalysisXAOD/signal_process_counter.txt");
	
    for ( SH::SampleHandler::iterator iter = sh.begin(); iter != sh.end(); ++ iter)
    {
	auto sh = *iter;
	if ( !sh->meta()->castBool("isSignal") )
	{
	    std::cout << "Not a signal sample, skipping!" << std::endl;
	    continue;
	}

	xAOD::TEvent event( xAOD::TEvent::kClassAccess );
	event.readFrom( (TTree*)sh->makeTChain() ).isSuccess();
	SusyTauGlobals::s_event = &event;
	
	Long64_t entries = event.getEntries();
	
	for( Long64_t entry = 0; entry < entries; ++entry )
	{
	    event.getEntry( entry );
	    if ( ! SusyTauGlobals::s_susyDef )
		SusyTauGlobals::InitialiseSusyTools( sh->meta(),&event );
	    
	    const xAOD::TruthParticleContainer* truthP = 0;
	    event.retrieve( truthP, "TruthParticles" ).isSuccess();
	    SusyTauGlobals::s_susyDef->FindSusyHP(truthP, pdgid1, pdgid2).isSuccess();
	    
	    const xAOD::EventInfo* eventInfo = 0;
	    if( ! event.retrieve( eventInfo, "EventInfo").isSuccess() ){
		Error("execute()", "Failed to retrieve event info collection. Exiting." );
		return EL::StatusCode::FAILURE;
	    }
	    
	    unsigned int process = SUSY::finalState( pdgid1, pdgid2 );
	    
	    counter[eventInfo->mcChannelNumber()][process]++;
	}

	for ( auto signal_point : counter )
	{
	    for ( auto signal_process : signal_point.second )
	    {
		process_counter << signal_point.first << "\t" << signal_process.first << "\t" << signal_process.second << std::endl;
	    }
	}
	counter.clear();
    }
    
    process_counter.close();
    delete SusyTauGlobals::s_susyDef;
    
    return 0;
}
