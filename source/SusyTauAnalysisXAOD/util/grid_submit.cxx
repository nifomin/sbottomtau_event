#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/Job.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SusyTauAnalysisXAOD/CalibrationAlg.h"
#include "SusyTauAnalysisXAOD/EventParametersAlg.h"
#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
#include "SusyTauAnalysisXAOD/NtupleVectorWriter.h"
#include "SusyTauAnalysisXAOD/JetSmearingAlg.h"
#include "SusyTauAnalysisXAOD/MakeTriggerDecision.h"

int main( int argc, char* argv[] ) {

  SusyTauGlobals::s_config.enableGridOptions();

  if ( ! SusyTauGlobals::s_config.ParseCommandLine( argc, argv ) )
    return 0;

  auto config_list = SusyTauGlobals::s_config.GetOptions();
    
  // submit directory
  std::string submitDir = config_list["subDir"].as<std::string>();
    
  // construct the samples to run on
  SH::SampleHandler sh = SusyTauGlobals::s_config.GetSampleHandler();

  // retrieve DAOD_SUSY format
  int SUSYformat = SusyTauConfig::GetDerivNum( sh );
  
  // baseline selection that can be applied before jet smearing (not involving smeared objects or overlap removal)
  std::vector< std::string > cutflow_baseline;
  cutflow_baseline.push_back("initial");
  cutflow_baseline.push_back("GRL");
  cutflow_baseline.push_back("EventCleaning");
  cutflow_baseline.push_back("NoPVtxVeto");
  cutflow_baseline.push_back("CosmicMuonVeto");
  cutflow_baseline.push_back("BadMuonVeto");
  cutflow_baseline.push_back("BadJetVeto");
    
  if ( config_list["jetSmear"].as<bool>() ) {
    // tau requirement can be applied before smearing (to speed things up) as long as tau smearing is not used
    // when tau smearing will be used, 'min1tauORmu' requirement will have to be moved after smearing
    cutflow_baseline.push_back("min1tauORmu");
    cutflow_baseline.push_back("pass_HLT_jX");
    // MET significance cut would go here
  }
  else {
    cutflow_baseline.push_back( SUSYformat==5? "min1mu" : "min1tau" );
  }

  // baseline ntuples
  std::string cutflow_baseline_name = "Baseline";
  std::vector< std::string > cutflow_vector_baseline =  cutflow_baseline;
  cutflow_vector_baseline.push_back("min100metORmetnomu");
  if( SUSYformat == 5 ) {
    cutflow_vector_baseline.push_back("min120jet1");
    cutflow_vector_baseline.push_back("min2jet25");
  }

  // MET plateau ntuples
  std::string cutflow_XEplateau_name = "XEplateau";
  std::vector< std::string > cutflow_vector_XEplateau = cutflow_baseline;
  cutflow_vector_XEplateau.push_back("min180met");
  cutflow_vector_XEplateau.push_back("min120jet1");
  cutflow_vector_XEplateau.push_back("min2jet25");

  // sbottom plateau ntuples with Zmmu CR
  std::string cutflow_SbotPlateau_name = "SbotPlateau";
  std::vector< std::string > cutflow_vector_SbotPlateau = cutflow_baseline;
  if( SUSYformat == 5 ) {cutflow_vector_SbotPlateau.push_back("Zcuts");}
  else {cutflow_vector_SbotPlateau.push_back("min160met");}
  cutflow_vector_SbotPlateau.push_back("min140jet1");
  cutflow_vector_SbotPlateau.push_back("min2jet100");
  
  // create an EventLoop job
  EL::Job job = SusyTauGlobals::s_config.GetJob();
  job.sampleHandler( sh );

  // add our algorithms to the job
  job.algsAdd( new CalibrationAlg() );

  job.algsAdd( new EventParametersAlg() ); 

  if ( config_list["bjetTrig"].as<bool>() ) {
    job.algsAdd( new MakeTriggerDecision() ); 
  }

  if ( config_list["jetSmear"].as<bool>() ) {
    job.algsAdd( new JetSmearingAlg( "Jet Smearing Baseline", cutflow_baseline ) );
  }  
  
  if (config_list["baselineNtuple"].as<bool>()) {    
    NtupleVectorWriter* NVW_baseline = new NtupleVectorWriter(cutflow_baseline_name,cutflow_vector_baseline);
    // ignore systematics for baseline ntuples, regardless of systematics options
    NVW_baseline->DeactivateSyst();
    job.algsAdd(NVW_baseline);
  }
  
  if (config_list["plateauNtuple"].as<bool>()) {
    NtupleVectorWriter* NVW_plateau  = new NtupleVectorWriter(cutflow_XEplateau_name,cutflow_vector_XEplateau);
    NVW_plateau->DeactivateSyst();
    job.algsAdd(NVW_plateau);
  }
  
  if (config_list["sbottomNtuple"].as<bool>()) {
    NtupleVectorWriter* NVW_sbottom  = new NtupleVectorWriter(cutflow_SbotPlateau_name,cutflow_vector_SbotPlateau);
    job.algsAdd(NVW_sbottom);
  }
    
  // run the job using the grid driver
  EL::PrunDriver driver;
    
  // sometimes too long
  driver.options()->setString("nc_outputSampleName","user.%nickname%."+config_list["outputPrefix"].as<std::string>()+".%in:sampleType%.%in:name[2]%.%in:name[5]%.%in:name[6]%");
  //driver.options()->setString("nc_outputSampleName", "user.%nickname%." + config_list["outputPrefix"].as<std::string>() + ".%in:sampleType%.%in:name[2]%.%in:name[5]%");

  //driver.options()->setDouble( EL::Job::optGridNFiles, 1);
  //driver.options()->setDouble( EL::Job::optGridNFilesPerJob,4 );
  //driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_HPC2N,ANALY_NSC");

  driver.submitOnly( job, submitDir );

  return 0;
}
