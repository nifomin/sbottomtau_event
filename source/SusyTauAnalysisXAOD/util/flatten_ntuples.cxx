#include <TSystem.h>
#include <TTree.h>
#include <TKey.h>
#include <TFile.h>
#include <utility>
#include <map>
#include <set>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include "util/leaves.h"
#include <algorithm>
#include "TLorentzVector.h"
#include "SusyTauAnalysisXAOD/lester_mt2_bisect.h"
#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
using namespace std;


// flat ntuple
void RegisterTree(TTree* tree, TreeLeavesFlat * leaves, bool isMC) {

  tree->Branch("EventNumber", &leaves->EventNumber);
  tree->Branch("RunNumber"      ,&leaves->RunNumber);
  tree->Branch("ProcessID", &leaves->ProcessID);
  tree->Branch("SampleID", &leaves->SampleID );
  tree->Branch("mcEventWeight", &leaves->mcEventWeight);
  tree->Branch("mcEventWeights", &leaves->mcEventWeights);
  tree->Branch("treatAsYear", &leaves->treatAsYear);
  //tree->Branch("PrescaleWeight", &leaves->PrescaleWeight);
  tree->Branch("isDebugStream", &leaves->isDebugStream);

  tree->Branch("tau_weight",       &leaves->tau_weight);
  tree->Branch("tau_medium_weight", &leaves->tau_medium_weight);
  tree->Branch("bjet_weight",       &leaves->bjet_weight);
  tree->Branch("jvt_weight",       &leaves->jvt_weight);
  tree->Branch("lumiweight",       &leaves->lumiWeight);
  tree->Branch("lumiweight_up",       &leaves->lumiWeight_up);
  tree->Branch("lumiweight_down",       &leaves->lumiWeight_down);
  tree->Branch("pileupweight",       &leaves->pileupweight);
  tree->Branch("pileupweight_up",       &leaves->pileupweight_up);
  tree->Branch("pileupweight_down",       &leaves->pileupweight_down);
  tree->Branch("TauSpinnerWeight",       &leaves->TauSpinnerWeight);
  tree->Branch("ele_weight",       &leaves->ele_weight);
  tree->Branch("mu_weight",       &leaves->mu_weight);
  tree->Branch("lumi_bjet",       &leaves->lumi_bjet);
  tree->Branch("pile_bjet",       &leaves->pile_bjet);
  tree->Branch("pile_bjet_up",       &leaves->pile_bjet_up);
  tree->Branch("pile_bjet_down",       &leaves->pile_bjet_down);
  tree->Branch("pile_bjet2",       &leaves->pile_bjet2);

  tree->Branch("muR10_muF20Weight",       &leaves->muR10_muF20Weight);
  tree->Branch("muR10_muF05Weight",       &leaves->muR10_muF05Weight);
  tree->Branch("muR20_muF10Weight",       &leaves->muR20_muF10Weight);
  tree->Branch("muR05_muF10Weight",       &leaves->muR05_muF10Weight);
  tree->Branch("muR05_muF05Weight",       &leaves->muR05_muF05Weight);
  tree->Branch("muR20_muF20Weight",       &leaves->muR20_muF20Weight);
  tree->Branch("fsr_muRfac05",       &leaves->fsr_muRfac05);
  tree->Branch("fsr_muRfac20",       &leaves->fsr_muRfac20);
  tree->Branch("alphasUp",       &leaves->alphasUp);
  tree->Branch("alphasDown",       &leaves->alphasDown);
  tree->Branch("Var3cDown",       &leaves->Var3cDown);
  tree->Branch("Var3cUp",       &leaves->Var3cUp);


  tree->Branch("lumi_muR10_muF20Weight",       &leaves->lumi_muR10_muF20Weight);
  tree->Branch("lumi_muR10_muF05Weight",       &leaves->lumi_muR10_muF05Weight);
  tree->Branch("lumi_muR20_muF10Weight",       &leaves->lumi_muR20_muF10Weight);
  tree->Branch("lumi_muR05_muF10Weight",       &leaves->lumi_muR05_muF10Weight);
  tree->Branch("lumi_muR05_muF05Weight",       &leaves->lumi_muR05_muF05Weight);
  tree->Branch("lumi_muR20_muF20Weight",       &leaves->lumi_muR20_muF20Weight);
  tree->Branch("lumi_fsr_muRfac05",       &leaves->lumi_fsr_muRfac05);
  tree->Branch("lumi_fsr_muRfac20",       &leaves->lumi_fsr_muRfac20);
  tree->Branch("lumi_alphasUp",       &leaves->lumi_alphasUp);
  tree->Branch("lumi_alphasDown",       &leaves->lumi_alphasDown);
  tree->Branch("lumi_Var3cDown",       &leaves->lumi_Var3cDown);
  tree->Branch("lumi_Var3cUp",       &leaves->lumi_Var3cUp);

  tree->Branch("JetTileVeto",	&leaves->JetTileVeto);
  tree->Branch("TauTileVeto",	&leaves->TauTileVeto);
  tree->Branch("JetTileVetoLBA52",   &leaves->JetTileVetoLBA52);
  tree->Branch("TauTileVetoLBA52",   &leaves->TauTileVetoLBA52);

  tree->Branch("tau_n",        &leaves->tau_n);
  tree->Branch("tau_nTruthMatched",	&leaves->tau_nTruthMatched);

  tree->Branch("tau_1_pt",    &leaves->tau_1_pt);
  tree->Branch("tau_1_eta",   &leaves->tau_1_eta);
  tree->Branch("tau_1_phi",   &leaves->tau_1_phi);
  tree->Branch("tau_1_charge",   &leaves->tau_1_charge);
  tree->Branch("tau_1_match", &leaves->tau_1_match);
  tree->Branch("tau_1_JetBDTLoose"   , &leaves->tau_1_JetBDTLoose);
  tree->Branch("tau_1_JetBDTMedium"  , &leaves->tau_1_JetBDTMedium);
  tree->Branch("tau_1_JetBDTTight"   , &leaves->tau_1_JetBDTTight);
  tree->Branch("tau_1_truthMatchedType"   , &leaves->tau_1_truthMatchedType);
  tree->Branch("tau_1_delPhiMet", &leaves->tau_1_delPhiMet);
  tree->Branch("tau_1_mtMet", &leaves->tau_1_mtMet);

  tree->Branch("tau_2_pt",    &leaves->tau_2_pt);
  tree->Branch("tau_2_eta",   &leaves->tau_2_eta);
  tree->Branch("tau_2_phi",   &leaves->tau_2_phi);
  tree->Branch("tau_2_charge",   &leaves->tau_2_charge);
  tree->Branch("tau_2_match", &leaves->tau_2_match);
  tree->Branch("tau_2_JetBDTLoose"   , &leaves->tau_2_JetBDTLoose);
  tree->Branch("tau_2_JetBDTMedium"  , &leaves->tau_2_JetBDTMedium);
  tree->Branch("tau_2_JetBDTTight"   , &leaves->tau_2_JetBDTTight);
  tree->Branch("tau_2_truthMatchedType"   , &leaves->tau_2_truthMatchedType);
  tree->Branch("tau_2_delPhiMet", &leaves->tau_2_delPhiMet);
  tree->Branch("tau_2_mtMet", &leaves->tau_2_mtMet);

  tree->Branch("jet_n",        &leaves->jet_n);
  tree->Branch("jet_n_btag",        &leaves->jet_n_btag);
//  tree->Branch("jet_truth_n",   &leaves->jet_truth_n);

  tree->Branch("jet_1_pt",    &leaves->jet_1_pt);
  tree->Branch("jet_1_eta",   &leaves->jet_1_eta);
  tree->Branch("jet_1_phi",   &leaves->jet_1_phi);
  tree->Branch("jet_1_btag",   &leaves->jet_1_btag);
  tree->Branch("jet_1_isBadTight",   &leaves->jet_1_isBadTight);
  tree->Branch("jet_1_delPhiMet", &leaves->jet_1_delPhiMet);

  tree->Branch("jet_2_pt",    &leaves->jet_2_pt);
  tree->Branch("jet_2_eta",   &leaves->jet_2_eta);
  tree->Branch("jet_2_phi",   &leaves->jet_2_phi);
  tree->Branch("jet_2_btag",   &leaves->jet_2_btag);
  tree->Branch("jet_2_delPhiMet", &leaves->jet_2_delPhiMet);

  tree->Branch("mu_1_pt",    &leaves->mu_1_pt);
  tree->Branch("mu_1_eta",   &leaves->mu_1_eta);
  tree->Branch("mu_1_phi",   &leaves->mu_1_phi);
  tree->Branch("mu_1_charge",   &leaves->mu_1_charge);

  tree->Branch("mu_2_pt",    &leaves->mu_2_pt);
  tree->Branch("mu_2_eta",   &leaves->mu_2_eta);
  tree->Branch("mu_2_phi",   &leaves->mu_2_phi);
  tree->Branch("mu_2_charge",   &leaves->mu_2_charge);

  tree->Branch("IsMETTrigPassed",  &leaves->IsMETTrigPassed);
  tree->Branch("IsBjetTrigPassed",  &leaves->IsBjetTrigPassed);
  tree->Branch("IsMuTrigPassed",  &leaves->IsMuTrigPassed);
  tree->Branch("met",         &leaves->met);
  tree->Branch("met_phi",     &leaves->met_phi);
  tree->Branch("met_sigPow3", &leaves->met_sigPow3);
  tree->Branch("meff",        &leaves->meff);
  tree->Branch("ht",          &leaves->ht);
  tree->Branch("sumMT", &leaves->sumMT);
  tree->Branch("sumMTJet", &leaves->sumMTJet);
  tree->Branch("sumMTTauJet", &leaves->sumMTTauJet);
  tree->Branch("Mt2",         &leaves->Mt2);
  tree->Branch("Mtaumu", &leaves->Mtaumu);
  tree->Branch("Mt2_taumu", &leaves->Mt2_taumu);

  tree->Branch("ele_n",        &leaves->ele_n);
  tree->Branch("ele_1_mtMet",    &leaves->ele_1_mtMet);

  tree->Branch("mu_n",        &leaves->mu_n);
  tree->Branch("mu_1_mtMet",    &leaves->mu_1_mtMet);

  tree->Branch("bjet_1_pt",    &leaves->bjet_1_pt);
  tree->Branch("bjet_1_eta",   &leaves->bjet_1_eta);
  tree->Branch("bjet_1_phi",   &leaves->bjet_1_phi);

  tree->Branch("bjet_2_pt",    &leaves->bjet_2_pt);
  tree->Branch("bjet_2_eta",   &leaves->bjet_2_eta);
  tree->Branch("bjet_2_phi",   &leaves->bjet_2_phi);

  tree->Branch("notBjet_1_pt",    &leaves->notBjet_1_pt);
  tree->Branch("notBjet_1_eta",   &leaves->notBjet_1_eta);
  tree->Branch("notBjet_1_phi",   &leaves->notBjet_1_phi);

  tree->Branch("ditau_mass", &leaves->ditau_mass);
  tree->Branch("dimu_mass", &leaves->dimu_mass);
  tree->Branch("dimu_pt", &leaves->dimu_pt);

  tree->Branch("tau_mt2", &leaves->tau_mt2);
  tree->Branch("taujet_mt2", &leaves->taujet_mt2);
  tree->Branch("bjet_angles", &leaves->bjet_angles);
  tree->Branch("taumu_angles", &leaves->taumu_angles);
  tree->Branch("bjet_truth_n", &leaves->bjet_truth_n);
  tree->Branch("passBjetGRL", &leaves->passBjetGRL);
  tree->Branch("bjet_trig_sf", &leaves->bjet_trig_sf);
  tree->Branch("bjet_trig_sf_up", &leaves->bjet_trig_sf_up);
  tree->Branch("bjet_trig_sf_down", &leaves->bjet_trig_sf_down);
  tree->Branch("mu_trig_sf", &leaves->mu_trig_sf);
  tree->Branch("pass_CleaningCut", &leaves->pass_CleaningCut);
  tree->Branch("pass_QCDCut", &leaves->pass_QCDCut);
  tree->Branch("qcdweight", &leaves->qcdweight);
  tree->Branch("METSigSoftSub", &leaves->METSigSoftSub);
  tree->Branch("METSigSoftTrk", &leaves->METSigSoftTrk);
  tree->Branch("METmeff", &leaves->METmeff);
  tree->Branch("avgIntXing", &leaves->avgIntXing);
  tree->Branch("actualIntXing", &leaves->actualIntXing);
  tree->Branch("deltaphib", &leaves->deltaphib);
  tree->Branch("combIntXing", &leaves->combIntXing);
  tree->Branch("isBadTile", &leaves->isBadTile);
  tree->Branch("deltaphitau", &leaves->deltaphitau);


  if ( tree->GetName() == std::string("NOMINAL") && isMC ) {
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
    tree->Branch("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
    tree->Branch("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
    tree->Branch("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
    tree->Branch("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);

    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down);
    tree->Branch("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up);
    tree->Branch("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up);

    tree->Branch("ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->Branch("ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->Branch("ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->Branch("ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->Branch("ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->Branch("ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->Branch("mu_weight_MUON_EFF_BADMUON_STAT__1down", &leaves->mu_weight_MUON_EFF_BADMUON_STAT__1down);
    tree->Branch("mu_weight_MUON_EFF_BADMUON_STAT__1up", &leaves->mu_weight_MUON_EFF_BADMUON_STAT__1up);
    tree->Branch("mu_weight_MUON_EFF_BADMUON_SYS__1down", &leaves->mu_weight_MUON_EFF_BADMUON_SYS__1down);
    tree->Branch("mu_weight_MUON_EFF_BADMUON_SYS__1up", &leaves->mu_weight_MUON_EFF_BADMUON_SYS__1up);
    tree->Branch("mu_weight_MUON_EFF_ISO_STAT__1down", &leaves->mu_weight_MUON_EFF_ISO_STAT__1down);
    tree->Branch("mu_weight_MUON_EFF_ISO_STAT__1up", &leaves->mu_weight_MUON_EFF_ISO_STAT__1up);
    tree->Branch("mu_weight_MUON_EFF_ISO_SYS__1down", &leaves->mu_weight_MUON_EFF_ISO_SYS__1down);
    tree->Branch("mu_weight_MUON_EFF_ISO_SYS__1up", &leaves->mu_weight_MUON_EFF_ISO_SYS__1up);
    tree->Branch("mu_weight_MUON_EFF_RECO_STAT__1down", &leaves->mu_weight_MUON_EFF_RECO_STAT__1down);
    tree->Branch("mu_weight_MUON_EFF_RECO_STAT__1up", &leaves->mu_weight_MUON_EFF_RECO_STAT__1up);
    tree->Branch("mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down", &leaves->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down);
    tree->Branch("mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up", &leaves->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up);
    tree->Branch("mu_weight_MUON_EFF_RECO_SYS__1down", &leaves->mu_weight_MUON_EFF_RECO_SYS__1down);
    tree->Branch("mu_weight_MUON_EFF_RECO_SYS__1up", &leaves->mu_weight_MUON_EFF_RECO_SYS__1up);
    tree->Branch("mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down", &leaves->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down);
    tree->Branch("mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up", &leaves->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up);
    tree->Branch("mu_weight_MUON_EFF_TTVA_STAT__1down", &leaves->mu_weight_MUON_EFF_TTVA_STAT__1down);
    tree->Branch("mu_weight_MUON_EFF_TTVA_STAT__1up", &leaves->mu_weight_MUON_EFF_TTVA_STAT__1up);
    tree->Branch("mu_weight_MUON_EFF_TTVA_SYS__1down", &leaves->mu_weight_MUON_EFF_TTVA_SYS__1down);
    tree->Branch("mu_weight_MUON_EFF_TTVA_SYS__1up", &leaves->mu_weight_MUON_EFF_TTVA_SYS__1up);
    tree->Branch("mu_weight_MUON_EFF_TrigStatUncertainty__1down", &leaves->mu_weight_MUON_EFF_TrigStatUncertainty__1down);
    tree->Branch("mu_weight_MUON_EFF_TrigStatUncertainty__1up", &leaves->mu_weight_MUON_EFF_TrigStatUncertainty__1up);
    tree->Branch("mu_weight_MUON_EFF_TrigSystUncertainty__1down", &leaves->mu_weight_MUON_EFF_TrigSystUncertainty__1down);
    tree->Branch("mu_weight_MUON_EFF_TrigSystUncertainty__1up", &leaves->mu_weight_MUON_EFF_TrigSystUncertainty__1up);
    tree->Branch("jet_weight_FT_EFF_B_systematics__1down", &leaves->jet_weight_FT_EFF_B_systematics__1down);
    tree->Branch("jet_weight_FT_EFF_B_systematics__1up", &leaves->jet_weight_FT_EFF_B_systematics__1up);
    tree->Branch("jet_weight_FT_EFF_C_systematics__1down", &leaves->jet_weight_FT_EFF_C_systematics__1down);
    tree->Branch("jet_weight_FT_EFF_C_systematics__1up", &leaves->jet_weight_FT_EFF_C_systematics__1up);
    tree->Branch("jet_weight_FT_EFF_Light_systematics__1down", &leaves->jet_weight_FT_EFF_Light_systematics__1down);
    tree->Branch("jet_weight_FT_EFF_Light_systematics__1up", &leaves->jet_weight_FT_EFF_Light_systematics__1up);
    tree->Branch("jet_weight_FT_EFF_extrapolation__1down", &leaves->jet_weight_FT_EFF_extrapolation__1down);
    tree->Branch("jet_weight_FT_EFF_extrapolation__1up", &leaves->jet_weight_FT_EFF_extrapolation__1up);
    tree->Branch("jet_weight_FT_EFF_extrapolation_from_charm__1down", &leaves->jet_weight_FT_EFF_extrapolation_from_charm__1down);
    tree->Branch("jet_weight_FT_EFF_extrapolation_from_charm__1up", &leaves->jet_weight_FT_EFF_extrapolation_from_charm__1up);
    tree->Branch("jet_weight_JET_JvtEfficiency__1down", &leaves->jet_weight_JET_JvtEfficiency__1down);
    tree->Branch("jet_weight_JET_JvtEfficiency__1up", &leaves->jet_weight_JET_JvtEfficiency__1up);
    tree->Branch("jet_weight_JET_fJvtEfficiency__1down", &leaves->jet_weight_JET_fJvtEfficiency__1down);
    tree->Branch("jet_weight_JET_fJvtEfficiency__1up", &leaves->jet_weight_JET_fJvtEfficiency__1up);

  }

}

// vector ntuple
void setLeavesAddress ( TTree * tree, TreeLeavesVector *leaves, bool isMC ) {

  // Registering branches to the tree
  tree->SetBranchAddress("EventNumber"    ,&leaves->EventNumber);
  tree->SetBranchAddress("RunNumber"      ,&leaves->RunNumber);
  tree->SetBranchAddress("pileupweight"   ,&leaves->pileupweight);
  tree->SetBranchAddress("pileupweightUp"   ,&leaves->pileupweight_up);
  tree->SetBranchAddress("pileupweightDown"   ,&leaves->pileupweight_down);
  tree->SetBranchAddress("mcEventWeight"  ,&leaves->mcEventWeight);
  tree->SetBranchAddress("mcEventWeights"  ,&leaves->mcEventWeights);
  tree->SetBranchAddress("lumiweight"     ,&leaves->lumiweight);
  tree->SetBranchAddress("lumiweight_up"     ,&leaves->lumiweight_up);
  tree->SetBranchAddress("lumiweight_down"     ,&leaves->lumiweight_down);
  tree->SetBranchAddress("SampleID"       ,&leaves->SampleID);
  tree->SetBranchAddress("ProcessID"      ,&leaves->ProcessID);
  //tree->SetBranchAddress("TauSpinnerWeight"     ,&leaves->TauSpinnerWeight);
  tree->SetBranchAddress("tau_weight"     ,&leaves->tau_weight);
  tree->SetBranchAddress("tau_medium_weight"     ,&leaves->tau_medium_weight);
  tree->SetBranchAddress("ele_weight"     ,&leaves->ele_weight);
  tree->SetBranchAddress("mu_weight"     ,&leaves->mu_weight);
  tree->SetBranchAddress("bjet_weight",     &leaves->bjet_weight);
  tree->SetBranchAddress("jvt_weight",      &leaves->jvt_weight);
  tree->SetBranchAddress("JetTileVeto",	&leaves->JetTileVeto);
  tree->SetBranchAddress("TauTileVeto",	&leaves->TauTileVeto);
  tree->SetBranchAddress("JetTileVetoLBA52",  &leaves->JetTileVetoLBA52);
  tree->SetBranchAddress("TauTileVetoLBA52",  &leaves->TauTileVetoLBA52);
  tree->SetBranchAddress("treatAsYear",  &leaves->treatAsYear);
  tree->SetBranchAddress("isDebugStream", &leaves->isDebugStream);
  tree->SetBranchAddress("IsMETTrigPassed",  &leaves->IsMETTrigPassed);
  tree->SetBranchAddress("HLT_j80_bmv2c2060_split_xe60", &leaves->HLT_j80_bmv2c2060_split_xe60);
  tree->SetBranchAddress("HLT_j80_bmv2c1050_split_xe60", &leaves->HLT_j80_bmv2c1050_split_xe60);
  tree->SetBranchAddress("passBjetGRL", &leaves->passBjetGRL);
  tree->SetBranchAddress("Bjet_PileupWeight", &leaves->Bjet_PileupWeight);
  tree->SetBranchAddress("Bjet_PileupWeight_up", &leaves->Bjet_PileupWeight_up);
  tree->SetBranchAddress("Bjet_PileupWeight_down", &leaves->Bjet_PileupWeight_down);
  tree->SetBranchAddress("HLT_mu50", &leaves->HLT_mu50);
  tree->SetBranchAddress("HLT_mu26_ivarmedium", &leaves->HLT_mu26_ivarmedium);
  tree->SetBranchAddress("muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50", &leaves->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50);

  tree->SetBranchAddress("muR10_muF20Weight",       &leaves->muR10_muF20Weight);
  tree->SetBranchAddress("muR10_muF05Weight",       &leaves->muR10_muF05Weight);
  tree->SetBranchAddress("muR20_muF10Weight",       &leaves->muR20_muF10Weight);
  tree->SetBranchAddress("muR05_muF10Weight",       &leaves->muR05_muF10Weight);
  tree->SetBranchAddress("muR05_muF05Weight",       &leaves->muR05_muF05Weight);
  tree->SetBranchAddress("muR20_muF20Weight",       &leaves->muR20_muF20Weight);
  tree->SetBranchAddress("fsr_muRfac05",       &leaves->fsr_muRfac05);
  tree->SetBranchAddress("fsr_muRfac20",       &leaves->fsr_muRfac20);
  tree->SetBranchAddress("alphasUp",       &leaves->alphasUp);
  tree->SetBranchAddress("alphasDown",       &leaves->alphasDown);
  tree->SetBranchAddress("Var3cDown",       &leaves->Var3cDown);
  tree->SetBranchAddress("Var3cUp",       &leaves->Var3cUp);


  if ( tree->GetName() == std::string("NOMINAL") && isMC ) {
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &leaves->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &leaves->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);

    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down);
    tree->SetBranchAddress("tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &leaves->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up);
    tree->SetBranchAddress("tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up", &leaves->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up);


    tree->SetBranchAddress("ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->SetBranchAddress("ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->SetBranchAddress("ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->SetBranchAddress("ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->SetBranchAddress("ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leaves->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    tree->SetBranchAddress("ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leaves->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_BADMUON_STAT__1down", &leaves->mu_weight_MUON_EFF_BADMUON_STAT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_BADMUON_STAT__1up", &leaves->mu_weight_MUON_EFF_BADMUON_STAT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_BADMUON_SYS__1down", &leaves->mu_weight_MUON_EFF_BADMUON_SYS__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_BADMUON_SYS__1up", &leaves->mu_weight_MUON_EFF_BADMUON_SYS__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_ISO_STAT__1down", &leaves->mu_weight_MUON_EFF_ISO_STAT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_ISO_STAT__1up", &leaves->mu_weight_MUON_EFF_ISO_STAT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_ISO_SYS__1down", &leaves->mu_weight_MUON_EFF_ISO_SYS__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_ISO_SYS__1up", &leaves->mu_weight_MUON_EFF_ISO_SYS__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_STAT__1down", &leaves->mu_weight_MUON_EFF_RECO_STAT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_STAT__1up", &leaves->mu_weight_MUON_EFF_RECO_STAT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down", &leaves->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up", &leaves->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_SYS__1down", &leaves->mu_weight_MUON_EFF_RECO_SYS__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_SYS__1up", &leaves->mu_weight_MUON_EFF_RECO_SYS__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down", &leaves->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up", &leaves->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TTVA_STAT__1down", &leaves->mu_weight_MUON_EFF_TTVA_STAT__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TTVA_STAT__1up", &leaves->mu_weight_MUON_EFF_TTVA_STAT__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TTVA_SYS__1down", &leaves->mu_weight_MUON_EFF_TTVA_SYS__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TTVA_SYS__1up", &leaves->mu_weight_MUON_EFF_TTVA_SYS__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TrigStatUncertainty__1down", &leaves->mu_weight_MUON_EFF_TrigStatUncertainty__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TrigStatUncertainty__1up", &leaves->mu_weight_MUON_EFF_TrigStatUncertainty__1up);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TrigSystUncertainty__1down", &leaves->mu_weight_MUON_EFF_TrigSystUncertainty__1down);
    tree->SetBranchAddress("mu_weight_MUON_EFF_TrigSystUncertainty__1up", &leaves->mu_weight_MUON_EFF_TrigSystUncertainty__1up);
    tree->SetBranchAddress("jet_weight_FT_EFF_B_systematics__1down", &leaves->jet_weight_FT_EFF_B_systematics__1down);
    tree->SetBranchAddress("jet_weight_FT_EFF_B_systematics__1up", &leaves->jet_weight_FT_EFF_B_systematics__1up);
    tree->SetBranchAddress("jet_weight_FT_EFF_C_systematics__1down", &leaves->jet_weight_FT_EFF_C_systematics__1down);
    tree->SetBranchAddress("jet_weight_FT_EFF_C_systematics__1up", &leaves->jet_weight_FT_EFF_C_systematics__1up);
    tree->SetBranchAddress("jet_weight_FT_EFF_Light_systematics__1down", &leaves->jet_weight_FT_EFF_Light_systematics__1down);
    tree->SetBranchAddress("jet_weight_FT_EFF_Light_systematics__1up", &leaves->jet_weight_FT_EFF_Light_systematics__1up);
    tree->SetBranchAddress("jet_weight_FT_EFF_extrapolation__1down", &leaves->jet_weight_FT_EFF_extrapolation__1down);
    tree->SetBranchAddress("jet_weight_FT_EFF_extrapolation__1up", &leaves->jet_weight_FT_EFF_extrapolation__1up);
    tree->SetBranchAddress("jet_weight_FT_EFF_extrapolation_from_charm__1down", &leaves->jet_weight_FT_EFF_extrapolation_from_charm__1down);
    tree->SetBranchAddress("jet_weight_FT_EFF_extrapolation_from_charm__1up", &leaves->jet_weight_FT_EFF_extrapolation_from_charm__1up);
    tree->SetBranchAddress("jet_weight_JET_JvtEfficiency__1down", &leaves->jet_weight_JET_JvtEfficiency__1down);
    tree->SetBranchAddress("jet_weight_JET_JvtEfficiency__1up", &leaves->jet_weight_JET_JvtEfficiency__1up);
    tree->SetBranchAddress("jet_weight_JET_fJvtEfficiency__1down", &leaves->jet_weight_JET_fJvtEfficiency__1down);
    tree->SetBranchAddress("jet_weight_JET_fJvtEfficiency__1up", &leaves->jet_weight_JET_fJvtEfficiency__1up);

  }

  tree->SetBranchAddress("tau_n"             ,       &leaves->tau_n);
  tree->SetBranchAddress("tau_pt"            ,       &leaves->tau_pt);
  tree->SetBranchAddress("tau_eta"           ,       &leaves->tau_eta);
  tree->SetBranchAddress("tau_phi"           ,       &leaves->tau_phi);
  tree->SetBranchAddress("tau_m"             ,       &leaves->tau_m);
  tree->SetBranchAddress("tau_charge"        ,       &leaves->tau_charge);
  tree->SetBranchAddress("tau_ntracks"       ,       &leaves->tau_nTracks);
  tree->SetBranchAddress("tau_JetBDTLoose"   ,       &leaves->tau_JetBDTLoose);
  tree->SetBranchAddress("tau_JetBDTMedium"  ,       &leaves->tau_JetBDTMedium);
  tree->SetBranchAddress("tau_JetBDTTight"   ,       &leaves->tau_JetBDTTight);
  tree->SetBranchAddress("tau_isTruthMatchedTau"      ,       &leaves->tau_isTruthMatchedTau);
  tree->SetBranchAddress("tau_truthMatchedType"      ,        &leaves->tau_truthMatchedType);
  tree->SetBranchAddress("tau_eff_SF"                 ,       &leaves->tau_effSF);
  tree->SetBranchAddress("tau_delPhiMet"              ,       &leaves->tau_delPhiMet);
  tree->SetBranchAddress("tau_mtMet"                  ,       &leaves->tau_mtMet);
  tree->SetBranchAddress("tau_nTruthMatched" , &leaves->tau_nTruthMatched);
  tree->SetBranchAddress("jet_isBjetMatched" , &leaves->jet_isBjetMatched);

  tree->SetBranchAddress("jet_n",             &leaves->jet_n);
  tree->SetBranchAddress("jet_n_btag",        &leaves->jet_n_btag);
  tree->SetBranchAddress("jet_pt",            &leaves->jet_pt);
  tree->SetBranchAddress("jet_eta",           &leaves->jet_eta);
  tree->SetBranchAddress("jet_phi",           &leaves->jet_phi);
  tree->SetBranchAddress("jet_m",             &leaves->jet_m);
  tree->SetBranchAddress("jet_isBjet",        &leaves->jet_isBjet);
  tree->SetBranchAddress("jet_BjetSF",        &leaves->jet_BjetSF);
  tree->SetBranchAddress("jet_isBadTight",    &leaves->jet_isBadTight);
//  tree->SetBranchAddress("jet_truth_n",       &leaves->jet_truth_n);
  tree->SetBranchAddress("jet_delPhiMet",     &leaves->jet_delPhiMet);
  tree->SetBranchAddress("sumMTJet",          &leaves->sumMTJet);
  tree->SetBranchAddress("sumMTTauJet",       &leaves->sumMTTauJet);
  tree->SetBranchAddress("jet_hadronTruthID",        &leaves->jet_hadronTruthID);

  tree->SetBranchAddress("ele_n",             &leaves->ele_n);
  tree->SetBranchAddress("ele_pt",            &leaves->ele_pt);
  tree->SetBranchAddress("ele_eta",           &leaves->ele_eta);
  tree->SetBranchAddress("ele_phi",           &leaves->ele_phi);
  tree->SetBranchAddress("ele_mtMet",         &leaves->ele_mtMet);
  tree->SetBranchAddress("ele_effSF",         &leaves->ele_effSF);

  tree->SetBranchAddress("mu_n",       &leaves->mu_n);
  tree->SetBranchAddress("mu_pt",            &leaves->mu_pt);
  tree->SetBranchAddress("mu_eta",           &leaves->mu_eta);
  tree->SetBranchAddress("mu_phi",           &leaves->mu_phi);
  tree->SetBranchAddress("mu_m",           &leaves->mu_m);
  tree->SetBranchAddress("mu_mtMet",           &leaves->mu_mtMet);
  tree->SetBranchAddress("mu_effSF"     ,&leaves->mu_effSF);
  tree->SetBranchAddress("mu_charge",           &leaves->mu_charge);

  tree->SetBranchAddress("met",             &leaves->met);
  tree->SetBranchAddress("met_phi",         &leaves->met_phi);
  tree->SetBranchAddress("METSigPow3_seed", &leaves->METSigPow3_seed);
  tree->SetBranchAddress("METSigSoftSub_seed", &leaves->METSigSoftSub_seed);
  tree->SetBranchAddress("METSigSoftSub", &leaves->METSigSoftSub);
  tree->SetBranchAddress("METSigSoftTrk", &leaves->METSigSoftTrk);
  tree->SetBranchAddress("meff",            &leaves->meff);
  tree->SetBranchAddress("ht",              &leaves->ht);
  tree->SetBranchAddress("Mt2",	      &leaves->Mt2);
  tree->SetBranchAddress("Mt2_taumu",       &leaves->Mt2_taumu);
  tree->SetBranchAddress("Mtaumu",          &leaves->Mtaumu);
  tree->SetBranchAddress("sumMT",           &leaves->sumMT);
  tree->SetBranchAddress("bjet_trig_sf",    &leaves->bjet_trig_sf);
  tree->SetBranchAddress("bjet_trig_sf_up",    &leaves->bjet_trig_sf_up);
  tree->SetBranchAddress("bjet_trig_sf_down",    &leaves->bjet_trig_sf_down);
  tree->SetBranchAddress("HLT_jX_prescale",    &leaves->HLT_jX_prescale);
  tree->SetBranchAddress("actualIntXing",    &leaves->actualIntXing);
  tree->SetBranchAddress("avgIntXing",    &leaves->avgIntXing);
  tree->SetBranchAddress("isBadTile", &leaves->isBadTile);

}




void fillTree( TTree * origin, TTree * dest, std::map<int, vector<double> > sumw_CBK, bool isAlt, bool isUnknownSignal) {
  TreeLeavesVector * leaves_vector = new TreeLeavesVector();
  TreeLeavesFlat * leaves_flat = new TreeLeavesFlat();


  TLorentzVector tau1, tau2, bjet1, bjet2, mu1, mu2, notBjet1,metvec;
  // check if MC-specific branches must be enabled
  bool isMC = false;
  int sampleID = -1;
  origin->SetBranchAddress("SampleID", &sampleID );

  Long64_t nentries = origin->GetEntries();
  if(nentries>0) {
    origin->GetEntry(0);
    if ( sampleID >= 0 ) isMC = true;
  }

  RegisterTree( dest, leaves_flat, isMC );
  setLeavesAddress( origin, leaves_vector, isMC );

  for (Long64_t i=0; i<nentries; i++) {
    origin->GetEntry(i);

    if (i>0 && i%100000 == 0) cout << i << " / " << nentries << endl;

    leaves_flat->EventNumber = leaves_vector->EventNumber;
    leaves_flat->RunNumber = leaves_vector->RunNumber;
    leaves_flat->ProcessID = leaves_vector->ProcessID;
    leaves_flat->SampleID  = leaves_vector->SampleID ;
    leaves_flat->mcEventWeight = leaves_vector->mcEventWeight;
    leaves_flat->mcEventWeights = leaves_vector->mcEventWeights;
    leaves_flat->treatAsYear = leaves_vector->treatAsYear;
    leaves_flat->isDebugStream = leaves_vector->isDebugStream;

    leaves_flat->tau_weight = leaves_vector->tau_weight;
    leaves_flat->tau_medium_weight = leaves_vector->tau_medium_weight;
    leaves_flat->ele_weight = leaves_vector->ele_weight;
    leaves_flat->mu_weight = leaves_vector->mu_weight;
    leaves_flat->bjet_weight = leaves_vector->bjet_weight;
    leaves_flat->jvt_weight = leaves_vector->jvt_weight;
    leaves_flat->lumiWeight = leaves_vector->lumiweight;
    leaves_flat->lumiWeight_up = leaves_vector->lumiweight_up;
    leaves_flat->lumiWeight_down = leaves_vector->lumiweight_down;
    leaves_flat->pileupweight = leaves_vector->pileupweight;
    leaves_flat->pileupweight_up = leaves_vector->pileupweight_up;
    leaves_flat->pileupweight_down = leaves_vector->pileupweight_down;
    leaves_flat->TauSpinnerWeight = leaves_vector->TauSpinnerWeight;

//if ( leaves_vector->ProcessID ==410080 || leaves_vector->ProcessID ==410081 || leaves_vector->ProcessID == 304014 || leaves_vector->ProcessID == 407321 || leaves_vector->ProcessID == 410560 || leaves_vector->ProcessID == 410408 || (leaves_vector->ProcessID >=345940 && leaves_vector->ProcessID <=345942) ) //ttX

//  if ( ( leaves_vector->ProcessID >= 410155 && leaves_vector->ProcessID <= 410157 ) ||  ( leaves_vector->ProcessID >= 410218 && leaves_vector->ProcessID <= 410220 ) ||  ( leaves_vector->ProcessID >= 410276 && leaves_vector->ProcessID <= 410278 )) //ttbarV

//std::cout<< SusyTauConfig::SampleType(leaves_vector->SampleID)<<std::endl;
//std::cout<<leaves_vector->SampleID<<std::endl;

    if (isMC && !isAlt) {
      if (!isUnknownSignal) {
        if(SusyTauConfig::SampleType(leaves_vector->SampleID) != "ttbarV" && SusyTauConfig::SampleType(leaves_vector->SampleID) != "ttX") {
          leaves_flat->muR10_muF20Weight = leaves_vector->muR10_muF20Weight;
          leaves_flat->muR10_muF05Weight = leaves_vector->muR10_muF05Weight;
          leaves_flat->muR20_muF10Weight = leaves_vector->muR20_muF10Weight;
          leaves_flat->muR05_muF10Weight = leaves_vector->muR05_muF10Weight;
          leaves_flat->muR05_muF05Weight = leaves_vector->muR05_muF05Weight;
          leaves_flat->muR20_muF20Weight = leaves_vector->muR20_muF20Weight;
          leaves_flat->fsr_muRfac05 = leaves_vector->fsr_muRfac05;
          leaves_flat->fsr_muRfac20 = leaves_vector->fsr_muRfac20;
          leaves_flat->alphasUp = leaves_vector->alphasUp;
          leaves_flat->alphasDown = leaves_vector->alphasDown;
          leaves_flat->Var3cDown = leaves_vector->Var3cDown;
          leaves_flat->Var3cUp = leaves_vector->Var3cUp;
        } else if(leaves_vector->SampleID==304014 || leaves_vector->SampleID==410080|| leaves_vector->SampleID==410081) {
          leaves_flat->muR10_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR10_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac05 = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac20 = leaves_vector->mcEventWeight;
          leaves_flat->alphasUp = leaves_vector->mcEventWeight;
          leaves_flat->alphasDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cUp = leaves_vector->mcEventWeight;
        } else if((SusyTauConfig::SampleType(leaves_vector->SampleID) == "ttbarV" || SusyTauConfig::SampleType(leaves_vector->SampleID) == "ttX") && leaves_vector->SampleID != 304014 && leaves_vector->SampleID != 410080 && leaves_vector->SampleID != 410081 && origin->GetName() == std::string("NOMINAL")) {
          leaves_flat->muR10_muF20Weight = leaves_vector->mcEventWeights->at(1);
          leaves_flat->muR10_muF05Weight = leaves_vector->mcEventWeights->at(2);
          leaves_flat->muR20_muF10Weight = leaves_vector->mcEventWeights->at(3);
          leaves_flat->muR05_muF10Weight = leaves_vector->mcEventWeights->at(6);
          leaves_flat->muR05_muF05Weight = leaves_vector->mcEventWeights->at(8);
          leaves_flat->muR20_muF20Weight = leaves_vector->mcEventWeights->at(4);
          leaves_flat->fsr_muRfac05 = leaves_vector->fsr_muRfac05;
          leaves_flat->fsr_muRfac20 = leaves_vector->fsr_muRfac20;
          leaves_flat->alphasUp = leaves_vector->alphasUp;
          leaves_flat->alphasDown = leaves_vector->alphasDown;
          leaves_flat->Var3cDown = leaves_vector->Var3cDown;
          leaves_flat->Var3cUp = leaves_vector->Var3cUp;
        } else {
          leaves_flat->muR10_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR10_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac05 = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac20 = leaves_vector->mcEventWeight;
          leaves_flat->alphasUp = leaves_vector->mcEventWeight;
          leaves_flat->alphasDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cUp = leaves_vector->mcEventWeight;
        }
      } else {
          // same as above
          leaves_flat->muR10_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR10_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF10Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR05_muF05Weight = leaves_vector->mcEventWeight;
          leaves_flat->muR20_muF20Weight = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac05 = leaves_vector->mcEventWeight;
          leaves_flat->fsr_muRfac20 = leaves_vector->mcEventWeight;
          leaves_flat->alphasUp = leaves_vector->mcEventWeight;
          leaves_flat->alphasDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cDown = leaves_vector->mcEventWeight;
          leaves_flat->Var3cUp = leaves_vector->mcEventWeight;
      }
  
      if (!isUnknownSignal) {
        // some by-hand matching of lumiweights to the metadata
        if(SusyTauConfig::SampleType(leaves_vector->SampleID) == "ttbar") {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][1];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][2];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][3];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][5];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][199];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][198];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][13];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][12];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][194];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][193];
        } else if ((SusyTauConfig::SampleType(leaves_vector->SampleID) == "ttX" || SusyTauConfig::SampleType(leaves_vector->SampleID) == "ttbarV") && leaves_vector->SampleID != 304014 && leaves_vector->SampleID != 410080  && leaves_vector->SampleID != 410081) {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][1];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][2];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][3];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][8];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
        } else if(leaves_vector->SampleID ==410658 || leaves_vector->SampleID ==410659 || leaves_vector->SampleID ==410644 || leaves_vector->SampleID ==410645  || leaves_vector->SampleID ==  411032 || leaves_vector->SampleID == 411033 || leaves_vector->SampleID == 411034 || leaves_vector->SampleID ==  411035) {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][1];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][2];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][3];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][5];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][172];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][171];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][167];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][166];
        } else if(leaves_vector->SampleID == 410646 || leaves_vector->SampleID == 410647 || leaves_vector->SampleID == 410648 || leaves_vector->SampleID == 410649 ) {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][1];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][2];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][3];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][5];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][148];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][147];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][167];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][166];
        } else if(SusyTauConfig::SampleType(leaves_vector->SampleID)=="wenu" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="wmunu" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="wtaunu" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="ztautau" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="zee" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="znunu" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="diboson" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="triboson" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="zlowmass" || SusyTauConfig::SampleType(leaves_vector->SampleID)=="V2jets") {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][8];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][9];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][5];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][10];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][112];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][111];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
        } else if(SusyTauConfig::SampleType(leaves_vector->SampleID)=="zmumu") {
          leaves_flat->lumi_muR10_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][8];
          leaves_flat->lumi_muR10_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][6];
          leaves_flat->lumi_muR20_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][9];
          leaves_flat->lumi_muR05_muF10Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][5];
          leaves_flat->lumi_muR05_muF05Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][4];
          leaves_flat->lumi_muR20_muF20Weight = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][10];
          leaves_flat->lumi_fsr_muRfac05 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_fsr_muRfac20 = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_alphasDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cDown = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
          leaves_flat->lumi_Var3cUp = sumw_CBK[leaves_vector->SampleID][0]/sumw_CBK[leaves_vector->SampleID][0];
        } else {
          leaves_flat->lumi_muR10_muF20Weight = 1.;
          leaves_flat->lumi_muR10_muF05Weight = 1.;
          leaves_flat->lumi_muR20_muF10Weight = 1.;
          leaves_flat->lumi_muR05_muF10Weight = 1.;
          leaves_flat->lumi_muR05_muF05Weight = 1.;
          leaves_flat->lumi_muR20_muF20Weight = 1.;
          leaves_flat->lumi_fsr_muRfac05 = 1.;
          leaves_flat->lumi_fsr_muRfac20 = 1.;
          leaves_flat->lumi_alphasUp = 1.;
          leaves_flat->lumi_alphasDown = 1.;
          leaves_flat->lumi_Var3cDown = 1.;
          leaves_flat->lumi_Var3cUp = 1.;
        }
      } else {
          // same as above
          leaves_flat->lumi_muR10_muF20Weight = 1.;
          leaves_flat->lumi_muR10_muF05Weight = 1.;
          leaves_flat->lumi_muR20_muF10Weight = 1.;
          leaves_flat->lumi_muR05_muF10Weight = 1.;
          leaves_flat->lumi_muR05_muF05Weight = 1.;
          leaves_flat->lumi_muR20_muF20Weight = 1.;
          leaves_flat->lumi_fsr_muRfac05 = 1.;
          leaves_flat->lumi_fsr_muRfac20 = 1.;
          leaves_flat->lumi_alphasUp = 1.;
          leaves_flat->lumi_alphasDown = 1.;
          leaves_flat->lumi_Var3cDown = 1.;
          leaves_flat->lumi_Var3cUp = 1.;
      }
    }


    leaves_flat->JetTileVeto = leaves_vector->JetTileVeto;
    leaves_flat->TauTileVeto = leaves_vector->TauTileVeto;
    leaves_flat->JetTileVetoLBA52 = leaves_vector->JetTileVetoLBA52;
    leaves_flat->TauTileVetoLBA52 = leaves_vector->TauTileVetoLBA52;

    if ( origin->GetName() == std::string("NOMINAL") && isMC ) {
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
      leaves_flat->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down = leaves_vector->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down = leaves_vector->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
      leaves_flat->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up = leaves_vector->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up = leaves_vector->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
      leaves_flat->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down = leaves_vector->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down = leaves_vector->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
      leaves_flat->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up = leaves_vector->tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up = leaves_vector->tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;

      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
      leaves_flat->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up = leaves_vector->tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;
      leaves_flat->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up = leaves_vector->tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;



//	leaves_flat->ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down = leaves_vector->ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down;
//	leaves_flat->ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up = leaves_vector->ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up;
      leaves_flat->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = leaves_vector->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
      leaves_flat->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = leaves_vector->ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
      leaves_flat->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = leaves_vector->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
      leaves_flat->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = leaves_vector->ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
      leaves_flat->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = leaves_vector->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
      leaves_flat->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = leaves_vector->ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
      leaves_flat->mu_weight_MUON_EFF_BADMUON_STAT__1down = leaves_vector->mu_weight_MUON_EFF_BADMUON_STAT__1down;
      leaves_flat->mu_weight_MUON_EFF_BADMUON_STAT__1up = leaves_vector->mu_weight_MUON_EFF_BADMUON_STAT__1up;
      leaves_flat->mu_weight_MUON_EFF_BADMUON_SYS__1down = leaves_vector->mu_weight_MUON_EFF_BADMUON_SYS__1down;
      leaves_flat->mu_weight_MUON_EFF_BADMUON_SYS__1up = leaves_vector->mu_weight_MUON_EFF_BADMUON_SYS__1up;
      leaves_flat->mu_weight_MUON_EFF_ISO_STAT__1down = leaves_vector->mu_weight_MUON_EFF_ISO_STAT__1down;
      leaves_flat->mu_weight_MUON_EFF_ISO_STAT__1up = leaves_vector->mu_weight_MUON_EFF_ISO_STAT__1up;
      leaves_flat->mu_weight_MUON_EFF_ISO_SYS__1down = leaves_vector->mu_weight_MUON_EFF_ISO_SYS__1down;
      leaves_flat->mu_weight_MUON_EFF_ISO_SYS__1up = leaves_vector->mu_weight_MUON_EFF_ISO_SYS__1up;
      leaves_flat->mu_weight_MUON_EFF_RECO_STAT__1down = leaves_vector->mu_weight_MUON_EFF_RECO_STAT__1down;
      leaves_flat->mu_weight_MUON_EFF_RECO_STAT__1up = leaves_vector->mu_weight_MUON_EFF_RECO_STAT__1up;
      leaves_flat->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down = leaves_vector->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down;
      leaves_flat->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up = leaves_vector->mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up;
      leaves_flat->mu_weight_MUON_EFF_RECO_SYS__1down = leaves_vector->mu_weight_MUON_EFF_RECO_SYS__1down;
      leaves_flat->mu_weight_MUON_EFF_RECO_SYS__1up = leaves_vector->mu_weight_MUON_EFF_RECO_SYS__1up;
      leaves_flat->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down = leaves_vector->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down;
      leaves_flat->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up = leaves_vector->mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up;
      leaves_flat->mu_weight_MUON_EFF_TTVA_STAT__1down = leaves_vector->mu_weight_MUON_EFF_TTVA_STAT__1down;
      leaves_flat->mu_weight_MUON_EFF_TTVA_STAT__1up = leaves_vector->mu_weight_MUON_EFF_TTVA_STAT__1up;
      leaves_flat->mu_weight_MUON_EFF_TTVA_SYS__1down = leaves_vector->mu_weight_MUON_EFF_TTVA_SYS__1down;
      leaves_flat->mu_weight_MUON_EFF_TTVA_SYS__1up = leaves_vector->mu_weight_MUON_EFF_TTVA_SYS__1up;
      leaves_flat->jet_weight_FT_EFF_B_systematics__1down = leaves_vector->jet_weight_FT_EFF_B_systematics__1down;
      leaves_flat->jet_weight_FT_EFF_B_systematics__1up = leaves_vector->jet_weight_FT_EFF_B_systematics__1up;
      leaves_flat->jet_weight_FT_EFF_C_systematics__1down = leaves_vector->jet_weight_FT_EFF_C_systematics__1down;
      leaves_flat->jet_weight_FT_EFF_C_systematics__1up = leaves_vector->jet_weight_FT_EFF_C_systematics__1up;
      leaves_flat->jet_weight_FT_EFF_Light_systematics__1down = leaves_vector->jet_weight_FT_EFF_Light_systematics__1down;
      leaves_flat->jet_weight_FT_EFF_Light_systematics__1up = leaves_vector->jet_weight_FT_EFF_Light_systematics__1up;
      leaves_flat->jet_weight_FT_EFF_extrapolation__1down = leaves_vector->jet_weight_FT_EFF_extrapolation__1down;
      leaves_flat->jet_weight_FT_EFF_extrapolation__1up = leaves_vector->jet_weight_FT_EFF_extrapolation__1up;
      leaves_flat->jet_weight_FT_EFF_extrapolation_from_charm__1down = leaves_vector->jet_weight_FT_EFF_extrapolation_from_charm__1down;
      leaves_flat->jet_weight_FT_EFF_extrapolation_from_charm__1up = leaves_vector->jet_weight_FT_EFF_extrapolation_from_charm__1up;
      leaves_flat->jet_weight_JET_JvtEfficiency__1down = leaves_vector->jet_weight_JET_JvtEfficiency__1down;
      leaves_flat->jet_weight_JET_JvtEfficiency__1up = leaves_vector->jet_weight_JET_JvtEfficiency__1up;
      leaves_flat->jet_weight_JET_fJvtEfficiency__1down = leaves_vector->jet_weight_JET_fJvtEfficiency__1down;
      leaves_flat->jet_weight_JET_fJvtEfficiency__1up = leaves_vector->jet_weight_JET_fJvtEfficiency__1up;

    }

    leaves_flat->tau_n = leaves_vector->tau_n;
    leaves_flat->tau_nTruthMatched = leaves_vector->tau_nTruthMatched;

    leaves_flat->tau_1_pt = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_pt->at(0)   : 0;
    leaves_flat->tau_1_eta = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_eta->at(0) : 0;
    leaves_flat->tau_1_phi = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_phi->at(0) : 0;
    leaves_flat->tau_1_charge = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_charge->at(0) : 0;
    leaves_flat->tau_1_match = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_isTruthMatchedTau->at(0) : 0;
    leaves_flat->tau_1_JetBDTLoose = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_JetBDTLoose->at(0) : 0;
    leaves_flat->tau_1_JetBDTMedium = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_JetBDTMedium->at(0) : 0;
    leaves_flat->tau_1_JetBDTTight = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_JetBDTMedium->at(0)  : 0;
    leaves_flat->tau_1_truthMatchedType = ( leaves_vector->tau_n > 0 ) ? (int)leaves_vector->tau_truthMatchedType->at(0) : 0;
    leaves_flat->tau_1_mtMet  = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_mtMet->at(0) : 0;
    leaves_flat->tau_1_delPhiMet = ( leaves_vector->tau_n > 0 ) ? leaves_vector->tau_delPhiMet->at(0) : 0;

    leaves_flat->tau_2_pt = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_pt->at(1)  :0;
    leaves_flat->tau_2_eta = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_eta->at(1):0;
    leaves_flat->tau_2_phi = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_phi->at(1):0;
    leaves_flat->tau_2_charge = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_charge->at(1):0;
    leaves_flat->tau_2_match = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_isTruthMatchedTau->at(1)  :0;
    leaves_flat->tau_2_JetBDTLoose = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_JetBDTLoose->at(1)  :0;
    leaves_flat->tau_2_JetBDTMedium = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_JetBDTMedium->at(1):0;
    leaves_flat->tau_2_JetBDTTight = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_JetBDTMedium->at(1) :0;
    leaves_flat->tau_2_truthMatchedType = ( leaves_vector->tau_n > 1 ) ? (int)leaves_vector->tau_truthMatchedType->at(1) : 0;
    leaves_flat->tau_2_mtMet  = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_mtMet->at(1) : 0;
    leaves_flat->tau_2_delPhiMet = ( leaves_vector->tau_n > 1 ) ? leaves_vector->tau_delPhiMet->at(1) : 0;

    leaves_flat->ele_n  = leaves_vector->ele_n;
    leaves_flat->ele_1_mtMet = ( leaves_vector->ele_n > 0 ) ? leaves_vector->ele_mtMet->at(0) : 0;

    leaves_flat->mu_n  = leaves_vector->mu_n;
    leaves_flat->mu_1_mtMet = ( leaves_vector->mu_n > 0 ) ? leaves_vector->mu_mtMet->at(0) : 0;

    leaves_flat->jet_n = leaves_vector->jet_n;
    leaves_flat->jet_n_btag = leaves_vector->jet_n_btag;
//    leaves_flat->jet_truth_n = leaves_vector->jet_truth_n;

    leaves_flat->jet_1_pt =   ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_pt->at(0) : 0;
    leaves_flat->jet_1_eta =  ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_eta->at(0) : 0;
    leaves_flat->jet_1_phi =  ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_phi->at(0) : 0;
    leaves_flat->jet_1_btag = ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_isBjet->at(0) : 0;
    leaves_flat->jet_1_isBadTight = ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_isBadTight->at(0) : 0;
    leaves_flat->jet_1_delPhiMet = ( leaves_vector->jet_n > 0 ) ? leaves_vector->jet_delPhiMet->at(0) : 0;

    leaves_flat->mu_1_pt =   ( leaves_vector->mu_n > 0 ) ? leaves_vector->mu_pt->at(0) : 0;
    leaves_flat->mu_1_eta =  ( leaves_vector->mu_n > 0 ) ? leaves_vector->mu_eta->at(0) : 0;
    leaves_flat->mu_1_phi =  ( leaves_vector->mu_n > 0 ) ? leaves_vector->mu_phi->at(0) : 0;
    leaves_flat->mu_1_charge =  ( leaves_vector->mu_n > 0 ) ? leaves_vector->mu_charge->at(0) : 0;

    leaves_flat->mu_2_pt =   ( leaves_vector->mu_n > 1 ) ? leaves_vector->mu_pt->at(1) : 0;
    leaves_flat->mu_2_eta =  ( leaves_vector->mu_n > 1 ) ? leaves_vector->mu_eta->at(1) : 0;
    leaves_flat->mu_2_phi =  ( leaves_vector->mu_n > 1 ) ? leaves_vector->mu_phi->at(1) : 0;
    leaves_flat->mu_2_charge =  ( leaves_vector->mu_n > 1 ) ? leaves_vector->mu_charge->at(1) : 0;

    leaves_flat->jet_2_pt =   ( leaves_vector->jet_n > 1 ) ? leaves_vector->jet_pt->at(1) : 0;
    leaves_flat->jet_2_eta =  ( leaves_vector->jet_n > 1 ) ? leaves_vector->jet_eta->at(1) : 0;
    leaves_flat->jet_2_phi =  ( leaves_vector->jet_n > 1 ) ? leaves_vector->jet_phi->at(1) : 0;
    leaves_flat->jet_2_btag = ( leaves_vector->jet_n > 1 ) ? leaves_vector->jet_isBjet->at(1) : 0;
    leaves_flat->jet_2_delPhiMet = ( leaves_vector->jet_n > 1 ) ? leaves_vector->jet_delPhiMet->at(1) : 0;

    leaves_flat->IsMETTrigPassed = leaves_vector->IsMETTrigPassed;
    leaves_flat->met = leaves_vector->met;
    leaves_flat->met_phi = leaves_vector->met_phi;
    leaves_flat->meff = leaves_vector->meff;
    leaves_flat->ht = leaves_vector->ht;  //need to add muon pts to this
    if(leaves_vector->mu_n > 0) {
      for(int i = 0; i<leaves_vector->mu_n; i++) {
        leaves_flat->ht = leaves_flat->ht + leaves_vector->mu_pt->at(i);
      }
    }
    leaves_flat->sumMT = leaves_vector->sumMT;
    leaves_flat->sumMTJet = leaves_vector->sumMTJet;
    leaves_flat->sumMTTauJet = leaves_vector->sumMTTauJet;
    leaves_flat->Mt2 = leaves_vector->Mt2;
    leaves_flat->Mtaumu = leaves_vector->Mtaumu;
    leaves_flat->Mt2_taumu = leaves_vector->Mt2_taumu;
    leaves_flat->METSigSoftSub = leaves_vector->METSigSoftSub;
    leaves_flat->METSigSoftTrk = leaves_vector->METSigSoftTrk;
    leaves_flat->METmeff = leaves_vector->met / leaves_vector->meff;
    leaves_flat->avgIntXing = leaves_vector->avgIntXing;
    leaves_flat->actualIntXing = leaves_vector->actualIntXing;
    leaves_flat->combIntXing = 0;
    leaves_flat->isBadTile = leaves_vector->isBadTile;



    if(leaves_vector->treatAsYear == 2015 || leaves_vector->treatAsYear == 2016) {
      leaves_flat->combIntXing = leaves_vector->avgIntXing;
    } else if (leaves_vector->treatAsYear == 2017 || leaves_vector->treatAsYear == 2018) {
      leaves_flat->combIntXing = leaves_vector->actualIntXing;
    } else {
      leaves_flat->combIntXing = 0;
    }
    leaves_flat->bjet_trig_sf = leaves_vector->bjet_trig_sf;
    leaves_flat->bjet_trig_sf_up = leaves_vector->bjet_trig_sf_up;
    leaves_flat->bjet_trig_sf_down = leaves_vector->bjet_trig_sf_down;
    leaves_flat->pass_CleaningCut = 1;
    leaves_flat->pass_QCDCut = 0;

    leaves_flat->mu_trig_sf = leaves_vector->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50;
    leaves_flat->IsMuTrigPassed = 0;
    if(leaves_vector->HLT_mu50 || leaves_vector->HLT_mu26_ivarmedium) {
      leaves_flat->IsMuTrigPassed = 1;
    }

    int nbjet = 0;
    int notbjet = 0;
    int truenbjet = 0;
    double bjetpt = 0;

    for(unsigned int j = 0; j<leaves_vector->jet_pt->size(); j++) {
      if(leaves_vector->jet_pt->at(j) > 20000 && leaves_vector->jet_isBjet->at(j) == 1) {
        nbjet++;
        if(leaves_vector->jet_hadronTruthID->at(j) == 5) {
          truenbjet++;
        }
        if(nbjet==1) {
          leaves_flat->bjet_1_pt = leaves_vector->jet_pt->at(j);
          leaves_flat->bjet_1_eta = leaves_vector->jet_eta->at(j);
          leaves_flat->bjet_1_phi = leaves_vector->jet_phi->at(j);
          bjet1.SetPtEtaPhiM(leaves_flat->bjet_1_pt, leaves_flat->bjet_1_eta, leaves_flat->bjet_1_phi, 0);
        } else if(nbjet==2) {
          leaves_flat->bjet_2_pt = leaves_vector->jet_pt->at(j);
          leaves_flat->bjet_2_eta = leaves_vector->jet_eta->at(j);
          leaves_flat->bjet_2_phi = leaves_vector->jet_phi->at(j);
          bjet2.SetPtEtaPhiM(leaves_flat->bjet_2_pt, leaves_flat->bjet_2_eta, leaves_flat->bjet_2_phi, 0);
        }
        if (leaves_vector->jet_pt->at(j) > 100000 && leaves_vector->jet_isBjetMatched->at(j)) {
          if(leaves_vector->jet_pt->at(j) > bjetpt) {
            bjetpt = leaves_vector->jet_pt->at(j);
          }
        }
      } else if(leaves_vector->jet_pt->at(j) > 20000 && leaves_vector->jet_isBjet->at(j) == 0) {
        notbjet++;
        if(notbjet==1) {
          leaves_flat->notBjet_1_pt = leaves_vector->jet_pt->at(j);
          leaves_flat->notBjet_1_eta = leaves_vector->jet_eta->at(j);
          leaves_flat->notBjet_1_phi = leaves_vector->jet_phi->at(j);
          notBjet1.SetPtEtaPhiM(leaves_flat->notBjet_1_pt, leaves_flat->notBjet_1_eta, leaves_flat->notBjet_1_phi, 0);
        }
      }
    }

    leaves_flat->bjet_truth_n = truenbjet;

    if(leaves_vector->METSigSoftSub_seed < (0.5 + 0.1 * nbjet))
//    if(leaves_vector->METSigSoftSub_seed < (0.65 + 0.13 * nbjet))
//    if(leaves_vector->METSigSoftSub_seed < (0.35 + 0.07 * nbjet))
    {
      leaves_flat->pass_QCDCut = 1;
    }

//   if((leaves_vector->treatAsYear == 2015 || leaves_vector->treatAsYear == 2016) && (leaves_vector->JetTileVetoLBA52 || leaves_vector->TauTileVetoLBA52 || leaves_vector->JetTileVeto || leaves_vector->TauTileVeto))  {leaves_flat->pass_CleaningCut =0;}



    if((leaves_vector->treatAsYear == 2015 || leaves_vector->treatAsYear == 2016) && leaves_vector->HLT_j80_bmv2c2060_split_xe60 && bjetpt > 100000) {
      leaves_flat->IsBjetTrigPassed = 1;
      leaves_flat->lumi_bjet=24.7/36.5;
      leaves_flat->passBjetGRL = leaves_vector->passBjetGRL;
      leaves_flat->pile_bjet = leaves_vector->Bjet_PileupWeight;
      leaves_flat->pile_bjet_up = leaves_vector->Bjet_PileupWeight_up;
      leaves_flat->pile_bjet_down = leaves_vector->Bjet_PileupWeight_down;
//      leaves_flat->IsBjetTrigPassed = 1;leaves_flat->lumi_bjet=24.7/36.5;leaves_flat->passBjetGRL = 1;leaves_flat->pile_bjet = leaves_vector->Bjet_PileupWeight;
//        leaves_flat->IsBjetTrigPassed = 1;leaves_flat->lumi_bjet=leaves_vector->HLT_jX_prescale*0.1*1.8*leaves_flat->pass_QCDCut ;leaves_flat->passBjetGRL = 1;leaves_flat->pile_bjet = leaves_vector->pileupweight;

    } else if(leaves_vector->treatAsYear == 2017 && leaves_vector->HLT_j80_bmv2c1050_split_xe60 && bjetpt > 100000) {
      leaves_flat->IsBjetTrigPassed = 1;
      leaves_flat->lumi_bjet = 1;
      leaves_flat->passBjetGRL =1;
      leaves_flat->pile_bjet = leaves_vector->pileupweight;
      leaves_flat->pile_bjet_up = leaves_vector->pileupweight_up;
      leaves_flat->pile_bjet_down = leaves_vector->pileupweight_down;
    } else if(leaves_vector->treatAsYear == 2018 && leaves_vector->HLT_j80_bmv2c1050_split_xe60 && bjetpt > 100000) {
      leaves_flat->IsBjetTrigPassed = 1;
      leaves_flat->lumi_bjet = 1;
      leaves_flat->passBjetGRL =1;
      leaves_flat->pile_bjet = leaves_vector->pileupweight;
      leaves_flat->pile_bjet_up = leaves_vector->pileupweight_up;
      leaves_flat->pile_bjet_down = leaves_vector->pileupweight_down;
//leaves_flat->IsBjetTrigPassed = 1;leaves_flat->lumi_bjet = leaves_vector->HLT_jX_prescale*0.04*1.12 * leaves_flat->pass_QCDCut ;leaves_flat->passBjetGRL =1;leaves_flat->pile_bjet = leaves_vector->pileupweight;
    } else {
      leaves_flat->IsBjetTrigPassed = 0;
      leaves_flat->lumi_bjet = 1;
      leaves_flat->passBjetGRL =leaves_vector->passBjetGRL;
      leaves_flat->pile_bjet =leaves_vector->pileupweight;
      leaves_flat->pile_bjet_up = leaves_vector->pileupweight_up;
      leaves_flat->pile_bjet_down = leaves_vector->pileupweight_down;
    }

    if(leaves_flat->IsMETTrigPassed && leaves_flat->met > 200000) {
      leaves_flat->IsBjetTrigPassed = 0;
      leaves_flat->lumi_bjet = 1;
      leaves_flat->passBjetGRL =leaves_vector->passBjetGRL;
      leaves_flat->pile_bjet =leaves_vector->pileupweight;
      leaves_flat->bjet_trig_sf=1.;
      leaves_flat->pile_bjet_up = leaves_vector->pileupweight_up;
      leaves_flat->pile_bjet_down = leaves_vector->pileupweight_down;
    }

// For single muon regions:
    if( leaves_flat->met < 100000) {
      leaves_flat->pile_bjet =leaves_vector->pileupweight;
      leaves_flat->pile_bjet_up = leaves_vector->pileupweight_up;
      leaves_flat->pile_bjet_down = leaves_vector->pileupweight_down;
      leaves_flat->bjet_trig_sf=leaves_vector->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50;
      leaves_flat->lumi_bjet = 1;
    }

    leaves_flat->pile_bjet2 = leaves_flat->pile_bjet;
    leaves_flat->qcdweight =1;
//QCD SPECIFIC STUFF, SHOULD BE COMMENTED OUT OTHERWISE
//if(leaves_vector->treatAsYear == 2015 || leaves_vector->treatAsYear == 2016 || leaves_vector->treatAsYear == 2018 || leaves_vector->treatAsYear == 2017)
//{
//leaves_flat->qcdweight = leaves_vector->HLT_jX_prescale*0.002  * leaves_flat->pass_QCDCut;
//leaves_flat->IsBjetTrigPassed = 1;leaves_flat->IsMETTrigPassed = 1; leaves_flat->passBjetGRL = 1;
//if(leaves_vector->met < 200000){leaves_flat->qcdweight = leaves_flat->qcdweight * 0.6;}
//}

    if(leaves_flat->tau_n >= 1) {
      tau1.SetPtEtaPhiM(leaves_flat->tau_1_pt, leaves_flat->tau_1_eta, leaves_flat->tau_1_phi, 0);
    }
    if(leaves_flat->tau_n >= 2) {
      tau2.SetPtEtaPhiM(leaves_flat->tau_2_pt, leaves_flat->tau_2_eta, leaves_flat->tau_2_phi, 0);
    }
    if(leaves_flat->mu_n >= 1) {
      mu1.SetPtEtaPhiM(leaves_vector->mu_pt->at(0), leaves_vector->mu_eta->at(0), leaves_vector->mu_phi->at(0), 0);
    }
    if(leaves_flat->mu_n >= 2) {
      mu2.SetPtEtaPhiM(leaves_vector->mu_pt->at(1), leaves_vector->mu_eta->at(1), leaves_vector->mu_phi->at(1), 0);
    }

    if(leaves_flat->tau_n >= 1 && leaves_flat->mu_n >= 1) {
      leaves_flat->taumu_angles = tau1.Angle(mu1.Vect());
    } else {
      leaves_flat->taumu_angles = 0;
    }

    if(leaves_flat->tau_n >= 2) {
      leaves_flat->ditau_mass = (tau1+tau2).M();
    } else {
      leaves_flat->ditau_mass = 0;
    }

    if(leaves_flat->mu_n >= 2) {
      leaves_flat->dimu_mass = (mu1+mu2).M();
    } else {
      leaves_flat->dimu_mass = 0;
    }

    if(leaves_flat->mu_n >= 2) {
      leaves_flat->dimu_pt = (mu1+mu2).Pt();
    } else {
      leaves_flat->dimu_pt = 0;
    }

    if(leaves_flat->tau_n >= 2 && nbjet>=2) {
      leaves_flat->tau_mt2 = asymm_mt2_lester_bisect::get_mT2(tau1.M(), tau1.Pt()*TMath::Cos(tau1.Phi()), tau1.Pt()*TMath::Sin(tau1.Phi()), tau2.M(), tau2.Pt()*TMath::Cos(tau2.Phi()), tau2.Pt()*TMath::Sin(tau2.Phi()), leaves_flat->met*TMath::Cos(leaves_flat->met_phi), leaves_flat->met*TMath::Sin(leaves_flat->met_phi), 120000.,120000. );
      leaves_flat->bjet_angles = min(min(bjet1.Angle(tau1.Vect()), bjet1.Angle(tau2.Vect())), min(bjet2.Angle(tau1.Vect()), bjet2.Angle(tau2.Vect())))  ;
      leaves_flat->taujet_mt2 = 0;
    } else if(leaves_flat->tau_n == 1 && nbjet==2 && notbjet >=1 && leaves_flat->mu_n==0) {
      leaves_flat->taujet_mt2 = asymm_mt2_lester_bisect::get_mT2(tau1.M(), tau1.Pt()*TMath::Cos(tau1.Phi()), tau1.Pt()*TMath::Sin(tau1.Phi()), notBjet1.M(), notBjet1.Pt()*TMath::Cos(notBjet1.Phi()), notBjet1.Pt()*TMath::Sin(notBjet1.Phi()), leaves_flat->met*TMath::Cos(leaves_flat->met_phi), leaves_flat->met*TMath::Sin(leaves_flat->met_phi), 120000.,120000. );
      leaves_flat->bjet_angles = min(min(bjet1.Angle(tau1.Vect()), bjet1.Angle(notBjet1.Vect())), min(bjet2.Angle(tau1.Vect()), bjet2.Angle(notBjet1.Vect())))  ;
      leaves_flat->tau_mt2 = 0;
    } else if(leaves_flat->tau_n == 1 && nbjet==2 && leaves_flat->mu_n>=1) {
      leaves_flat->taujet_mt2 = asymm_mt2_lester_bisect::get_mT2(tau1.M(), tau1.Pt()*TMath::Cos(tau1.Phi()), tau1.Pt()*TMath::Sin(tau1.Phi()), mu1.M(), mu1.Pt()*TMath::Cos(mu1.Phi()), mu1.Pt()*TMath::Sin(notBjet1.Phi()), leaves_flat->met*TMath::Cos(leaves_flat->met_phi), leaves_flat->met*TMath::Sin(leaves_flat->met_phi), 120000.,120000. );
      leaves_flat->bjet_angles = min(min(bjet1.Angle(tau1.Vect()), bjet1.Angle(mu1.Vect())), min(bjet2.Angle(tau1.Vect()), bjet2.Angle(mu1.Vect())))  ;
      leaves_flat->tau_mt2 = 0;
    } else {
      leaves_flat->tau_mt2 = 0;
      leaves_flat->taujet_mt2 = 0;
      leaves_flat->bjet_angles = 0;
    }

    metvec.SetPtEtaPhiM(leaves_vector->met,0,leaves_vector->met_phi,0);
    leaves_flat->deltaphib =0;
    leaves_flat->deltaphitau =0;
    if(nbjet > 0) {
      leaves_flat->deltaphib =  abs(bjet1.DeltaPhi(metvec));
    }
    if(leaves_flat->tau_n > 0) {
      leaves_flat->deltaphitau  = abs(tau1.DeltaPhi(metvec));
    }
    dest->Fill();
  }

  delete leaves_vector;
  delete leaves_flat;
}


int main( int argc, char* argv[] ) {
  if ( argc < 3 )
    return 1;

  // new: avoid splitting files that are bigger than 100 GB
  TTree::SetMaxTreeSize( 1000000000000LL ); // 1 TB

  string outtreeprefix = "";

  if (argc > 3) {
    outtreeprefix = string(argv[3]);
//    outtreeprefix += "_";
  }

  //Get old file, old tree and set top branch address
  TFile *oldfile = new TFile(argv[1]);
  TFile *newfile = new TFile(argv[2],"recreate");

  // option "isUnknownSignal" will disable some checks based on MCID
  bool isUnknownSignal = false;
  if (argc > 4)
    if (strcmp(argv[4], "--isUnknownSignal") == 0)
      isUnknownSignal = true;
  std::cout << "Treating as signal: " << (isUnknownSignal ? "true" : "false") << std::endl;

  std::map<int, vector<double> > sumw_CBK;


  TTree *metadata2 = (TTree*) ((TTree*)oldfile->Get("skim_metadata"))->CloneTree();
//       metadata->Write();
  vector<double>* EventWeightSumVect = 0;

//  TTree * metadata2 = (TTree*) oldfile->Get("skim_metadata");
  if(outtreeprefix == "HERWIG7" || outtreeprefix == "AMCNLO" || outtreeprefix == "DS" ||outtreeprefix == "MADGRAPH") {
    std::cout<<"Skipping metadata step, some alt samples have weird metadata"<<std::endl;
  } else {
    if( metadata2->GetEntries()>0 ) {
      std::cout<<"got metadata"<<std::endl;
//      vector<double>* EventWeightSumVect = 0;
      int SampleID;
      metadata2->SetBranchAddress("SampleID", &SampleID);
      metadata2->GetEntry(0);
      if ( SampleID == 0 ) {
        std::cout<<"This is data"<<std::endl; // data has SampleID==0, need to skip since EventWeightSumVect is not defined in data
      } else {
        metadata2->SetBranchAddress("EventWeightSumVect", &EventWeightSumVect);
        for ( int i = 0; i < metadata2->GetEntries(); i++ ) {
          metadata2->GetEntry( i );
          if(sumw_CBK[SampleID].size()==0) {
            for(uint j=0; j<EventWeightSumVect->size(); j++)
              sumw_CBK[SampleID].push_back(EventWeightSumVect->at(j));
          } else {
            if(sumw_CBK[SampleID].size() != EventWeightSumVect->size()) {
              cout << "Inconsistent vector size: " << sumw_CBK[SampleID].size() << " vs " << EventWeightSumVect->size() << ". Aborting..." << endl;
              return 1;
            }
            for(uint j=0; j<EventWeightSumVect->size(); j++)
              sumw_CBK[SampleID][j] += EventWeightSumVect->at(j);
          }
        }
      }
    }

//  TFile *newfile = new TFile(argv[2],"recreate");

    metadata2->Write();
  }

  TIter next(oldfile->GetListOfKeys());
  TKey *key;

  vector< string > processed_keys;

  while ((key=(TKey*)next())) {
    if ( key->GetClassName() != std::string("TTree") )
      continue;
    // needed for PDF uncertainties
    if ( key->GetName() == std::string("skim_metadata") ) {
//	TTree *metadata = (TTree*)metadata2->CloneTree();
//	metadata2->Write();
      continue;
    }
    if ( key->GetName() == std::string("signal_metadata") || key->GetName() == std::string("signal_metadata_CBK") )
      continue;


    // process trees only once
    if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
      continue;
    else processed_keys.push_back( key->GetName() );


    TTree *origin = (TTree*)oldfile->Get( key->GetName() );
    //Create a new file + a clone of old tree in new file
    string outtrename = outtreeprefix;
    if (outtrename == "data") {
      outtrename = "data";
    } else if((outtrename == "HERWIG7" || outtrename == "AMCNLO" || outtrename == "DS" || outtrename == "MADGRAPH") && origin->GetName() == std::string("NOMINAL")) {
      std::cout<<"Renaming exception hit, if you are not doing alternative samples something is wrong."<<std::endl;
    } else {
//        outtrename += "_";
      outtrename += string(origin->GetName());
    }

    TTree *newtree = new TTree( outtrename.data(), origin->GetTitle() );

    cout << "flattening tree " << key->GetName() << endl;
    if(outtreeprefix == "HERWIG7" || outtreeprefix == "AMCNLO" || outtreeprefix == "DS" ||outtreeprefix ==  "MADGRAPH") {
      fillTree( origin, newtree, sumw_CBK, true , isUnknownSignal );
    } else {
      fillTree( origin, newtree, sumw_CBK, false, isUnknownSignal  );
    }
    newtree->Write();

    delete newtree;
    delete origin;
  }
  newfile->Close();

  delete oldfile;
  delete newfile;

  return 0;
}

