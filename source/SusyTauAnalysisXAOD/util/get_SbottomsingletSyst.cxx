#include <TSystem.h>
#include <TTree.h>
#include <TKey.h>
#include <TFile.h>
#include <TCut.h>
#include <utility>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include "util/leaves.h"
#include <algorithm>

using namespace std;

// NNPDF30_NNLO has 100 PDF error sets
static const int Npdf = 100;

void FillSumw(TreeLeavesFlat* leaves, vector<string> region, vector<int> index, std::map<int, vector<double> > sumw_CBK, double** sumw, double** sumwsq, bool doall)
{

  // don't process samples that don't have LHE3 weights (and we need them)
  if(leaves->mcEventWeights->size() < Npdf && doall) return;


  double weight = leaves->lumiWeight *leaves->tau_weight * leaves->bjet_weight * leaves->mu_weight * leaves->ele_weight * leaves->jvt_weight * leaves->pileupweight*leaves->bjet_trig_sf * leaves->lumi_bjet;

  // WARNING !!! DO WE APPLY OTHER WEIGHTS??
  // there can be correlations between PDF/scale and the pt/HT regime...
  // some weights are clearly not needed, like PRW, JVT (totally uncorrelated)
  //weight *= leaves->tau_weight;
  //weight *= leaves->bjet_weight;
  // just to cross check the pre-fit yields, don't need to use these for theory uncertainty calculation
  //weight *= leaves->pileupweight;
  //weight *= leaves->jvt_weight;
//  weight *= leaves->mu_weight;
//  weight *= leaves->ele_weight;

 //need to add more regions here, TODO 
  for(uint i=0;i<region.size();i++) {

        bool pass = false;
bool bjetcuts = ((leaves->IsBjetTrigPassed == 1 && leaves->passBjetGRL) || (leaves->IsMETTrigPassed && leaves->met > 200000));  // && leaves->mcEventWeight > -100;
bool muoncuts = (leaves->IsMuTrigPassed && leaves->mcEventWeight > -100 && leaves->mu_1_pt > 30000);
bool platcuts = (leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->met > 160000 && leaves->isBadTile==0  && leaves->jet_1_isBadTight==0);

    if(region[i]=="Nominal"){pass=true;}
    else if(region[i]=="TrueTauCR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 400000 && (leaves->ht+leaves->mu_1_pt) < 800000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 1 && leaves->tau_1_mtMet <80000 && leaves->tau_1_charge!=leaves->mu_1_charge && leaves->jet_n_btag == 2;}
    else if(region[i]=="TrueTauVR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 800000 && (leaves->ht+leaves->mu_1_pt) < 1000000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 1 && leaves->tau_1_mtMet <80000 && leaves->tau_1_charge!=leaves->mu_1_charge && leaves->jet_n_btag == 2;}
    else if(region[i]=="FakeTauCR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 400000 && (leaves->ht+leaves->mu_1_pt) < 800000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 1 && leaves->tau_1_mtMet > 100000 && leaves->tau_1_charge!=leaves->mu_1_charge && leaves->jet_n_btag == 2;}
    else if(region[i]=="FakeTauVR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 800000 && (leaves->ht+leaves->mu_1_pt) < 1000000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 1 && leaves->tau_1_mtMet > 100000 && leaves->tau_1_charge!=leaves->mu_1_charge && leaves->jet_n_btag == 2;}
    else if(region[i]=="OneTauCR"){pass = bjetcuts && platcuts && leaves->ht > 400000 && leaves->ht < 800000 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 2 && leaves->tau_n == 1 &&  leaves->tau_1_mtMet <80000 ;}
    else if(region[i]=="OneTauVR"){pass = bjetcuts && platcuts && leaves->ht > 800000 && leaves->ht < 1000000 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 2 && leaves->tau_n == 1 && leaves->tau_1_mtMet <80000 ;}
    else if(region[i]=="TwoLCR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt+leaves->mu_2_pt) > 400000 && (leaves->ht+leaves->mu_1_pt+leaves->mu_2_pt) < 800000 && leaves->mu_n==2 && leaves->jet_n >=3 && leaves->tau_n == 0 && leaves->jet_n_btag == 2 && leaves->mu_2_charge!=leaves->mu_1_charge ;}
    else if(region[i]=="TwoLVR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt+leaves->mu_2_pt) > 800000 && (leaves->ht+leaves->mu_1_pt+leaves->mu_2_pt) < 1000000 && leaves->mu_n==2 && leaves->jet_n >=3 && leaves->tau_n == 0 && leaves->jet_n_btag == 2 && leaves->mu_2_charge!=leaves->mu_1_charge ;}
    else if(region[i]=="OneLCR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 400000 && (leaves->ht+leaves->mu_1_pt) < 800000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 0 && leaves->jet_n_btag == 2 ;}
    else if(region[i]=="OneLVR"){pass = bjetcuts && platcuts && (leaves->ht+leaves->mu_1_pt) > 800000 && (leaves->ht+leaves->mu_1_pt) < 1000000 && leaves->mu_n==1 && leaves->jet_n >=3 && leaves->tau_n == 0 && leaves->jet_n_btag == 2 ;}
    else if(region[i]=="BtagCR"){pass = bjetcuts && platcuts && leaves->ht > 400000 && leaves->ht < 800000 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 3 && leaves->tau_n == 1 &&  leaves->tau_1_mtMet <80000 ;}
    else if(region[i]=="BtagVR"){pass = bjetcuts && platcuts && leaves->ht > 800000 && leaves->ht < 1000000 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 3 && leaves->tau_n == 1 && leaves->tau_1_mtMet <80000 ;}
    else if(region[i]=="DitauVR"){pass = bjetcuts && platcuts && leaves->ht > 400000 && leaves->ht < 1000000 && leaves->mu_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 2 && leaves->tau_n == 2 && (leaves->ditau_mass < 40000 || leaves->ditau_mass > 90000) && leaves->tau_1_charge!=leaves->tau_2_charge ;}
    else if(region[i]=="MuTauCR"){pass = muoncuts && leaves->ht > 400000  && leaves->ht <800000 && leaves->met < 100000  && leaves->jet_n >=3 && leaves->jet_n_btag ==0 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->mu_n == 2 && leaves->isBadTile==0  && leaves->jet_1_isBadTight==0 && leaves->dimu_mass < 101000 && leaves->dimu_mass > 81000 && leaves->dimu_pt > 200000 && leaves->tau_n == 0 && leaves->mu_1_charge != leaves->mu_2_charge;}
    else if(region[i]=="MuTau2CR"){pass = (leaves->IsMETTrigPassed && leaves->met > 200000)  && leaves->ht < 700000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0  && leaves->ele_n == 0 && leaves->jet_n >=3 && leaves->jet_n_btag == 0 && leaves->tau_n == 2   && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->tau_1_charge != leaves->tau_2_charge && (leaves->tau_1_mtMet + leaves->tau_2_mtMet) < 100000 && (leaves->treatAsYear != 2018 || (min(abs(leaves->tau_1_delPhiMet),abs(leaves->tau_2_delPhiMet)) > 0.12 || leaves->met > 250000));}
    else if(region[i]=="ZmumuCR"){pass = muoncuts  && leaves->ht > 400000 && leaves->ht < 800000 && leaves->met < 100000  && leaves->jet_n >=3 && leaves->jet_n_btag ==2 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->mu_n == 2 && leaves->isBadTile==0  && leaves->jet_1_isBadTight==0 && leaves->dimu_mass < 101000 && leaves->dimu_mass > 81000 && leaves->dimu_pt > 200000 && leaves->tau_n == 0 && leaves->mu_1_charge != leaves->mu_2_charge;}
    else if(region[i]=="ZmumuVR"){pass = muoncuts  && leaves->ht < 1000000 && leaves->ht > 800000 && leaves->met < 100000  && leaves->jet_n >=3 && leaves->jet_n_btag ==2 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->mu_n == 2 && leaves->isBadTile==0  && leaves->jet_1_isBadTight==0 && leaves->dimu_mass < 101000 && leaves->dimu_mass > 81000 && leaves->dimu_pt > 200000 && leaves->tau_n == 0 && leaves->mu_1_charge != leaves->mu_2_charge;}
    else if(region[i]=="SR"){pass = bjetcuts && leaves->ht > 1100000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.6 && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_HT1"){pass = bjetcuts && leaves->ht > 1000000 && leaves->ht < 1200000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.6 && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_HT2"){pass = bjetcuts && leaves->ht > 1200000 && leaves->ht > 1400000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.6 && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_HT3"){pass = bjetcuts && leaves->ht > 1400000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.6 && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_A1"){pass = bjetcuts && leaves->ht > 1100000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.0 && leaves->bjet_angles < 0.5  && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_A2"){pass = bjetcuts && leaves->ht > 1100000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 0.5 && leaves->bjet_angles < 1.0 && leaves->tau_mt2 > 140000;}
    else if(region[i]=="SR_A3"){pass = bjetcuts && leaves->ht > 1100000 && leaves->jet_1_pt > 140000 && leaves->jet_2_pt > 100000 && leaves->bjet_1_pt > 100000 && leaves->jet_1_delPhiMet > 0.5 && leaves->jet_2_delPhiMet > 0.5 && leaves->mu_n == 0 && leaves->jet_n_btag >= 2 && leaves->jet_n >= 3 && leaves->tau_n >= 2 && leaves->met > 160000 && leaves->isBadTile==0 && leaves->jet_1_isBadTight==0 && leaves->ditau_mass > 55000 && leaves->ditau_mass < 120000 && leaves->bjet_angles > 1.0 && leaves->tau_mt2 > 140000;}



    else{continue;}


    if(pass==true) {

      // nominal also should be mcEventWeights[0]
      sumw[i][0] += weight*leaves->mcEventWeight;
      sumwsq[i][0] += pow(weight*leaves->mcEventWeight,2);

if(!doall){continue;}

      if(leaves->mcEventWeight != leaves->mcEventWeights->at(0))
	cout << "PROBLEM WITH NOMINAL WEIGHT" << endl;

      // weight systematics
      for(uint j=1;j<index.size();j++) {
	if(sumw_CBK[leaves->SampleID][index[j]] == 0.) 
//if(1==1)
	  cout << "Empty CBK for SampleID " << leaves->SampleID << " weight index " << index[j] << endl;
	else 
{//std::cout<<index[j]<<"   "<<leaves->SampleID<<std::endl;
	  // normalize w.r.t sumw_CBK of the systematics variation
if(index[j]==166 || index[j]==142)
{
sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]] * leaves->mcEventWeights->at(index[j-2]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-2]] / leaves->mcEventWeight;
sumwsq[i][j] += pow(weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]] * leaves->mcEventWeights->at(index[j-2]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-2]] / leaves->mcEventWeight,2);
}
else if(index[j]==167 || index[j]==143)
{
sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]]* leaves->mcEventWeights->at(index[j-6]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-6]] / leaves->mcEventWeight;
sumwsq[i][j] += pow(weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]]* leaves->mcEventWeights->at(index[j-6]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j-6]] / leaves->mcEventWeight,2);
}

else
{
sumw[i][j] += weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]];
sumwsq[i][j] += pow(weight * leaves->mcEventWeights->at(index[j]) * sumw_CBK[leaves->SampleID][0]/sumw_CBK[leaves->SampleID][index[j]],2);
}
}
      }
    }

  }
}


int main( int argc, char* argv[] ) {

  if ( argc >4  )
    return 1;
std::string samplename;
std::string susytag;
  bool isSUSY5 = false;

if(argc==4){
susytag = argv[3];
}
if(susytag=="susy5"){isSUSY5 = true;}

if(argc >= 3){samplename = argv[2];}
else if(argc == 2){samplename = "SAMPLE";}
else{return 1;}


bool doall = true;
if(samplename == "amcnlo" || samplename == "herwig7" || samplename == "dr"){doall = false;}

if(isSUSY5)  std::cout<<"singletDict['"<<samplename<<"'] = {}"<<std::endl;

  TFile *file = new TFile(argv[1],"READ");  

//TODO add more regions here
  vector<string> region;
  region.push_back("Nominal");
  region.push_back("TrueTauCR");
  region.push_back("FakeTauCR");
  region.push_back("BtagCR");
  region.push_back("QCDCR");
  region.push_back("TrueTauVR");
  region.push_back("FakeTauVR");
  region.push_back("BtagVR");
  region.push_back("OneLCR");
  region.push_back("TwoLCR");
  region.push_back("OneTauCR");
  region.push_back("OneLVR");
  region.push_back("TwoLVR");
  region.push_back("OneTauVR");
  region.push_back("DitauVR");
  region.push_back("SR");
  region.push_back("MuTau2CR");
  region.push_back("MuTauCR");
  region.push_back("ZmumuCR");
  region.push_back("ZmumuVR");
  region.push_back("SR_HT1");
  region.push_back("SR_HT2");
  region.push_back("SR_HT3");
  region.push_back("SR_A1");
  region.push_back("SR_A2");
  region.push_back("SR_A3");

  const uint Nregion = region.size();

  

  // indices of weight systematic variation, with rel21/mc16 the strategy changes a bit, updating indices
  vector<int> index;
  // nominal
  index.push_back(0);
  // PDF weights

  for(uint j=0;j<Npdf;j++) index.push_back(12+j);
  // isr:muRfac=10_fsr:muRfac=20 
  index.push_back(171);
  // isr:muRfac=10_fsr:muRfac=05
  index.push_back(172);
  // MUR0.5_MUF0.5
  index.push_back(5);
  // MUR0.5_MUF1
  index.push_back(4);
  // MUR1_MUF0.5
  index.push_back(2);
  // MUR1_MUF2
  index.push_back(1);
  // MUR2_MUF1
  index.push_back(3);
  // MUR2_MUF2
  index.push_back(6);
  // Var3cUp
  index.push_back(166);
  // Var3cDown
  index.push_back(167);
  // 1 nominal, Npdf(=100) PDF variations, 2 alpha_s variations, 6 scale variations 



  vector<int> index2;
  index2.push_back(0);
  for(uint j=0;j<Npdf;j++) index2.push_back(12+j);
  index2.push_back(147);
  index2.push_back(148);
  index2.push_back(5);
  index2.push_back(4);
  index2.push_back(2);
  index2.push_back(1);
  index2.push_back(3);
  index2.push_back(6);
  index2.push_back(142);
  index2.push_back(143);



  vector<int> index3;
  index3.push_back(0);
  for(uint j=0;j<Npdf;j++) index3.push_back(9+j);
  index3.push_back(0);
  index3.push_back(0);
  index3.push_back(8);
  index3.push_back(6);
  index3.push_back(2);
  index3.push_back(1);
  index3.push_back(3);
  index3.push_back(4);
  index3.push_back(0);
  index3.push_back(0);


  const uint Nsyst = index.size();

  double** sumw = new double*[Nregion];
  double** sumwsq = new double*[Nregion];
  // shall we compute sumw2 to keep track of high-weight events?

  for(uint i=0;i<Nregion;i++) {
    sumw[i] = new double[Nsyst];    
    sumwsq[i] = new double[Nsyst];

    for(uint j=0;j<Nsyst;j++) 
{
sumw[i][j] = 0.;
sumwsq[i][j] = 0.; 
}
  }


  // rescale sumw[i] to sumw[0] using CutBookKeepers
  std::map<int, vector<double> > sumw_CBK;
  
  TTree * metadata = (TTree*) file->Get("skim_metadata");
  if( metadata )
    {
      vector<double>* EventWeightSumVect = 0;
      int SampleID;
      metadata->SetBranchAddress("SampleID", &SampleID);
      metadata->SetBranchAddress("EventWeightSumVect", &EventWeightSumVect);
      for ( int i = 0; i < metadata->GetEntries(); i++ )
	{
	  metadata->GetEntry( i );

	  if(sumw_CBK[SampleID].size()==0) {
	    for(uint j=0;j<EventWeightSumVect->size();j++)
	      sumw_CBK[SampleID].push_back(EventWeightSumVect->at(j));
	  }
	  else {
	    if(sumw_CBK[SampleID].size() != EventWeightSumVect->size()) {
	      cout << "Inconsistent vector size: " << sumw_CBK[SampleID].size() << " vs " << EventWeightSumVect->size() << ". Aborting..." << endl;
	      return 1;
	    }
	    for(uint j=0;j<EventWeightSumVect->size();j++)
	      sumw_CBK[SampleID][j] += EventWeightSumVect->at(j);
	  }
	}
    }
  else {
    cout << "Could not find metadata tree. Aborting..." << endl;
    return 1;
  }



  


//here we do the loop over trees

int ntrees;
if(isSUSY5) ntrees = 3;
else ntrees = 6;


std::vector<std::string> ntuple;

if(isSUSY5)
{
ntuple =  {"NoTau_2bjet","NoTau_0bjet","NOMINAL2"};
}
else
{
ntuple = {"NOMINAL2_0bjet","TrueTau_2bjet_0mu","TrueTau_2bjet_1mu","TrueTau_3bjet","FakeTau_2bjet_0mu","FakeTau_2bjet_1mu","FakeTau_3bjet","TwoTau_ff_2bjet","TwoTau_ff_3bjet","TwoTau_tt_2bjet","TwoTau_tt_3bjet","TwoTau_tf_2bjet","TwoTau_tf_3bjet"};
}



for (const std::string &treename : ntuple)
{  
  


   for(uint i=0;i<Nregion;i++) {
    for(uint j=0;j<Nsyst;j++) 
{
sumw[i][j] = 0.;
sumwsq[i][j] = 0.;
}
  }

  TIter next(file->GetListOfKeys());
  TKey *key;
  vector< string > processed_keys;

  while ((key=(TKey*)next()))
    {
      if ( key->GetClassName() != std::string("TTree")) {continue;}
     if( key->GetName() != treename){continue;} 
      
      // process trees only once
      if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
	continue;
      else processed_keys.push_back( key->GetName() );

//      cout << "processing tree " << key->GetName() << endl;

      TTree *tree = (TTree*)file->Get( key->GetName() );

      // load flat ntuple structure from flattener
      TreeLeavesFlat * leaves = new TreeLeavesFlat();

      // set branch address for needed variables
      tree->SetBranchAddress("taumu_angles", &leaves->taumu_angles);
      tree->SetBranchAddress("bjet_1_pt", &leaves->bjet_1_pt);
      tree->SetBranchAddress("bjet_2_pt", &leaves->bjet_2_pt);
      tree->SetBranchAddress("jet_n_btag", &leaves->jet_n_btag);
      tree->SetBranchAddress("passBjetGRL", &leaves->passBjetGRL);
      tree->SetBranchAddress("IsBjetTrigPassed", &leaves->IsBjetTrigPassed);
      tree->SetBranchAddress("IsMETTrigPassed", &leaves->IsMETTrigPassed);
      tree->SetBranchAddress("tau_n", &leaves->tau_n);
      tree->SetBranchAddress("tau_1_pt", &leaves->tau_1_pt);
      tree->SetBranchAddress("tau_1_JetBDTMedium", &leaves->tau_1_JetBDTMedium);
      tree->SetBranchAddress("tau_1_mtMet", &leaves->tau_1_mtMet);
      tree->SetBranchAddress("tau_1_delPhiMet", &leaves->tau_1_delPhiMet);
      tree->SetBranchAddress("tau_1_charge", &leaves->tau_1_charge);
      tree->SetBranchAddress("tau_2_pt", &leaves->tau_2_pt);
      tree->SetBranchAddress("tau_2_JetBDTMedium", &leaves->tau_2_JetBDTMedium);
      tree->SetBranchAddress("tau_2_mtMet", &leaves->tau_2_mtMet);
      tree->SetBranchAddress("tau_2_delPhiMet", &leaves->tau_2_delPhiMet);
      tree->SetBranchAddress("tau_2_charge", &leaves->tau_2_charge);
      tree->SetBranchAddress("jet_n", &leaves->jet_n);
      tree->SetBranchAddress("jet_n_btag", &leaves->jet_n_btag);
      tree->SetBranchAddress("jet_1_pt", &leaves->jet_1_pt);
      tree->SetBranchAddress("jet_1_isBadTight", &leaves->jet_1_isBadTight);
      tree->SetBranchAddress("jet_1_delPhiMet", &leaves->jet_1_delPhiMet);
      tree->SetBranchAddress("jet_2_pt", &leaves->jet_2_pt);
      tree->SetBranchAddress("jet_2_delPhiMet", &leaves->jet_2_delPhiMet);
      tree->SetBranchAddress("mu_n", &leaves->mu_n);
      tree->SetBranchAddress("mu_1_mtMet", &leaves->mu_1_mtMet);
      tree->SetBranchAddress("met", &leaves->met);
      tree->SetBranchAddress("meff", &leaves->meff);
      tree->SetBranchAddress("ht", &leaves->ht);
      tree->SetBranchAddress("sumMT", &leaves->sumMT);
      tree->SetBranchAddress("sumMTJet", &leaves->sumMTJet);
      tree->SetBranchAddress("sumMTTauJet", &leaves->sumMTTauJet);
      tree->SetBranchAddress("Mt2", &leaves->Mt2);
      tree->SetBranchAddress("Mtaumu", &leaves->Mtaumu);
      tree->SetBranchAddress("JetTileVeto", &leaves->JetTileVeto);
      tree->SetBranchAddress("TauTileVeto", &leaves->TauTileVeto);
      tree->SetBranchAddress("JetTileVetoLBA52", &leaves->JetTileVetoLBA52);
      tree->SetBranchAddress("TauTileVetoLBA52", &leaves->TauTileVetoLBA52);
      tree->SetBranchAddress("lumiweight", &leaves->lumiWeight);
      tree->SetBranchAddress("tau_medium_weight", &leaves->tau_medium_weight);
      tree->SetBranchAddress("tau_weight", &leaves->tau_weight);
      tree->SetBranchAddress("mu_weight", &leaves->mu_weight);
      tree->SetBranchAddress("ele_weight", &leaves->ele_weight);
      tree->SetBranchAddress("bjet_weight", &leaves->bjet_weight);
      tree->SetBranchAddress("pile_bjet" , &leaves->pileupweight);
      tree->SetBranchAddress("jvt_weight" , &leaves->jvt_weight);
      tree->SetBranchAddress("mcEventWeights", &leaves->mcEventWeights);
      tree->SetBranchAddress("mcEventWeight", &leaves->mcEventWeight);
      tree->SetBranchAddress("SampleID", &leaves->SampleID);
      tree->SetBranchAddress("deltaphib", &leaves->deltaphib);
      tree->SetBranchAddress("isBadTile", &leaves->isBadTile);
      tree->SetBranchAddress("IsMuTrigPassed",&leaves->IsMuTrigPassed);
      tree->SetBranchAddress("dimu_pt",&leaves->dimu_pt);
      tree->SetBranchAddress("dimu_mass",&leaves->dimu_mass);
      tree->SetBranchAddress("treatAsYear",&leaves->treatAsYear);
      tree->SetBranchAddress("mu_1_pt",&leaves->mu_1_pt);
      tree->SetBranchAddress("mu_2_pt",&leaves->mu_2_pt);
      tree->SetBranchAddress("mu_1_charge",&leaves->mu_1_charge);
      tree->SetBranchAddress("mu_2_charge",&leaves->mu_2_charge);
      tree->SetBranchAddress("ditau_mass",&leaves->ditau_mass);
      tree->SetBranchAddress("bjet_trig_sf",&leaves->bjet_trig_sf);
      tree->SetBranchAddress("lumi_bjet", &leaves->lumi_bjet);
      tree->SetBranchAddress("bjet_angles", &leaves->bjet_angles);
      tree->SetBranchAddress("tau_mt2", &leaves->tau_mt2);

      Long64_t nentries = tree->GetEntries();

      for (Long64_t i=0;i<nentries; i++) {

//	if (i>0 && i%100000 == 0) cout << i << " / " << nentries << endl;

	tree->GetEntry(i);
	
//	FillSumw(leaves,region,index,sumw_CBK,sumw);
if(leaves->SampleID ==410658 || leaves->SampleID ==410659 || leaves->SampleID ==410644 || leaves->SampleID ==410645 || leaves->SampleID ==412004 || leaves->SampleID ==412005  || leaves->SampleID ==  411032 || leaves->SampleID == 411033 || leaves->SampleID == 411034 || leaves->SampleID ==  411035) {FillSumw(leaves,region,index,sumw_CBK,sumw,sumwsq,doall);}

else if(leaves->SampleID == 410646 || leaves->SampleID == 410647 || leaves->SampleID == 410648 || leaves->SampleID == 410649 || leaves->SampleID == 412002 || leaves->SampleID == 412003 || leaves->SampleID == 411036 || leaves->SampleID == 411037 || leaves->SampleID == 411038 || leaves->SampleID == 411039 || leaves->SampleID == 410654 || leaves->SampleID == 410655 || leaves->SampleID == 411036 || leaves->SampleID ==411037 || leaves->SampleID == 412002) {FillSumw(leaves,region,index2,sumw_CBK,sumw,sumwsq,doall);}
else{continue;}
//else{FillSumw(leaves,region,index,sumw_CBK,sumw,doall);}
      }

    }


  // perform the actual PDF+alpha_s and scale uncertainty calculation
  // NNPDF seems to have a symmetric error prescription (RMS estimator)
  // mc16 - prescription (somewhat) changed. Alpha_s and scale are somewhat different now?
  // radLow is now derived from nominal samples, radHigh from alternative samples.
  // we compute both, need to select them based on what hdamp samples were produced with. Nominal hdamp = 1.5, alternative hdamp = 3.0
  // this also means tot error is kind of useless
  double* PDF_err = new double[Nregion];
 

std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st'] = {}"<<std::endl;

  for(uint i=0;i<Nregion;i++) {

if(!doall)
{
if(region[i] == "Nominal"){continue;}
if(isSUSY5 && !(region[i] == "KinTauCR" || region[i] == "MuTauCR" || region[i] == "ZmumuCR" || region[i] == "ZmumuVR" || region[i] =="OneLCR"  || region[i] =="OneLVR")){continue;}
if(!isSUSY5 && (region[i] == "KinTauCR" || region[i] == "MuTauCR" || region[i] == "ZmumuCR" || region[i] == "ZmumuVR" || region[i] =="OneLCR"  || region[i] =="OneLVR")){continue;}
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"'] = {}"<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['nevents'] = "<<abs(sumw[i][0])<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['neventssq'] = "<<abs(sumwsq[i][0])<<std::endl;

//std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['nevents'] = "<<sqrt(pow(sumw[i][0],2) + sumwsq[i][0])<<std::endl;

}
else
{

    PDF_err[i] = 0.; 

    for(uint j=1;j<=Npdf;j++) {
      PDF_err[i] += pow( sumw[i][j]-sumw[i][0], 2.);
    }    
    PDF_err[i] /= (Npdf-1);
    PDF_err[i] = sqrt(PDF_err[i]);

// we don't have alpha_s weights in singlet...    

    double scale_min = sumw[i][0];
    double scale_max = sumw[i][0];
    for(uint j=Npdf+3;j<=Npdf+8;j++) {
      if(sumw[i][j] > scale_max) scale_max = sumw[i][j];
      if(sumw[i][j] < scale_min) scale_min = sumw[i][j];
    }

    double scale_err = ( scale_max - scale_min )/2.;
    double fsr_err = max(abs( sumw[i][Npdf+2] - sumw[i][0] ), abs( sumw[i][Npdf+1] - sumw[i][0] ));


    double muRup_err = abs(sumw[i][Npdf+7]-sumw[i][0]);
    double muRdown_err = abs(sumw[i][Npdf+4]-sumw[i][0]);
    double muFup_err = abs(sumw[i][Npdf+6]-sumw[i][0]);
    double muFdown_err = abs(sumw[i][Npdf+5]-sumw[i][0]);
    double muRmuFdown_err = abs(sumw[i][Npdf+3]-sumw[i][0]);
    double muRmuFup_err = abs(sumw[i][Npdf+8]-sumw[i][0]);
    
// unlike ttbar, radHigh is coming from the same sample, no need to add statistical error to it
    double radLow_err =  abs(sumw[i][Npdf+10]-sumw[i][0]) ;
    double radHigh_err = abs(sumw[i][Npdf+9]-sumw[i][0]) ;

// this is basicall PDF error, everything else treated separately
    double err_tot = sqrt( pow(PDF_err[i],2.)); //+ pow(alphas_err,2.) + pow(rad_err,2.));


//    double err_tot = sqrt( pow(PDF_err[i],2.) + pow(alphas_err,2.) + pow(scale_err,2.) );
if(region[i] == "Nominal"){continue;}
if(isSUSY5 && !(region[i] == "KinTauCR" || region[i] == "MuTauCR" || region[i] == "ZmumuCR" || region[i] == "ZmumuVR" || region[i] =="OneLCR"  || region[i] =="OneLVR")){continue;}
if(!isSUSY5 && (region[i] == "KinTauCR" || region[i] == "MuTauCR" || region[i] == "ZmumuCR" || region[i] == "ZmumuVR" || region[i] =="OneLCR"  || region[i] =="OneLVR")){continue;}

  std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"'] = {}"<<std::endl;

if(samplename =="singlet")
{
if(sumw[i][0] != 0)
  {
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['tot_error'] = "<<err_tot/sumw[i][0]<<std::endl;
/*
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['fsr_error'] = "<<fsr_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRup_error'] = "<<muRup_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRdown_error'] = "<<muRdown_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muFup_error'] = "<<muFup_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muFdown_error'] = "<<muFdown_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRmuFup_error'] = "<<muRmuFup_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRmuFdown_error'] = "<<muRmuFdown_err/sumw[i][0]<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['scale_error'] = "<<scale_err/sumw[i][0]<<std::endl;
*/
}

else
  {
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['tot_error'] = "<<0<<std::endl;
/*
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['fsr_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRup_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRdown_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muFup_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muFdown_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRmuFup_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['muRmuFdown_error'] = "<<0<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['scale_error'] = "<<0<<std::endl;
*/
}
}

std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['nevents'] = "<<abs(sumw[i][0])<<std::endl; 
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['radVar'] = "<<radLow_err<<std::endl;
std::cout<<"singletDict['"<<samplename<<"']['"<<treename<<"_st']['"<<region[i]<<"']['radVarHigh'] = "<<radHigh_err<<std::endl;

  // need to decide which output format we want for HistFitter
  
 //and here loop over trees ends 
 } 
} 

}
 return 0;
}

