#include <stdio.h>
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"
#include "TH1F.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TVirtualPad.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TPad.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include <map>
#include <iterator>
#include "TLatex.h"
#include "TIterator.h"
#include "TKey.h"

using namespace std;

int main( int argc, char* argv[] ) {

std::string samplename;
std::string resultname;

if(argc > 3){std::cout<<"Too many arguments. Exiting."<<std::endl; return 0;}

else if (argc ==3)
{samplename = argv[1]; resultname = argv[2];}

else if (argc < 3){std::cout<<"Please provide input and output as arguments. Exiting."<<std::endl; return 0;}


std::cout<<samplename<<std::endl;
std::cout<<resultname<<std::endl;


TFile *f = new TFile(samplename.c_str());
TFile *f2 = new TFile(resultname.c_str(),"recreate");


std::vector< std::string > processed_keys;

TIter next(f->GetListOfKeys());
TKey *key;

while ((key=(TKey*)next()))
{

if ( key->GetClassName() != std::string("TTree") ) {continue;}
if ( key->GetName() == std::string("signal_metadata") || key->GetName() == std::string("signal_metadata_CBK") || key->GetName() == std::string("skim_metadata")){continue;}



TTree *tree = (TTree*)f->Get(key->GetName());

if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
      continue;
else processed_keys.push_back( key->GetName() );

std::cout<< key->GetName() << std::endl;


TTree *tree3a = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 0");
std::string tree3aname;
if(key->GetName() == std::string("NOMINAL")){tree3aname = "TrueTau_0bjet";}
else{tree3aname = "TrueTau_0bjet_" + string(key->GetName());}
tree3a->SetName(tree3aname.c_str());
tree3a->Write();
delete tree3a;

TTree *tree3c = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 2 && mu_n==0");
std::string tree3cname;
if(key->GetName() == std::string("NOMINAL")){tree3cname = "TrueTau_2bjet_0mu";}
else{tree3cname = "TrueTau_2bjet_0mu_" + string(key->GetName());}
tree3c->SetName(tree3cname.c_str());
tree3c->Write();
delete tree3c;

TTree *tree3e = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 2 && mu_n==1");
std::string tree3ename;
if(key->GetName() == std::string("NOMINAL")){tree3ename = "TrueTau_2bjet_1mu";}
else{tree3ename = "TrueTau_2bjet_1mu_" + string(key->GetName());}
tree3e->SetName(tree3ename.c_str());
tree3e->Write();
delete tree3e;

TTree *tree3f = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 1 && mu_n==1");
std::string tree3fname;
if(key->GetName() == std::string("NOMINAL")){tree3fname = "TrueTau_1bjet_1mu";}
else{tree3fname = "TrueTau_1bjet_1mu_" + string(key->GetName());}
tree3f->SetName(tree3fname.c_str());
tree3f->Write();
delete tree3f;

TTree *tree3d = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag >= 3");
std::string tree3dname;
if(key->GetName() == std::string("NOMINAL")){tree3dname = "TrueTau_3bjet";}
else{tree3dname = "TrueTau_3bjet_" + string(key->GetName());}
tree3d->SetName(tree3dname.c_str());
tree3d->Write();
delete tree3d;

TTree *tree4a = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 0");
std::string tree4aname;
if(key->GetName() == std::string("NOMINAL")){tree4aname = "FakeTau_0bjet";}
else{tree4aname = "FakeTau_0bjet_" + string(key->GetName());}
tree4a->SetName(tree4aname.c_str());
tree4a->Write();
delete tree4a;

TTree *tree4c = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 2 && mu_n==0");
std::string tree4cname;
if(key->GetName() == std::string("NOMINAL")){tree4cname = "FakeTau_2bjet_0mu";}
else{tree4cname = "FakeTau_2bjet_0mu_" + string(key->GetName());}
tree4c->SetName(tree4cname.c_str());
tree4c->Write();
delete tree4c;

TTree *tree4d = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 2 && mu_n==1");
std::string tree4dname;
if(key->GetName() == std::string("NOMINAL")){tree4dname = "FakeTau_2bjet_1mu";}
else{tree4dname = "FakeTau_2bjet_1mu_" + string(key->GetName());}
tree4d->SetName(tree4dname.c_str());
tree4d->Write();
delete tree4d;

TTree *tree4f = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 1 && mu_n==1");
std::string tree4fname;
if(key->GetName() == std::string("NOMINAL")){tree4fname = "FakeTau_1bjet_1mu";}
else{tree4fname = "FakeTau_1bjet_1mu_" + string(key->GetName());}
tree4f->SetName(tree4fname.c_str());
tree4f->Write();
delete tree4f;

TTree *tree4e = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag >= 3");
std::string tree4ename;
if(key->GetName() == std::string("NOMINAL")){tree4ename = "FakeTau_3bjet";}
else{tree4ename = "FakeTau_3bjet_" + string(key->GetName());}
tree4e->SetName(tree4ename.c_str());
tree4e->Write();
delete tree4e;

TTree *tree5c = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag == 2");
std::string tree5cname;
if(key->GetName() == std::string("NOMINAL")){tree5cname = "TwoTau_ff_2bjet";}
else{tree5cname = "TwoTau_ff_2bjet_" + string(key->GetName());}
tree5c->SetName(tree5cname.c_str());
tree5c->Write();
delete tree5c;

TTree *tree5d = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag > 2");
std::string tree5dname;
if(key->GetName() == std::string("NOMINAL")){tree5dname = "TwoTau_ff_3bjet";}
else{tree5dname = "TwoTau_ff_3bjet_" + string(key->GetName());}
tree5d->SetName(tree5dname.c_str());
tree5d->Write();
delete tree5d;

TTree *tree6c = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag == 2");
std::string tree6cname;
if(key->GetName() == std::string("NOMINAL")){tree6cname = "TwoTau_tt_2bjet";}
else{tree6cname = "TwoTau_tt_2bjet_" + string(key->GetName());}
tree6c->SetName(tree6cname.c_str());
tree6c->Write();
delete tree6c;

TTree *tree6d = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag > 2");
std::string tree6dname;
if(key->GetName() == std::string("NOMINAL")){tree6dname = "TwoTau_tt_3bjet";}
else{tree6dname = "TwoTau_tt_3bjet_" + string(key->GetName());}
tree6d->SetName(tree6dname.c_str());
tree6d->Write();
delete tree6d;

TTree *tree7c = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag == 2");
std::string tree7cname;
if(key->GetName() == std::string("NOMINAL")){tree7cname = "TwoTau_tf_2bjet";}
else{tree7cname = "TwoTau_tf_2bjet_" + string(key->GetName());}
tree7c->SetName(tree7cname.c_str());
tree7c->Write();
delete tree7c;

TTree *tree7d = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag > 2");
std::string tree7dname;
if(key->GetName() == std::string("NOMINAL")){tree7dname = "TwoTau_tf_3bjet";}
else{tree7dname = "TwoTau_tf_3bjet_" + string(key->GetName());}
tree7d->SetName(tree7dname.c_str());
tree7d->Write();
delete tree7d;

TTree *treeex2 = tree->CopyTree("jet_n_btag==0");
std::string treeexname2;
if(key->GetName() == std::string("NOMINAL")){treeexname2 = "NOMINAL2_0bjet";}
else{treeexname2 = "NOMINAL2_0bjet_" + string(key->GetName());}
treeex2->SetName(treeexname2.c_str());
treeex2->Write();
delete treeex2;

TTree *treeex3 = tree->CopyTree("jet_n_btag==1");
std::string treeexname3;
if(key->GetName() == std::string("NOMINAL")){treeexname3 = "NOMINAL2_1bjet";}
else{treeexname3 = "NOMINAL2_1bjet_" + string(key->GetName());}
treeex3->SetName(treeexname3.c_str());
treeex3->Write();
delete treeex3;

TTree *treeex4 = tree->CopyTree("jet_n_btag>=2");
std::string treeexname4;
if(key->GetName() == std::string("NOMINAL")){treeexname4 = "NOMINAL2_2bjet";}
else{treeexname4 = "NOMINAL2_2bjet_" + string(key->GetName());}
treeex4->SetName(treeexname4.c_str());
treeex4->Write();
delete treeex4;

}
TTree *meta = (TTree*)f->Get("skim_metadata");
TTree *meta2 = meta->CopyTree("");
meta2->Write();


f2->Close();
f->Close();



return 0;
}
