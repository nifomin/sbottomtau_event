// definition of flat and vector ntuple structure

struct TreeLeavesFlat{

  unsigned long long EventNumber;
  unsigned int RunNumber;
  int ProcessID;
  int SampleID;
  float mcEventWeight;
  std::vector<float>* mcEventWeights = 0;
  float TauSpinnerWeight;
  int treatAsYear;
  //float PrescaleWeight;
  bool isDebugStream;

  double lumiWeight;
  double lumiWeight_up;
  double lumiWeight_down;
  float pileupweight;
  float pileupweight_up;
  float pileupweight_down;
  float tau_weight;
  float tau_medium_weight;
  float bjet_weight;
  float jvt_weight;
  float mu_weight;
  float ele_weight;
  float lumi_bjet;
  float pile_bjet;
  float pile_bjet_up;
  float pile_bjet_down;
  float pile_bjet2;
  bool JetTileVeto;
  bool TauTileVeto;  
  bool JetTileVetoLBA52;
  bool TauTileVetoLBA52;

  float muR10_muF20Weight;
  float muR10_muF05Weight;
  float muR20_muF10Weight;
  float muR05_muF10Weight;
  float muR05_muF05Weight;
  float muR20_muF20Weight;
  float fsr_muRfac05;
  float fsr_muRfac20;
  float alphasUp;
  float alphasDown;
  float Var3cDown;
  float Var3cUp; 

  float lumi_muR10_muF20Weight;
  float lumi_muR10_muF05Weight;
  float lumi_muR20_muF10Weight;
  float lumi_muR05_muF10Weight;
  float lumi_muR05_muF05Weight;
  float lumi_muR20_muF20Weight;
  float lumi_fsr_muRfac05;
  float lumi_fsr_muRfac20;
  float lumi_alphasUp;
  float lumi_alphasDown;
  float lumi_Var3cDown;
  float lumi_Var3cUp;

//  float   ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down;
//  float   ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;

  float   jet_weight_FT_EFF_B_systematics__1down;                           
  float   jet_weight_FT_EFF_B_systematics__1up;                              
  float   jet_weight_FT_EFF_C_systematics__1down;            
  float   jet_weight_FT_EFF_C_systematics__1up;                              
  float   jet_weight_FT_EFF_Light_systematics__1down;         
  float   jet_weight_FT_EFF_Light_systematics__1up;                          
  float   jet_weight_FT_EFF_extrapolation__1down;              
  float   jet_weight_FT_EFF_extrapolation__1up;                              
  float   jet_weight_FT_EFF_extrapolation_from_charm__1down;
  float   jet_weight_FT_EFF_extrapolation_from_charm__1up;                   
  float   jet_weight_JET_JvtEfficiency__1down;           
  float   jet_weight_JET_JvtEfficiency__1up;                                 
  float   jet_weight_JET_fJvtEfficiency__1down;
  float   jet_weight_JET_fJvtEfficiency__1up;     

  float   mu_weight_MUON_EFF_BADMUON_STAT__1down;                           
  float   mu_weight_MUON_EFF_BADMUON_STAT__1up;                              
  float   mu_weight_MUON_EFF_BADMUON_SYS__1down;                              
  float   mu_weight_MUON_EFF_BADMUON_SYS__1up;                               
  float   mu_weight_MUON_EFF_ISO_STAT__1down;               
  float   mu_weight_MUON_EFF_ISO_STAT__1up;                                  
  float   mu_weight_MUON_EFF_ISO_SYS__1down;              
  float   mu_weight_MUON_EFF_ISO_SYS__1up; 
  float   mu_weight_MUON_EFF_RECO_STAT__1down;                              
  float   mu_weight_MUON_EFF_RECO_STAT__1up;                                 
  float   mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down;    
  float   mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up;                           
  float   mu_weight_MUON_EFF_RECO_SYS__1down;              
  float   mu_weight_MUON_EFF_RECO_SYS__1up;                                  
  float   mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down;      
  float   mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up;                            
  float   mu_weight_MUON_EFF_TTVA_STAT__1down;            
  float   mu_weight_MUON_EFF_TTVA_STAT__1up;                                 
  float   mu_weight_MUON_EFF_TTVA_SYS__1down;           
  float   mu_weight_MUON_EFF_TTVA_SYS__1up;  
  float mu_weight_MUON_EFF_TrigStatUncertainty__1down;
  float mu_weight_MUON_EFF_TrigStatUncertainty__1up;
  float mu_weight_MUON_EFF_TrigSystUncertainty__1down;
  float mu_weight_MUON_EFF_TrigSystUncertainty__1up;

  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;                
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;          
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;                    
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;           
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;                 
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;           
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;                     
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;  
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
  float tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
  float tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
  float tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
  float tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;

float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;

  int tau_n;
  int tau_nTruthMatched;
  float mu_trig_sf;  
  float bjet_trig_sf;
  float bjet_trig_sf_up;
  float bjet_trig_sf_down;
  float tau_1_pt;
  float tau_1_eta;
  float tau_1_phi;
  float tau_1_charge;
  bool   tau_1_match;
  bool   tau_1_JetBDTLoose;
  bool   tau_1_JetBDTMedium;
  bool   tau_1_JetBDTTight;
  int    tau_1_truthMatchedType;
  float tau_1_delPhiMet;
  float tau_1_mtMet;

  float tau_2_pt;
  float tau_2_eta;
  float tau_2_phi;
  float tau_2_charge;
  bool   tau_2_match;
  bool   tau_2_JetBDTLoose;
  bool   tau_2_JetBDTMedium;
  bool   tau_2_JetBDTTight;
  int    tau_2_truthMatchedType;
  float tau_2_delPhiMet;
  float tau_2_mtMet;

  int ele_n;
  float ele_1_mtMet;

  int mu_n;
  float mu_1_mtMet;

  int jet_n;
  int jet_n_btag;
  // int jet_truth_n;

  float jet_1_pt;
  float jet_1_eta;
  float jet_1_phi;
  bool   jet_1_btag;
  bool   jet_1_isBadTight;
  float jet_1_delPhiMet;
  
  float jet_2_pt;
  float jet_2_eta;
  float jet_2_phi;
  bool   jet_2_btag;
  float jet_2_delPhiMet;	

  bool   IsMETTrigPassed;
  bool   IsBjetTrigPassed;
  bool   IsMuTrigPassed;
  float met;
  float met_phi;
  float met_sigPow3;
  float ht;
  float meff;
  float sumMT;  
  float sumMTJet;
  float sumMTTauJet;
  float Mt2;
  float Mtaumu;
  float Mt2_taumu;

  float bjet_1_pt;
  float bjet_1_eta;
  float bjet_1_phi;

  float bjet_2_pt;
  float bjet_2_eta;
  float bjet_2_phi;

  float mu_1_pt;
  float mu_1_eta;
  float mu_1_phi;
  float mu_1_charge;

  float mu_2_pt;
  float mu_2_eta;
  float mu_2_phi;
  float mu_2_charge;

  float notBjet_1_pt;
  float notBjet_1_eta;
  float notBjet_1_phi;

  float ditau_mass;
  float dimu_mass;
  float dimu_pt;
  float bjet_angles;
  float tau_mt2;
  float taujet_mt2;
  float taumu_angles;
  int bjet_truth_n;
  bool passBjetGRL;
  bool pass_CleaningCut; 
  bool pass_QCDCut;
  float qcdweight;
  float METSigSoftSub;
  float METSigSoftTrk;
  float METmeff;
  float avgIntXing;
  float actualIntXing;
  float deltaphib;
  int combIntXing; 
  bool isBadTile;
  float deltaphitau;
};




struct TreeLeavesVector{

  unsigned long long EventNumber;
  unsigned int RunNumber;
  int SampleID;
  int ProcessID;
  int treatAsYear;   
  bool isDebugStream; 

  float mcEventWeight;
  std::vector<float>* mcEventWeights = 0;
  double lumiweight;
  double lumiweight_up;
  double lumiweight_down;
  float pileupweight;
  float pileupweight_up;
  float pileupweight_down;
  float bjet_weight;
  float jvt_weight;
  float TauSpinnerWeight = -1; // no longer read
  float tau_weight;
  float tau_medium_weight;
  float ele_weight;
  float mu_weight;
  float Bjet_PileupWeight;
  float Bjet_PileupWeight_up;
  float Bjet_PileupWeight_down;
  float muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50;

  float muR10_muF20Weight;
  float muR10_muF05Weight;
  float muR20_muF10Weight;
  float muR05_muF10Weight;
  float muR05_muF05Weight;
  float muR20_muF20Weight;
  float fsr_muRfac05;
  float fsr_muRfac20;
  float alphasUp;
  float alphasDown;
  float Var3cDown;
  float Var3cUp; 

  bool JetTileVeto;
  bool TauTileVeto;
  bool JetTileVetoLBA52;
  bool TauTileVetoLBA52;

  float HLT_jX_prescale;

//  float   ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down;
//  float   ele_weight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
  float   ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
  float   ele_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;

  float   jet_weight_FT_EFF_B_systematics__1down;                           
  float   jet_weight_FT_EFF_B_systematics__1up;                              
  float   jet_weight_FT_EFF_C_systematics__1down;            
  float   jet_weight_FT_EFF_C_systematics__1up;                              
  float   jet_weight_FT_EFF_Light_systematics__1down;         
  float   jet_weight_FT_EFF_Light_systematics__1up;                          
  float   jet_weight_FT_EFF_extrapolation__1down;              
  float   jet_weight_FT_EFF_extrapolation__1up;                              
  float   jet_weight_FT_EFF_extrapolation_from_charm__1down;
  float   jet_weight_FT_EFF_extrapolation_from_charm__1up;                   
  float   jet_weight_JET_JvtEfficiency__1down;           
  float   jet_weight_JET_JvtEfficiency__1up;                                 
  float   jet_weight_JET_fJvtEfficiency__1down;
  float   jet_weight_JET_fJvtEfficiency__1up;     

  float   mu_weight_MUON_EFF_BADMUON_STAT__1down;                           
  float   mu_weight_MUON_EFF_BADMUON_STAT__1up;                              
  float   mu_weight_MUON_EFF_BADMUON_SYS__1down;                              
  float   mu_weight_MUON_EFF_BADMUON_SYS__1up;                               
  float   mu_weight_MUON_EFF_ISO_STAT__1down;               
  float   mu_weight_MUON_EFF_ISO_STAT__1up;                                  
  float   mu_weight_MUON_EFF_ISO_SYS__1down;              
  float   mu_weight_MUON_EFF_ISO_SYS__1up; 
  float   mu_weight_MUON_EFF_RECO_STAT__1down;                              
  float   mu_weight_MUON_EFF_RECO_STAT__1up;                                 
  float   mu_weight_MUON_EFF_RECO_STAT_LOWPT__1down;    
  float   mu_weight_MUON_EFF_RECO_STAT_LOWPT__1up;                           
  float   mu_weight_MUON_EFF_RECO_SYS__1down;              
  float   mu_weight_MUON_EFF_RECO_SYS__1up;                                  
  float   mu_weight_MUON_EFF_RECO_SYS_LOWPT__1down;      
  float   mu_weight_MUON_EFF_RECO_SYS_LOWPT__1up;                            
  float   mu_weight_MUON_EFF_TTVA_STAT__1down;            
  float   mu_weight_MUON_EFF_TTVA_STAT__1up;                                 
  float   mu_weight_MUON_EFF_TTVA_SYS__1down;           
  float   mu_weight_MUON_EFF_TTVA_SYS__1up;  
  float mu_weight_MUON_EFF_TrigStatUncertainty__1down;
  float mu_weight_MUON_EFF_TrigStatUncertainty__1up;
  float mu_weight_MUON_EFF_TrigSystUncertainty__1down;
  float mu_weight_MUON_EFF_TrigSystUncertainty__1up;

  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;                
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;          
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;                    
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
  float   tau_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
  float   tau_medium_weight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
  float tau_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
  float tau_medium_weight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
  float tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
  float tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
  float tau_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
  float tau_medium_weight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;


float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT__1up;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1down;
float tau_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;
float tau_medium_weight_TAUS_TRUEHADTAU_EFF_RNNID_SYST__1up;
	
  bool IsMETTrigPassed;
  bool HLT_j80_bmv2c2060_split_xe60;
  bool HLT_j80_bmv2c1050_split_xe60;
  bool passBjetGRL;
  bool HLT_mu50;
  bool HLT_mu26_ivarmedium;
  
  // jet smearing
  /*
  float HLT_j15;	//L1_RD0_FILLED
  float HLT_j25;	//L1_RD0_FILLED
  float HLT_j35;	//L1_RD0_FILLED
  float HLT_j55;	//L1_J15
  float HLT_j60;	//L1_J20
  float HLT_j85;	//L1_J20
  float HLT_j100;	//L1_J25
  float HLT_j110;	//L1_J30
  float HLT_j150;	//L1_J50
  float HLT_j175;	//L1_J50
  float HLT_j200;	//L1_J50
  float HLT_j260;	//L1_J75
  float HLT_j300;	//L1_J85
  float HLT_j320;	//L1_J85
  float HLT_j360;	//L1_J100
  float HLT_j380;	//L1_J100
  float HLT_j400;	//L1_J100
  float HLT_j420;	//L1_J100
  float HLT_j440;	//L1_J120
  float HLT_j460;	//L1_J120
  */
  
  int tau_n;
  int tau_nTruthMatched;
  std::vector<float> * tau_pt = 0;
  std::vector<float> * tau_eta = 0;
  std::vector<float> * tau_phi = 0;
  std::vector<float> * tau_m = 0;
  std::vector<float> * tau_charge = 0;
  std::vector<int> * tau_nTracks = 0;
  std::vector<char> * tau_JetBDTLoose = 0;
  std::vector<char> * tau_JetBDTMedium = 0;
  std::vector<char> * tau_JetBDTTight = 0;
  std::vector<char> * tau_isTruthMatchedTau = 0;
  std::vector<char> * tau_truthMatchedType = 0;
  std::vector<float> * tau_effSF = 0;
  std::vector<char> *  tau_isSignalTau = 0;
  std::vector<float> * tau_delPhiMet = 0;
  std::vector<float> * tau_mtMet = 0;
  std::vector<int> *jet_isBjetMatched = 0;

  int jet_n;
  int jet_n_btag;
  std::vector<float> * jet_pt = 0;
  std::vector<float> * jet_eta = 0;
  std::vector<float> * jet_phi = 0;
  std::vector<float> * jet_m = 0;
  std::vector<float> * jet_mtMet = 0;
  std::vector<float> * jet_mv2 = 0;   
  std::vector<char> *   jet_isBjet = 0;
  std::vector<float> *  jet_BjetSF = 0;
  std::vector<char> *   jet_isBadTight = 0;
  std::vector<float> *  jet_delPhiMet = 0;
  std::vector<int>  * jet_hadronTruthID = 0;
  //  int jet_truth_n;

  int ele_n;
  std::vector<float> * ele_pt = 0;    
  std::vector<float> * ele_eta = 0;  
  std::vector<float> * ele_phi = 0;  
  std::vector<float> * ele_mtMet = 0;
  std::vector<float> * ele_effSF = 0;

  int mu_n;
  std::vector<float> * mu_pt = 0;
  std::vector<float> * mu_eta = 0;
  std::vector<float> * mu_phi = 0;
  std::vector<float> * mu_m = 0; 
  std::vector<float> * mu_mtMet = 0;
  std::vector<float> * mu_effSF = 0;
  std::vector<float> * mu_charge = 0;
 
 
  float met;
  float met_phi;
  float METSigPow3_seed;
  float METSigSoftSub_seed;
  float METSigSoftSub;
  float METSigSoftTrk;
  float ht;
  float meff;
  float sumMT; 
  float sumMTJet;
  float sumMTTauJet;
  float Mt2;
  float Mtaumu;
  float Mt2_taumu; 
  float bjet_trig_sf;
  float bjet_trig_sf_up;
  float bjet_trig_sf_down;
  float actualIntXing;
  float avgIntXing;
  bool isBadTile;
};
