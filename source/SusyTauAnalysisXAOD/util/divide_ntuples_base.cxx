#include <stdio.h>
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"
#include "TH1F.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TVirtualPad.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TPad.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include <map>
#include <iterator>
#include "TLatex.h"
#include "TIterator.h"
#include "TKey.h"

using namespace std;

int main( int argc, char* argv[] ) {

std::string samplename;
std::string resultname;

if(argc > 3){std::cout<<"Too many arguments. Exiting."<<std::endl; return 0;}

else if (argc ==3)
{samplename = argv[1]; resultname = argv[2];}

else if (argc < 3){std::cout<<"Please provide input and output as arguments. Exiting."<<std::endl; return 0;}


std::cout<<samplename<<std::endl;
std::cout<<resultname<<std::endl;


TFile *f = new TFile(samplename.c_str());
TFile *f2 = new TFile(resultname.c_str(),"recreate");


std::vector< std::string > processed_keys;

TIter next(f->GetListOfKeys());
TKey *key;

while ((key=(TKey*)next()))
{

if ( key->GetClassName() != std::string("TTree") ) {continue;}
if ( key->GetName() == std::string("signal_metadata") || key->GetName() == std::string("signal_metadata_CBK") || key->GetName() == std::string("skim_metadata")){continue;}



TTree *tree = (TTree*)f->Get(key->GetName());

if ( std::find(processed_keys.begin(),processed_keys.end(),key->GetName()) != processed_keys.end() )
      continue;
else processed_keys.push_back( key->GetName() );

std::cout<< key->GetName() << std::endl;



TTree *tree2a = tree->CopyTree("met>160000 && tau_n==0");
//TTree *tree2a = tree->CopyTree("tau_n == 0 && jet_n_btag == 0");
std::string tree2aname;
if(key->GetName() == std::string("NOMINAL")){tree2aname = "NOMINAL2";}
else{tree2aname = "NOMINAL2_" + string(key->GetName());}
tree2a->SetName(tree2aname.c_str());
tree2a->Write();
delete tree2a;

/*
TTree *tree2b = tree->CopyTree("tau_n == 0 && jet_n_btag == 1 && met < 100000");
//TTree *tree2b = tree->CopyTree("tau_n == 0 && jet_n_btag == 1 && mu_n==1");
std::string tree2bname;
if(key->GetName() == std::string("NOMINAL")){tree2bname = "NoTau_1bjet_1mu";}
else{tree2bname = "NoTau_1bjet_1mu_" + string(key->GetName());}
tree2b->SetName(tree2bname.c_str());
tree2b->Write();

TTree *tree2e = tree->CopyTree("tau_n == 0 && jet_n_btag == 1 && mu_n==2");
std::string tree2ename;
if(key->GetName() == std::string("NOMINAL")){tree2ename = "NoTau_1bjet_2mu";}
else{tree2ename = "NoTau_1bjet_2mu_" + string(key->GetName());}
tree2e->SetName(tree2ename.c_str());
tree2e->Write();
*/


TTree *tree2c = tree->CopyTree("tau_n == 0 && jet_n_btag == 2 && met < 160000 && mu_n==2");
//TTree *tree2c = tree->CopyTree("tau_n == 0 && jet_n_btag == 2 && mu_n==1");
std::string tree2cname;
if(key->GetName() == std::string("NOMINAL")){tree2cname = "NoTau_2bjet";}
else{tree2cname = "NoTau_2bjet_" + string(key->GetName());}
tree2c->SetName(tree2cname.c_str());
tree2c->Write();
delete tree2c;


TTree *tree2d = tree->CopyTree("tau_n == 0 && jet_n_btag == 0 && met < 160000 && mu_n==2");
//TTree *tree2d = tree->CopyTree("tau_n == 0 && jet_n_btag == 2 && mu_n==1");
std::string tree2dname;
if(key->GetName() == std::string("NOMINAL")){tree2dname = "NoTau_0bjet";}
else{tree2dname = "NoTau_0bjet_" + string(key->GetName());}
tree2d->SetName(tree2dname.c_str());
tree2d->Write();
delete tree2d;

/*
TTree *tree2d = tree->CopyTree("met > 160000 && tau_n == 0");
std::string tree2dname;
if(key->GetName() == std::string("NOMINAL")){tree2dname = "NOMINAL2";}
else{tree2dname = "NOMINAL2_" + string(key->GetName());}
tree2d->SetName(tree2dname.c_str());
tree2d->Write();
*/

/*
TTree *tree2f = tree->CopyTree("tau_n == 0 && jet_n_btag == 2 && mu_n==2");
std::string tree2fname;
if(key->GetName() == std::string("NOMINAL")){tree2fname = "NoTau_2bjet_2mu";}
else{tree2fname = "NoTau_2bjet_2mu_" + string(key->GetName());}
tree2f->SetName(tree2fname.c_str());
tree2f->Write();


TTree *tree2d = tree->CopyTree("tau_n == 0 && jet_n_btag >= 3 && met < 100000");
//TTree *tree2d = tree->CopyTree("tau_n == 0 && jet_n_btag >= 3");
std::string tree2dname;
if(key->GetName() == std::string("NOMINAL")){tree2dname = "NoTau_3bjet";}
else{tree2dname = "NoTau_3bjet_" + string(key->GetName());}
tree2d->SetName(tree2dname.c_str());
tree2d->Write();


TTree *tree3a = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 0");
std::string tree3aname;
if(key->GetName() == std::string("NOMINAL")){tree3aname = "TrueTau_0bjet";}
else{tree3aname = "TrueTau_0bjet_" + string(key->GetName());}
tree3a->SetName(tree3aname.c_str());
tree3a->Write();


TTree *tree3b = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 1");
std::string tree3bname;
if(key->GetName() == std::string("NOMINAL")){tree3bname = "TrueTau_1bjet";}
else{tree3bname = "TrueTau_1bjet_" + string(key->GetName());}
tree3b->SetName(tree3bname.c_str());
tree3b->Write();

TTree *tree3c = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 2 && mu_n==0");
std::string tree3cname;
if(key->GetName() == std::string("NOMINAL")){tree3cname = "TrueTau_2bjet_0mu";}
else{tree3cname = "TrueTau_2bjet_0mu_" + string(key->GetName());}
tree3c->SetName(tree3cname.c_str());
tree3c->Write();

TTree *tree3e = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag == 2 && mu_n==1");
std::string tree3ename;
if(key->GetName() == std::string("NOMINAL")){tree3ename = "TrueTau_2bjet_1mu";}
else{tree3ename = "TrueTau_2bjet_1mu_" + string(key->GetName());}
tree3e->SetName(tree3ename.c_str());
tree3e->Write();

TTree *tree3d = tree->CopyTree("tau_n == 1  && tau_1_match && jet_n_btag >= 3");
std::string tree3dname;
if(key->GetName() == std::string("NOMINAL")){tree3dname = "TrueTau_3bjet";}
else{tree3dname = "TrueTau_3bjet_" + string(key->GetName());}
tree3d->SetName(tree3dname.c_str());
tree3d->Write();



TTree *tree4a = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 0");
std::string tree4aname;
if(key->GetName() == std::string("NOMINAL")){tree4aname = "FakeTau_0bjet";}
else{tree4aname = "FakeTau_0bjet_" + string(key->GetName());}
tree4a->SetName(tree4aname.c_str());
tree4a->Write();


TTree *tree4b = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 1 && mu_n==0");
std::string tree4bname;
if(key->GetName() == std::string("NOMINAL")){tree4bname = "FakeTau_1bjet_0mu";}
else{tree4bname = "FakeTau_1bjet_0mu_" + string(key->GetName());}
tree4b->SetName(tree4bname.c_str());
tree4b->Write();

TTree *tree4x = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 1  && mu_n==1");
std::string tree4xname;
if(key->GetName() == std::string("NOMINAL")){tree4xname = "FakeTau_1bjet_1mu";}
else{tree4xname = "FakeTau_1bjet_1mu_" + string(key->GetName());}
tree4x->SetName(tree4xname.c_str());
tree4x->Write();

TTree *tree4c = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 2 && mu_n==0");
std::string tree4cname;
if(key->GetName() == std::string("NOMINAL")){tree4cname = "FakeTau_2bjet_0mu";}
else{tree4cname = "FakeTau_2bjet_0mu_" + string(key->GetName());}
tree4c->SetName(tree4cname.c_str());
tree4c->Write();

TTree *tree4d = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag == 2 && mu_n==1");
std::string tree4dname;
if(key->GetName() == std::string("NOMINAL")){tree4dname = "FakeTau_2bjet_1mu";}
else{tree4dname = "FakeTau_2bjet_1mu_" + string(key->GetName());}
tree4d->SetName(tree4dname.c_str());
tree4d->Write();

TTree *tree4e = tree->CopyTree("tau_n == 1  && !tau_1_match && jet_n_btag >= 3");
std::string tree4ename;
if(key->GetName() == std::string("NOMINAL")){tree4ename = "FakeTau_3bjet";}
else{tree4ename = "FakeTau_3bjet_" + string(key->GetName());}
tree4e->SetName(tree4ename.c_str());
tree4e->Write();

TTree *tree5a = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag == 0");
std::string tree5aname;
if(key->GetName() == std::string("NOMINAL")){tree5aname = "TwoTau_ff_0bjet";}
else{tree5aname = "TwoTau_ff_0bjet_" + string(key->GetName());}
tree5a->SetName(tree5aname.c_str());
tree5a->Write();


TTree *tree5b = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag == 1");
std::string tree5bname;
if(key->GetName() == std::string("NOMINAL")){tree5bname = "TwoTau_ff_1bjet";}
else{tree5bname = "TwoTau_ff_1bjet_" + string(key->GetName());}
tree5b->SetName(tree5bname.c_str());
tree5b->Write();

TTree *tree5c = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag == 2");
std::string tree5cname;
if(key->GetName() == std::string("NOMINAL")){tree5cname = "TwoTau_ff_2bjet";}
else{tree5cname = "TwoTau_ff_2bjet_" + string(key->GetName());}
tree5c->SetName(tree5cname.c_str());
tree5c->Write();

TTree *tree5d = tree->CopyTree("tau_n >= 2  && !tau_1_match && !tau_2_match && jet_n_btag > 2");
std::string tree5dname;
if(key->GetName() == std::string("NOMINAL")){tree5dname = "TwoTau_ff_3bjet";}
else{tree5dname = "TwoTau_ff_3bjet_" + string(key->GetName());}
tree5d->SetName(tree5dname.c_str());
tree5d->Write();


TTree *tree6a = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag == 0");
std::string tree6aname;
if(key->GetName() == std::string("NOMINAL")){tree6aname = "TwoTau_tt_0bjet";}
else{tree6aname = "TwoTau_tt_0bjet_" + string(key->GetName());}
tree6a->SetName(tree6aname.c_str());
tree6a->Write();


TTree *tree6b = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag == 1");
std::string tree6bname;
if(key->GetName() == std::string("NOMINAL")){tree6bname = "TwoTau_tt_1bjet";}
else{tree6bname = "TwoTau_tt_1bjet_" + string(key->GetName());}
tree6b->SetName(tree6bname.c_str());
tree6b->Write();

TTree *tree6c = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag == 2");
std::string tree6cname;
if(key->GetName() == std::string("NOMINAL")){tree6cname = "TwoTau_tt_2bjet";}
else{tree6cname = "TwoTau_tt_2bjet_" + string(key->GetName());}
tree6c->SetName(tree6cname.c_str());
tree6c->Write();

TTree *tree6d = tree->CopyTree("tau_n >= 2  && tau_1_match && tau_2_match && jet_n_btag > 2");
std::string tree6dname;
if(key->GetName() == std::string("NOMINAL")){tree6dname = "TwoTau_tt_3bjet";}
else{tree6dname = "TwoTau_tt_3bjet_" + string(key->GetName());}
tree6d->SetName(tree6dname.c_str());
tree6d->Write();


TTree *tree7a = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag == 0");
std::string tree7aname;
if(key->GetName() == std::string("NOMINAL")){tree7aname = "TwoTau_tf_0bjet";}
else{tree7aname = "TwoTau_tf_0bjet_" + string(key->GetName());}
tree7a->SetName(tree7aname.c_str());
tree7a->Write();


TTree *tree7b = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag == 1");
std::string tree7bname;
if(key->GetName() == std::string("NOMINAL")){tree7bname = "TwoTau_tf_1bjet";}
else{tree7bname = "TwoTau_tf_1bjet_" + string(key->GetName());}
tree7b->SetName(tree7bname.c_str());
tree7b->Write();

TTree *tree7c = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag == 2");
std::string tree7cname;
if(key->GetName() == std::string("NOMINAL")){tree7cname = "TwoTau_tf_2bjet";}
else{tree7cname = "TwoTau_tf_2bjet_" + string(key->GetName());}
tree7c->SetName(tree7cname.c_str());
tree7c->Write();

TTree *tree7d = tree->CopyTree("tau_n >= 2  && ((tau_1_match && !tau_2_match)||(!tau_1_match && tau_2_match)) && jet_n_btag > 2");
std::string tree7dname;
if(key->GetName() == std::string("NOMINAL")){tree7dname = "TwoTau_tf_3bjet";}
else{tree7dname = "TwoTau_tf_3bjet_" + string(key->GetName());}
tree7d->SetName(tree7dname.c_str());
tree7d->Write();

TTree *treeex = tree->CopyTree("");
std::string treeexname;
if(key->GetName() == std::string("NOMINAL")){treeexname = "NOMINAL2";}
else{treeexname = "NOMINAL2_" + string(key->GetName());}
treeex->SetName(treeexname.c_str());
treeex->Write();


TTree *treeex2 = tree->CopyTree("jet_n_btag==0");
std::string treeexname2;
if(key->GetName() == std::string("NOMINAL")){treeexname2 = "NOMINAL2_0bjet";}
else{treeexname2 = "NOMINAL2_0bjet_" + string(key->GetName());}
treeex2->SetName(treeexname2.c_str());
treeex2->Write();

TTree *treeex3 = tree->CopyTree("jet_n_btag==1");
std::string treeexname3;
if(key->GetName() == std::string("NOMINAL")){treeexname3 = "NOMINAL2_1bjet";}
else{treeexname3 = "NOMINAL2_1bjet_" + string(key->GetName());}
treeex3->SetName(treeexname3.c_str());
treeex3->Write();

TTree *treeex4 = tree->CopyTree("jet_n_btag>=2");
std::string treeexname4;
if(key->GetName() == std::string("NOMINAL")){treeexname4 = "NOMINAL2_2bjet";}
else{treeexname4 = "NOMINAL2_2bjet_" + string(key->GetName());}
treeex4->SetName(treeexname4.c_str());
treeex4->Write();
*/
}
TTree *meta = (TTree*)f->Get("skim_metadata");
TTree *meta2 = meta->CopyTree("");
meta2->Write();


f2->Close();
f->Close();



return 0;
}
