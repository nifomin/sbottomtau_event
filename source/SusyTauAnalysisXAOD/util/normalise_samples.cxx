// superseded by PMG tool
#include "SUSYTools/SUSYCrossSection.h"
#include "AsgTools/AnaToolHandle.h"
#include "PMGAnalysisInterfaces/IPMGCrossSectionTool.h"
#include "PMGTools/PMGCrossSectionTool.h"
//#include "PileupReweighting/PileupReweightingTool.h"
//#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//#include "PathResolver/PathResolver.h"
#include "TFile.h"
#include "TTree.h"
#include "TKey.h"
#include "TVector2.h"
#include <utility>
#include <map>
#include <set>
#include <fstream>

using namespace std;

//static SUSY::CrossSectionDB crossDB; //( PathResolverFindCalibFile("SusyTauAnalysisXAOD/MGPy8EG_A14N23LO_GG_2stepStau.txt") );
static asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> PMGCrossSectionTool; 
//static CP::PileupReweightingTool* PRWtool;
//static GoodRunsListSelectionTool * GRLtool;

// declaring local variable in main() crashes!! to be understood
static std::vector<std::string> configFiles;
static std::vector<std::string> lumiFiles;    

// if still needed, will have to dump an isSherpa flag in the ntuples (in metadata)
//static set<int> SherpaDSID = {};

// combine HT and MET slices for ttbar and single top (Wt)
bool filterSamples( int sampleID, float truthMET, float truthHT )
{
  // ttbar
  // incl
  if ( sampleID == 410470 )
    return ( truthHT < 600000. && truthMET < 200000. ) ;
  // HT 600-1000
  if ( sampleID == 407344 )
    return ( truthMET < 300000. ) ;      
  // HT 1000-1500 and 1500+
  if ( sampleID == 407343 || sampleID == 407342 )
    return ( truthMET < 400000. ) ;
  // MET 200-300
  if ( sampleID == 407345 )
    return ( truthHT < 600000. ) ;
  // MET 300-400
  if ( sampleID == 407346 )
    return ( truthHT < 1000000. ) ;
  // MET 400+ not filtered
  
  // single top Wt
  // inclusive
  //if ( sampleID == 410013 || sampleID == 410014 )
  //return ( truthHT < 500000. && truthMET < 200000. ) ;
  // HT 500+
  //if ( sampleID == 407018 || sampleID == 407020 )
  //return ( truthMET < 200000. ) ;
  // MET 200+ not filtered

  // ISR up
  // incl
  //if ( sampleID == 410001 )
  //return ( truthHT < 600000. && truthMET < 200000. ) ;
  // MET 200+ 
  //if ( sampleID == 407032 )
  //return ( truthHT < 600000. );
  // HT 600-1000, 1000-1500 and 1500+ not filtered

  // ISR down
  // incl
  //if ( sampleID == 410002 )
  //return ( truthHT < 600000. && truthMET < 200000. ) ;
  // MET 200+ 
  //if ( sampleID == 407036 )
  //return ( truthHT < 600000. );
  // HT 600-1000, 1000-1500 and 1500+ not filtered

  // Powheg + Herwigpp
  // incl
  //if ( sampleID == 410004 )
  //return ( truthHT < 600000. && truthMET < 200000. ) ;
  // MET 200+ 
  //if ( sampleID == 407040 )
  //return ( truthHT < 600000. );
  // HT 600-1000, 1000-1500 and 1500+ not filtered

  return true;
}

double getLumiWeight( bool isSignal, bool removeProcessID0,
                      int sampleID, int processID, float lumi, double xsec, 
		      std::map<int, std::map<int, double > > & event_count,
		      std::map<int, std::map<int, double > > & event_count_noweight )
{
  if ( sampleID <= 0 )
    return 1;
    
  // GMSB processes which fail classification
  if ((processID == 0) && removeProcessID0 && isSignal) 
    return 0.;
    
  double weight = 1.0;
  
  /*  
  // WARNING !!! TEST PMG TOOL
  //auto xsec = ( ( processID > 0 ) ? crossDB.xsectTimesEff(sampleID,processID) : crossDB.xsectTimesEff(sampleID) ) ;
  
  // WARNING !!!! WAS THIS BEFORE!!!!
  //auto xsec = ( processID > 0 ) ? crossDB.xsectTimesEff(sampleID,processID) : PMGCrossSectionTool->getSampleXsection(sampleID) ;
  auto xsec = PMGCrossSectionTool->getSampleXsection(sampleID) ;

    
  //if((float)crossDB.xsectTimesEff(sampleID) != (float)PMGCrossSectionTool->getSampleXsection(sampleID))
  //cout << "Inconsistent cross sections for DSID " << sampleID 
  //	   << " SUSY " << crossDB.xsectTimesEff(sampleID) << " PMG " << PMGCrossSectionTool->getSampleXsection(sampleID) << endl; 
    
  if (xsec<0) 
    cout << "WARNING !!! CROSS SECTION FOR DSID " << sampleID << " = " << xsec << endl;       
  else 
    std::cout << "INFO: cross section for DSID " << sampleID << " is " << xsec << std::endl;
  */

  if ( sampleID > 0 && xsec < 0 && processID != 51 && processID != 52 && processID != 4 )
    {
      if ( sampleID != 0 )
	{
	  std::cout << "Error for sample " << sampleID << "  " << processID << std::endl;
	  return 0;
	}
      else
	{
	  std::cout << "Unknown sample " << sampleID << std::endl;
	  return 0;
	}
    }
  /*
  // quick tweak of sbottom processes being accounted for differently in process-counting and NLO xsec calculation
  if (processID == 51 || processID == 52 || processID == 4){
    double nProc = event_count[sampleID][51] + event_count[sampleID][52] + event_count[sampleID][4];
    auto xsec_tmp = crossDB.xsectTimesEff(sampleID, 4);
    weight *= xsec_tmp * lumi / nProc;
  }
  else
  */
  {
    // special treatment for dijet MC
    if(sampleID >= 361020 && sampleID <= 361032) 
      weight *= xsec * lumi / event_count_noweight[sampleID][processID];
    else
      weight *= xsec * lumi / event_count[sampleID][processID];
  }
  return weight;
}


TTree * normaliseTree( TTree * origin,
		       bool isSignal,
                       bool removeProcessID0,
		       std::string prefix,
		       int filterMode,
		       float lumi,
		       std::map<int, std::map<int, double > > & event_count,
		       std::map<int, std::map<int, double > > & event_count_noweight)
{
  std::map< int, std::set< std::pair<int, uint64_t> > > eventRegister;
  std::map< int, uint64_t > eventRegisterIn;
  std::map< int, uint64_t > eventRegisterOut;

  /*
    CP::SystematicVariation m_systUp("PRW_DATASF", 1 );
    CP::SystematicSet m_systUpSet; m_systUpSet.insert(m_systUp);
    CP::SystematicVariation m_systDown("PRW_DATASF", -1);
    CP::SystematicSet m_systDownSet; m_systDownSet.insert(m_systDown);
    CP::SystematicSet m_defaultSyst = CP::SystematicSet();
  */
  
  int sampleID;
  int processID;
  float truthHT;
  float truthMET;
  uint RunNumber;
  unsigned long long EventNumber;
  //float pileupweight, pileupweightUp, pileupweightDown;
  //float mcEventWeight;
  //std::vector<float>* mcEventWeights = 0;
   
  origin->SetBranchStatus("*", 1);
  origin->SetBranchAddress("SampleID",  &sampleID);
  origin->SetBranchAddress("ProcessID", &processID);    
  origin->SetBranchAddress("RunNumber",   &RunNumber);
  origin->SetBranchAddress("EventNumber", &EventNumber);
  origin->SetBranchAddress("truthHT",     &truthHT);
  origin->SetBranchAddress("truthMET",    &truthMET);
  //origin->SetBranchAddress("pileupweight",&pileupweight);
  //origin->SetBranchAddress("pileupweightUp",&pileupweightUp);
  //origin->SetBranchAddress("pileupweightDown",&pileupweightDown);
  //origin->SetBranchAddress("mcEventWeight",    &mcEventWeight);
  //origin->SetBranchAddress("mcEventWeights",    &mcEventWeights);


  TTree *newtree = origin->CloneTree(0);
  newtree->SetName( (prefix + origin->GetName() ).c_str() );

  double lumiweight, lumiweight_up, lumiweight_down;
  newtree->Branch("lumiweight",&lumiweight);
  newtree->Branch("lumiweight_up",&lumiweight_up);
  newtree->Branch("lumiweight_down",&lumiweight_down);

  bool isBadTile;
  newtree->Branch("isBadTile",&isBadTile);

  int treatAsYear;
  bool JetTileVeto;
  bool TauTileVeto;
  bool JetTileVetoLBA52;
  bool TauTileVetoLBA52;
  int jet_n;
  std::vector<float>* jet_eta=0;
  std::vector<float>* jet_phi=0;
  std::vector<float>* jet_delPhiMet=0;
  int tau_n;    
  std::vector<float>* tau_eta=0;
  std::vector<float>* tau_phi=0;
  std::vector<float>* tau_delPhiMet=0;

  origin->SetBranchAddress("treatAsYear",&treatAsYear);
  origin->SetBranchAddress("JetTileVeto",&JetTileVeto);
  origin->SetBranchAddress("TauTileVeto",&TauTileVeto);
  origin->SetBranchAddress("JetTileVetoLBA52",&JetTileVetoLBA52);
  origin->SetBranchAddress("TauTileVetoLBA52",&TauTileVetoLBA52);
  origin->SetBranchAddress("jet_n",&jet_n);
  origin->SetBranchAddress("jet_eta",&jet_eta);
  origin->SetBranchAddress("jet_phi",&jet_phi);
  origin->SetBranchAddress("jet_delPhiMet",&jet_delPhiMet);
  origin->SetBranchAddress("tau_n",&tau_n);
  origin->SetBranchAddress("tau_eta",&tau_eta);
  origin->SetBranchAddress("tau_phi",&tau_phi);
  origin->SetBranchAddress("tau_delPhiMet",&tau_delPhiMet);

  // for jet smearing
  float met;
  float metNoMuon;
  origin->SetBranchAddress("met",&met);
  origin->SetBranchAddress("metNoMuon",&metNoMuon);

  Long64_t nentries = origin->GetEntries();

  for (Long64_t ientry=0;ientry<nentries; ientry++) {
    
    origin->GetEntry(ientry);

    if ( ientry % 100000 == 0 )
      std::cout << ientry << " / " << nentries << std::endl; 
	
    if ( sampleID > 0 && filterMode==1 )
      if ( filterSamples(sampleID, truthMET, truthHT) == false ) {
        std::cout << sampleID << " skipped" << std::endl;
	continue;
      }

    // QCDdata
    //if(met<100000. && metNoMuon<100000.) continue;

    
    // WARNING !!! COMMENTED OUT FOR QCDdata
    // Duplicate event filter
    eventRegisterIn[sampleID]++;
	
    // now remove duplicates only for data
    if ( sampleID < 0 ) {
      if( eventRegister[sampleID].count( std::pair<uint,uint64_t>( RunNumber, EventNumber ) ) ) {
	continue;
      }
      else {
	eventRegisterOut[sampleID]++;
	eventRegister[sampleID].insert( std::pair<uint,uint64_t>( RunNumber, EventNumber ) );
      }
    }
    
    // retrieve xsec and compute lumi weight
    double xsec = PMGCrossSectionTool->getSampleXsection(sampleID);
    if (xsec<0) 
      cout << "WARNING !!! CROSS SECTION FOR DSID " << sampleID << " = " << xsec << endl;       
    else if (!ientry)
      std::cout << "INFO: cross section for DSID " << sampleID << " is " << xsec << std::endl;
    lumiweight = getLumiWeight( isSignal, removeProcessID0, sampleID, processID, lumi, xsec, event_count, event_count_noweight);

    double rel_uncert = 0.;
    //if(isSignal) rel_uncert = crossDB.rel_uncertainty(sampleID, processID);
    //else if(sampleID>0) rel_uncert = crossDB.rel_uncertainty(sampleID);
    //else if(sampleID>0) rel_uncert = PMGCrossSectionTool->getXsectionUncertainty(sampleID);
    // for now, use the Up uncertainty from PMG tool, as Down = 0, and getXsectionUncertainty returns (Up+Down)/2..., i.e. twice too small
    // theory uncertainties MUST be checked for all relevant samples (uncertainties seems to be filled a bit randomly)
    rel_uncert = PMGCrossSectionTool->getXsectionUncertaintyUP(sampleID);

    lumiweight_up = lumiweight*(1. + rel_uncert);
    lumiweight_down = lumiweight*(1. - rel_uncert);


    // update tile cleaning flag
    isBadTile = false;

    // NEW: these flags apply only to 2015-16 data
    if( (treatAsYear==2015 || treatAsYear==2016) && (JetTileVeto==true || TauTileVeto==true) ) isBadTile = true;
    // NEW: this flag only applies to 2016 data
    if( treatAsYear==2016 && (JetTileVetoLBA52==true || TauTileVetoLBA52==true) ) isBadTile = true;

    int index_allj = -1;
    double mindphi_allj = jet_n==0? 0. : 999.;
    for(int i=0;i<jet_n;i++)
      if(jet_delPhiMet->at(i)<mindphi_allj) {
	mindphi_allj = jet_delPhiMet->at(i);
	index_allj = i;
      }

    if(treatAsYear==2017) {
      // use 0.2 instead of the aggressive 0.4 for jets
      if(index_allj>=0) {
	// LBC63 bad
	if( jet_eta->at(index_allj)>(-0.9-0.2) && jet_eta->at(index_allj)<(0.+0.2) && fabs(TVector2::Phi_mpi_pi(jet_phi->at(index_allj) + 0.147)) < (0.2+9.81e-02/2.) )
	  isBadTile = true;
	// EBA03 bad
	if( jet_eta->at(index_allj)>0.8 && jet_eta->at(index_allj)<1.7 && fabs(TVector2::Phi_mpi_pi(jet_phi->at(index_allj) - 0.245)) < 9.81e-02 )
	  isBadTile = true;
      }
      // taus
      for(int i=0; i<tau_n;i++) 
	if(tau_delPhiMet->at(i)<mindphi_allj || tau_delPhiMet->at(i)<0.2) {
	  float bad_phi = 0.2; //tau_ntracks->at(i)==1 ? 0.2 : 0.3;
	  if( tau_eta->at(i)>(-0.9-0.2) && tau_eta->at(i)<(0.+0.2) && fabs(TVector2::Phi_mpi_pi(tau_phi->at(i) + 0.147)) < (bad_phi+9.81e-02/2.) ) {
	    isBadTile = true;
	    break;
	  }
	}
    }
    
    // LBA29 bad
    if(RunNumber >= 350310 && RunNumber <= 352514) {
      if(index_allj>=0)
	if( jet_eta->at(index_allj)>(0.-0.2) && jet_eta->at(index_allj)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(jet_phi->at(index_allj) - 2.798)) < (0.2+9.81e-02/2.) )
	  isBadTile = true;
      // taus
      for(int i=0; i<tau_n;i++) 
	if(tau_delPhiMet->at(i)<mindphi_allj || tau_delPhiMet->at(i)<0.2)
	  if( tau_eta->at(i)>(0.-0.2) && tau_eta->at(i)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(tau_phi->at(i) - 2.798)) < (0.2+9.81e-02/2.) ) {
	    isBadTile = true;
	    break;
	  }
    }
    // LBA30 bad
    if(RunNumber >= 350361 && RunNumber <= 352514) {
      // jets
      if(index_allj>=0)
	if( jet_eta->at(index_allj)>(0.-0.2) && jet_eta->at(index_allj)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(jet_phi->at(index_allj) - 2.896)) < (0.2+9.81e-02/2.) )
	  isBadTile = true;
      // taus
      for(int i=0; i<tau_n;i++) 
	if(tau_delPhiMet->at(i)<mindphi_allj || tau_delPhiMet->at(i)<0.2)
	  if( tau_eta->at(i)>(0.-0.2) && tau_eta->at(i)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(tau_phi->at(i) - 2.896)) < (0.2+9.81e-02/2.) ) {
	    isBadTile = true;
	    break;
	  }
    }
    // LBA32 bad 
    if(RunNumber >= 355261 && RunNumber <= 364292) {
      // jets
      if(index_allj>=0)
	if( jet_eta->at(index_allj)>(0.-0.2) && jet_eta->at(index_allj)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(jet_phi->at(index_allj) - 3.093)) < (0.2+9.81e-02/2.) )
	  isBadTile = true;
      // taus
      for(int i=0; i<tau_n;i++) 
	if(tau_delPhiMet->at(i)<mindphi_allj || tau_delPhiMet->at(i)<0.2)
	  if( tau_eta->at(i)>(0.-0.2) && tau_eta->at(i)<(0.9+0.2) && fabs(TVector2::Phi_mpi_pi(tau_phi->at(i) - 3.093)) < (0.2+9.81e-02/2.) ) {
	    isBadTile = true;
	    break;
	  }
    }
        

    // deactivated for now
    /*
      if(sampleID > 0 && SherpaDSID.find(sampleID) != SherpaDSID.end()) {
	  
      // cap high cross section weights
      if(fabs(mcEventWeight)>100.) {
      mcEventWeight = 1.;
      }
      for(uint i=0;i<mcEventWeights->size();i++) 
      // not appropriate for non-PDF or non-scale info stored in mcEventWeights (which is not used)
      if(fabs(mcEventWeights->at(i))>100.) {
      mcEventWeights->at(i) = 1.;
      }
	  
      // recompute pileup weights with mu profiles cleaned from high-weight events
      PRWtool->applySystematicVariation(m_defaultSyst).ignore();
      pileupweight = PRWtool->expert()->GetCombinedWeight( RunNumberPRW, sampleID, avgIntXing, 0);
      if(!isfinite(pileupweight)) pileupweight = 1.;
	  
      PRWtool->applySystematicVariation(m_systUpSet).ignore();
      double pileupweightUp = PRWtool->expert()->GetCombinedWeight( RunNumberPRW, sampleID, avgIntXing, 0);
      if(!isfinite(pileupweightUp)) pileupweightUp = 1.;
	  
      PRWtool->applySystematicVariation(m_systDownSet).ignore();
      double pileupweightDown = PRWtool->expert()->GetCombinedWeight( RunNumberPRW, sampleID, avgIntXing, 0);
      if(!isfinite(pileupweightDown)) pileupweightDown = 1.;
      }	
    */

    newtree->Fill();
  }
    
  if ( sampleID < 0 )
    for ( std::map< int, uint64_t >::iterator it = eventRegisterIn.begin(); it != eventRegisterIn.end(); ++it )
      if ( eventRegisterIn[it->first] != eventRegisterOut[it->first] )
	std::cout << "Duplicated found in sample " << it->first << ", fraction " << ( (float)eventRegisterIn[it->first] - eventRegisterOut[it->first] ) / eventRegisterIn[it->first] << std::endl;
 
  return newtree;
  
}


int main( int argc, char* argv[] ) {
  if ( argc < 4 )
    return 1;

  // avoid splitting files that are bigger than 100 GB
  TTree::SetMaxTreeSize( 1000000000000LL ); // 1 TB

  //crossDB.loadFile( PathResolverFindCalibFile("SusyTauAnalysisXAOD/HppEG_UE5C6L1_GMSB_tau_LambdaXX_tanbetaYY.txt") );
    
//  crossDB.loadFile("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/xsdb/mc15_13TeV/Backgrounds.txt");
    
  ASG_SET_ANA_TOOL_TYPE( PMGCrossSectionTool, PMGTools::PMGCrossSectionTool);
  PMGCrossSectionTool.setName("PMGCrossSectionTool");
  PMGCrossSectionTool.retrieve();

  std::vector<std::string> CS_files;
  CS_files.push_back("/sbottomtau_event/grls/PMGxsecDB_mc16.txt");
  CS_files.push_back("PMGxsecDB_mc16.txt");
  std::cout << "Looking in these locations for cross sections:" << std::endl;
  for (std::string s: CS_files)
    std::cout << "  " << s << std::endl;
  PMGCrossSectionTool->readInfosFromFiles(CS_files);

  int filterMode = 0;
  if ( argc > 4 )
    filterMode = atoi( argv[4] );

  std::cout << "Filter mode: " << filterMode << std::endl;
    
  std::string prefix = "";
  if ( argc == 6 )
    {
      prefix = argv[5];
      prefix += "_";
    }
    
  bool removeProcessID0 = true;
  for (int i = 0; i < argc; ++i)
    if (strcmp(argv[i], "--keepProcessID0") == 0) {
      removeProcessID0 = false;
      break;
    }
  std::cout << "Removing events with process ID 0 (by setting lumi weight to 0): " << (removeProcessID0 ? "true" : "false") << std::endl;


  // target lumi in units of pb-1
  double lumi = atof( argv[3] ); 
    
  std::cout << "Processing..." << std::endl;

    
  //Get old file, old tree and set top branch address
  TFile *oldfile = new TFile(argv[1]);
    
  std::map<int, std::map<int, double > > event_count;
  std::map<int, std::map<int, double > > event_count_noweight;
  // handle split systematics 
  // it is assumed that systematics splitting is done in 2 batches
  // --enableWeightSyst --enableTauSyst --enableMuonSyst --enableElecSyst --enableMETSyst
  // --enableJetSyst --skipNominal
  std::map<int, std::map<int, double > > event_count_split;
  std::map<int, std::map<int, double > > event_count_noweight_split;
    
  // it is assumed that signal systematics are not split
  TTree * signal_metadata = (TTree*) oldfile->Get("signal_metadata_CBK");

  // --enableWeightSyst --enableTauSyst --enableMuonSyst --enableElecSyst --enableMETSyst
  TTree * metadata = (TTree*) oldfile->Get("skim_metadata");
  // --enableJetSyst --skipNominal
  TTree * metadata_split = (TTree*) oldfile->Get("skim_metadata_skipNominal");

  if ( signal_metadata )
    {
      double eventWeight;
      int sampleID;
      int processID;

      signal_metadata->SetBranchAddress("SampleID", &sampleID);
      signal_metadata->SetBranchAddress("ProcessID", &processID);
      signal_metadata->SetBranchAddress("EventWeight", &eventWeight);
	
      for ( int i = 0; i < signal_metadata->GetEntries(); i++ )
        {
	  signal_metadata->GetEntry( i );
	  event_count[sampleID][processID] += eventWeight;
        }
    }
  if( metadata )
    {
      /*
      // load sumw for Sherpa samples
      TFile* file_sumw = new TFile( PathResolverFindCalibFile("SusyTauAnalysisXAOD/Sherpa_PRW_sumw.root").c_str(), "READ" );
      TTree* tree_sumw = (TTree*) file_sumw->Get("sumw");
      int SampleID;
      // damn it, used floats instead of CBK doubles
      vector<float>* sumw_good = 0;

      tree_sumw->SetBranchAddress("SampleID", &SampleID);
      tree_sumw->SetBranchAddress("sumw_good", &sumw_good);

      for ( int i = 0; i < tree_sumw->GetEntries(); i++ )
      {
      tree_sumw->GetEntry( i );

      if(SherpaDSID.find(SampleID) == SherpaDSID.end()) {
      continue;
      }
      if(sumw_good->size()==0) {
      cout << "Error: empty sumw for sample " << SampleID << endl;
      return 1;
      }
      event_count[SampleID][0] += sumw_good->at(0);
      }
      delete tree_sumw;
      file_sumw->Close();
      delete file_sumw;
      */

      // standard metadata
      int SampleID;
      double EventWeightSum;
      unsigned int EventSum;

      metadata->SetBranchAddress("SampleID", &SampleID);
      metadata->SetBranchAddress("EventWeightSum", &EventWeightSum);
      metadata->SetBranchAddress("EventSum", &EventSum);    

      for ( int i = 0; i < metadata->GetEntries(); i++ )
	{
	  metadata->GetEntry( i );

	  //if(SherpaDSID.find(SampleID) != SherpaDSID.end()) continue;

	  event_count[SampleID][0] += EventWeightSum;
	  event_count_noweight[SampleID][0] += EventSum; 
	}
    }
  if( metadata_split ) 
    {
      int SampleID;
      double EventWeightSum;
      unsigned int EventSum;
	
      metadata_split->SetBranchAddress("SampleID", &SampleID);
      metadata_split->SetBranchAddress("EventWeightSum", &EventWeightSum);
      metadata_split->SetBranchAddress("EventSum", &EventSum);    

      for ( int i = 0; i < metadata_split->GetEntries(); i++ )
	{
	  metadata_split->GetEntry( i );

	  //if(SherpaDSID.find(SampleID) != SherpaDSID.end()) continue;

	  event_count_split[SampleID][0] += EventWeightSum;
	  event_count_noweight_split[SampleID][0] += EventSum; 
	}
    }
  if(!metadata && !metadata_split && !signal_metadata)
    {
      cout << "Could not retrieve metadata. Aborting..." << endl;
      return 1;
    }
  
  /*
  PRWtool = new CP::PileupReweightingTool("PRWtool");
  // actual mu
  configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root");
  configFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20181111/purw.actualMu.root");
  // average mu
  lumiFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root");
  lumiFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root");
  lumiFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root") ;
  lumiFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root");

  PRWtool->setProperty("ConfigFiles",configFiles).ignore();
  PRWtool->setProperty("LumiCalcFiles",lumiFiles).ignore();
  PRWtool->setProperty("DataScaleFactor",1./1.03).ignore();
  PRWtool->setProperty("DataScaleFactorUP",1./0.99).ignore();
  PRWtool->setProperty("DataScaleFactorDOWN",1./1.07).ignore();
  PRWtool->setProperty("OutputLevel",MSG::WARNING).ignore();
  PRWtool->initialize().ignore();
  */

  /*
  GRLtool = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");

  std::vector<std::string> mygrls;
  mygrls.push_back( PathResolverFindCalibFile("SusyTauAnalysisXAOD/data17_ditauMET_GRL.xml") );
  
  GRLtool->setProperty("GoodRunsListVec", mygrls); //.ignore();
  GRLtool->setProperty("PassThrough", false); //.ignore()
  GRLtool->initialize(); //.ignore();
  */

  //Create a new file + a clone of old tree in new file
  TFile *newfile = new TFile(argv[2],"recreate");

  std::cout << "Writing out ntuple" << std::endl;
    
  TIter next(oldfile->GetListOfKeys());
  TKey *key = 0;
  while ((key=(TKey*)next()))
    {
      if ( key->GetClassName() != std::string("TTree") ) continue;
      if ( std::string(key->GetName()).find("metadata") != string::npos ) continue;

      std::cout << "Processing tree " << key->GetName() << std::endl;
		
      TTree *origin = (TTree*)oldfile->Get( key->GetName() );

      TTree *newtree = 0;
      if(metadata_split) {
	if( std::string(key->GetName()).find("JET_") != string::npos ) {
	  newtree = normaliseTree( origin, false, false, prefix, filterMode, lumi, event_count_split, event_count_noweight_split );
	}
	else {
	  newtree = normaliseTree( origin, false, false, prefix, filterMode, lumi, event_count, event_count_noweight );
	}
      }
      else { 
	newtree = normaliseTree( origin, signal_metadata ? true : false, removeProcessID0, prefix, filterMode, lumi, event_count, event_count_noweight );
      }
      newtree->Write();
      delete newtree;
    }

  if (signal_metadata) signal_metadata->CloneTree()->Write();
  if (metadata) metadata->CloneTree()->Write();
  if (metadata_split) metadata_split->CloneTree()->Write();

  newfile->Close();
  delete oldfile;
  delete newfile;
    
  return 0;
}
