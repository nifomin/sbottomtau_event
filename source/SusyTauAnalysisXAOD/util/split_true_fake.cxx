#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <TTree.h>
#include <TKey.h>

#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"

// SUSYTools
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "SUSYTools/SUSYCrossSection.h"

#include <utility>
#include <map>
#include <set>
#include <fstream>

#include <cstdlib>

int main( int argc, char* argv[] ) {
    if ( argc < 2 )
	return 1;

    // new: avoid splitting files that are bigger than 100 GB
    TTree::SetMaxTreeSize( 1000000000000LL ); // 1 TB
    
    std::string prefix = argv[3];

    //Get old file, old tree and set top branch address
    TFile *oldfile = new TFile(argv[1]);

    bool tau_1_match;
    
    //Create a new file + a clone of old tree in new file
    TFile *truefile = new TFile( (std::string("true_") + argv[1]).c_str(),"recreate");
    TFile *fakefile = new TFile( (std::string("fake_") + argv[1]).c_str(),"recreate");

    TIter next(oldfile->GetListOfKeys());
    TKey *key;
    std::set<std::string> trees;
    while ((key=(TKey*)next()))
    {
        if ( key->GetClassName() != std::string("TTree") )
            continue;
        if ( key->GetName() == std::string("skim_metadata") )
            continue;
        if ( key->GetName() == std::string("signal_metadata") )
            continue;
	
	trees.insert(key->GetTitle() );
    }
    
    for ( std::string name : trees )
    {
        TTree *origin = (TTree*)oldfile->Get( name.c_str() );
	origin->SetBranchAddress("tau_1_match",&tau_1_match);
	TTree *true_tree = origin->CloneTree(0);
	true_tree->SetDirectory( truefile );
	TTree *fake_tree = origin->CloneTree(0);
	fake_tree->SetDirectory( fakefile );

	Long64_t nentries = origin->GetEntries();
	
	for (Long64_t i=0;i<nentries; i++) {
	    origin->GetEntry(i);
	    
	    if ( i % 100000 == 0 )
		std::cout << i << " / " << nentries << std::endl; 
	    
	    if ( tau_1_match )
		true_tree->Fill();
	    else
		fake_tree->Fill();
	    
	}

	true_tree->Write();
	fake_tree->Write();

    }
    truefile->Write();
    fakefile->Write();
    
    delete truefile;
    delete fakefile;
    delete oldfile;
    
    return 0;
}
