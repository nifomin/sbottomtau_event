#ifndef SusyDiTauBaselineCutflow_H
#define SusyDiTauBaselineCutflow_H


#include "SusyTauAnalysisXAOD/CutflowBase.h"

class SusyDiTauBaselineCutflow : public CutflowBase {
public:
    SusyDiTauBaselineCutflow():
	CutflowBase( "SusyDiTauBaseline" )
	{
	  
	  RegisterCut( ">=2 tau2" );
	  RegisterCut( "Missing ET > 150 GeV" );
	  RegisterCut( "Leading jet pT > 130 GeV" );
	  RegisterCut( "Sub-leading jet pT > 30 GeV" );
	  RegisterCut( "Electron veto" );
	  RegisterCut( "Muon veto" );
	}

    virtual std::pair<bool, int> ExecuteCuts( ContainerHandler & handler, double weight )
	{
	    auto taus = handler.taus();
	    if ( taus.size() < 2 )
		return std::pair<bool,int>(false, 0);
	    FillCut( 0, weight );
	    
	    auto met = handler.met();
	    if ( (*met)["Final"]->met() < 150000. )
		return std::pair<bool,int>(false, 1);
	    FillCut( 1, weight );
	    
	    auto jets = handler.jets();
	    if ( jets.empty() || jets[0]->pt() < 130000. )
		return std::pair<bool,int>(false, 2);
	    FillCut( 2, weight );

	    if ( jets.size() < 2 || jets[1]->pt() < 30000. )
		return std::pair<bool,int>(false, 3);
	    FillCut( 3, weight );

	    if ( ! handler.electrons().empty() )
		return std::pair<bool,int>(false, 4);
	    FillCut( 4, weight );
	    
	    if ( ! handler.muons().empty() )
		return std::pair<bool,int>(false, 5);
	    FillCut( 5, weight );
	    
	    return std::pair<bool,int>( true, false );
	}
    
};

#endif
