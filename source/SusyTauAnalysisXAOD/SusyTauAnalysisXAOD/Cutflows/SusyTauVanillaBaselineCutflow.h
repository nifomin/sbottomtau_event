#ifndef SusyTauVanillaBaselineCutflow_H
#define SusyTauVanillaBaselineCutflow_H

#include "SusyTauAnalysisXAOD/CutflowBase.h"
#include <SusyTauAnalysisXAOD/NtupleVectorWriter.h>

class SusyTauVanillaBaselineCutflow : public CutflowBase {
public:
  // constructor that needs a cutflow name and a vector of cuts
 SusyTauVanillaBaselineCutflow( std::string cutflow_name, std::vector< std::string > cutflow ):
  CutflowBase( cutflow_name ),
    m_cutflow( cutflow )
    {
      // loop over all strings in the cutflow vector to register the cuts
      for( unsigned int cutIdx = 0; cutIdx < m_cutflow.size(); cutIdx++ ){
	RegisterCut( m_cutflow.at( cutIdx ) );
      } // end loop over all strings in the cutflow vector
    }

  virtual std::pair<bool, int> ExecuteCuts( ContainerHandler & handler, double weight )
  {
    // Initialisation of Containers from ContainerHandler to work with
    // ToDo: Add ExceptionHandling in case a Container is not present!
    // ToDo: Add ExceptionHandling for unrecognized cuts
    // ToDo: Add DEBUG/INFO for applied cuts
    auto taus = handler.taus();
    auto met = handler.met();
    auto jets = handler.jets();
    auto electrons = handler.electrons();
    auto muons = handler.muons();

    // Loop over all cuts, identifying every cut by string-mapping
    for( unsigned int cutIdx = 0; cutIdx < m_cutflow.size(); cutIdx++ ){
      std::string cut = m_cutflow.at(cutIdx);
      
      if (cut == "initial")
	{
	  //All initial events 
	}
      else if (cut == "GRL")
        {
	  const xAOD::EventInfo* ei = nullptr;
	  if (!SusyTauGlobals::s_event->retrieve( ei, "EventInfo" ).isSuccess() ){
	    Error("execute()", "Failed to retrieve event info collection." );
	    return std::pair<bool,int>(false,cutIdx);
	  }

	  if ( SusyTauGlobals::s_grl && ! SusyTauGlobals::s_grl->passRunLB( ei->runNumber(), ei->lumiBlock() ) )
	    return std::pair<bool,int>(false,cutIdx);
	}
      else if (cut == "SingleLepTrigger")
	{
	  bool TriggerPassed = false;
	  
	  TriggerPassed = SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu26_imedium") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu24_imedium") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_e24_medium_iloose") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_e24_tight_iloose");

	  if (!TriggerPassed)
	    return std::pair<bool,int>(false,cutIdx);
	  
	}
      else if (cut == "SingleLepTauTrigger")
	{
	  bool TriggerPassed = false;

	  TriggerPassed = SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu26_imedium") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu24_imedium") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_e24_medium_iloose") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_e24_tight_iloose") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu14_iloose_tau25_medium1_tracktwo") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_mu14_iloose_tau35_medium1_tracktwo") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_e17_medium_iloose_tau25_medium1_tracktwo");
	  
	  if (!TriggerPassed)
	    return std::pair<bool,int>(false,cutIdx);
	}

      else if (cut == "MetDiTauMetTrigger")
	
	{
	  bool TriggerPassed=false;
	  
	  // TriggerPassed = SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_xe100_mht") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_xe100") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_xe80") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo");

	  TriggerPassed =  SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo") || SusyTauGlobals::s_susyDef->IsTrigPassed("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM") ;
	  
	  if (!TriggerPassed)                                                                                                                                                         
            return std::pair<bool,int>(false,cutIdx); 
	  
	}

      else if (cut == "NoPVtxVeto")
	{
	    if (SusyTauGlobals::s_susyDef->GetPrimVtx() == nullptr)
	    return std::pair<bool,int>(false,cutIdx);
	}
      
      else if ( cut == "EventCleaning" )
        {
	    const xAOD::EventInfo* ei = nullptr;
	    if (!SusyTauGlobals::s_event->retrieve( ei, "EventInfo" ).isSuccess() ){
		Error("execute()", "Failed to retrieve event info collection." );
		return std::pair<bool,int>(false,cutIdx);
	    }
	    if ( ! ei->eventType( xAOD::EventInfo::IS_SIMULATION ) )
	    {
		if ( ei->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error )
		    return std::pair<bool,int>(false,cutIdx);
		if ( ei->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error )
		    return std::pair<bool,int>(false,cutIdx);
		if ( ei->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error )
		    return std::pair<bool,int>(false,cutIdx);
		if ( ei->eventFlags(xAOD::EventInfo::Core) & 0x40000 )
		    return std::pair<bool,int>(false,cutIdx);
	    }
	}
      
      else if( cut == "CosmicMuonVeto")
	{
	  for (auto mu : handler.muons()) 
	    {
	      if (mu->auxdataConst<char>("cosmic") == 1)
		return std::pair<bool,int>(false,cutIdx);
	    }
	}
      else if (cut == "BadMuonVeto")
	{
	    if ( handler.hasBadMuons() )
		return std::pair<bool,int>(false,cutIdx);
	}
      else if (cut == "BadJetVeto")
	{
	    if ( handler.hasBadJets() )
		return std::pair<bool,int>(false,cutIdx);
	}
      else if( cut == "exact1tau" ){
	if ( taus.size() != 1 )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if( cut == "min2tau" ){
	if ( taus.size() < 2 )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if (cut == "min1tau") {
	if (taus.size() < 1 )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min180met" ){
	if ( (*met)["Final"]->met() < 180000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min160met" ){
        if ( (*met)["Final"]->met() < 160000. )
          return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min150met" ){
	if ( (*met)["Final"]->met() < 150000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min100met" ){
	if ( (*met)["Final"]->met() < 100000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min100metORmetnomu" ){
	auto metNoMuon = handler.metNoMuon();
	if ( (*met)["Final"]->met() < 100000. && (*metNoMuon)["Final"]->met() < 100000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if (cut == "min5jets") {
	if (jets.size() < 5 )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if (cut == "min2jets") {
	if (jets.size() < 2 )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min1jet" ){
	if ( jets.size() < 1)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min130jet1" ){
	if ( jets.empty() || jets[0]->pt() < 130000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min120jet1" ){
	if ( jets.empty() || jets[0]->pt() < 120000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min140jet1" ){
        if ( jets.empty() || jets[0]->pt() < 140000. )
          return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min30jet2" ){
	if ( jets.size() < 2 || jets[1]->pt() < 30000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min2jet25" ){
	if ( jets.size() < 2 || jets[1]->pt() < 25000. )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min2jet100" ){
        if ( jets.size() < 2 || jets[1]->pt() < 100000. )
          return std::pair<bool,int>(false, cutIdx);
      }
      else if (cut == "minZpt200"){
        if(muons.size() < 2){return std::pair<bool,int>(false, cutIdx);}
	float Zpt = (muons[0]->p4() + muons[1]->p4()).Pt();
        if(Zpt < 200000.){return std::pair<bool,int>(false, cutIdx);}
      }
      else if (cut == "Zmass"){
        if(muons.size() < 2){return std::pair<bool,int>(false, cutIdx);}
	float Zmass = (muons[0]->p4() + muons[1]->p4()).M();
        if(Zmass < 81000. || Zmass > 101000.){return std::pair<bool,int>(false, cutIdx);}
      }
      else if (cut == "Zcuts"){
        float Zmass = 0;
        float Zpt = 0;
        if(muons.size() >= 2)
        {
        Zmass = (muons[0]->p4() + muons[1]->p4()).M();
        Zpt = (muons[0]->p4() + muons[1]->p4()).Pt();
        }

        if(((*met)["Final"]->met() < 160000.) && ((Zmass < 81000. || Zmass > 101000.) || Zpt < 200000.)){return std::pair<bool,int>(false, cutIdx);}
      }
      else if ( cut == "elVeto" ){
	if ( ! handler.electrons().empty() )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "muVeto" ){
	if ( ! handler.muons().empty() )
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if (cut == "1Lep"){
	if ( muons.size() + electrons.size() !=1)
	  return std::pair<bool,int>(false,cutIdx);
      }
      else if (cut == "min1Lep"){
	if ( muons.size() + electrons.size() == 0) 
	  return std::pair<bool,int>(false,cutIdx);
      }
      else if (cut == "min1mu"){
        if ( handler.signalMuons().empty() )
          return std::pair<bool,int>(false,cutIdx);
      }
      else if (cut == "min1tauORmu"){
	if ( handler.signalMuons().empty() && taus.empty() )
          return std::pair<bool,int>(false,cutIdx);
      }
      else if ( cut == "METSig" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double metsig = (*met)["Final"]->met()/1000/sqrt((*met)["Final"]->sumet()/1000);
	if (metsig>.7)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "pass_HLT_jX") {
	// means non-zero prescale weight
	const xAOD::EventInfo* eventInfo = nullptr;
	if (!SusyTauGlobals::s_event->retrieve( eventInfo, "EventInfo" ).isSuccess() ){
	  Error("execute()", "Failed to retrieve EventInfo." );
	  return std::pair<bool,int>(false,cutIdx);
	}
	if(eventInfo->auxdataConst<double>("HLT_jX_prescale")==0.)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "METMEFF" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double ht = 0.;
	// ht calculation
	for ( auto tau : taus )
	  ht += tau->pt();
	for ( auto jet : jets )
	  ht += jet->pt();
	double meff = ht + (*met)["Final"]->met();
	double metmeff = (*met)["Final"]->met() / meff;
	if (metmeff > 0.4)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "JetMETSeparation"){
	
	double dPhi1 = 0;
	double dPhi2 = 0;
	double met_phi = (*met)["Final"]->phi();
	
	if(jets.size() > 0)
	{
	  double jet_phi1 = jets[0]->phi();
	  dPhi1=std::fabs(jet_phi1-met_phi);
	  if (dPhi1>M_PI) dPhi1=2*M_PI-dPhi1;
	}
	if(jets.size() > 1)
	{
	  double jet_phi2 = jets[1]->phi();
	  dPhi2=std::fabs(jet_phi2-met_phi);
	  if (dPhi2>M_PI) dPhi2=2*M_PI-dPhi2;
	}
	if (dPhi1 < 0.4 || dPhi2 < 0.4)
	  return std::pair<bool,int>(false, cutIdx);
      }      else if ( cut == "JetMETAntiSeparation"){

	double dPhi1 = 999;
	double dPhi2 = 999;
	double met_phi = (*met)["Final"]->phi();
	
	if(jets.size() > 0)
	{
	  double jet_phi1 = jets[0]->phi();
	  dPhi1=std::fabs(jet_phi1-met_phi);
	  if (dPhi1>M_PI) dPhi1=2*M_PI-dPhi1;
	}
	if(jets.size() > 1)
	{
	  double jet_phi2 = jets[1]->phi();
	  dPhi2=std::fabs(jet_phi2-met_phi);
	  if (dPhi2>M_PI) dPhi2=2*M_PI-dPhi2;
	}
	if (dPhi1 > 0.4 && dPhi2 > 0.4)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min800Ht" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double ht = 0.;
	// ht calculation
	for ( auto tau : taus )
	  ht += tau->pt();
	for ( auto jet : jets )
	  ht += jet->pt();
	if (ht < 800000)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min1400Mt" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double sumMT = 0.;
	// sumMT calculation
	double delPhiMet;
	int tau_num=0;
	int jet_num=0;
	double met_v = (*met)["Final"]->met();
	double met_phi = (*met)["Final"]->phi();
	for ( auto tau : taus )
	  {
	    tau_num++;
	    delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(tau->phi()-met_phi)-TMath::Pi())-TMath::Pi());
	    if (tau_num<3){
	      sumMT+=TMath::Sqrt(2*tau->pt()*met_v*(1-TMath::Cos(delPhiMet)));
	    }
	  }
	for ( auto jet : jets )
	  {
	    jet_num++;
	    delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(jet->phi()-met_phi)-TMath::Pi())-TMath::Pi());
	    if (jet_num<3){
	      sumMT+=TMath::Sqrt(2*jet->pt()*met_v*(1-TMath::Cos(delPhiMet)));
	    }
	  }
	if (sumMT < 1400000)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min500Mt" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double sumMT = 0.;
	// sumMT calculation
	double delPhiMet;
	int tau_num=0;
	int jet_num=0;
	double met_v = (*met)["Final"]->met();
	double met_phi = (*met)["Final"]->phi();
	for ( auto tau : taus )
	  {
	    tau_num++;
	    delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(tau->phi()-met_phi)-TMath::Pi())-TMath::Pi());
	    if (tau_num<3){
	      sumMT+=TMath::Sqrt(2*tau->pt()*met_v*(1-TMath::Cos(delPhiMet)));
	    }
	  }
	for ( auto jet : jets )
	  {
	    jet_num++;
	    delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(jet->phi()-met_phi)-TMath::Pi())-TMath::Pi());
	    if (jet_num<3){
	      sumMT+=TMath::Sqrt(2*jet->pt()*met_v*(1-TMath::Cos(delPhiMet)));
	    }
	  }
	if (sumMT < 500000)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "min350sumMT" ){
	///This is ugly given we calculate the same in the EventParameters but I don't see a good way around it right now given we can not easily access the event info at this point
	double sumMT = 0.;
	// sumMT calculation
	double delPhiMet;
	int tau_num=0;
	double met_v = (*met)["Final"]->met();
	double met_phi = (*met)["Final"]->phi();
	for ( auto tau : taus )
	  {
	    tau_num++;
	    delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(tau->phi()-met_phi)-TMath::Pi())-TMath::Pi());
	    if (tau_num<3){
	      sumMT+=TMath::Sqrt(2*tau->pt()*met_v*(1-TMath::Cos(delPhiMet)));
	    }
	  }
	if (sumMT < 350000)
	  return std::pair<bool,int>(false, cutIdx);
      }
      else if ( cut == "StransverseMass60" ){	
	double chiA = 0.; // hypothesised mass of invisible on side A.  Must be >=0.
	double chiB = 0.; // hypothesised mass of invisible on side B.  Must be >=0.

	double mVisA = taus[0]->m();
	double pxA = taus[0]->pt()*TMath::Cos(taus[0]->phi());
	double pyA = taus[0]->pt()*TMath::Sin(taus[0]->phi());

	double mVisB = taus[1]->m();
	double pxB = taus[1]->pt()*TMath::Cos(taus[1]->phi());
	double pyB = taus[1]->pt()*TMath::Sin(taus[1]->phi());

	double pxMiss = (*met)["Final"]->met()*TMath::Cos((*met)["Final"]->phi());
	double pyMiss = (*met)["Final"]->met()*TMath::Sin((*met)["Final"]->phi());

	if (asymm_mt2_lester_bisect::get_mT2(mVisA, pxA, pyA, mVisB, pxB, pyB, pxMiss, pyMiss, chiA, chiB, 0.) < 60000)
	  return std::pair<bool,int>(false, cutIdx);
      }

      else{
	cout << "!!!!! CUT " << cutIdx << " NOT RECOGNIZED !!!!!" << endl;
      }
      
      FillCut( cutIdx, weight ).ignore();	    
    }	    	   	        
    return std::pair<bool,int>( true, false );
  } // END loop over all cuts

 protected:
  
  std::vector< std::string > m_cutflow;    
};

#endif
