#ifndef SusyOneTauBaselineCutflow_H
#define SusyOneTauBaselineCutflow_H


#include "SusyTauAnalysisXAOD/CutflowBase.h"

class SusyOneTauBaselineCutflow : public CutflowBase {
public:
    SusyOneTauBaselineCutflow():
	CutflowBase( "SusyOneTauBaseline" )
	{
	    RegisterCut( "EventCleaning" );
	    RegisterCut( "BadMuonVeto" );
	    RegisterCut( "BadJetVeto" );
	    RegisterCut( "GoodPV" );
	    RegisterCut( "CosmicMuon" );
	    RegisterCut( "OneTau" );
	    RegisterCut( "OneElectron" );
	    RegisterCut( "OneMuon" );
	    RegisterCut( "OneBjet" );
	    RegisterCut( "MET>100" );
	    RegisterCut( "OneLepton" );
	    RegisterCut( "TwoLepton" );
	    RegisterCut( "OneJet" );
	    RegisterCut( "OneBaselineTau" );
	    RegisterCut( "tau125_medium1_tracktwo");
	    RegisterCut( "mu14_tau25_medium1_tracktwo_xe50");
	    RegisterCut( "j80_xe80");
	    RegisterCut( "2J1T");
	}

    virtual std::pair<bool, int> ExecuteCuts( ContainerHandler & handler, double weight )
	{
	    const xAOD::EventInfo* ei = nullptr;
            if (!SusyTauGlobals::s_event->retrieve( ei, "EventInfo" ).isSuccess() ){
                Error("execute()", "Failed to retrieve event info collection." );
                return std::pair<bool,int>(false,0);
            }
            if ( ! ei->eventType( xAOD::EventInfo::IS_SIMULATION ) )
            {
                if ( ei->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error )
                    return std::pair<bool,int>(false,0);
                if ( ei->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error )
                    return std::pair<bool,int>(false,0);
                if ( ei->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error )
                    return std::pair<bool,int>(false,0);
                if ( ei->eventFlags(xAOD::EventInfo::Core) & 0x40000 )
                    return std::pair<bool,int>(false,0);
            }

	    FillCut( 0, weight );

	    if ( handler.hasBadMuons() )
		return std::pair<bool,int>(false,1);
	    FillCut( 1, weight );
	    
	    if ( handler.hasBadJets() )
		return std::pair<bool,int>(false,2);
	    FillCut( 2, weight );

	    if (SusyTauGlobals::s_susyDef->GetPrimVtx() == nullptr)
		return std::pair<bool,int>(false,3);
	    FillCut( 3, weight );

	    /*
	      Has this once been used for debugging?
	    
	    if ( ei->eventNumber() == 3050134461 || ei->eventNumber() == 3049944197 || ei->eventNumber() == 3052170853 )
	    {
		std::cerr <<  ei->eventNumber() << std::endl;
		for (auto mu : handler.muons()) 
		{
		    std::cerr << "pT     " << mu->pt() << std::endl;
		    std::cerr << "eta    " << mu->eta() << std::endl;
		    std::cerr << "phi    " << mu->phi() << std::endl;
		    std::cerr << "cosmics" << (bool)mu->auxdata<char>("cosmic") << std::endl;
		    std::cerr << "OR     " << (bool)mu->auxdata<char>("passOR") << std::endl;
		}
		std::cerr << "JET TIME!" << std::endl;
		for (auto jet : handler.jets())
		{
		    std::cerr << "pT     " << jet->pt() << std::endl;
		    std::cerr << "eta    " << jet->eta() << std::endl;
		    std::cerr << "phi    " << jet->phi() << std::endl;
		    std::cerr << "OR     " << (bool)jet->auxdata<char>("passOR") << std::endl;
		}
		std::cerr << "ELE TIME!" << std::endl;
		for (auto jet : handler.electrons())
		{
		    std::cerr << "pT     " << jet->pt() << std::endl;
		    std::cerr << "eta    " << jet->eta() << std::endl;
		    std::cerr << "phi    " << jet->phi() << std::endl;
		    std::cerr << "OR     " << (bool)jet->auxdata<char>("passOR") << std::endl;
		}
	    }
		    
	    */
	    
	    for (auto mu : handler.muons()) 
	    {
		if (mu->auxdata<char>("cosmic") == 1 && mu->auxdata<char>("passOR") == 1 )
		{
		    //std::cerr << ei->eventNumber() << std::endl;
		    return std::pair<bool,int>(false,4);
		}
		
	    }
	    FillCut( 4, weight );
	    
	    if ( handler.taus().size() == 1 )
		FillCut( 5, weight);
	    if ( handler.electrons().size() == 1 )
	    {
		FillCut( 6, weight);
	    }
	    if ( handler.muons().size() == 1 )
		FillCut( 7, weight);
	    
	    int bjet = 0;
	    for (auto jet : handler.jets())
            {
		if ( jet->auxdata< char >("bjet") )
		    bjet++;
            }
	    if ( bjet == 1 )
		FillCut( 8, weight);
	    
	    auto metCont = handler.met();

	    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
	    if (metcst_it != metCont->end())
		if ( (*metcst_it)->met() > 100000. )
		    FillCut( 9, weight);
	    
	    if ( handler.electrons().size() + handler.muons().size()  + handler.baselineTaus().size() == 1 )
	    {
	      /*
		const xAOD::EventInfo* ei = nullptr;
		if (SusyTauGlobals::s_event->retrieve( ei, "EventInfo" ).isSuccess() )
		    std::cerr << " Event number: " << ei->eventNumber() << std::endl;
		std::cerr << "Number of electrons: " << handler.electrons().size() << std::endl;

		// xAOD::ElectronContainer * _electrons;
		// SusyTauGlobals::s_event->retrieve( _electrons, "CalibratedElectrons" );
		// int i = 0;
		// for ( auto elec_itr = _electrons->begin(); elec_itr != _electrons->end(); ++elec_itr )
		// {
		//     std::cerr << "Electron index: " << i << std::endl;
		//     std::cerr << " - Is baseline: " << (int)(*elec_itr)->auxdata<char>("baseline") << std::endl;
		//     std::cerr << " - Is signal:   " << (int)(*elec_itr)->auxdata<char>("signal") << std::endl;
		//     std::cerr << " - Passes OR:   " << (int)(*elec_itr)->auxdata<char>("passOR") << std::endl;
		//     std::cerr << " - pT:          " << (*elec_itr)->pt() << std::endl;
		//     std::cerr << " - eta:         " << (*elec_itr)->eta() << std::endl;
		//     std::cerr << " - phi:         " << (*elec_itr)->phi() << std::endl;
		//     i++;
		// }
		xAOD::TauJetContainer * _taus;
		SusyTauGlobals::s_event->retrieve( _taus, "CalibratedTaus" );
		int i = 0;
		for ( auto taus_itr = _taus->begin(); taus_itr != _taus->end(); ++taus_itr )
		{
		    std::cerr << "Tau index: " << i << std::endl;
		    std::cerr << " - Is baseline: " << (int)(*taus_itr)->auxdata<char>("baseline") << std::endl;
		    std::cerr << " - Is signal:   " << (int)(*taus_itr)->auxdata<char>("signal") << std::endl;
		    std::cerr << " - Passes OR:   " << (int)(*taus_itr)->auxdata<char>("passOR") << std::endl;
		    std::cerr << " - Weight:      " << (*taus_itr)->auxdata<double>("effscalefact") << std::endl;
		    std::cerr << " - Weight/A:    " << (*taus_itr)->auxdata<float>("TauScaleFactorReconstructionHadTau") << std::endl;
		    std::cerr << " - Weight/B:    " << (*taus_itr)->auxdata<float>("TauScaleFactorEleOLRHadTau") << std::endl;
		    std::cerr << " - Weight/C:    " << (*taus_itr)->auxdata<float>("TauScaleFactorJetIDHadTau") << std::endl;
		    std::cerr << " - Weight (2):  " << (*taus_itr)->auxdata<double>("sf") << std::endl;
		    std::cerr << " - pT:          " << (*taus_itr)->pt() << std::endl;
		    std::cerr << " - eta:         " << (*taus_itr)->eta() << std::endl;
		    std::cerr << " - phi:         " << (*taus_itr)->phi() << std::endl;
		    i++;
		}
	      */
		FillCut( 10, weight);
	    }
	    if ( handler.electrons().size() + handler.muons().size() + handler.baselineTaus().size() == 2 )
		FillCut( 11, weight);
	    
	    if ( handler.jets().size() == 1 )
		FillCut( 12, weight);
	    
	    if ( handler.baselineTaus().size() == 1 )
		FillCut( 13, weight);
	    
	    if ( SusyTauGlobals::s_susyDef->IsTrigPassed( "HLT_tau125_medium1_tracktwo" ) )
		FillCut( 14, weight);

	    if ( SusyTauGlobals::s_susyDef->IsTrigPassed( "HLT_mu14_tau25_medium1_tracktwo_xe50" ) )
		FillCut( 15, weight);

	    if ( SusyTauGlobals::s_susyDef->IsTrigPassed( "HLT_j80_xe80" ) )
		FillCut( 16, weight);
	    
	    if ( handler.baselineJets().size() > 2 && handler.baselineTaus().size() == 1 )
		FillCut( 17, weight);
	    
	    
	    return std::pair<bool,int>( true, false );
	}
    
};

#endif
