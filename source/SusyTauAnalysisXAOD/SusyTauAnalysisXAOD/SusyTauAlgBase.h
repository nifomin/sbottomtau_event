#ifndef SusyTauAnalysisXAOD_SusyTauAlgBase_H
#define SusyTauAnalysisXAOD_SusyTauAlgBase_H

#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusyTauSystVariation.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "SusyTauAnalysisXAOD/CutflowBase.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"
#include "CPAnalysisExamples/errorcheck.h"

class SusyTauAlgBase : public EL::Algorithm
{
public:
    // this is a standard constructor
    SusyTauAlgBase();
    SusyTauAlgBase ( std::string cutflow_name );
    SusyTauAlgBase ( std::string cutflow_name, std::vector<std::string> cutflow_config );
    
    SusyTauAlgBase * setCutflowByName( std::string cutflow_name )
	{
	    m_cutflow_name = cutflow_name;
	    return this;
	}
    
    SusyTauAlgBase * setCutflowByConfig( std::string cutflow_name, std::vector<std::string> cutflow_config )
	{
	    m_cutflow_name = cutflow_name;
	    m_cutflow_vector = cutflow_config;

	    return this;
	}
    
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);

    virtual EL::StatusCode histInitialize ();

    virtual EL::StatusCode initialize ();

    virtual EL::StatusCode histFinalize ();


    // this is needed to distribute the algorithm to the workers
    ClassDef(SusyTauAlgBase, 1);

protected:
    std::string m_cutflow_name;
    std::vector<std::string> m_cutflow_vector;

    xAOD::TEvent *m_event;  //!
    xAOD::TStore *m_store;  //!
    CutflowBase  *m_cutflow; //!
    
    // Systematic configuration
    SusyTauConfig::SystematicSettings m_systSettings;
    std::vector<SusyTauSystVariation> m_systematics; //!

    bool checkTrigger( std::string trig_name )
	{
	    return SusyTauGlobals::s_susyDef->IsTrigPassed( trig_name );
	}
	
    float GetTriggerPrescale( std::string trig_name )
	{
	  return SusyTauGlobals::s_susyDef->GetTrigPrescale( trig_name );
	}
	
    // function to set a cutflow to a given cutflow object
    EL::StatusCode setCutflow( CutflowBase *cutflow );
    
    // out-dated function to look up cutflow files by file name given as strings
    CutflowBase *lookupCutflow( std::string cutflow );
    // function to create a cut-flow object based on a provided name and the cutflow vector
    CutflowBase *createCutflow( std::string cutflowName, std::vector< std::string > cutflow );
};

#endif
