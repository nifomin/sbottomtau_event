#ifndef SusyTauAnalysisXAOD_CONTAINERHANDLER_H
#define SusyTauAnalysisXAOD_CONTAINERHANDLER_H

#include <string>
#include <sstream>
#include <iostream>
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"

class ContainerHandler
{
public:

    // normal running condition
    EL::StatusCode RetrieveContainers( CP::SystematicSet syst );
    // jet and tau smearing
    EL::StatusCode RetrieveSmearContainers( CP::SystematicSet syst, int smearIteration , bool smearTau=false ); 
    // method to handle both situations
    EL::StatusCode GetContainers( CP::SystematicSet syst, int smearIteration , bool smearTau=false );

  
    // Methods that return the final collection with selection applied
    virtual xAOD::JetContainer jets();
    virtual xAOD::JetContainer baselineJets();
    virtual xAOD::JetContainer allJets();
 
    virtual xAOD::TauJetContainer taus();
    virtual xAOD::TauJetContainer baselineTaus();
    
    virtual xAOD::MuonContainer muons();
    virtual xAOD::MuonContainer signalMuons();
    
    virtual xAOD::ElectronContainer electrons();
    virtual xAOD::ElectronContainer signalElectrons();

    virtual xAOD::PhotonContainer photons();
    
    virtual xAOD::MissingETContainer * met();
    virtual xAOD::MissingETContainer * metNoMuon(); 

    virtual xAOD::JetContainer jets_truth ();
    virtual xAOD::TruthParticleContainer taus_truth();
    virtual xAOD::TruthParticleContainer electrons_truth();
    virtual xAOD::TruthParticleContainer muons_truth();
    virtual const xAOD::MissingETContainer * met_truth();
    
    inline bool hasBadJets()
    {
      for ( auto jet_itr = _jets->begin(); jet_itr != _jets->end(); ++jet_itr )
	if( (*jet_itr)->auxdataConst<char>("bad")==1 
	    && (*jet_itr)->auxdataConst<char>("passOR")==1 )
	  return true;
      
      return false;
    }
    
    inline bool hasBadMuons()
    {
      for ( auto muon_itr = _muons->begin(); muon_itr != _muons->end(); ++muon_itr )
	if( (*muon_itr)->auxdataConst<char>("baseline") == 1
	    && (*muon_itr)->auxdataConst<char>("bad") == 1 )
	  return true;
      
      return false;
    }
    
    inline double getTauSF()
    {
      double SF = 1.0;
      for ( auto tau : taus() )
	SF *= tau->auxdataConst<double>("effscalefact");
      return SF;
    }
    
    inline double getBTagSF()
    {
      double SF = 1.0;
      for ( auto obj : jets() )
	SF *= obj->auxdataConst<double>("effscalefact");
      return SF;
    }

    
private:    
    xAOD::JetContainer      * _jets;
    xAOD::TauJetContainer   * _taus;
    xAOD::MuonContainer     * _muons;
    xAOD::ElectronContainer * _electrons;
    xAOD::PhotonContainer   * _photons;
    xAOD::MissingETContainer * _met;
    xAOD::MissingETContainer * _metNoMuon;

    const xAOD::JetContainer      *_jets_truth;
    const xAOD::TruthParticleContainer   *_taus_truth;
    const xAOD::TruthParticleContainer     *_muons_truth;
    const xAOD::TruthParticleContainer *_electrons_truth;
    const xAOD::MissingETContainer *_met_truth;

    CP::SystematicSet m_systSet;
};

#endif
