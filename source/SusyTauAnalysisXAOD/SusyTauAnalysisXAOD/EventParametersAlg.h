#ifndef SusyTauAnalysisXAOD_EventParametersAlg_H
#define SusyTauAnalysisXAOD_EventParametersAlg_H

#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SusyTauAnalysisXAOD/ContainerHandler.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include <map>

class EventParametersAlg : public SusyTauAlgBase
{
public:
  EventParametersAlg ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(EventParametersAlg, 1);

    static std::string DecorationName( std::string dec, CP::SystematicSet set )
	{
	    auto it = s_parameters_syst[dec].find( set );
	    if ( it == s_parameters_syst[dec].end() )
		return dec;
	    
	    return it->second;
	}
    
private:
    /// Common code to handle systematic variations in the calculated observables
    template <typename calculator_t>
    void SystAwareCalculator( calculator_t calc, std::string observable, SusyTauSystVariation & syst )
	{
	    if ( ! syst.isNominal() )
	    {
		auto it = s_parameters_syst[observable].find( syst.m_systset );
		// Check if this systematic is interesting for the observable
		if ( it == s_parameters_syst[observable].end() )
		    return;
		else
		    observable = (*it).second;
	    }
	    
	    const xAOD::EventInfo* eventInfo = nullptr;
	    
	    if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
		Error("execute()", "Failed to retrieve event info collection. Exiting." );
		return;
	    }
	    if ( eventInfo->isAvailable<double>( observable ) && ! eventInfo->isAvailableWritable<double>( observable ) )
		return;
	    
	    eventInfo->auxdecor< double >( observable ) = (this->*calc)();
	}
    
    double CalculateMetSigSoftTrk();
    double CalculateMetSig();
    double CalculateMetSigPow3();
    double CalculateMetSigSoftSub();
    double CalculateMetOverPtMean();
    double CalculateMetOverPtMean12();
    int    CalculateNbjet();
    double CalculateMetMeff();
    double CalculateHT();
    double CalculateMeff();
    double CalculateMT2();
    double CalculateMCT();
    double CalculateMT_tau1met();
    double CalculateMT_tau2met();
    double CalculateMT_tau1tau2();
    double CalculateDelPhi_jet1met();
    double CalculateDelPhi_jet2met();
    double CalculateDelPhi_tau1met();
    double CalculatePrescaleWeight(int RunNumber);
      
    double deltaPhi(double phi1, double phi2);
    
    ContainerHandler m_handler; //!
    
    static std::map< std::string, std::map< CP::SystematicSet, std::string > > s_parameters_syst; //!
    
    bool m_isMC;
    std::vector<std::string> m_HLT_jX;

};

#endif
