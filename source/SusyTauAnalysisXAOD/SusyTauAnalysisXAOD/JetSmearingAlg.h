#ifndef SusyTauAnalysisXAOD_JetSmearingAlg_H
#define SusyTauAnalysisXAOD_JetSmearingAlg_H

#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "JetSmearing/JetMCSmearingTool.h"
#include "SusyTauAnalysisXAOD/TauSmearingTool.h"

class JetSmearingAlg : public SusyTauAlgBase
{
public:
  // standard constructors
  using SusyTauAlgBase::SusyTauAlgBase;

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode finalize();
  virtual EL::StatusCode histFinalize();
  virtual EL::StatusCode execute ();

  ClassDef(JetSmearingAlg, 1);
    
private:

    JetSmearing::JetMCSmearingTool* m_JetSmearingTool;  //!
    unsigned int m_numSmear;
    bool m_doTauSmearing;

    TauSmearing::TauSmearingTool* m_TauSmearingTool;  //!   

  // this is needed to distribute the algorithm to the workers
};

#endif
