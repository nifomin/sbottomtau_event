// Dear emacs, this is -*- c++ -*-
// $Id$
#ifndef XAODMETADATACNV_FILEMETADATATOOL_H
#define XAODMETADATACNV_FILEMETADATATOOL_H

// System include(s):
#include <string>
#include <memory>

// Infrastructure include(s):
#include "AsgTools/AsgMetadataTool.h"
#ifdef ASGTOOL_ATHENA
#   include "AthenaPoolKernel/IMetaDataTool.h"
#endif // ASGTOOL_ATHENA

// EDM include(s):
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeperAuxContainer.h"

namespace xAODMaker {

    /// Tool taking care of propagating xAOD::FileMetaData information
    ///
    /// This dual-use tool can be used both in Athena and in AnalysisBase
    /// to propagate the generic file-level metadata from the processed
    /// input files to an output file.
    ///
    /// It relies on the input already containing the information in an
    /// xAOD format.
    ///
    /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
    ///
    /// $Revision$
    /// $Date$
    ///
    class FileMetaDataTool : public asg::AsgMetadataTool
#ifdef ASGTOOL_ATHENA
			   , public virtual ::IMetaDataTool
#endif // ASGTOOL_ATHENA
    {

	/// Declare the correct constructor for Athena
	ASG_TOOL_CLASS0( FileMetaDataTool )

	public:
	/// Regular AsgTool constructor
	FileMetaDataTool( const std::string& name = "FileMetaDataTool" );

	/// Function initialising the tool
	virtual StatusCode initialize();

    protected:
	/// @name Functions called by the AsgMetadataTool base class
	/// @{

	/// Function collecting the metadata from a new input file
	virtual StatusCode beginInputFile();

	/// Function writing the collected metadata to the output
	virtual StatusCode metaDataStop();

	/// @}

    private:
	/// Key of the metadata object in the input file
	std::string m_inputKey;
	/// Key of the metadata object for the output file
	std::string m_outputKey;

	/// The output interface object
	std::unique_ptr< xAOD::CutBookkeeperContainer > m_md;
	/// The output auxiliary object
	std::unique_ptr< xAOD::CutBookkeeperAuxContainer > m_mdAux;

    }; // class FileMetaDataTool

} // namespace xAODMaker

#endif // XAODMETADATACNV_FILEMETADATATOOL_H
