#ifndef SusyTauAnalysisXAOD_SkimWriter_H
#define SusyTauAnalysisXAOD_SkimWriter_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include <TTree.h>

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "SusyTauAnalysisXAOD/Cutflows/SusyOneTauBaselineCutflow.h"

#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"

#include "SusyTauAnalysisXAOD/FileMetaDataTool.h"
#include "xAODTriggerCnv/TriggerMenuMetaDataTool.h"
class SkimWriter : public SusyTauAlgBase
{
public:
  SkimWriter (  );
  SkimWriter ( std::string skim_name, 
	       std::string cutflow_name, 
	       std::vector<std::string> cutflow_config );

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SkimWriter, 1);

private:
    std::string outputName;
    std::vector<std::string> containerNames;
    std::vector<std::string> containerMcNames;

 
    TTree * m_metaData; //!
    
    int sampleID; //!
    unsigned int EventSum; //!
    double EventWeightSum; //!
    double EventWeightSqSum; //!
   
    xAODMaker::FileMetaDataTool * m_metatool; //!
    xAODMaker::TriggerMenuMetaDataTool * m_trig_metatool; //!
    xAOD::TEvent *m_event;  //!
    xAOD::TStore *m_store;  //!
};

#endif
