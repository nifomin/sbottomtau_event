#ifndef SusyTauAnalysisXAOD_Calculators_H
#define SusyTauAnalysisXAOD_Calculators_H

#include "SusyTauAnalysisXAOD/ContainerHandler.h"
// #include "SusyTauAnalysisXAOD/Calculators/mct.h"
// #include "SusyTauAnalysisXAOD/Calculators/mt2.h"

namespace Calculators {
  
  // "inline" keyword added to cure the re-definition compiler error 
  // when including EventParametersAlg.h together with this header file  
  

  inline double CalculateHT(ContainerHandler *m_handler)
  {
    double ht = 0.;

    const xAOD::TauJetContainer tauCont = m_handler->taus();
    for ( auto tau : tauCont )
      ht += tau->pt();
    
    const xAOD::JetContainer jetCont = m_handler->jets();
    for ( auto jet : jetCont )
      ht += jet->pt();
    
    return ht;
  }
  
  
  inline double CalculateMeff(ContainerHandler *m_handler)
  {
    double meff = CalculateHT(m_handler);
    
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    meff += (*metcst_it)->met();
    return meff;
  }
  
  
  inline double CalculateMetSig(ContainerHandler *m_handler)
  {
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met()/1000;	//Convert to GeV
    double sumet = (*metcst_it)->sumet()/1000;	//Convert to GeV
    
    return met/std::sqrt(sumet);			//In sqrt(GeV)
  }
  

  inline double CalculateMetSigPow3(ContainerHandler *m_handler)
  {
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met()/1000;	//Convert to GeV
    double sumet = (*metcst_it)->sumet()/1000;	//Convert to GeV
    
    return met/std::pow(sumet,1./3.);			
  }


  inline double CalculateMetSigSoftSub(ContainerHandler *m_handler)
  {
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

    double met = (*metcst_it)->met()/1000;      //Convert to GeV
    double sumet = (*metcst_it)->sumet()/1000;  //Convert to GeV

    // subtract magical 8 GeV contribution from the soft term
    return (met - 8.)/std::sqrt(sumet);                        //In sqrt(GeV)
  }


  inline double CalculateMetOverPtMean(ContainerHandler *m_handler)
  {  
    double ptmean = 0.;
    
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    for ( auto tau : tauCont )
      ptmean += tau->pt();
    
    const xAOD::JetContainer jetCont = m_handler->jets();
    for ( auto jet : jetCont )
      ptmean += jet->pt();
    
    if(ptmean>0.)
      ptmean /= (tauCont.size() + jetCont.size());
    
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met();
    
    return ptmean>0. ? met / ptmean : 0.; 
  }


  inline double CalculateMetOverPtMean12(ContainerHandler *m_handler)
  {  
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    const xAOD::JetContainer jetCont = m_handler->jets();
    
    if(tauCont.size() + jetCont.size() < 2) return -1.; 
    
    double pt1=0., pt2=0.;
    
    for ( auto jet : jetCont ) {
      if(jet->pt()>pt1) {
	pt2 = pt1;
	pt1 = jet->pt(); 
      }
      else if(jet->pt()>pt2) {
	pt2 = jet->pt(); 
      }
    }
    
    for ( auto tau : tauCont ) {
      if(tau->pt()>pt1) {
	pt2 = pt1;
	pt1 = tau->pt(); 
      }
      else if(tau->pt()>pt2) {
	pt2 = tau->pt(); 
      }
    }
    
    double ptmean = (pt1 + pt2)/2.;
    
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met();
    
    return ptmean>0. ? met / ptmean : 0.; 
  }




  inline double CalculateMetMeff(ContainerHandler *m_handler)
  {
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met();
    double meff = CalculateMeff(m_handler);
    
    return met/meff;
  }
  
  
  inline double deltaPhi(double phi1, double phi2) 
  {
    double dPhi=std::fabs(phi1-phi2);
    if (dPhi>M_PI) dPhi=2*M_PI-dPhi;
    return dPhi;
    
  }

    
  inline double CalculateMT_tau1met(ContainerHandler *m_handler)
  {
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    if( tauCont.size() == 0 ) return -1;
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double tau_pt = tauCont[0]->pt();
    double tau_phi  = tauCont[0]->phi();
    
    double met = (*metcst_it)->met();
    double met_phi = (*metcst_it)->phi();
    
    return std::sqrt(2*tau_pt*met*(1 - std::cos(deltaPhi(tau_phi, met_phi))));
  }
  
  
  inline double CalculateMT_tau2met(ContainerHandler *m_handler)
  {
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    if( tauCont.size() < 2 ) return -1;
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double tau_pt = tauCont[1]->pt();
    double tau_phi  = tauCont[1]->phi();
    
    double met = (*metcst_it)->met();
    double met_phi = (*metcst_it)->phi();
    
    return std::sqrt(2*tau_pt*met*(1 - std::cos(deltaPhi(tau_phi, met_phi))));
  }
  
  
  inline double CalculateMT_tau1tau2(ContainerHandler *m_handler)
  {
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    if (tauCont.size() < 2) return -1;
    
    TLorentzVector tau1;
    tau1.SetPtEtaPhiM(tauCont[0]->pt(), tauCont[0]->eta(), tauCont[0]->phi(), tauCont[0]->pt()/std::cosh(tauCont[0]->eta()));
    
    TLorentzVector tau2;
    tau2.SetPtEtaPhiM(tauCont[1]->pt(), tauCont[1]->eta(), tauCont[1]->phi(), tauCont[1]->pt()/std::cosh(tauCont[1]->eta()));
    
    TLorentzVector MT_tautau;
    MT_tautau = tau1+tau2;
    
    return MT_tautau.M();
  }
  
  
  inline double CalculateDelPhi_jet1met(ContainerHandler *m_handler)
  {
    const xAOD::JetContainer jetCont = m_handler->jets();
    if(jetCont.size() < 1) return -1;
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double jet_phi = jetCont[0]->phi();
    double met_phi = (*metcst_it)->phi();
    
    return deltaPhi(jet_phi, met_phi);
  }
  
  
  inline double CalculateDelPhi_jet2met(ContainerHandler *m_handler)
  {
    const xAOD::JetContainer jetCont = m_handler->jets();
    if(jetCont.size() < 2) return -1;
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double jet_phi = jetCont[1]->phi();
    double met_phi = (*metcst_it)->phi();
    
    return deltaPhi(jet_phi, met_phi);
  }
  
  
  inline double CalculateDelPhi_tau1met(ContainerHandler *m_handler)
  {
    const xAOD::TauJetContainer tauCont = m_handler->taus();
    if( tauCont.size() == 0 ) return -1;
    auto metCont = m_handler->met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double tau_phi  = tauCont[0]->phi();
    double met_phi = (*metcst_it)->phi();
    
    return deltaPhi(tau_phi, met_phi);
  }

// inline double CalculateMT2(ContainerHandler *m_handler)
// {
//
//   const xAOD::TauJetContainer tauCont = m_handler->taus();
//   auto metCont = m_handler->met();
//   xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
//   return ComputeMaxMT2( tauCont, *metcst_it, 0, 0 );
//
// }

// inline double CalculateMCT(ContainerHandler *m_handler)
// {
//   return ComputeMaxMCT( m_handler->taus() );
// }

}
#endif
