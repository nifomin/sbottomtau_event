#ifndef SusyTauAnalysisXAOD_NtupleVectorWriter_H
#define SusyTauAnalysisXAOD_NtupleVectorWriter_H

#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "TTree.h"
#include <map>
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SUSYTools/SUSYCrossSection.h"
#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"
#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SusyTauAnalysisXAOD/EventParametersAlg.h"
#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"
#include "SusyTauAnalysisXAOD/lester_mt2_bisect.h"
#include "SusyTauAnalysisXAOD/Cutflows/SusyTauVanillaBaselineCutflow.h"
#include "PMGTools/PMGTruthWeightTool.h"
#include "AsgTools/ToolHandle.h"


class NtupleVectorWriter : public SusyTauAlgBase
{
public:
  NtupleVectorWriter ():
  SusyTauAlgBase(),
    outputName( "miniVectorNtuple" )
      {
      }
  
  NtupleVectorWriter ( std::string cutflow ):
  SusyTauAlgBase( cutflow ),
    outputName( "miniVectorNtuple" )
      {
      }
  
  NtupleVectorWriter ( std::string cutflow, std::vector<std::string> cutflow_config):
  SusyTauAlgBase( cutflow, cutflow_config ),
    outputName( "miniVectorNtuple_" + cutflow)
    {
      m_deactivateSyst = false;
    }

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode histFinalize ();


    // use a struct to store all Ntuple contents
    struct TreeLeaves{

      uint RunNumber;
      unsigned long long EventNumber;
      uint LumiBlock;
      int sampleID;     
      int processID;
      bool isDebugStream;
      int treatAsYear;
      float mcEventWeight;
      std::vector<float> mcEventWeights;
      float TauSpinnerWeight;
      float pileupweight;
      float pileupweightUp;
      float pileupweightDown;
      float tau_weight;
      float tau_medium_weight;
      float ele_weight;
      float mu_weight;
      float jvt_weight;
      float bjet_weight;
      float actualIntXing;
      float avgIntXing;
      int nvtx;
      float PVz;
   

      bool HLT_xe35;
      bool HLT_xe60_mht;
      bool HLT_xe70_mht;
      bool HLT_xe80_mht_L1XE50;
      bool HLT_xe90_mht_L1XE50;
      bool HLT_xe100_mht_L1XE50;
      bool HLT_xe110_mht_L1XE50;
      bool HLT_xe110_pufit_L1XE50;
      bool HLT_xe110_pufit_L1XE55;
      bool HLT_xe110_pufit_xe70_L1XE50;
      bool HLT_xe110_pufit_xe65_L1XE50;
      bool IsMETTrigPassed;
      bool IsMETTrigPassed_noL1J400;
      bool HLT_noalg_L1J400;
      float HLT_jX_prescale;

      bool HLT_mu24_ivarmedium;
      bool HLT_mu26_ivarmedium;
      bool HLT_mu26_imedium;
      bool HLT_mu24_ivarloose;
      bool HLT_mu50; 

      bool L1_XE45;
      bool L1_XE50;
      bool L1_XE55;
      bool L1_XE60;
      bool L1_2J15_XE55;
      bool L1_2J50_XE40;
      bool L1_TAU12;
      bool L1_TAU40_2TAU12IM_XE40;
      bool L1_TAU20IM_2TAU12IM_J25_2J20_3J12;
      bool L1_TAU20IM_2J20_XE45;
      bool L1_TAU20IM_2TAU12IM;

      bool HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;
      bool HLT_tau35_medium1_tracktwo_xe70_L1XE45;
      bool HLT_tau35_medium1_tracktwoEF_xe70_L1XE45;
      bool HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;

      bool HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;
      bool HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
      bool HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50; 
      bool HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50; 
      bool HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50; 

      bool HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo;
      bool HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
      bool HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;

      bool HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;
      bool HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
      bool HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
      bool HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
      bool HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
      bool HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
      bool HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
      bool HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;

      bool passDitauMetGRL;
      bool passBjetGRL;
      bool passBjetGRLTight;
      float Bjet_PileupWeight;
      float Bjet_PileupWeight_up;
      float Bjet_PileupWeight_down;
      bool HLT_j80_bmv2c2060_split_xe60;
      bool HLT_j60_gsc100_bmv2c1050_split_xe80_mht;
      bool HLT_2j25_gsc45_bmv2c1070_split_xe80_mht;
      bool HLT_j80_bmv2c1050_split_xe60;

      float muonTriggerSF_HLT_mu24_ivarloose;
      float muonTriggerSF_HLT_mu24_ivarmedium;
      float muonTriggerSF_HLT_mu26_ivarmedium;
      float muonTriggerSF_HLT_mu26_imedium;
      float muonTriggerSF_HLT_mu50;
      float muonTriggerSF_HLT_mu24_ivarloose_OR_HLT_mu50;
      float muonTriggerSF_HLT_mu24_ivarmedium_OR_HLT_mu50;
      float muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50;
      float muonTriggerSF_HLT_mu26_imedium_OR_HLT_mu50;
 
      bool LeptonVeto;
      bool JetTileVeto;
      bool TauTileVeto;
      bool JetTileVetoLBA52;
      bool TauTileVetoLBA52;
      bool badLBTileDQ;
      bool isBadBatman; 
      bool isBadTile;

      //Tau Related leaves
      int tau_n;
      int tau_nTruthMatched;
      std::vector<float> tau_pt;
      std::vector<float> tau_eta;
      std::vector<float> tau_phi;
      std::vector<float> tau_m;
      std::vector<float> tau_charge;
      std::vector<int> tau_nTracks;
      std::vector<float> tau_BDTJetScoreSigTrans;
      std::vector<char> tau_JetBDTLoose;
      std::vector<char> tau_JetBDTMedium;
      std::vector<char> tau_JetBDTTight;
      std::vector<float> tau_RNNJetScoreSigTrans;
      std::vector<char> tau_JetRNNLoose;
      std::vector<char> tau_JetRNNMedium;
      std::vector<char> tau_JetRNNTight;
      std::vector<float> tau_BDTEleScoreSigTrans;
      std::vector<unsigned int> tau_TileStatus;  
      std::vector<unsigned int> tau_LBA52status;
      std::vector<float>  tau_width;
      //std::vector<float>  tau_n90;
      std::vector<float> tau_jetpt; 
      std::vector<int>  tau_PantauDecayMode;
      std::vector<char> tau_isTruthMatchedTau;
      std::vector<char> tau_truthMatchedType;
      std::vector<unsigned int> tau_truthOrigin;
      std::vector<unsigned int> tau_truthType;
      std::vector<int>  tau_jetHadronTruthID;
      std::vector<int>  tau_jetPartonTruthID;
      
      // trigger matching
      std::map< std::string, vector<char> > tau_match_L1;
      std::map< std::string, vector<char> > tau_match_HLT;

      std::vector<float> tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70;

      std::vector<float> tau_effSF;
      //std::map< CP::SystematicSet, std::vector<float> > tau_effSF_syst;
      std::map< CP::SystematicSet, float > tau_effSF_total_syst;
      std::vector<float> tau_medium_effSF;
      //std::map< CP::SystematicSet, std::vector<float> > tau_medium_effSF_syst;
      std::map< CP::SystematicSet, float > tau_medium_effSF_total_syst;
	
      std::vector<float> tau_leadTrackPt;
      std::vector<float> tau_leadTrackEta;
      std::vector<float> tau_delPhiMet;
      std::vector<float> tau_mtMet;
      std::vector<size_t>   tau_nPi0;
      std::vector<size_t>   tau_nCharged;
      std::vector<float> tau_resolution;

      float sumMT;
      float sumMTJet;
      float sumMTTauJet;
      float Mt2;
      float Mt2_taumu;
      float Mt2_tauel;
      float Mtaumu;
      float Mtauel;

      //Jets
      int jet_n;
      int jet_n_btag;
      std::vector<float> jet_pt;
      std::vector<float> jet_eta;
      std::vector<float> jet_phi;
      std::vector<float> jet_m;
      std::vector<float> jet_mtMet;
      std::vector<float> jet_mv2c10;
      std::vector<float> jet_mv2c20;   
      std::vector<char>   jet_isBjet;
      std::vector<float>  jet_BjetSF;
      std::vector<float>  jet_JVTSF;
      //std::map< CP::SystematicSet, std::vector<float> > jet_effSF_syst;
      std::map< CP::SystematicSet, float > jet_effSF_total_syst;
      std::vector<int>    jet_hadronTruthID;
      std::vector<int>    jet_partonTruthID;
      std::vector<float>  jet_width;
      //std::vector<float>  jet_n90;
      std::vector<char>   jet_isBadTight;
      std::vector<float>  jet_jvt; 
      std::vector<float>  jet_delPhiMet;
      std::vector<unsigned int>  jet_TileStatus;
      std::vector<unsigned int>  jet_LBA52status;   
      std::vector<char>   jet_eleMatch;
      std::vector<char>   jet_muMatch;
      std::vector<char>   jet_isBjetMatched;
      std::vector<float>  jet_onlineBtag;

      std::vector<float> jet_resolution;
      std::vector<float> jet_HLT_resolution;

      //Electron
      int ele_n;
      std::vector<float> ele_pt;
      std::vector<float> ele_eta;
      std::vector<float> ele_phi;
      std::vector<float> ele_m;
      std::vector<float> ele_mtMet;
      std::vector<int>  ele_charge;
      std::vector<char> ele_isSignal;
      std::vector<float> ele_effSF;
      //std::map< CP::SystematicSet, std::vector<float> > ele_effSF_syst;
      std::map< CP::SystematicSet, float > ele_effSF_total_syst;
      std::vector<float> ele_resolution;

      //Muon
      int mu_n;
      std::vector<float> mu_pt;
      std::vector<float> mu_eta;
      std::vector<float> mu_phi;
      std::vector<float> mu_m;
      std::vector<float> mu_mtMet;
      std::vector<int>    mu_charge;
      std::vector<int>    mu_author;
      std::vector<int>    mu_muonType;
      std::vector<char>    mu_isSignalExp;
      std::vector<char>    mu_isBad;
      std::vector<float>   mu_effSF;
      //std::map< CP::SystematicSet, std::vector<float> > mu_effSF_syst;
      std::map< CP::SystematicSet, float > mu_effSF_total_syst;
      //std::vector<int>    mu_Quality;
      std::vector<float> mu_resolution;
      
      float met;
      float met_phi;
      float metNoMuon;
      float metNoMuon_phi;

      float METSigSoftTrk;
      float METSig;
      float METSigPow3;
      float METSigSoftSub;
      float METoverPtMean;
      float METoverPtMean12;

      // jet smearing
      float METSigSoftTrk_seed;
      float METSig_seed;
      float METSigPow3_seed;
      float METSigSoftSub_seed;
      float METoverPtMean_seed;
      float METoverPtMean12_seed;
      int Nbjet_seed;

      float ht;
      float meff;

      float truthHT;
      float truthMET;
      float met_truth;
      float met_truth_phi;

      float muR10_muF20Weight;
      float muR10_muF05Weight;
      float muR20_muF10Weight;
      float muR05_muF10Weight;
      float muR05_muF05Weight;
      float muR20_muF20Weight;
      float fsr_muRfac05; 
      float fsr_muRfac20; 
      float alphasUp;  
      float alphasDown; 
      float Var3cDown;
      float Var3cUp;


    }; //!


    void ClearLeaves(TreeLeaves*);
    void DeactivateSyst() { m_deactivateSyst = true; };

    bool IsTauTrigMatched_HLT(string trigger, const xAOD::TauJet* tau, const xAOD::EmTauRoIContainer* L1taus);
    bool IsTauTrigMatched_L1(string trigger, const xAOD::TauJet* tau, const xAOD::EmTauRoIContainer* L1taus);

    std::string outputName;
    bool nometaTree = true;

  // this is needed to distribute the algorithm to the workers
  ClassDef(NtupleVectorWriter, 1);

private:

    asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool>  s_weightTool; //! 

    xAOD::TEvent *m_event;  //!
    xAOD::TStore *m_store;  //!

    TTree * m_metaData; //!
    TTree * m_metaDataSignalCBK; //! 

    // to be able to switch on/off systematics for each NtupleVectorWriter separately
    bool m_deactivateSyst;

    int processID; //!
    int sampleID; //!

    int sampleID_CBK; //!
    int processID_CBK; //!
    double eventWeight_CBK; //!

    unsigned int EventSum; //!
    double EventWeightSum; //!
    double EventWeightSqSum; //!
    // for weight systematics
    vector<std::string> EventWeightNameVect; //!
    vector<unsigned int> EventSumVect; //!
    vector<double> EventWeightSumVect; //!
    vector<double> EventWeightSqSumVect; //!

    bool m_isMC; //!
    bool m_isDebugStream; //! 
    bool m_isSignal;

    bool m_jetSmear;
    unsigned int m_numSmear;
    bool m_tauSmear;
    bool m_skipNominal;

    std::map<CP::SystematicSet, TTree*> m_systMap;
    std::map<CP::SystematicSet, TreeLeaves*> m_dataMap;

    // tau trigger matching
    std::vector<std::string> tau_L1_names;
    std::vector<std::string> tau_HLT_names;

};

#endif
