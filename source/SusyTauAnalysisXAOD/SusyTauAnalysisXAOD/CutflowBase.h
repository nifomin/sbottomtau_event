#ifndef CutflowBase_H
#define CutflowBase_H

#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SusyTauAnalysisXAOD/ContainerHandler.h"
#include "TH1F.h"

class CutflowBase{
public:
    CutflowBase( std::string name ):
	m_name( name ),
	m_cuts(),
	m_hist( nullptr ),
	m_hist_raw( nullptr )
	{}
    
    virtual ~CutflowBase()
	{
	}
    
    /// Return a pair of values, the bool expresses if the cutflow
    /// passed and the integer specifies at which step the cut failed
    virtual std::pair<bool, int> ExecuteCuts( ContainerHandler & collection, double weight ) = 0;
    
    TH1F * GetHist();
    TH1F * GetHistRaw();
    
    void PrintCutFlow();

protected:
    
    void RegisterCut( std::string cutname );
    
    EL::StatusCode FillCut( std::string cutname, double weight );

    EL::StatusCode FillCut( unsigned int pos, double weight );
    
    int GetCutPos( std::string cutname );
    
    std::string m_name;
    std::vector< std::pair< std::string, double> > m_cuts;
    TH1F * m_hist;
    TH1F * m_hist_raw;
};

#endif
