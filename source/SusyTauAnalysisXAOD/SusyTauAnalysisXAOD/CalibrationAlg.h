#ifndef SusyTauAnalysisXAOD_CalibrationAlg_H
#define SusyTauAnalysisXAOD_CalibrationAlg_H

#include <iostream>
#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"
#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"
#include "SampleHandler/MetaObject.h"
#include "TH1I.h"

class CalibrationAlg : public SusyTauAlgBase
{
public:
    const char *GetName () const
	{
	    return m_name.c_str();
	}

    std::string m_name;
    xAOD::TEvent *m_event;  //!
    xAOD::TStore *m_store;  //!
    SusyTauSystVariation m_prw_up; //!
    SusyTauSystVariation m_prw_down; //!
    static std::map< Phys::ContainerType, std::vector<SusyTauSystVariation> > s_eff_syst;
    TH1I *h_runnumber; //!

    // this is a standard constructor
    CalibrationAlg ();
    
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& /*job*/);
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode histFinalize ();

    // Retrieve the name of calibrated containers
    // Intended for communication between the calibration and analysis algorithms
    static std::string ContainerName( Phys::ContainerType type )
	{
	    return s_contNames_calibrated[type];
	}
    
    // Same as above, but for applied systematic variations
    static std::string ContainerName( Phys::ContainerType type, CP::SystematicSet set )
	{
	    auto it = s_contNames_syst[type].find( set );
	    if ( it == s_contNames_syst[type].end() )
		return ContainerName( type );
	    
	    return it->second;
	}
    
    // For jet smearing
    static std::string ContainerName( Phys::ContainerType type, CP::SystematicSet set, unsigned int Nsmear )
	{
	  auto it = s_contNames_syst[type].find( set );
	  if ( it == s_contNames_syst[type].end() )
	    return (ContainerName( type ) + "_" + std::to_string(Nsmear) );
	  
	  return ( it->second + "_" + std::to_string(Nsmear) );
	}

    // For jet smearing
    static std::vector<SusyTauSystVariation> * WeightSystematics( Phys::ContainerType type )
	{
	  return ( &s_eff_syst[type] );
	}

    // this is needed to distribute the algorithm to the workers
    ClassDef(CalibrationAlg, 1);

private:
    std::map< int, std::string > s_contNames_original;
    static std::map< Phys::ContainerType, std::string > s_contNames_calibrated;
    static std::map< Phys::ContainerType, std::string > s_objNames;
    static std::map< Phys::ContainerType, std::map< CP::SystematicSet, std::string > > s_contNames_syst;
    
    // Calibrate a container
    // No systematic variation is applied, so it is assumed that ST is without systematic variations set.
    template< typename cont_t >
    cont_t * CalibrateContainer ( std::string & container_name, std::string & calibrated_name )
	{
	    std::pair< cont_t*, xAOD::ShallowAuxContainer* > cont;
	    calibrate_and_decorate( cont.first, cont.second, container_name );

	    if( !m_store->record( cont.first, calibrated_name ).isSuccess())
            {
                Error("CalibrationAlg", "Failed to record object" );
		// should throw an exception here                                                                                                                                                                                                                              
            }
            if( !m_store->record( cont.second, calibrated_name+"Aux." ).isSuccess())
            {
                Error("CalibrationAlg", "Failed to record Aux object" );
                //return EL::StatusCode::FAILURE;                                                                                                                                                                                                                              
            }

	    return cont.first;
	}

    // Decorate a container
    // No systematic variation is applied, so it is assumed that ST is without systematic variations set.
    template< typename cont_t >
      cont_t * DecorateContainer ( cont_t * original_container, std::string & calibrated_name, const CP::SystematicSet & /*syst*/ )
	{
	    std::pair< cont_t*, xAOD::ShallowAuxContainer* > cont;
	    makeShallowContainer( cont, original_container );
	    decorate( cont.first );

	    if( !m_store->record( cont.first, calibrated_name ).isSuccess())
	    { 
		Error("CalibrationAlg", "Failed to record object" );
                // should throw an exception here
	    }
	    if( !m_store->record( cont.second, calibrated_name+"Aux." ).isSuccess()) 
	    {
		Error("CalibrationAlg", "Failed to record Aux object" );
		//return EL::StatusCode::FAILURE;
	    }
	    
	    return cont.first;
	}

    // Make a swallow container
    // No systematic variation is applied, so it is assumed that ST is without systematic variations set.
    template< typename cont_t >
    cont_t * ShallowContainer ( cont_t * original_container, std::string & calibrated_name )
	{
	    std::pair< cont_t*, xAOD::ShallowAuxContainer* > cont;
	    makeShallowContainer( cont, original_container );
	    if( !m_store->record( cont.first, calibrated_name ).isSuccess())
	    { 
		Error("CalibrationAlg", "Failed to record object" );
                // should throw an exception here
	    }
	    if( !m_store->record( cont.second, calibrated_name+"Aux." ).isSuccess()) 
	    {
		Error("CalibrationAlg", "Failed to record Aux object" );
		//return EL::StatusCode::FAILURE;
	    }
	    
	    for ( size_t i = 0; i < cont.first->size(); i++ )
	    {
	    	cont.first->at(i)->template auxdata<char>("passOR");
		cont.first->at(i)->template auxdata<char>("baseline");
		cont.first->at(i)->template auxdata<char>("signal");
	    }
	    
	    return cont.first;
	}
    
    // General method to create a shallow container
    template< typename cont_t >
    void makeShallowContainer ( std::pair< cont_t*, xAOD::ShallowAuxContainer*> & cont, const cont_t * originalCont )
	{
	    cont = xAOD::shallowCopyContainer( *originalCont );
	    // TODO: THIS SHOULD BE CHECKED                                                                                                  
	    if ( ! xAOD::setOriginalObjectLink( *originalCont, *cont.first ) )
		Error( "CalibrationAlg", "Failed to set object links" );
	}
    
    // General method to create a shallow container
    template< typename cont_t >
    void makeShallowContainer ( std::pair< cont_t*, xAOD::ShallowAuxContainer*> & cont, std::string contents )
	{
	    const cont_t* originalCont;
	    
	    if ( ! m_event->retrieve( originalCont, contents ).isSuccess() ) 
	    {
		Error( "CalibrationAlg", (std::string( "Failed to retrieve container!" ) + contents) .c_str() );
	    }
	    cont = xAOD::shallowCopyContainer( *originalCont );
	    // TODO: THIS SHOULD BE CHECKED                                                                                                  
	    if ( ! xAOD::setOriginalObjectLink( *originalCont, *cont.first ) )
		Error( "CalibrationAlg", "Failed to set object links" );
	}
    
    // Overloaded method to calibrate and decorate objects,
    //   hides the calls to ST, any decorations per object
    //   should happen in here.
    inline void calibrate_and_decorate( xAOD::JetContainer *& jet_cont, xAOD::ShallowAuxContainer *& aux, std::string & key )
	{
	  if ( !SusyTauGlobals::s_susyDef->GetJets( jet_cont, aux, false, key ).isSuccess() )
	    {
		Error( "CalibrationAlg", "Failed to calibrate jets!" );
	    }
	    
	  for ( auto jet : * jet_cont )
	    {
	      if(jet->isAvailable<char>("DFCommonJets_jetClean_TightBad")){
		if(jet->auxdataConst<char>("DFCommonJets_jetClean_TightBad")==1)
		  {
		    jet->auxdata<char>("bad_tight") = 0;
		  }
		else
		  {
		    jet->auxdata<char>("bad_tight") = 1;
		  }
	      }  
	      else
                {
		  // no longer supported, variables dropped from smart slimming list
		  //jet->auxdata<char>("bad_tight") = !SusyTauGlobals::s_jetCleaningTool->keep( *jet );

		  // PFlow jets are currently not decorated by the DerivationFramework
		  std::vector<float> sumPtTrkvec;
		  if (!jet->getAttribute( xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec ))
		    cout << "Missing SumPtTrkPt500" << endl;
		  float sumpttrk = 0;
		  if (!sumPtTrkvec.empty()) sumpttrk = sumPtTrkvec[0];
      
		  float FracSamplingMax = 0;
		  if (!jet->getAttribute( xAOD::JetAttribute::FracSamplingMax, FracSamplingMax ))
		    cout << "Missing FracSamplingMax" << endl;

		  bool isbad = false;
		  if (FracSamplingMax==0. || (FracSamplingMax>0. && std::fabs(jet->eta())<2.4 && sumpttrk/jet->pt()/FracSamplingMax<0.1)) isbad = true;
		  jet->auxdata<char>("bad_tight") = isbad;
		}
	      
	      SusyTauGlobals::s_susyDef->IsBJet( *jet );

	      // flag jets in dead tile modules, don't correct them
	      // using hacked version of the tile tool
	      jet->auxdata<unsigned int>("TileStatus") = SusyTauGlobals::s_jetTileCorrectionTool->GetTileStatus(jet->jetP4(xAOD::JetConstitScaleMomentum).eta(), 
														jet->jetP4(xAOD::JetConstitScaleMomentum).phi(), 
														0.4, SusyTauGlobals::s_susyDef->GetRunNumber());
	      jet->auxdata<char>( "isBjetMatched" ) = false;
              jet->auxdata<float>( "onlineBtag" ) = -2.;

	      if ( wk()->metaData()->castBool( "isMC" ) )
		{
		  jet->auxdata<double>("effscalefact") = 1.0;
		  jet->auxdata<float>("jvtscalefact") = 1.0;
		}
	    }

	    // This is called only to decorate the jets. We don't use this scale factor.
	    if ( wk()->metaData()->castBool( "isMC" ) )
	      {
		SusyTauGlobals::s_susyDef->BtagSF( jet_cont );
		SusyTauGlobals::s_susyDef->JVT_SF( jet_cont );
	      }
	}
    
    inline void calibrate_and_decorate( xAOD::TauJetContainer *& tau_cont, xAOD::ShallowAuxContainer *& aux, std::string & key )
	{
	    if ( !SusyTauGlobals::s_susyDef->GetTaus( tau_cont, aux, false, key ).isSuccess() )
	    {
		Error( "CalibrationAlg", "Failed to calibrate taus!" );
	    }

	    for ( auto tau : * tau_cont )
	      {
		// ideally, would use etaPanTauCellBased and etaPanTauCellBased, but may not be available for all derivations
	        tau->auxdata<unsigned int>("TileStatus") = SusyTauGlobals::s_jetTileCorrectionTool->GetTileStatus(tau->eta(), tau->phi(), 
														  0.2, SusyTauGlobals::s_susyDef->GetRunNumber());
		
		if ( wk()->metaData()->castBool("isMC") )
		  {
		    if( tau->auxdataConst < ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") )
		      SusyTauGlobals::s_tauTruthMatch->applyTruthMatch(*tau);
		    
		    tau->auxdata<double>("effscalefact") = 1.;
		    double SF_medium = 1.;
		    
		    if( tau->auxdataConst<char>("baseline") && tau->auxdataConst<char>("signal") ) {
		      SusyTauGlobals::s_susyDef->GetSignalTauSF( *tau, true, false );
		      
		      if( tau->isTau(xAOD::TauJetParameters::JetRNNSigMedium) ) 
			if(SusyTauGlobals::s_tauEffTool->getEfficiencyScaleFactor(*tau, SF_medium) != CP::CorrectionCode::Ok)
			  cout << "Failed to retrieve medium tau efficiency scale factor." << endl;
		    }
		    tau->auxdata<double>("effscalefact_medium") = SF_medium;
		  }
	      }
	}
    
    inline void calibrate_and_decorate( xAOD::MuonContainer *& muon_cont, xAOD::ShallowAuxContainer *& aux, std::string & key )
    {
	if ( !SusyTauGlobals::s_susyDef->GetMuons( muon_cont, aux, false, key ).isSuccess() )
	{
	    Error( "CalibrationAlg", "Failed to calibrate muons!" );
	}
	
	if ( wk()->metaData()->castBool( "isMC" ) )
	  {	
	    for ( auto muon : * muon_cont )
	      {
		muon->auxdata<double>("effscalefact") = 1.0;
		
		if( muon->auxdataConst<char>("signal")==1 )
		  SusyTauGlobals::s_susyDef->GetSignalMuonSF( *muon );
	      }
	  }
    }
    
    inline void calibrate_and_decorate( xAOD::ElectronContainer *& electron_cont, xAOD::ShallowAuxContainer *& aux, std::string & key )
    {
	if ( !SusyTauGlobals::s_susyDef->GetElectrons( electron_cont, aux, false, key ).isSuccess() )
	{
	    Error( "CalibrationAlg", "Failed to calibrate electrons!" );
	}
	
	if ( wk()->metaData()->castBool( "isMC" ) ) 
	  {
	    for ( auto electron : * electron_cont )
	      {
		electron->auxdata<double>("effscalefact") = 1.0; 

		SusyTauGlobals::s_susyDef->GetSignalElecSF( *electron, true, true, false );
	      }
	  }
    }
    
    inline void calibrate_and_decorate( xAOD::PhotonContainer *& photon_cont, xAOD::ShallowAuxContainer *& aux, std::string & key )
	{
	    if ( !SusyTauGlobals::s_susyDef->GetPhotons( photon_cont, aux, false, key ).isSuccess() )
	    {
		Error( "CalibrationAlg", "Failed to calibrate photons!" );
	    }
	}

    inline void decorate( xAOD::JetContainer * jet_cont )
	{
	  if ( wk()->metaData()->castBool( "isMC" ) ) {

	    for ( auto jet_itr =jet_cont->begin(); jet_itr != jet_cont->end(); ++jet_itr )
	      {
		(*jet_itr)->auxdata<double>("effscalefact") = 1.;
		(*jet_itr)->auxdata<float>("jvtscalefact") = 1.;
	      }

	    //This is called only to decorate the jets. We don't use this scale factor.
	    SusyTauGlobals::s_susyDef->BtagSF( jet_cont );
	    SusyTauGlobals::s_susyDef->JVT_SF( jet_cont );
	  }
	}
    
    inline void decorate( xAOD::TauJetContainer * tau_cont )
	{
	  if ( wk()->metaData()->castBool( "isMC" ) ) {
	    
	    for ( auto tau : * tau_cont )
	      {
		tau->auxdata<double>("effscalefact") = 1.;
		double SF_medium = 1.; 
		
		if ( tau->auxdataConst< char >( "baseline" ) && tau->auxdataConst< char >( "signal" ) )
		  {
		    SusyTauGlobals::s_susyDef->GetSignalTauSF( *tau, true, false ); 
		    
		    if( tau->isTau(xAOD::TauJetParameters::JetRNNSigMedium) ) 
		      if(SusyTauGlobals::s_tauEffTool->getEfficiencyScaleFactor(*tau, SF_medium) != CP::CorrectionCode::Ok)
			cout << "Failed to retrieve medium tau efficiency scale factor." << endl;
		  }		
		tau->auxdata<double>("effscalefact_medium") = SF_medium;
	      }
	  }
	}
    
    inline void decorate( xAOD::MuonContainer * muon_cont )
	{
	  if ( wk()->metaData()->castBool( "isMC" ) ) {

	    for ( auto muon : * muon_cont )
	      {
		muon->auxdata<double>("effscalefact") = 1.;  
		
		if ( muon->auxdataConst< char >( "baseline" ) && muon->auxdataConst< char >( "signal" ) )
		  {
		    SusyTauGlobals::s_susyDef->GetSignalMuonSF( *muon );
		  }
	      }
	  }
	}

    inline void decorate( xAOD::ElectronContainer * electron_cont )
	{
	  if ( wk()->metaData()->castBool( "isMC" ) ) {
	    
	    for ( auto electron : * electron_cont )
	      {
		electron->auxdata<double>("effscalefact") = 1.;
		
		if ( electron->auxdataConst<char>( "baseline" ) && electron->auxdataConst<char>( "signal" ) )
		  {
		    SusyTauGlobals::s_susyDef->GetSignalElecSF( *electron, true, true, false );
		  }
	      }
	  }
	}
    
    inline void decorate( xAOD::PhotonContainer * /*photon_cont*/ )
	{
	}
    
    
    // Created a mising ET container and push to TStore with name calibrated_name
    xAOD::MissingETContainer * MakeMissingETContainer ( std::string calibrated_name,
							const xAOD::JetContainer* jet,
							const xAOD::ElectronContainer* elec,
							const xAOD::MuonContainer* muon,
							const xAOD::PhotonContainer* photon,
							const xAOD::TauJetContainer* tau );

    xAOD::MissingETContainer * MakeMissingETnoMuonContainer ( std::string calibrated_name,
							      const xAOD::JetContainer* jet,
							      const xAOD::ElectronContainer* elec,
                                                               const xAOD::MuonContainer* muon,
							      const xAOD::PhotonContainer* photon,
							      const xAOD::TauJetContainer* tau );

};

#endif
