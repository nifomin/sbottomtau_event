#ifndef SusyTauAnalysisXAOD_TauSmearingTool_H
#define SusyTauAnalysisXAOD_TauSmearingTool_H

#include "AsgTools/AsgTool.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"
#include "TH2F.h"
#include <vector>
#include "TRandom.h"
#include <sstream>

namespace TauSmearing {


  class SmearData
  {
  public:
    
    SmearData(const xAOD::TauJetContainer& theTaus)
      {
	std::pair< xAOD::TauJetContainer*, xAOD::ShallowAuxContainer* > taus_shallowCopy = xAOD::shallowCopyContainer( theTaus );
	tauContainer = taus_shallowCopy.first;
	tauAuxContainer = taus_shallowCopy.second;
	if( !xAOD::setOriginalObjectLink(theTaus,*tauContainer) )
	  std::cout << "WARNING: In SmearData::SmearData(const xAOD::TauJetContainer& theTaus) Could not update tau links to match original taus " << std::endl;
      };
    
    ~SmearData() {} ;
    
    xAOD::TauJetContainer* tauContainer;
    xAOD::ShallowAuxContainer* tauAuxContainer;
  };


  class TauSmearingTool : public asg::AsgTool {
	
  public:

    /** Rootcore constructor */
    TauSmearingTool(const std::string& name);
    ~TauSmearingTool() ;

    virtual StatusCode initialize();

    /** Set pointers to response maps */
    void SetResponseMaps(const TH2F* Rmap) { this->AddResponseMaps(Rmap) ; }

    /** Do the tau smearing */
    //void DoSmearing(std::vector<std::unique_ptr<SmearData >>& smrMc, const xAOD::TauJetContainer& taus, const xAOD::JetContainer& alljets);
    void DoSmearing(std::vector<std::unique_ptr<SmearData >>& smrMc, const xAOD::TauJetContainer& taus);

    void SetOutOfConeHist( const TH1F* h) { this->AddOutOfConeHist(h); }

  private:
    /** Vector of fake tau response maps */
    std::vector<TH2F*> m_Rmap;

    /** For each light jet response map we create N 1D response histograms via projections, and store them in this vector */
    std::vector< std::vector<TH1F*> > m_Rvector; 

    // correct for out-of-cone energy (> 0.2 cone, < 0.4 anti-kt)
    TH1F* h_TauPtOverSeedJetPt;

    /** Number of times to smear each event */
    unsigned int m_nSmear;

    /** Pt Binning of response histograms */
    double m_ptBinning;

     /** Add response map */
    void AddResponseMaps(const TH2F* Rmap);

    void AddOutOfConeHist( const TH1F* h) {
      h_TauPtOverSeedJetPt = (TH1F*) h->Clone();  
    };

    /** Create 1D smearing histogram for fake taus */
    const TH1F* MakeSmearingHist(const double& pt, const unsigned int& i); 
    
    /** Calculate pt bin, in 2-D smearing histogram, for a jet with momentum pt */
    inline int getPtBin(const double& pt) { return static_cast<int>(pt/m_ptBinning) ; }

    /** Set the 4-vector of an xAOD::TauJet */
    void setTau4Vector(xAOD::TauJet& theTau, const TLorentzVector& theLorentzVector);

    /** Do kinematic selection of jets to smear */
    bool passSelection(const xAOD::TauJet& theTau);

  };

}

#endif
