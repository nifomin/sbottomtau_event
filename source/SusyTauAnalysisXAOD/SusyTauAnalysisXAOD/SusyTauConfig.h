#ifndef SusyTauAnalysisXAOD_SusyTauConfig_H
#define SusyTauAnalysisXAOD_SusyTauConfig_H

#include <boost/program_options.hpp>
#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"
#include "SampleHandler/SampleHandler.h"
#include "EventLoop/Job.h"

namespace SusyTauConfig
{
    class SystematicSettings
    {
    public:
	SystematicSettings():
	    doJetSyst(false),
	    doTauSyst(false),
	    doMuonSyst(false),
	    doElectronSyst(false),
	    doPhotonSyst(false),
	    doMETSyst(false),
	    doWeightSyst(false)
	    {}
	
	virtual ~SystematicSettings() {}
	
	bool doJet()      { return doJetSyst; }
	bool doTau()      { return doTauSyst; }
	bool doElectron() { return doElectronSyst; }
	bool doPhoton()   { return doPhotonSyst; }
	bool doMuon()     { return doMuonSyst; }
	bool doMET()      { return doMETSyst; }
	bool doWeight()   { return doWeightSyst; }

	void enableJet()      { doJetSyst = true; }
	void enableTau()      { doTauSyst = true; }
	void enableElectron() { doElectronSyst = true; }
	void enablePhoton()   { doPhotonSyst = true; }
	void enableMuon()     { doMuonSyst = true; }
	void enableMET()      { doMETSyst = true; }
	void enableWeight()   { doWeightSyst = true; }
	
    private:
	bool doJetSyst;
	bool doTauSyst;
	bool doMuonSyst;
	bool doElectronSyst;
	bool doPhotonSyst;
	bool doMETSyst;
	bool doWeightSyst;
	
	ClassDef(SystematicSettings, 1);
    };

    
    class SusyTauConfigParser
    {
    public:
	SusyTauConfigParser();
	bool ParseCommandLine( int ac, char* av[]);
	
	inline boost::program_options::variables_map GetOptions()
	    {
		return m_variables;
	    }
	
	void enableBatchOptions();
	void enableGridOptions();
	
	SH::SampleHandler GetSampleHandler();
	EL::Job           GetJob();
	
	SusyTauConfig::SystematicSettings GetSystConfig();

	std::string GetParametersPBS();

        void ConfigureSamples( SH::SampleHandler & sh );

    private:
	boost::program_options::options_description m_allOptions;

	boost::program_options::options_description m_commonOptions;
	boost::program_options::options_description m_analysisOptions;
	boost::program_options::options_description m_systOptions;
	boost::program_options::options_description m_batchOptions;
	boost::program_options::options_description m_gridOptions;
	boost::program_options::options_description m_containerOptions;

	boost::program_options::variables_map m_variables;
	
	bool m_isGrid;
    };    

    std::vector<SusyTauSystVariation> RetrieveSystematicList( SusyTauConfig::SystematicSettings & settings, bool isFullSim, bool addNominal = true );
    
    int GetDerivNum( SH::SampleHandler & sh );
    std::string SampleType( int process_id );
    
}

#endif
