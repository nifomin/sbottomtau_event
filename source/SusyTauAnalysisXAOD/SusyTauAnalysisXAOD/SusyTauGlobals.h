#ifndef SusyTauAnalysisXAOD_SUSYTAUGLOBALS_H
#define SusyTauAnalysisXAOD_SUSYTAUGLOBALS_H

#include <vector>
#include <map>
#include <string>
#include <utility>
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"
#include "SusyTauAnalysisXAOD/SusyTauConfig.h"
#include "EventLoop/StatusCode.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/ToolHandle.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
//#include "TauSpinnerTool/xAODEventReaderTool.h"
//#include "TauSpinnerTool/StandardModelTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetSmearing/PreScaleTool.h"
#include "JetTileCorrection/JetTileCorrectionTool.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "AsgTools/AnaToolHandle.h"

namespace ST
{
  class SUSYObjDef_xAOD;
}


// Pointers to objects that should be accessible globally
//
// Any tools that are required in a specific algorithm but would be usefull
// in other places should be placed here too.
namespace SusyTauGlobals
{
    extern SusyTauConfig::SusyTauConfigParser s_config;
    extern ST::SUSYObjDef_xAOD* s_susyDef; // Managed by CalibrationAlgorithm
    extern GoodRunsListSelectionTool* s_grl; // Managed by CalibrationAlgorithm
    extern GoodRunsListSelectionTool* s_grl_tile;
    extern GoodRunsListSelectionTool* s_grl_ditauMET;

    extern xAOD::TEvent * s_event;           // Managed by CalibrationAlgorithm
    extern xAOD::TStore * s_store;           // Managed by CalibrationAlgorithm
    extern CP::PileupReweightingTool * s_prwtool;// Managed by CalibrationAlgorithm
    
    extern TauAnalysisTools::TauTruthMatchingTool * s_tauTruthMatch; //!
    extern asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> s_tauSelTool;
    extern TauAnalysisTools::TauEfficiencyCorrectionsTool* s_tauEffTool; //!

    //extern TauSpinnerTools::xAODEventReaderTool* s_tauSpinnerReader; 
    //extern TauSpinnerTools::StandardModelTool* s_tauSpinnerWeightTool;
    
    extern JetCleaningTool * s_jetCleaningTool;
    extern CP::JetTileCorrectionTool* s_jetTileCorrectionTool; 
    extern JetSmearing::PreScaleTool* s_prescaleTool;

   
    extern bool doPhotons;
    extern bool isMC;
    
    EL::StatusCode InitialiseGRL();
    EL::StatusCode InitialiseSusyTools( const SH::MetaObject * meta, xAOD::TEvent* event );
    EL::StatusCode InitialiseTauTruthTools( const SH::MetaObject * metadata );
    EL::StatusCode InitialiseTriggerTools();
    EL::StatusCode InitialisePRWTools( const SH::MetaObject * metadata );

}

#endif
