#ifndef SusyTauAnalysisXAOD_SusyTauSystVariation_H
#define SusyTauAnalysisXAOD_SusyTauSystVariation_H

#include <string>
#include "SUSYTools/ISUSYObjDef_xAODTool.h"
#include "EventLoop/Worker.h"

using namespace std;

namespace Phys {
  enum ContainerType {
      Unknown = 0,
      Jet,
      Egamma,
      Electron,
      Photon,
      Muon,
      Tau,
      BTag,
      MissingET,
      AntiKt4TruthJets,
      TruthTaus,
      TruthElectrons,
      TruthMuons,
      MET_Truth };
}

class SusyTauSystVariation{
  public:
    SusyTauSystVariation(CP::SystematicSet systset,
                         string fullname, string suffix, 
                         bool affKin, bool affWgt);
    
    
    SusyTauSystVariation( ST::SystInfo & susySyst )
	{
	    m_systset = susySyst.systset;
	    m_fullname = susySyst.systset.name();
	    m_suffix =  susySyst.systset.name();
	    m_affectsKinematics = susySyst.affectsKinematics;
	    m_affectsWeights = susySyst.affectsWeights;
	    m_isNominal = false;
	    switch ( susySyst.affectsType )
	    {
	    case ST::SystObjType::Jet:
		m_affectsType = Phys::ContainerType::Jet;
		break;
	    case ST::SystObjType::BTag:
	      m_affectsType = Phys::ContainerType::Jet;
	      break;
	    case ST::SystObjType::Tau:
		m_affectsType = Phys::ContainerType::Tau;
		break;
	    case ST::SystObjType::Muon:
		m_affectsType = Phys::ContainerType::Muon;
		break;
	    case ST::SystObjType::Electron:
		m_affectsType = Phys::ContainerType::Electron;
		break;
	    case ST::SystObjType::Photon:
		m_affectsType = Phys::ContainerType::Photon;
		break;
	    case ST::SystObjType::MET_TST:
		m_affectsType = Phys::ContainerType::MissingET;
		break;
	    case ST::SystObjType::MET_CST:
		m_affectsType = Phys::ContainerType::MissingET;
		break;
	    case ST::SystObjType::MET_Track:
		m_affectsType = Phys::ContainerType::MissingET;
		break;
	    }
	}
    
    SusyTauSystVariation():
	m_systset{"NOMINAL"}, m_fullname("NOMINAL"), m_suffix("NOMINAL"),
	m_affectsKinematics(false), m_affectsWeights(false),m_isNominal(true)
	{}
    
    string fullname();
    string suffix();
    bool affectsKinematics();
    bool affectsWeights();
    Phys::ContainerType affectsType();

    bool isNominal();
    // potentially also give back isAffected(ContainerType) with MET = OR(all)

    void setContainerName( Phys::ContainerType, std::string );
    void setWeights( /* Params to be designed */ );

    CP::SystematicSet m_systset;
    
  private:
    string m_fullname;
    string m_suffix;
    bool m_affectsKinematics;
    bool m_affectsWeights;
    bool m_isNominal;
    // let's see: unsigned int affectsType;
    Phys::ContainerType m_affectsType;
};

inline string SusyTauSystVariation::fullname() { return m_fullname; }

inline string SusyTauSystVariation::suffix()   { return m_suffix; }

inline bool   SusyTauSystVariation::isNominal() {
    return m_isNominal;
}

inline bool SusyTauSystVariation::affectsKinematics() { return m_affectsKinematics; }

inline bool SusyTauSystVariation::affectsWeights()    { return m_affectsWeights;    }
inline Phys::ContainerType SusyTauSystVariation::affectsType()       { return m_affectsType;       }

template <typename hist_t>
class SystAwareHist
{
public:
    SystAwareHist():
	m_systMap()
	{
	}

    // There is a memory leak here
    void RegisterHists( hist_t * original, std::vector<SusyTauSystVariation> & systList, EL::Worker * worker )
	{
	    for ( auto syst : systList )
	    {
		if ( syst.isNominal() )
		{
		    m_systMap[syst.m_systset] = original;
		}
		else
		{
		    m_systMap[syst.m_systset] = (hist_t*)original->Clone( (syst.fullname() + "/" + original->GetName()).c_str() );
		}
	    }
	    
	    for ( auto & hist : m_systMap )
		worker->addOutput( hist.second );
	}
    
    hist_t * Get( CP::SystematicSet & syst )
	{
	    return m_systMap[syst];
	}
    
private:
    std::map<CP::SystematicSet, hist_t *> m_systMap;
};


#endif
