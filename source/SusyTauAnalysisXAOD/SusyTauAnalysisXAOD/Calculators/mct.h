// Slimmed down version of mctlib
// from http://mctlib.hepforge.org/svn/trunk/

#ifndef mctlib_h
#define mctlib_h


namespace Calculators
{
    namespace Details
    {
	double mct(const double v1[4],const double v2[4])
	{
	    double et1 = sqrt(fmax(v1[0]*v1[0]-v1[3]*v1[3],0.0));
	    double et2 = sqrt(fmax(v2[0]*v2[0]-v2[3]*v2[3],0.0));
	    return sqrt(fmax(pow(et1+et2,2)-pow(v1[1]-v2[1],2)
			     -pow(v1[2]-v2[2],2),0.0));
	}
    }
    
    template <typename T, typename U>
    double ComputeMCT(const T & v1,const U & v2)
    {
	double v1t[4] = {v1.p4().E(), v1.p4().Px(), v1.p4().Py(), v1.p4().Pz()};
	double v2t[4] = {v2.p4().E(), v2.p4().Px(), v2.p4().Py(), v2.p4().Pz()};
	return Details::mct(v1t,v2t);
    }

    
    template<class T>
    double ComputeMaxMCT( const T& container )
    {
	if ( container.size() < 2 )
	    return -1;
	
	double mct = -1;
	double mct_temp = 0;

        for (auto par = container.begin(); par != container.end(); par++) {
            auto secondPar = par;
            for( secondPar++; secondPar != container.end(); secondPar++) {
		mct_temp = ComputeMCT( **par, **secondPar );
		mct = ( mct_temp > mct ) ? mct_temp : mct;
            }
	}

        return mct;
    }

}
    
#endif


  
