/*
 * Most of the following code is from the MT2 package by Lester and Nachmann found at: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MT2Package
 * Since its original wrapper is awful, we wrap it ourselves and give it a nice name!
 */

#ifndef MT2_H
#define MT2_H

#include "SusyTauAnalysisXAOD/Externals/lester_mt2_bisect.h"

#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"
#include "xAODTau/TauJet.h"
#include "xAODJet/Jet.h"
#include "xAODMissingET/MissingET.h"

namespace Calculators {
    template<class T, class U>
    double ComputeMT2(const T& visa, const U& visb, const xAOD::MissingET& MET, double ma, double mb){
	return asymm_mt2_lester_bisect::get_mT2(visa.p4().M(), visa.p4().Px(), visa.p4().Py(), 
						visb.p4().M(), visb.p4().Px(), visb.p4().Py(),
						MET.mpx(), MET.mpy(), ma, mb );
    }


    
    double ComputeMT2(const TLorentzVector& visa, const TLorentzVector& visb, const TVector2& MET, double ma, double mb){
	return asymm_mt2_lester_bisect::get_mT2(visa.M(), visa.Px(), visa.Py(), 
						visb.M(), visb.Px(), visb.Py(),
						MET.X(), MET.Y(), ma, mb );
    }


    

    template<class T>
    double ComputeMaxMT2( const T& container, const xAOD::MissingET& MET,  double ma, double mb )
    {
	if ( container.size() < 2 )
	    return -1;

	double mt2 = -1;
	double mt2_temp = 0;
	for (auto par = container.begin(); par != container.end(); par++) {
	    auto secondPar = par;
	    for( secondPar++; secondPar != container.end(); secondPar++) {
		mt2_temp = ComputeMT2( **par, **secondPar, MET, ma, mb);
		mt2 = ( mt2_temp > mt2 ) ? mt2_temp : mt2;
	    }
	}
	
	return mt2;
    }


    double ComputeMaxMT2( std::vector<TLorentzVector> container, const TVector2& MET,  double ma, double mb )
    {
      
      if ( container.size() < 2 )
	    return -1;

	double mt2 = -1;
	double mt2_temp = 0;

	for (unsigned int i = 0; i<container.size()-1 ; ++i)
	  {
	    
	    for (unsigned int i2 =i+1; i2<container.size(); ++i2)
	      {
		mt2_temp = ComputeMT2(container[i],container[i2],MET,ma,mb);
		mt2 =  (mt2_temp > mt2) ? mt2_temp :mt2;
	      }
	  }
	return mt2;
    }


}

#endif
