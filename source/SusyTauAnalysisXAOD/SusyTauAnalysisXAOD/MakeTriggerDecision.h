#ifndef SusyTauAnalysisXAOD_MakeTriggerDecision_H
#define SusyTauAnalysisXAOD_MakeTriggerDecision_H

#include <EventLoop/Algorithm.h>
#include "TTree.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "TTree.h"
#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "PMGTools/PMGTruthWeightTool.h"

class MakeTriggerDecision : public SusyTauAlgBase
{
public:
  MakeTriggerDecision ():
  SusyTauAlgBase(),
    outputName( "miniVectorNtuple" )
      {
      }
  
  MakeTriggerDecision ( std::string cutflow ):
  SusyTauAlgBase( cutflow ),
    outputName( "miniVectorNtuple" )
      {
      }
  
  MakeTriggerDecision ( std::string cutflow, std::vector<std::string> cutflow_config):
  SusyTauAlgBase( cutflow, cutflow_config ),
    outputName( "miniVectorNtuple_" + cutflow)
    {
    }

    CutflowBase  * m_cutflowComp; //!
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:


  template<class Object, class Collection>
    const Object* getTrigObject(Trig::Feature<Collection>& feature){

    const Collection* trigCol = feature.cptr();
    if ( !trigCol ) {
      std::cout << "ERROR: No Trig Collection pointer" << std::endl;
      return 0;
    }

    if(trigCol->size() != 1){
      std::cout << "ERROR Trig Collection size " << trigCol->size() << std::endl;
      return 0;;
    }

    return trigCol->at(0);
}


  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  std::string outputName;


  GoodRunsListSelectionTool *m_grl2; //!
  GoodRunsListSelectionTool *m_grl3; //!
//   asg::AnaToolHandle<CP::IPileupReweightingTool> *m_prwTool2; //!
  CP::IPileupReweightingTool *m_prwTool2; //!
//  BTaggingEfficiencyTool offlineSFtool; //! 
//  BTaggingEfficiencyTool conditionalSFtool; //!


private:

  xAOD::TEvent *m_event;  //!
  xAOD::TStore *m_store;  //!
  bool m_isMC; //!
  std::string mcCampaign; //!


  // this is a standard constructor

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  virtual EL::StatusCode matchJets(double ptcut, const TLorentzVector bjet, bool isAlt, double btag);


  // this is needed to distribute the algorithm to the workers
  ClassDef(MakeTriggerDecision, 1);
};

#endif
