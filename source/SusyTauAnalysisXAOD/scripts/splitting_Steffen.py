
import ROOT
from ROOT import *
import sys
from sys import argv
import os
from os import path
import time

def getOutlists(mode,tree,fileprefix):
        treelist = []
        count = 0
        if mode == "notau":
                suffixlist = ["_notau_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(0))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
        elif mode == "susy5val":
                suffixlist = ["_CR_Fake_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(0))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
        elif mode == "copy":
                suffixlist = ["_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(-1,"fast"))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
        elif mode == "wlepnu":
                suffixlist = ["_CR_Fake_", "_SVR_Fake_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(0))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
                        count += 1
        elif mode == "wtaunu":
                suffixlist = ["_CR_Fake_", "_CR_True_", "_SVR_Fake_", "_SVR_True_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(0))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
                        count += 1
        elif mode == "top":
                suffixlist = ["_CR_Fake_", "_CR_True_", "_SVR_NoFake_", "_SVR_TrueFake_", "_SVR_DoubleFake_"]
                for suff  in suffixlist:
                        treelist.append(tree.CloneTree(0))
                        treelist[count].SetName(fileprefix+suff+tree.GetName())
                        count += 1
        return treelist

def getOutIndex(mode,event):
        if mode == "notau":
                if event.tau_n == 0 and event.ele_n + event.mu_n >= 1:
                        return 0
                else:
                        return -1
        elif mode == "susy5val":
                if event.tau_n == 1 and event.ele_n + event.mu_n >= 1 and event.tau_nTruthMatched == 0:
                        return 0
                else:
                        return -1
        elif mode == "wlepnu":
                if event.tau_n == 1:
                        return 0
                elif event.tau_n > 1:
                        return 1
        elif mode == "wtaunu":
                if event.tau_nTruthMatched == 0 and event.tau_n == 1:
                        return 0
                elif event.tau_nTruthMatched > 0 and event.tau_n == 1:
                        return 1
                elif event.tau_nTruthMatched == 0 and event.tau_n > 1:
                        return 2
                elif event.tau_nTruthMatched > 0 and event.tau_n > 1:
                        return 3
        if mode == "top":
                if event.tau_nTruthMatched == 0 and event.tau_n == 1:
                        return 0
                elif event.tau_nTruthMatched > 0 and event.tau_n == 1:
                        return 1
                elif event.tau_nTruthMatched >1 and event.tau_n > 1:
                        return 2
                elif event.tau_nTruthMatched == 1 and event.tau_n > 1:
                        return 3
                elif event.tau_nTruthMatched == 0 and event.tau_n > 1:
                        return 4

mode = "wtaunu"

if len(argv)>1:
        mode = argv[1]

ifilename = "wtaunu.root"

if len(argv)>2:
        ifilename = argv[2]


print "Running in", mode, "Mode on input file", ifilename

namelist = []

ifile = TFile(ifilename)
ofilename = path.basename(ifilename)
fileprefix = path.splitext(ofilename)[0]

for key in ifile.GetListOfKeys():
        tree = key.ReadObj()
        if tree.ClassName() == "TTree" and not tree.GetName() in namelist:
		if "skim_metadata" in tree.GetName():
			continue
                namelist.append(tree.GetName())

if os.path.exists(ofilename):
        print "WARNING: Tentative outfile", ofilename,"exist, using alternative name"
        ofilename = ofilename.replace(".","_split.")
        if os.path.exists(ofilename):
                print "WARNING: Tentative alternative outfile", ofilename,"exist, using timestamp name"
                ofilename = ofilename.replace("split", str(int(time.time())))
ofile = TFile(ofilename,"RECREATE")
print "Writing to outputfile", ofilename

for name in namelist:
        if mode == "susy5val" and not name == "NOMINAL":
                continue
        print "Running on input tree", name
        ifile.cd()
        tree = ifile.Get(name)

        ofile.cd()
        outtreelist = getOutlists(mode, tree, fileprefix)

        if mode != "copy":
                for event in tree:
                        if getOutIndex(mode,event) >= 0:
                                outtreelist[getOutIndex(mode,event)].Fill()

        ofile.Write()
        print "   ...done"
ofile.Close()
print "All done"
