#!/bin/bash

# set proper integrated luminosity (in pb-1): 3219.56 pb-1 for data15, 32988.1 pb-1 for data16
lumi=36207.66

# path to vector ntuples (baseline and plateau in the same directory)
dir="/disk/atlas2/data_mc_13TeV/ntuples_r21/21.2.39_first/SUSY3"
#dir="/disk/atlas2/data_mc_13TeV/ntuples_r21/21.2.39_first/SUSY5"

#ntupModes=("Baseline" "XEplateau")
ntupModes=("XEplateau")
#ntupModes=("Baseline")

# flag used to adjust true/fake splitting (could be parsed from directory and file name, but may be a bit dangerous)
SUSY="SUSY3"
#SUSY="SUSY5"

# batch queue
#queue="unlimited"
queue="short"

if [ "$WorkDir_DIR" == "" ]; then
    echo "Please source environment first"
    return
fi

for mode in ${ntupModes[@]}; do

    for flatvec in "vector" "flat" ; do

	if [ ! -d ${dir}/${flatvec}_${mode} ]; then
	    mkdir ${dir}/${flatvec}_${mode}
	fi
	if [ "$SUSY" == "SUSY3" ]; then
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_top ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_top
	    fi
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_wtaunu ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_wtaunu
	    fi
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_wlepnu ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_wlepnu
	    fi
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_copy ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_copy
	    fi
	elif [ "$SUSY" == "SUSY5" ]; then
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_notau ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_notau
	    fi
	    if [ ! -d ${dir}/${flatvec}_${mode}/split_susy5val ]; then
		mkdir ${dir}/${flatvec}_${mode}/split_susy5val
	    fi
	else
	    "Unrecognized SUSY flag for 2tau splitting. Aborting."
	    return
	fi
    done
done

if [ ! -d JobLogs ]; then
    mkdir JobLogs 
fi


merge() {

    args=("$@")
    process=${args[0]}
    mode=${args[1]}
    opt=${args[2]}
    IDs=""
    max=${#args[@]}
    max=$(( max - 1 ))
    for i in `seq 3 $max`; do
	IDs=${IDs}:${args[$i]}
    done
    
    echo "submitting process ${process}"
    qsub -q ${queue} run_merge.sh -o JobLogs/${SUSY}_${process}_${mode}.out -e JobLogs/${SUSY}_${process}_${mode}.err \
	-v dir=${dir},lumi=${lumi},process=${process},mode=${mode},opt=${opt},IDs=${IDs},WorkDir_DIR=${WorkDir_DIR},SUSY=${SUSY}

}



for mode in ${ntupModes[@]} ; do
    
    if [ "$SUSY" == "SUSY3" ]; then
	
	# signal
	while read line; do
	    
	    ID=${line/*mc15_13TeV\:mc15_13TeV./}
	    ID=${ID/.*/}
	    
	    name=""
	    if [ `echo $line | grep 2stepStau | wc -l` -ne 0 ]; then 
		name=${line/*2stepStau\_/}
		name="GG_"${name/.*/}
		
	    elif [ `echo $line | grep GMSB | wc -l` -ne 0 ]; then
		name=${line/*Lambda/}
		name=${name/tanbeta/}
		name="GMSB_"${name/.merge*/}
	    fi
	    
	    # signal not available yet
	    continue
	    
	    merge $name $mode 0 $ID
	    
	done < $WorkDir_DIR/data/SusyTauAnalysisXAOD/config/SUSY3_MC_sig_p2949.cfg

    fi




    # data
    merge "data" $mode 0 "data"

    
    # backgrounds

    #merge "dijet" $mode 0 `seq 361020 361032`

    #missing Sherpa_221_NN30NNLO_Zee_Mll10_40
    #merge "zee" $mode 0 `seq 364114 364127` `seq 364204 364209`
    merge "zee" $mode 0 `seq 364114 364127`

    #missing Sherpa_221_NN30NNLO_Zmm_Mll10_40
    #merge "zmumu" $mode 0 `seq 364100 364113` `seq 364198 364203`
    merge "zmumu" $mode 0 `seq 364100 364113`

    # missing Sherpa_221_NN30NNLO_Ztt_Mll10_40
    #merge "ztautau" $mode 0 `seq 364128 364141` `seq 364210 364215`
    merge "ztautau" $mode 0 `seq 364128 364141`

    merge "znunu" $mode 0 `seq 364142 364155`

    merge "wenu" $mode 0 `seq 364170 364183`

    merge "wmunu" $mode 0 `seq 364156 364169`

    merge "wtaunu" $mode 0 `seq 364184 364197`

    #merge "ztautau_MG" $mode 0 `seq 361510 361514`
    
    #merge "znunu_MG" $mode 0 `seq 361515 361519`   

    #merge "wtaunu_MG" $mode 0 `seq 361530 361534`

    #merge "wtaunu_MG_HT" $mode 0 `seq 363648 363671`
    
    #361071 has a stupid high weight which screws up the whole PRW, sample discarded until we fix the profile
    #merge "diboson" $mode 0 `seq 363355 363360` `seq 363489 363494` "361070" `seq 361072 361073` "361077"
    merge "diboson" $mode 0 `seq 363355 363360` 363489 `seq 364242 364250` 363494 `seq 364253 364255` 345723 `seq 364302 364305` `seq 407311 407315`

    # ttbar inclusive + HT slices + MET slices
    #merge "ttbar" $mode 1 410470 410471 `seq 407342 407347`
    # WARNING !!! BUG IN TRUTH HT/MET, sliced samples unusable with current ptag
    merge "ttbar" $mode 0 410470 410471

    # # WARNING !!! 410644-410647 unknown from PMG cross section tool 
    #merge "singletop_ttV" $mode 0 `seq 410644 410647` `seq 410155 410157` `seq 410218 410220`
    merge "singletop_ttV" $mode 0 `seq 410155 410157` `seq 410218 410220`

    #merge "top_HT" $mode 2 "410000" `seq 407009 407011` "410007" `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    #merge "top_Sherpa" $mode 2 `seq 410249 410252` "407018" "407020" `seq 410011 410014` "410025" "410026" "410081" `seq 410142 410144` "341177" "341270" "341271"

    #merge "top_MG" $mode 2 `seq 407200 407204` `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    #merge "top_PhPy8" $mode 2 `seq 410501 410503` `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    #merge "top_PhHw7" $mode 2 `seq 410525 410527` `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"
    
    #merge "top_PhHpp" $mode 2 "410004" `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    #merge "top_aMcAtNlo" $mode 2 "410003" `seq 410011 410014` "407018" "407020" "410025" "410026" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"
    
    #merge "top_radHi" $mode 0 "410001" "410018" "410020" "410099" "410101" "410107" "410109" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    #merge "top_radLo" $mode 0 "410002" "410017" "410019" "410100" "410102" "410108" "410110" `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271"

    # this is single top: `seq 410011 410014` "407018" "407020" "410025" "410026"
    # this is ttV: `seq 410066 410068` `seq 410073 410075` "410081" `seq 410111 410116` "341177" "341270" "341271" 

    

done

