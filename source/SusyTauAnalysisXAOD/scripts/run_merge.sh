#!/bin/bash

echo
echo "WorkDir_DIR = $WorkDir_DIR"
echo "dir = $dir"
echo "lumi = $lumi"
echo "process = $process"
echo "mode = $mode"
echo "SUSY = $SUSY"
echo "opt = $opt"
IDs=`echo $IDs | sed 's/\:/ /g'`
echo "IDs = $IDs"
echo

# random sleep to avoid accessing cvmfs too heavily
sleep $((RANDOM % 60))

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AnalysisBase,21.2.45
source $WorkDir_DIR/setup.sh

match=""

for ID in $IDs ; do
    
    if ! ls ${dir}/*${ID}*miniVectorNtuple_${mode}*/*root* 1> /dev/null 2>&1; then
	echo "WARNING ! Missing process ${process} ID ${ID}"
	continue
    fi
    
    match="${match} `ls ${dir}/*${ID}*miniVectorNtuple_${mode}*/*root*`"
done

if [ "$match" = "" ]; then
    echo "ERROR: COULD NOT FIND SAMPLES MATCHING ID $IDs"
    exit
fi

# hack of hadd set max tree size to 1 TB instead of 100 GB
if [ ! -e $WorkDir_DIR/../../source/SusyTauAnalysisXAOD/scripts/hadd_preload_C.so ]; then
    root.exe -b -l -q $WorkDir_DIR/../../source/SusyTauAnalysisXAOD/scripts/hadd_preload.C+
fi
cmd="LD_PRELOAD=$WorkDir_DIR/../../source/SusyTauAnalysisXAOD/scripts/hadd_preload_C.so hadd ${dir}/${process}_${mode}_tmp.root $match"
echo $cmd
eval $cmd
echo    


cmd="normalise_samples ${dir}/${process}_${mode}_tmp.root ${dir}/vector_${mode}/${process}.root ${lumi} ${opt}"
echo $cmd
eval $cmd
echo


#if [ "$SUSY" == "SUSY5" -a "$mode" == "Baseline" ]; then
cmd="rm ${dir}/${process}_${mode}_tmp.root"
echo $cmd
eval $cmd
echo
exit
#fi


cmd="flatten_ntuples ${dir}/vector_${mode}/${process}.root ${dir}/flat_${mode}/${process}.root"
echo $cmd
eval $cmd
echo


if [ "$process" == "zee" -o "$process" == "zmumu" -o "$process" == "ztautau" -o "$process" == "znunu" -o "$process" == "wtaunu" -o "$process" == "wenu" \
    -o "$process" == "wmunu" ]; then
    cmd="$WorkDir_DIR/user_scripts/SusyTauAnalysisXAOD/AddZJetsWeights.py ${dir}/flat_${mode}/${process}.root"
    echo $cmd
    eval $cmd
    echo
fi


if [ ${process} != "data" -a `echo ${process} | grep "GG" | wc -l` -eq 0 -a `echo ${process} | grep "GMSB" | wc -l` -eq 0 ]; then

    # determine 2tau splitting mode(s)
    declare -a splitmode=()

    if [ "$SUSY" == "SUSY3" ]; then
	if [ `echo ${process} | grep "top" | wc -l` -eq 1 ]; then
	    splitmode=("top")
	elif [ ${process} == "wtaunu" ]; then
	    splitmode=("wtaunu")
	elif [ ${process} == "wenu" -o ${process} == "wmunu" ]; then 
	    splitmode=("wlepnu")
	elif [ ${process} == "diboson" -o ${process} == "ztautau" -o ${process} == "znunu" ]; then 
	    splitmode=("copy")
	fi
    elif [ "$SUSY" == "SUSY5" ]; then
	if [ ${process} == "diboson" -o ${process} == "wmunu" -o ${process} == "wenu" -o ${process} == "ztautau" -o ${process} == "zmumu" -o ${process} == "zee" \
	    -o ${process} == "znunu" -o ${process} == "wtaunu" -o `echo ${process} | grep "top" | wc -l` -eq 1 ]; then
	    splitmode=("notau")
	fi    
	if [ ${process} == "wtaunu" -o ${process} == "wmunu" -o ${process} == "wenu" -o `echo ${process} | grep "top" | wc -l` -eq 1 ]; then
	    splitmode=("${splitmode[@]}" "susy5val")
	fi    
    fi
    
    echo "2tau splitting mode: ${splitmode[@]}"
    echo
    
    for flatvec in "vector" "flat" ; do
	
	cd ${dir}/${flatvec}_${mode}
	echo "cd $PWD"
	echo

	# 1tau splitting
	if [ "$flatvec" == "flat" -a "$SUSY" == "SUSY3" ]; then
	    cmd="split_true_fake ${process}.root"
	    echo $cmd
	    eval $cmd
	    echo
	fi
	
	# 2tau splitting	
	for split in ${splitmode[@]} ; do
	    
	    cd ${dir}/${flatvec}_${mode}/split_${split}

	    if [ -e ${process}.root ]; then
		rm ${process}.root
	    fi

	    ln -s ../${process}.root .
	    
	    if [ -e ${process}_split.root ]; then
		rm -f ${process}_split.root
	    fi 
	    
	    cmd="python $WorkDir_DIR/user_scripts/SusyTauAnalysisXAOD/splitting_Steffen.py ${split} ${process}.root"
	    echo $cmd
	    eval $cmd
	    echo 
	    rm ${process}.root

	done

    done
fi


cmd="rm ${dir}/${process}_${mode}_tmp.root"
echo $cmd
eval $cmd
echo
echo "Job done."
echo
