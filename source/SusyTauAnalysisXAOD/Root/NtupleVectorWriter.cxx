#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "xAODTau/TauxAODHelpers.h"
#include <SusyTauAnalysisXAOD/NtupleVectorWriter.h>
#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SusyTauAnalysisXAOD/ContainerHandler.h>
#include <SusyTauAnalysisXAOD/SusyTauSystVariation.h>
#include <SusyTauAnalysisXAOD/CalibrationAlg.h>
#include <SusyTauAnalysisXAOD/EventParametersAlg.h>
#include "SusyTauAnalysisXAOD/KinematicCalculators.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODTau/TauDefs.h"
#include "xAODTruth/TruthEventContainer.h"
#include <TFile.h>
#include <TLorentzVector.h>
#include <cmath>
#include <boost/algorithm/string/replace.hpp>
#include "CPAnalysisExamples/errorcheck.h"
#include "PMGTools/PMGTruthWeightTool.h"
#include "AsgTools/ToolHandle.h"

static const char* APP_NAME = "NtupleVectorWriter";
asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> s_weightTool;

// this is needed to distribute the algorithm to the workers
ClassImp(NtupleVectorWriter)

EL::StatusCode NtupleVectorWriter :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  if ( SusyTauAlgBase :: setupJob (job) != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;

  auto opts = SusyTauGlobals::s_config.GetOptions();
  m_jetSmear = opts["jetSmear"].as<bool>();
  m_numSmear = opts["numSmear"].as<unsigned int>();   
  m_tauSmear = opts["tauSmear"].as<bool>();
  m_skipNominal = opts["skipNominal"].as<bool>();
  m_isSignal = opts["isSignal"].as<bool>();
  
  // define an output and an ntuple associated to that output
  EL::OutputStream output(outputName);
  job.outputAdd (output);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode NtupleVectorWriter :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  if ( SusyTauAlgBase::histInitialize() != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode NtupleVectorWriter :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  // LHE3 weights
  EventWeightNameVect.clear(); EventSumVect.clear(); EventWeightSumVect.clear(); EventWeightSqSumVect.clear();
  
  TFile *outputFile = wk()->getOutputFile (outputName);

  if ( nometaTree )
    {
      m_event = wk()->xaodEvent();
      m_store = wk()->xaodStore();
      
      nometaTree = false;
      
      // give different names to metadata tree if splitting systematics in 2 batches
      string metadata_name = m_skipNominal? "skim_metadata_skipNominal" : "skim_metadata" ;
      m_metaData = new TTree ( metadata_name.c_str(), metadata_name.c_str() );
      m_metaData->SetDirectory (outputFile);

      m_metaData->Branch("SampleID", &sampleID);
      m_metaData->Branch("EventSum", &EventSum);
      m_metaData->Branch("EventWeightSum", &EventWeightSum);
      m_metaData->Branch("EventWeightSqSum", &EventWeightSqSum);

      // generator weight systematics 
      if ( wk()->metaData()->castBool("isMC") ) {
	  m_metaData->Branch("EventWeightNameVect", &EventWeightNameVect);
	  m_metaData->Branch("EventSumVect", &EventSumVect);
	  m_metaData->Branch("EventWeightSumVect", &EventWeightSumVect);
	  m_metaData->Branch("EventWeightSqSumVect", &EventWeightSqSumVect);
	}

      if ( wk()->metaData()->castBool( "isMC" ) && wk()->metaData()->castBool("isSignal") )
        {
	  string signal_metadata_name = m_skipNominal? "signal_metadata_CBK_skipNominal" : "signal_metadata_CBK" ;
          m_metaDataSignalCBK = new TTree ( signal_metadata_name.c_str(), signal_metadata_name.c_str() );
          m_metaDataSignalCBK->SetDirectory (outputFile);
	  m_metaDataSignalCBK->Branch("SampleID", &sampleID_CBK);
          m_metaDataSignalCBK->Branch("ProcessID", &processID_CBK);
          m_metaDataSignalCBK->Branch("EventWeight", &eventWeight_CBK);
	}
    }
  
  //Read the CutBookkeeper container
   
  const xAOD::CutBookkeeperContainer* bks = nullptr;
  if ( m_event->retrieveMetaInput(bks, "CutBookkeepers").isSuccess() )
    {
      const xAOD::CutBookkeeper* all = 0; int maxCycle=-1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents

      for(auto cbk : *bks) {
	
	if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle) {
	  maxCycle=cbk->cycle();
	  all = cbk; 
	} 
	
	// LHE3 weight systematics
	if ( wk()->metaData()->castBool( "isMC" ) && cbk->name().find("LHE3Weight_") != string::npos) {
	  EventWeightNameVect.push_back(cbk->name());
	  EventSumVect.push_back(cbk->nAcceptedEvents());
	  EventWeightSumVect.push_back(cbk->sumOfEventWeights());
	  EventWeightSqSumVect.push_back(cbk->sumOfEventWeightsSquared());
	}
	
	// signal sum of weight per subprocess
	if ( wk()->metaData()->castBool( "isMC" ) && wk()->metaData()->castBool("isSignal") && cbk->name().find("SUSYWeight_ID_") != string::npos ) {
	  // ignore inclusive count and empty subprocesses
	  if(cbk->name() == "SUSYWeight_ID_0" || cbk->sumOfEventWeights()==0.) continue;
	  
	  sampleID_CBK = wk()->metaData()->castInteger( "sampleID" );
	  processID_CBK = atoi(cbk->name().substr(14,cbk->name().size()-14).c_str());
	  eventWeight_CBK = cbk->sumOfEventWeights();
	  m_metaDataSignalCBK->Fill();	      
	}
      }
      
      if(all) {
	sampleID = wk()->metaData()->castBool( "isMC" ) ? wk()->metaData()->castInteger( "sampleID" ) : 0;
	EventSum = all->nAcceptedEvents();
	EventWeightSum = all->sumOfEventWeights();
	EventWeightSqSum = all->sumOfEventWeightsSquared();
      }
      
      m_metaData->Fill();
    }  	
  else
    {
      TFile *file = wk()->inputFile();
      string metadata_name = m_skipNominal? "skim_metadata_skipNominal" : "skim_metadata" ;
      TTree * meta_orig = (TTree*)file->Get(metadata_name.c_str());

      if ( meta_orig )
        {
	  int orig_sampleID;
	  double orig_EventSum, orig_EventWeightSum, orig_EventWeightSqSum;
	  vector<string> orig_EventWeightNameVect; vector<unsigned int> orig_EventSumVect;
	  vector<double> orig_EventWeightSumVect; vector<double> orig_EventWeightSqSumVect;
          
	  meta_orig->SetBranchAddress("SampleID", &orig_sampleID);
	  meta_orig->SetBranchAddress("EventSum", &orig_EventSum);
	  meta_orig->SetBranchAddress("EventWeightSum", &orig_EventWeightSum);
	  meta_orig->SetBranchAddress("EventWeightSqSum", &orig_EventWeightSqSum);
	  
	  if ( wk()->metaData()->castBool("isMC") ) {
	    
	    meta_orig->Branch("EventWeightNameVect", &orig_EventWeightNameVect);
	    meta_orig->Branch("EventSumVect", &orig_EventSumVect);
	    meta_orig->Branch("EventWeightSumVect", &orig_EventWeightSumVect);
	    meta_orig->Branch("EventWeightSqSumVect", &orig_EventWeightSqSumVect);
	  }

	  for ( int i = 0; i < meta_orig->GetEntries(); i++ )
            {
	      meta_orig->GetEntry(i);
	      sampleID         = orig_sampleID;
	      EventSum         = orig_EventSum;
	      EventWeightSum   = orig_EventWeightSum;
	      EventWeightSqSum = orig_EventWeightSqSum;
	      
	      if ( wk()->metaData()->castBool("isMC") ) {
		EventWeightNameVect = orig_EventWeightNameVect;
		EventSumVect = orig_EventSumVect;
		EventWeightSumVect = orig_EventWeightSumVect;
		EventWeightSqSumVect = orig_EventWeightSqSumVect;
	      }
	      
	      m_metaData->Fill();
            }
        }
    }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode NtupleVectorWriter :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

 
//    const xAOD::EventInfo* eventInfo = nullptr;
//    CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  
//    const xAOD::EventInfo* eventInfo = 0;
//    if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
//      Error("changeInput()", "Failed to retrieve event info collection. Exiting." );
//     return EL::StatusCode::FAILURE;}

  
//    m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
     m_isMC = wk()->metaData()->castBool( "isMC" );  
    // consistency check
    if(m_isMC != wk()->metaData()->castBool( "isMC" )) {
      Error("changeInput()", "Flag isMC inconsistent. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    cout << " ==========> is MC = " << m_isMC << endl;  
    m_isDebugStream = wk()->metaData()->castBool( "isDebugStream" );

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode NtupleVectorWriter :: initialize ()
{

  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  if ( SusyTauAlgBase::initialize() != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;

  // drop systematics if explicitly required (baseline)
  if ( m_deactivateSyst ) {
    m_systematics.clear();
    m_systematics.push_back(SusyTauSystVariation());
  }
  
  // PMG tool does not handle ill-formed MC weight vectors...
  if(m_isMC && wk()->metaData()->castString("sampleType")!="ttX" && wk()->metaData()->castString("sampleType")!="ttbarV" && wk()->metaData()->castString("sampleType").find("alt")==string::npos)
    if(!s_weightTool.isInitialized()) {
      s_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
      CHECK(s_weightTool.retrieve());
    }


  /*
  // tau trigger matching
  tau_L1_names.push_back("L1_TAU12");
  tau_L1_names.push_back("L1_TAU12IM");
  tau_L1_names.push_back("L1_TAU20IM");
  tau_L1_names.push_back("L1_TAU25IM");
  tau_L1_names.push_back("L1_TAU40");
  tau_L1_names.push_back("L1_TAU60");
  tau_L1_names.push_back("L1_TAU100");
  
  tau_HLT_names.push_back("HLT_tau25_idperf_tracktwo");
  tau_HLT_names.push_back("HLT_tau25_perf_tracktwo");
  tau_HLT_names.push_back("HLT_tau25_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau35_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau35_medium1_tracktwo_L1TAU12IM");
  tau_HLT_names.push_back("HLT_tau40_medium1_tracktwo");  // relies on emulation
  tau_HLT_names.push_back("HLT_tau50_medium1_tracktwo_L1TAU12");
  tau_HLT_names.push_back("HLT_tau60_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau80_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau80_medium1_tracktwo_L1TAU60");
  tau_HLT_names.push_back("HLT_tau125_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau160_medium1_tracktwo");
  tau_HLT_names.push_back("HLT_tau160_medium1_tracktwo_L1TAU100");
  
  if( wk()->metaData()->castString("MC16")==std::string("MC16e") || wk()->metaData()->castString("project_name")==std::string("data18_13TeV") ) {

    tau_HLT_names.push_back("HLT_tau25_idperf_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau25_idperf_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau25_perf_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau25_perf_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau25_medium1_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau25_mediumRNN_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau35_medium1_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau35_mediumRNN_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau35_medium1_tracktwoEF_L1TAU12IM");
    tau_HLT_names.push_back("HLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM");
    tau_HLT_names.push_back("HLT_tau40_medium1_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau40_mediumRNN_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau60_medium1_tracktwoEF");
    tau_HLT_names.push_back("HLT_tau60_mediumRNN_tracktwoMVA");
    tau_HLT_names.push_back("HLT_tau80_medium1_tracktwoEF_L1TAU60");
    tau_HLT_names.push_back("HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60");
    tau_HLT_names.push_back("HLT_tau160_medium1_tracktwoEF_L1TAU100");
    tau_HLT_names.push_back("HLT_tau160_mediumRNN_tracktwoMVA_L1TAU100");
  }
  */

  TFile *outputFile = wk()->getOutputFile (outputName);
  
  for ( auto syst : m_systematics ) {

    if ( !syst.isNominal() && !syst.affectsKinematics() )
      continue;

    if ( syst.isNominal() && m_skipNominal )
      continue; 

    std::cout << "=========== Initialising tree for: " << syst.fullname() << std::endl;

    // Initialising a tree for every systematic
    TTree *tree = new TTree ( syst.fullname().c_str(), syst.fullname().c_str() );
    tree->SetDirectory (outputFile);

    // Filling of the map "syst<->tree"
    m_systMap[syst.m_systset] = tree;

    // Creation of the TreeLeaves struct
    TreeLeaves *leaves = new TreeLeaves();
    // Filling of the map "tree<->leaves"
    m_dataMap[syst.m_systset] = leaves;

    // Registering branches to the tree
    tree->Branch("RunNumber"      ,&leaves->RunNumber);
    tree->Branch("EventNumber"    ,&leaves->EventNumber);
    tree->Branch("LumiBlock"      ,&leaves->LumiBlock);
    tree->Branch("SampleID"       ,&leaves->sampleID);
    tree->Branch("ProcessID"      ,&leaves->processID);
    tree->Branch("isDebugStream"  ,&leaves->isDebugStream);
    tree->Branch("treatAsYear"    ,&leaves->treatAsYear);
    tree->Branch("mcEventWeight"  ,&leaves->mcEventWeight);
    tree->Branch("mcEventWeights" ,&leaves->mcEventWeights);
    //tree->Branch("TauSpinnerWeight" ,&leaves->TauSpinnerWeight);
    tree->Branch("pileupweight"   ,&leaves->pileupweight);
    tree->Branch("pileupweightUp"   ,&leaves->pileupweightUp);
    tree->Branch("pileupweightDown"   ,&leaves->pileupweightDown);    
    tree->Branch("tau_weight"     ,&leaves->tau_weight);
    tree->Branch("tau_medium_weight" ,&leaves->tau_medium_weight);
    tree->Branch("ele_weight"     ,&leaves->ele_weight);
    tree->Branch("mu_weight"      ,&leaves->mu_weight);
    tree->Branch("jvt_weight"      ,&leaves->jvt_weight);
    tree->Branch("bjet_weight"     ,&leaves->bjet_weight);
    tree->Branch("actualIntXing"  ,&leaves->actualIntXing);
    tree->Branch("avgIntXing"     ,&leaves->avgIntXing);
    tree->Branch("nVtx"           ,&leaves->nvtx);
    tree->Branch("PVz"            ,&leaves->PVz); 

    // Trigger branches
    tree->Branch("HLT_xe60_mht", &leaves->HLT_xe60_mht);
    tree->Branch("HLT_xe70_mht", &leaves->HLT_xe70_mht);
    tree->Branch("HLT_xe80_mht_L1XE50", &leaves->HLT_xe80_mht_L1XE50);
    tree->Branch("HLT_xe90_mht_L1XE50", &leaves->HLT_xe90_mht_L1XE50);
    tree->Branch("HLT_xe100_mht_L1XE50", &leaves->HLT_xe100_mht_L1XE50);
    tree->Branch("HLT_xe110_mht_L1XE50", &leaves->HLT_xe110_mht_L1XE50);
    tree->Branch("HLT_xe110_pufit_L1XE50", &leaves->HLT_xe110_pufit_L1XE50);
    tree->Branch("HLT_xe110_pufit_L1XE55", &leaves->HLT_xe110_pufit_L1XE55);
    tree->Branch("HLT_xe110_pufit_xe70_L1XE50", &leaves->HLT_xe110_pufit_xe70_L1XE50);
    tree->Branch("HLT_xe110_pufit_xe65_L1XE50", &leaves->HLT_xe110_pufit_xe65_L1XE50);
    tree->Branch("IsMETTrigPassed", &leaves->IsMETTrigPassed);
    tree->Branch("IsMETTrigPassed_noL1J400", &leaves->IsMETTrigPassed_noL1J400);
    tree->Branch("HLT_jX_prescale", &leaves->HLT_jX_prescale);
    // sbottom
    tree->Branch("L1_2J50_XE40", &leaves->L1_2J50_XE40);
    tree->Branch("L1_XE50", &leaves->L1_XE50);
    tree->Branch("HLT_noalg_L1J400", &leaves->HLT_noalg_L1J400);
    tree->Branch("HLT_mu24_ivarmedium", &leaves->HLT_mu24_ivarmedium);
    tree->Branch("HLT_mu26_ivarmedium", &leaves->HLT_mu26_ivarmedium);
    tree->Branch("HLT_mu26_imedium", &leaves->HLT_mu26_imedium);
    tree->Branch("HLT_mu24_ivarloose", &leaves->HLT_mu24_ivarloose);
    tree->Branch("HLT_mu50", &leaves->HLT_mu50);
    tree->Branch("HLT_xe35", &leaves->HLT_xe35);
    tree->Branch("HLT_j80_bmv2c2060_split_xe60", &leaves->HLT_j80_bmv2c2060_split_xe60);
    tree->Branch("HLT_j60_gsc100_bmv2c1050_split_xe80_mht", &leaves->HLT_j60_gsc100_bmv2c1050_split_xe80_mht);
    tree->Branch("HLT_2j25_gsc45_bmv2c1070_split_xe80_mht", &leaves->HLT_2j25_gsc45_bmv2c1070_split_xe80_mht);
    tree->Branch("HLT_j80_bmv2c1050_split_xe60", &leaves->HLT_j80_bmv2c1050_split_xe60);

    tree->Branch("passDitauMetGRL", &leaves->passDitauMetGRL);
    tree->Branch("passBjetGRL", &leaves->passBjetGRL);
    tree->Branch("passBjetGRLTight", &leaves->passBjetGRLTight);
    tree->Branch("Bjet_PileupWeight", &leaves->Bjet_PileupWeight);
    tree->Branch("Bjet_PileupWeight_up", &leaves->Bjet_PileupWeight_up);
    tree->Branch("Bjet_PileupWeight_down", &leaves->Bjet_PileupWeight_down);

    tree->Branch("L1_XE45", &leaves->L1_XE45);
    tree->Branch("L1_XE55", &leaves->L1_XE55);
    tree->Branch("L1_XE60", &leaves->L1_XE60);
    tree->Branch("L1_TAU12", &leaves->L1_TAU12);
    tree->Branch("L1_2J15_XE55", &leaves->L1_2J15_XE55);
    tree->Branch("L1_TAU40_2TAU12IM_XE40", &leaves->L1_TAU40_2TAU12IM_XE40);
    tree->Branch("L1_TAU20IM_2TAU12IM_J25_2J20_3J12", &leaves->L1_TAU20IM_2TAU12IM_J25_2J20_3J12);
    tree->Branch("L1_TAU20IM_2J20_XE45", &leaves->L1_TAU20IM_2J20_XE45);
    tree->Branch("L1_TAU20IM_2TAU12IM", &leaves->L1_TAU20IM_2TAU12IM);

    tree->Branch("HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45", &leaves->HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45);
    tree->Branch("HLT_tau35_medium1_tracktwo_xe70_L1XE45", &leaves->HLT_tau35_medium1_tracktwo_xe70_L1XE45);
    tree->Branch("HLT_tau35_medium1_tracktwoEF_xe70_L1XE45", &leaves->HLT_tau35_medium1_tracktwoEF_xe70_L1XE45);
    tree->Branch("HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45", &leaves->HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45);

    tree->Branch("HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50", &leaves->HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50);
    tree->Branch("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
    tree->Branch("HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &leaves->HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
    tree->Branch("HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &leaves->HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
    tree->Branch("HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &leaves->HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);

    // ditau trigger list is incomplete
    tree->Branch("HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo", &leaves->HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo);
    tree->Branch("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
    tree->Branch("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);

    tree->Branch("HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12",&leaves->HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12);
    tree->Branch("HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",&leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
    tree->Branch("HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",&leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
    tree->Branch("HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I",&leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
    tree->Branch("HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",&leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
    tree->Branch("HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I",&leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
    tree->Branch("HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",&leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
    tree->Branch("HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I",&leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);


    tree->Branch("muonTriggerSF_HLT_mu24_ivarloose", &leaves-> muonTriggerSF_HLT_mu24_ivarloose);
    tree->Branch("muonTriggerSF_HLT_mu24_ivarmedium", &leaves->muonTriggerSF_HLT_mu24_ivarmedium);
    tree->Branch("muonTriggerSF_HLT_mu26_ivarmedium", &leaves->muonTriggerSF_HLT_mu26_ivarmedium);
    tree->Branch("muonTriggerSF_HLT_mu26_imedium", &leaves->muonTriggerSF_HLT_mu26_imedium);
    tree->Branch("muonTriggerSF_HLT_mu50", &leaves->muonTriggerSF_HLT_mu50);
    tree->Branch("muonTriggerSF_HLT_mu24_ivarloose_OR_HLT_mu50", &leaves->muonTriggerSF_HLT_mu24_ivarloose_OR_HLT_mu50);
    tree->Branch("muonTriggerSF_HLT_mu24_ivarmedium_OR_HLT_mu50", &leaves->muonTriggerSF_HLT_mu24_ivarmedium_OR_HLT_mu50);
    tree->Branch("muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50", &leaves->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50);
    tree->Branch("muonTriggerSF_HLT_mu26_imedium_OR_HLT_mu50", &leaves->muonTriggerSF_HLT_mu26_imedium_OR_HLT_mu50);
    tree->Branch("tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70", &leaves->tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70);

    tree->Branch("LeptonVeto"       ,&leaves->LeptonVeto); //lepton Veto on baseline leptons
    tree->Branch("JetTileVeto"      ,&leaves->JetTileVeto); //preliminary flag for 2015+2016 conditions
    tree->Branch("TauTileVeto"      ,&leaves->TauTileVeto); //preliminary flag for 2015+2016 conditions
    tree->Branch("JetTileVetoLBA52" ,&leaves->JetTileVetoLBA52); //includes special treatment of LBA52 DQ
    tree->Branch("TauTileVetoLBA52" ,&leaves->TauTileVetoLBA52); //includes special treatment of LBA52 DQ
    tree->Branch("badLBTileDQ"      ,&leaves->badLBTileDQ);
    tree->Branch("isBadBatman"      ,&leaves->isBadBatman);
    // maybe not final yet, to be implemented at postprocessing stage for now
    //tree->Branch("isBadTile"        ,&leaves->isBadTile);

    //Tau Branches
      
    tree->Branch("tau_n"             , &leaves->tau_n);
    tree->Branch("tau_nTruthMatched" , &leaves->tau_nTruthMatched);
    tree->Branch("tau_pt"            , &leaves->tau_pt);
    tree->Branch("tau_eta"           , &leaves->tau_eta);
    tree->Branch("tau_phi"           , &leaves->tau_phi);
    tree->Branch("tau_m"             , &leaves->tau_m);
    tree->Branch("tau_charge"        , &leaves->tau_charge);
    tree->Branch("tau_ntracks"       , &leaves->tau_nTracks);
    tree->Branch("tau_width"         , &leaves->tau_width);
    //tree->Branch("tau_n90"           , &leaves->tau_n90);
    tree->Branch("tau_jetpt"         , &leaves->tau_jetpt);
    tree->Branch("tau_BDTJetScoreSigTrans"   , &leaves->tau_BDTJetScoreSigTrans );
    tree->Branch("tau_JetBDTLoose"   , &leaves->tau_JetBDTLoose);
    tree->Branch("tau_JetBDTMedium"  , &leaves->tau_JetBDTMedium);
    tree->Branch("tau_JetBDTTight"   , &leaves->tau_JetBDTTight);
    tree->Branch("tau_RNNJetScoreSigTrans"   , &leaves->tau_RNNJetScoreSigTrans );
    tree->Branch("tau_JetRNNLoose"   , &leaves->tau_JetRNNLoose);
    tree->Branch("tau_JetRNNMedium"  , &leaves->tau_JetRNNMedium);
    tree->Branch("tau_JetRNNTight"   , &leaves->tau_JetRNNTight);
    tree->Branch("tau_BDTEleScoreSigTrans", &leaves->tau_BDTEleScoreSigTrans);
    tree->Branch("tau_TileStatus"    , &leaves->tau_TileStatus); 
    tree->Branch("tau_LBA52status"   , &leaves->tau_LBA52status);
    tree->Branch("tau_jetHadronTruthID", &leaves->tau_jetHadronTruthID);
    tree->Branch("tau_jetPartonTruthID", &leaves->tau_jetPartonTruthID);           
    tree->Branch("tau_leadTrackEta"  , &leaves->tau_leadTrackEta);
    tree->Branch("tau_isTruthMatchedTau"      , &leaves->tau_isTruthMatchedTau);
    tree->Branch("tau_truthMatchedType"      , &leaves->tau_truthMatchedType);
    tree->Branch("tau_truthOrigin"      , &leaves->tau_truthOrigin);
    tree->Branch("tau_truthType"      , &leaves->tau_truthType);
    tree->Branch("tau_resolution", &leaves->tau_resolution);
    tree->Branch("tau_eff_SF"                 , &leaves->tau_effSF);
    tree->Branch("tau_medium_eff_SF"          , &leaves->tau_medium_effSF);

    /*
    for(uint i=0;i<tau_L1_names.size();i++)
      tree->Branch( (std::string("tau_match_") + tau_L1_names[i]).c_str(), &leaves->tau_match_L1[tau_L1_names[i]] );
    
    for(uint i=0;i<tau_HLT_names.size();i++)
      tree->Branch( (std::string("tau_match_") + tau_HLT_names[i]).c_str(), &leaves->tau_match_HLT[tau_HLT_names[i]] );
    */

    // only dump total weight to save space, no longer vector of weights
    if ( syst.isNominal() && !m_deactivateSyst )
    {
	std::cout << "============= Booking efficiency branches =====" << std::endl;
	for ( auto & sys: * CalibrationAlg::WeightSystematics( Phys::ContainerType::Tau ) )
	{
	    std::cout << std::string("tau_weight_") + sys.suffix() << std::endl;
	    //leaves->tau_effSF_syst[sys.m_systset] = std::vector<double>();
	    leaves->tau_effSF_total_syst[sys.m_systset] = 1;
	    //tree->Branch( (std::string("tau_eff_SF_") + sys.suffix()).c_str(), &leaves->tau_effSF_syst[sys.m_systset] );
	    tree->Branch( (std::string("tau_weight_") + sys.suffix()).c_str(), &leaves->tau_effSF_total_syst[sys.m_systset] );

	    std::cout << std::string("tau_medium_weight_") + sys.suffix() << std::endl;
	    //leaves->tau_medium_effSF_syst[sys.m_systset] = std::vector<double>();
	    leaves->tau_medium_effSF_total_syst[sys.m_systset] = 1;
	    //tree->Branch( (std::string("tau_medium_eff_SF_") + sys.suffix()).c_str(), &leaves->tau_medium_effSF_syst[sys.m_systset] );
	    tree->Branch( (std::string("tau_medium_weight_") + sys.suffix()).c_str(), &leaves->tau_medium_effSF_total_syst[sys.m_systset] );
	}
	for ( auto & sys: * CalibrationAlg::WeightSystematics( Phys::ContainerType::Electron ) )
	{
	    std::cout << std::string("ele_weight_") + sys.suffix() << std::endl;
	    //leaves->ele_effSF_syst[sys.m_systset] = std::vector<double>();
	    leaves->ele_effSF_total_syst[sys.m_systset] = 1.0;
	    //tree->Branch( (std::string("ele_eff_SF_") + sys.suffix()).c_str(), &leaves->ele_effSF_syst[sys.m_systset] );
	    tree->Branch( (std::string("ele_weight_") + sys.suffix()).c_str(), &leaves->ele_effSF_total_syst[sys.m_systset] );
	}
	for ( auto & sys: * CalibrationAlg::WeightSystematics( Phys::ContainerType::Muon ) )
	{
	    std::cout << std::string("mu_weight_") + sys.suffix() << std::endl;
	    //leaves->mu_effSF_syst[sys.m_systset] = std::vector<double>();
	    leaves->mu_effSF_total_syst[sys.m_systset] = 1.0;
	    //tree->Branch( (std::string("mu_eff_SF_") + sys.suffix()).c_str(), &leaves->mu_effSF_syst[sys.m_systset] );
	    tree->Branch( (std::string("mu_weight_") + sys.suffix()).c_str(), &leaves->mu_effSF_total_syst[sys.m_systset] );
	}
	for ( auto & sys: * CalibrationAlg::WeightSystematics( Phys::ContainerType::Jet ) )
	{
	    std::cout << std::string("jet_weight_") + sys.suffix() << std::endl;
            //leaves->jet_effSF_syst[sys.m_systset] = std::vector<double>();
            leaves->jet_effSF_total_syst[sys.m_systset] = 1.0;
            //tree->Branch( (std::string("jet_eff_SF_") + sys.suffix()).c_str(), &leaves->jet_effSF_syst[sys.m_systset] );
            tree->Branch( (std::string("jet_weight_") + sys.suffix()).c_str(), &leaves->jet_effSF_total_syst[sys.m_systset] );
	}
	std::cout << "===============================================" << std::endl;
    }

    tree->Branch("tau_delPhiMet"              , &leaves->tau_delPhiMet);     //float
    tree->Branch("tau_mtMet"                  , &leaves->tau_mtMet); //float
    tree->Branch("tau_nPi0"                  , &leaves->tau_nPi0); //float
    tree->Branch("tau_nCharged"                  , &leaves->tau_nCharged); //float
    tree->Branch("tau_PantauDecayMode", &leaves->tau_PantauDecayMode);
    tree->Branch("sumMT"                     , &leaves->sumMT);
    tree->Branch("sumMTJet"                     , &leaves->sumMTJet);
    tree->Branch("sumMTTauJet"                     , &leaves->sumMTTauJet);
    tree->Branch("Mt2"                     , &leaves->Mt2);    
    tree->Branch("Mt2_taumu"                     , &leaves->Mt2_taumu);    
    tree->Branch("Mt2_tauel"                     , &leaves->Mt2_tauel);    
    tree->Branch("Mtaumu"                     , &leaves->Mtaumu);    
    tree->Branch("Mtauel"                     , &leaves->Mtauel);    

    //Jets
    tree->Branch("jet_n",       &leaves->jet_n);
    tree->Branch("jet_n_btag",  &leaves->jet_n_btag);
    tree->Branch("jet_pt",      &leaves->jet_pt);
    tree->Branch("jet_eta",     &leaves->jet_eta);
    tree->Branch("jet_phi",     &leaves->jet_phi);
    tree->Branch("jet_m",       &leaves->jet_m);
    tree->Branch("jet_mtMet",   &leaves->jet_mtMet);
    tree->Branch("jet_mv2c10",  &leaves->jet_mv2c10);
    tree->Branch("jet_mv2c20",  &leaves->jet_mv2c20);
    tree->Branch("jet_isBjet",  &leaves->jet_isBjet);
    tree->Branch("jet_BjetSF",  &leaves->jet_BjetSF);
    tree->Branch("jet_JVTSF",  &leaves->jet_JVTSF);
    tree->Branch("jet_isBadTight",&leaves->jet_isBadTight);
    tree->Branch("jet_jvt",     &leaves->jet_jvt);   
    tree->Branch("jet_delPhiMet",  &leaves->jet_delPhiMet);
    tree->Branch("jet_hadronTruthID", &leaves->jet_hadronTruthID);
    tree->Branch("jet_partonTruthID", &leaves->jet_partonTruthID);
    tree->Branch("jet_width", &leaves->jet_width);
    //tree->Branch("jet_n90", &leaves->jet_n90);
    tree->Branch("jet_TileStatus", &leaves->jet_TileStatus);
    tree->Branch("jet_LBA52status", &leaves->jet_LBA52status);    
    tree->Branch("jet_eleMatch", &leaves->jet_eleMatch);
    tree->Branch("jet_muMatch", &leaves->jet_muMatch);
    tree->Branch("jet_resolution", &leaves->jet_resolution);
    tree->Branch("jet_HLT_resolution", &leaves->jet_HLT_resolution);

    tree->Branch("jet_isBjetMatched", &leaves->jet_isBjetMatched);
    tree->Branch("jet_onlineBtag", &leaves->jet_onlineBtag);
 
    //Electrons
    tree->Branch("ele_n",       &leaves->ele_n);
    tree->Branch("ele_pt",      &leaves->ele_pt);
    tree->Branch("ele_eta",     &leaves->ele_eta);
    tree->Branch("ele_phi",     &leaves->ele_phi);
    tree->Branch("ele_m",       &leaves->ele_m);
    tree->Branch("ele_mtMet",   &leaves->ele_mtMet);
    tree->Branch("ele_charge",  &leaves->ele_charge);
    tree->Branch("ele_isSignal",&leaves->ele_isSignal);
    tree->Branch("ele_effSF"   ,&leaves->ele_effSF);
    tree->Branch("ele_resolution", &leaves->ele_resolution);

    //Muons      
    tree->Branch("mu_n",       &leaves->mu_n);
    tree->Branch("mu_pt",      &leaves->mu_pt);
    tree->Branch("mu_eta",     &leaves->mu_eta);
    tree->Branch("mu_phi",     &leaves->mu_phi);
    tree->Branch("mu_m",       &leaves->mu_m);
    tree->Branch("mu_mtMet",   &leaves->mu_mtMet);
    tree->Branch("mu_charge",  &leaves->mu_charge);
    tree->Branch("mu_author",  &leaves->mu_author);
    tree->Branch("mu_muonType",&leaves->mu_muonType);
    tree->Branch("mu_isSignalExp",&leaves->mu_isSignalExp);
    tree->Branch("mu_isBad"     ,&leaves->mu_isBad);
    tree->Branch("mu_effSF"     ,&leaves->mu_effSF);      
    tree->Branch("mu_resolution", &leaves->mu_resolution);

    // Missing energy branches
    tree->Branch("met",           &leaves->met);
    tree->Branch("met_phi",       &leaves->met_phi);
    tree->Branch("metNoMuon",     &leaves->metNoMuon);
    tree->Branch("metNoMuon_phi", &leaves->metNoMuon_phi);
    tree->Branch("METSigSoftTrk", &leaves->METSigSoftTrk);
    tree->Branch("METSig",        &leaves->METSig);
    tree->Branch("METSigPow3",    &leaves->METSigPow3);
    tree->Branch("METSigSoftSub", &leaves->METSigSoftSub);
    tree->Branch("METSigSoftTrk_seed", &leaves->METSigSoftTrk_seed);
    tree->Branch("METSig_seed",        &leaves->METSig_seed);
    tree->Branch("METSigPow3_seed",    &leaves->METSigPow3_seed);
    tree->Branch("METSigSoftSub_seed", &leaves->METSigSoftSub_seed);
    tree->Branch("METoverPtMean_seed", &leaves->METoverPtMean_seed);
    tree->Branch("METoverPtMean12_seed", &leaves->METoverPtMean12_seed);
    tree->Branch("Nbjet_seed", &leaves->Nbjet_seed);
    tree->Branch("METoverPtMean", &leaves->METoverPtMean);
    tree->Branch("METoverPtMean12", &leaves->METoverPtMean12);

    tree->Branch("meff",         &leaves->meff);
    tree->Branch("ht",           &leaves->ht);

    tree->Branch("truthHT",         &leaves->truthHT);
    tree->Branch("truthMET",         &leaves->truthMET);
    tree->Branch("met_truth",         &leaves->met_truth);
    tree->Branch("met_truth_phi",         &leaves->met_truth_phi);

    // Some systematics weights
    tree->Branch("muR10_muF20Weight", &leaves->muR10_muF20Weight);
    tree->Branch("muR10_muF05Weight", &leaves->muR10_muF05Weight);
    tree->Branch("muR20_muF10Weight", &leaves->muR20_muF10Weight);
    tree->Branch("muR05_muF10Weight", &leaves->muR05_muF10Weight);
    tree->Branch("muR05_muF05Weight", &leaves->muR05_muF05Weight);
    tree->Branch("muR20_muF20Weight", &leaves->muR20_muF20Weight);
    tree->Branch("fsr_muRfac05",      &leaves->fsr_muRfac05);
    tree->Branch("fsr_muRfac20",      &leaves->fsr_muRfac20);
    tree->Branch("alphasUp",          &leaves->alphasUp);
    tree->Branch("alphasDown",        &leaves->alphasDown);
    tree->Branch("Var3cDown",         &leaves->Var3cDown);
    tree->Branch("Var3cUp",           &leaves->Var3cUp);

  } // END loop over all systematics
  return EL::StatusCode::SUCCESS;

}

EL::StatusCode NtupleVectorWriter :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  /*
  double spin_weight = 1.;

  // WARNING: we may have to use TauSpinner on diboson as well...
  if( m_isMC && wk()->metaData()->castBool("isSherpa") && wk()->metaData()->castString("sampleType")=="ttbar" ) {

    // check if the event contains at least a true tau...
    const xAOD::TruthParticleContainer* taus_truth = nullptr;
    if( SusyTauGlobals::s_event->retrieve( taus_truth, "TruthTaus") != EL::StatusCode::SUCCESS) {
      cout << "NtupleVectorWriter :: execute () failed to retrieve TruthTaus container" << endl;
      return EL::StatusCode::FAILURE;
    }
    
    // count true taus from W bosons only
    int tau_fromW = 0; 
    for(auto tau_truth : *taus_truth )
      if(tau_truth->isAvailable< unsigned int >( "classifierParticleOrigin" ))
	// semileptonic
	if(tau_truth->auxdataConst< unsigned int >( "classifierParticleOrigin" ) == 12 
	   // for some reason, ttbar->tautau classified as diboson
	   || tau_truth->auxdataConst< unsigned int >( "classifierParticleOrigin" ) == 43)
	  tau_fromW ++;
    
    if(tau_fromW == 1 || tau_fromW == 2) {
      //cout << endl <<"NtupleVectorWriter number of true taus from W = " << tau_fromW << endl; 
      if(SusyTauGlobals::s_tauSpinnerReader->execute() !=  EL::StatusCode::SUCCESS)
	cout << "TauSpinner reader failed" << endl;
      SusyTauGlobals::s_tauSpinnerWeightTool->getSpinWeight(spin_weight);
    }
    if(tau_fromW > 2) cout << "WARNING: found " << tau_fromW << " true taus from W..." << endl;
  }
  */

  const xAOD::EventInfo* eventInfo = nullptr;
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  
  if ( m_isMC && wk()->metaData()->castBool("isSignal") )
  {
      if(eventInfo->isAvailable<int>( "SUSY_procID" )) processID = eventInfo->auxdataConst<int>("SUSY_procID");
      else {
	int pdgid1, pdgid2;
	const xAOD::TruthParticleContainer* truthP = nullptr;
	CHECK( m_event->retrieve( truthP, "TruthParticles" ) );

	CHECK( SusyTauGlobals::s_susyDef->FindSusyHP(truthP, pdgid1, pdgid2) );
	processID = SUSY::finalState( pdgid1, pdgid2 );
      }
  }

  // semileptonic ttbar: remove dileptonic contribution as we have a dedicated dilepton sample
  if( m_isMC && ( eventInfo->mcChannelNumber()==410501 || eventInfo->mcChannelNumber()==410525 ) ) {
    
    const xAOD::TruthParticleContainer* truthP = nullptr;
    CHECK( m_event->retrieve( truthP, "TruthParticles" ) );
    
    int nWlep = 0;
    for (auto truthParticle: *truthP) {
      if (truthParticle->isW()) {
	if (truthParticle->nChildren() > 0) {
	  if ( truthParticle->child(0)->isLepton())
	    nWlep ++;
	} 
      } 
    }
    
    if(nWlep == 2) return EL::StatusCode::SUCCESS;
    if(nWlep==0 || nWlep>2) cout << "ERROR: Found " << nWlep << " leptonically-decaying W Boson in semileptonic ttbar sample!" << endl;      
  }


  // determine whether we are running jet smearing
  unsigned int Niteration = 1;
  if(m_jetSmear) Niteration = m_numSmear;


  for ( auto syst : m_systematics ) {

    if ( !syst.isNominal() && !syst.affectsKinematics() )
      continue;

    if ( syst.isNominal() && m_skipNominal )
      continue; 
    
    ContainerHandler handler;
    
    for(unsigned int iter = 0; iter < Niteration; iter++) {
      
    CHECK ( handler.GetContainers( syst.m_systset, m_jetSmear? iter : -1, m_tauSmear ) );

    // Apply the cutflow if one is present
    if ( m_cutflow )
      {
	auto output = m_cutflow->ExecuteCuts( handler, m_isMC ? eventInfo->mcEventWeight() : 1.);
	//Check if the cuflow was successfull
	if ( ! output.first )
	  continue;
      }
    
  
    // Looking up the TreeLeaves struct
    TreeLeaves * leaves = m_dataMap[syst.m_systset];
    
    // for MC, use random run number and LB from PRW tool
    leaves->RunNumber   = m_isMC ? SusyTauGlobals::s_susyDef->GetRunNumber() : eventInfo->runNumber();
    leaves->EventNumber = eventInfo->eventNumber();
    leaves->LumiBlock   = m_isMC ? eventInfo->auxdataConst<unsigned int>("RandomLumiBlockNumber") : eventInfo->lumiBlock();
    leaves->treatAsYear = SusyTauGlobals::s_susyDef->treatAsYear();
    leaves->actualIntXing  = SusyTauGlobals::s_susyDef->GetCorrectedActualInteractionsPerCrossing(true);
    leaves->avgIntXing = SusyTauGlobals::s_susyDef->GetCorrectedAverageInteractionsPerCrossing(true); 
    leaves->isDebugStream = m_isDebugStream;

    // special DQ flag for LBA52 cleaning, from hand-made BadRunsList
    // passRunLB=true means LBA52 is misbehaving
    leaves->badLBTileDQ = SusyTauGlobals::s_grl_tile->passRunLB( leaves->RunNumber , leaves->LumiBlock ) ;

    leaves->isBadBatman = eventInfo->auxdataConst<char>("DFCommonJets_isBadBatman");
    
    if(m_isMC) {
      leaves->mcEventWeight  = eventInfo->mcEventWeight();
      if ( syst.isNominal() ) leaves->mcEventWeights = eventInfo->mcEventWeights();
      leaves->truthHT = eventInfo->auxdataConst<float>("GenFiltHT");
      leaves->truthMET = eventInfo->auxdataConst<float>("GenFiltMET");
      leaves->sampleID = eventInfo->mcChannelNumber();
      if ( wk()->metaData()->castBool("isSignal") ) leaves->processID = processID;
    }
    else {
      leaves->mcEventWeight = 1.;
      leaves->sampleID = -999;
    }

    //leaves->TauSpinnerWeight = spin_weight;

    leaves->bjet_weight     = handler.getBTagSF();

    if ( m_isMC )
    {
      if(isfinite(eventInfo->auxdataConst<float>("PileupWeight"))) leaves->pileupweight = eventInfo->auxdataConst<float>("PileupWeight");
      else leaves->pileupweight = 1.;
      if(isfinite(eventInfo->auxdataConst<float>("PileupWeightUp"))) leaves->pileupweightUp = eventInfo->auxdataConst<float>("PileupWeightUp");
      else leaves->pileupweightUp = 1.;

      if(isfinite(eventInfo->auxdataConst<float>("PileupWeightDown"))) leaves->pileupweightDown = eventInfo->auxdataConst<float>("PileupWeightDown");
      else leaves->pileupweightDown = 1.;
    }
    else
    {
      leaves->pileupweight  = 1.0;
    }
    
    const xAOD::VertexContainer* vertices = nullptr;
    CHECK ( m_event->retrieve( vertices, "PrimaryVertices" ) );
    
    leaves->nvtx = 0;
    for ( const auto& vx : *vertices )
      if (vx->vertexType() == xAOD::VxType::PriVtx || vx->vertexType() == xAOD::VxType::PileUp) {
	leaves->nvtx ++;      
      }
    
    const xAOD::Vertex* PV = SusyTauGlobals::s_susyDef->GetPrimVtx();
    // should always find a PV, as we veto events without PV
    leaves->PVz = PV ? PV->z() : -999.;


    // use IsMETTrigPassed instead of checkTrigger, to include the L1_XE50 recovery
    leaves->HLT_xe35 = checkTrigger("HLT_xe35");    
    leaves->HLT_xe60_mht = checkTrigger("HLT_xe60_mht");    
    leaves->HLT_xe70_mht = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe70_mht",true);
    leaves->HLT_xe80_mht_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe80_mht_L1XE50",true);
    leaves->HLT_xe90_mht_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe90_mht_L1XE50",true);
    leaves->HLT_xe100_mht_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe100_mht_L1XE50",true); 
    leaves->HLT_xe110_mht_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe110_mht_L1XE50",true);
    // force OR with L1J400 (not used by default in IsMETTrigPassed for data17-18)
    // data17
    if(leaves->treatAsYear == 2017) {
      leaves->HLT_xe110_pufit_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe110_pufit_L1XE50",true);
      leaves->HLT_xe110_pufit_L1XE55 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe110_pufit_L1XE55",true);
    }
    // data18
    if(leaves->treatAsYear == 2018) {
      leaves->HLT_xe110_pufit_xe70_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe110_pufit_xe70_L1XE50",true);
      leaves->HLT_xe110_pufit_xe65_L1XE50 = SusyTauGlobals::s_susyDef->IsMETTrigPassed("HLT_xe110_pufit_xe65_L1XE50",true);
    }
    leaves->IsMETTrigPassed = SusyTauGlobals::s_susyDef->IsMETTrigPassed(SusyTauGlobals::s_susyDef->GetRunNumber(),true);
    leaves->IsMETTrigPassed_noL1J400 = SusyTauGlobals::s_susyDef->IsMETTrigPassed(SusyTauGlobals::s_susyDef->GetRunNumber(),false);
    leaves->HLT_noalg_L1J400 = checkTrigger("HLT_noalg_L1J400");

    // unless we need individual trigger decisions, we can use a single prescale per event
    leaves->HLT_jX_prescale = eventInfo->auxdataConst< double >( "HLT_jX_prescale" );
    
    // for trigger eff calculations
    leaves->HLT_mu24_ivarloose = checkTrigger("HLT_mu24_ivarloose_L1_MU15");
    leaves->HLT_mu24_ivarmedium = checkTrigger("HLT_mu24_ivarmedium");
    leaves->HLT_mu26_imedium = checkTrigger("HLT_mu26_imedium");
    leaves->HLT_mu26_ivarmedium = checkTrigger("HLT_mu26_ivarmedium");
    leaves->HLT_mu50 = checkTrigger("HLT_mu50");

    leaves->passDitauMetGRL = leaves->treatAsYear==2017? SusyTauGlobals::s_grl_ditauMET->passRunLB( leaves->RunNumber , leaves->LumiBlock ) : false;
    leaves->passBjetGRL = eventInfo->isAvailable<bool>("passBjetGRL") ? eventInfo->auxdataConst<bool>("passBjetGRL") : false;
    leaves->passBjetGRLTight = (leaves->treatAsYear==2016 && eventInfo->isAvailable<bool>("passBjetGRLTight")) ? eventInfo->auxdataConst<bool>("passBjetGRLTight") : leaves->passBjetGRL;
    leaves->Bjet_PileupWeight = eventInfo->isAvailable<float>("BjetPileupWeight") ? eventInfo->auxdataConst<float>("BjetPileupWeight") : 0;
    leaves->Bjet_PileupWeight_up = eventInfo->isAvailable<float>("BjetPileupWeight_up") ? eventInfo->auxdataConst<float>("BjetPileupWeight_up") : 0;
    leaves->Bjet_PileupWeight_down = eventInfo->isAvailable<float>("BjetPileupWeight_down") ? eventInfo->auxdataConst<float>("BjetPileupWeight_down") : 0;

    leaves->HLT_j60_gsc100_bmv2c1050_split_xe80_mht = checkTrigger("HLT_j60_gsc100_bmv2c1050_split_xe80_mht_L1XE60");
    leaves->HLT_2j25_gsc45_bmv2c1070_split_xe80_mht = checkTrigger("HLT_2j25_gsc45_bmv2c1070_split_xe80_mht_L12J15_XE55");
    leaves->HLT_j80_bmv2c1050_split_xe60 = checkTrigger("HLT_j80_bmv2c1050_split_xe60_L12J50_XE40");
    leaves->HLT_j80_bmv2c2060_split_xe60 = checkTrigger("HLT_j80_bmv2c2060_split_xe60_L12J50_XE40");

    leaves->L1_XE45 = checkTrigger("L1_XE45");
    leaves->L1_XE50 = checkTrigger("L1_XE50");
    leaves->L1_XE55 = checkTrigger("L1_XE55");
    leaves->L1_XE60 = checkTrigger("L1_XE60");
    leaves->L1_2J15_XE55 = checkTrigger("L1_2J15_XE55");
    leaves->L1_2J50_XE40 = checkTrigger("L1_2J50_XE40");
    leaves->L1_TAU12 = checkTrigger("L1_TAU12");    
    leaves->L1_TAU40_2TAU12IM_XE40 = checkTrigger("L1_TAU40_2TAU12IM_XE40");
    leaves->L1_TAU20IM_2TAU12IM_J25_2J20_3J12 = checkTrigger("L1_TAU20IM_2TAU12IM_J25_2J20_3J12");
    leaves->L1_TAU20IM_2J20_XE45 = checkTrigger("L1_TAU20IM_2J20_XE45");
    leaves->L1_TAU20IM_2TAU12IM = checkTrigger("L1_TAU20IM_2TAU12IM");

    leaves->HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45 = checkTrigger("HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45");
    leaves->HLT_tau35_medium1_tracktwo_xe70_L1XE45 = checkTrigger("HLT_tau35_medium1_tracktwo_xe70_L1XE45");
    leaves->HLT_tau35_medium1_tracktwoEF_xe70_L1XE45 = checkTrigger("HLT_tau35_medium1_tracktwoEF_xe70_L1XE45");

    leaves->HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50 = checkTrigger("HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50");
    leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = checkTrigger("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50");
    leaves->HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = checkTrigger("HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50");
    leaves->HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 = checkTrigger("HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50");

    // only active for post-TS1 data
    if(leaves->RunNumber >= 355261) {
      leaves->HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45 = checkTrigger("HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45");
      leaves->HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50 = checkTrigger("HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50");
    }
    else {
      leaves->HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45 = false;
      leaves->HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50 = false;
    }

    leaves->HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo = checkTrigger("HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo");
    leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo = checkTrigger("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo");
    leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM = checkTrigger("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM");
      
    leaves->HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12 = checkTrigger("HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12");
    leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = checkTrigger("HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12");
    leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = checkTrigger("HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40");
    leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = checkTrigger("HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I");
    leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = checkTrigger("HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40");
    leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = checkTrigger("HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I");
    // only active for post-TS1 data
    if(leaves->RunNumber >= 355261) {
      leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = checkTrigger("HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40");
      leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = checkTrigger("HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I");
    }
    else {
      leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = false;
      leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = false;
    }

    const xAOD::TauJetContainer tauCont = handler.taus();
    const xAOD::JetContainer jetCont = handler.jets();
    const xAOD::MuonContainer muonCont = handler.signalMuons();    
    const xAOD::ElectronContainer eleCont = handler.signalElectrons();

    if(m_isMC && muonCont.size()>0) {
      leaves->muonTriggerSF_HLT_mu24_ivarloose = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu24_ivarloose_L1MU15");
      leaves->muonTriggerSF_HLT_mu24_ivarmedium = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu24_ivarmedium");
      leaves->muonTriggerSF_HLT_mu26_ivarmedium = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu26_ivarmedium");
      leaves->muonTriggerSF_HLT_mu26_imedium = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu26_imedium");
      leaves->muonTriggerSF_HLT_mu50 = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu50");
      leaves->muonTriggerSF_HLT_mu24_ivarloose_OR_HLT_mu50 = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu24_ivarloose_L1MU15_OR_HLT_mu50");
      leaves->muonTriggerSF_HLT_mu24_ivarmedium_OR_HLT_mu50 = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu24_ivarmedium_OR_HLT_mu50");
      leaves->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50 = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu26_ivarmedium_OR_HLT_mu50");
      leaves->muonTriggerSF_HLT_mu26_imedium_OR_HLT_mu50 = SusyTauGlobals::s_susyDef->GetTotalMuonTriggerSF(muonCont, "HLT_mu26_imedium_OR_HLT_mu50");
    }


    // Large chunk of code to work with weights. Problem - Powheg, Sherpa and MG5 all have diffrent namings, need to handle each case separately.
    // Also, some samples simply lack some weights, have to catch that too. 
    // This part is hard coded and can be a pain to update, but I don't see a better solution at the moment...
    if(m_isMC) {
      if (!m_isSignal) {
        if(SusyTauConfig::SampleType(sampleID)=="ttbar") {
          leaves->muR10_muF20Weight = s_weightTool->getWeight(" muR = 1.0, muF = 2.0 ");         
          leaves->muR10_muF05Weight = s_weightTool->getWeight(" muR = 1.0, muF = 0.5 ");      
          leaves->muR20_muF10Weight = s_weightTool->getWeight(" muR = 2.0, muF = 1.0 ");
          leaves->muR05_muF10Weight = s_weightTool->getWeight(" muR = 0.5, muF = 1.0 ");      
          leaves->muR05_muF05Weight = s_weightTool->getWeight(" muR = 0.5, muF = 0.5 ");     
          leaves->muR20_muF20Weight = s_weightTool->getWeight(" muR = 2.0, muF = 2.0 ");    
          leaves->fsr_muRfac05 =      s_weightTool->getWeight("isr:muRfac=1.0_fsr:muRfac=0.5");   
          leaves->fsr_muRfac20 =      s_weightTool->getWeight("isr:muRfac=1.0_fsr:muRfac=2.0");  
          leaves->alphasUp =          s_weightTool->getWeight(" PDF set = 266000 ");
          leaves->alphasDown =        s_weightTool->getWeight(" PDF set = 265000 ");
          leaves->Var3cDown =         s_weightTool->getWeight("Var3cDown");
          leaves->Var3cUp =           s_weightTool->getWeight("Var3cUp");
        }    
        else if(SusyTauConfig::SampleType(sampleID)=="singletop") {
          leaves->muR10_muF20Weight = s_weightTool->getWeight(" muR = 1.00, muF = 2.00 ");
          leaves->muR10_muF05Weight = s_weightTool->getWeight(" muR = 1.00, muF = 0.50 ");
          leaves->muR20_muF10Weight = s_weightTool->getWeight(" muR = 2.00, muF = 1.00 ");
          leaves->muR05_muF10Weight = s_weightTool->getWeight(" muR = 0.50, muF = 1.00 ");
          leaves->muR05_muF05Weight = s_weightTool->getWeight(" muR = 0.50, muF = 0.50 ");
          leaves->muR20_muF20Weight = s_weightTool->getWeight(" muR = 2.00, muF = 2.00 ");
          leaves->fsr_muRfac05 =      s_weightTool->getWeight("isr:muRfac=1.0_fsr:muRfac=0.5");
          leaves->fsr_muRfac20 =      s_weightTool->getWeight("isr:muRfac=1.0_fsr:muRfac=2.0");
          leaves->alphasUp =          s_weightTool->getWeight(" nominal ");  //not available for singlet
          leaves->alphasDown =        s_weightTool->getWeight(" nominal ");  //not available for singlet
          leaves->Var3cDown =         s_weightTool->getWeight("Var3cDown");
          leaves->Var3cUp =           s_weightTool->getWeight("Var3cUp");
        }
        /*
        else if(SusyTauConfig::SampleType(sampleID)=="ttX" || SusyTauConfig::SampleType(sampleID)=="ttbarV") {
          leaves->muR10_muF20Weight = s_weightTool->getWeight(" muR=0.10000E+01 muF=0.20000E+01 ");
          leaves->muR10_muF05Weight = s_weightTool->getWeight(" muR=0.10000E+01 muF=0.50000E+00 ");
          leaves->muR20_muF10Weight = s_weightTool->getWeight(" muR=0.20000E+01 muF=0.10000E+01 ");
          leaves->muR05_muF10Weight = s_weightTool->getWeight(" muR=0.50000E+00 muF=0.10000E+01 ");
          leaves->muR05_muF05Weight = s_weightTool->getWeight(" muR=0.50000E+00 muF=0.50000E+00 ");
          leaves->muR20_muF20Weight = s_weightTool->getWeight(" muR=0.20000E+01 muF=0.20000E+01 ");
          leaves->fsr_muRfac05 =      s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
          leaves->fsr_muRfac20 =      s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
          leaves->alphasUp =          s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
          leaves->alphasDown =        s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
          leaves->Var3cDown =         s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
          leaves->Var3cUp =           s_weightTool->getWeight(" muR=0.10000E+01 muF=0.10000E+01 "); //not available for ttX, nominal instead
        }
        */
        else if(SusyTauConfig::SampleType(sampleID)=="wenu" || SusyTauConfig::SampleType(sampleID)=="wmunu" || SusyTauConfig::SampleType(sampleID)=="wtaunu" || SusyTauConfig::SampleType(sampleID)=="ztautau" || SusyTauConfig::SampleType(sampleID)=="zee" || SusyTauConfig::SampleType(sampleID)=="znunu" || SusyTauConfig::SampleType(sampleID)=="diboson" || SusyTauConfig::SampleType(sampleID)=="triboson" || SusyTauConfig::SampleType(sampleID)=="zlowmass" || SusyTauConfig::SampleType(sampleID)=="V2jets") {
          leaves->muR10_muF20Weight = s_weightTool->getWeight("MUR1_MUF2_PDF261000");
          leaves->muR10_muF05Weight = s_weightTool->getWeight("MUR1_MUF0.5_PDF261000");
          leaves->muR20_muF10Weight = s_weightTool->getWeight("MUR2_MUF1_PDF261000");
          leaves->muR05_muF10Weight = s_weightTool->getWeight("MUR0.5_MUF1_PDF261000");
          leaves->muR05_muF05Weight = s_weightTool->getWeight("MUR0.5_MUF0.5_PDF261000");
          leaves->muR20_muF20Weight = s_weightTool->getWeight("MUR2_MUF2_PDF261000");
          leaves->fsr_muRfac05 =      s_weightTool->getWeight("Weight");
          leaves->fsr_muRfac20 =      s_weightTool->getWeight("Weight");
          leaves->alphasUp =          s_weightTool->getWeight("MUR1_MUF1_PDF270000");
          leaves->alphasDown =        s_weightTool->getWeight("MUR1_MUF1_PDF269000");
          leaves->Var3cDown =         s_weightTool->getWeight("Weight");
          leaves->Var3cUp =           s_weightTool->getWeight("Weight");
        }
        else if(SusyTauConfig::SampleType(sampleID)=="zmumu") {
          leaves->muR10_muF20Weight = s_weightTool->getWeight("MUR1_MUF2_PDF261000");
          leaves->muR10_muF05Weight = s_weightTool->getWeight("MUR1_MUF0.5_PDF261000");
          leaves->muR20_muF10Weight = s_weightTool->getWeight("MUR2_MUF1_PDF261000");
          leaves->muR05_muF10Weight = s_weightTool->getWeight("MUR0.5_MUF1_PDF261000");
          leaves->muR05_muF05Weight = s_weightTool->getWeight("MUR0.5_MUF0.5_PDF261000");
          leaves->muR20_muF20Weight = s_weightTool->getWeight("MUR2_MUF2_PDF261000");
          leaves->fsr_muRfac05 =      s_weightTool->getWeight("Weight");
          leaves->fsr_muRfac20 =      s_weightTool->getWeight("Weight");
          leaves->alphasUp =          s_weightTool->getWeight("Weight");
          leaves->alphasDown =        s_weightTool->getWeight("Weight");
          leaves->Var3cDown =         s_weightTool->getWeight("Weight");
          leaves->Var3cUp =           s_weightTool->getWeight("Weight");
        }
      } else {
	leaves->muR10_muF20Weight = leaves->mcEventWeight;
	leaves->muR10_muF05Weight = leaves->mcEventWeight;
	leaves->muR20_muF10Weight = leaves->mcEventWeight;
	leaves->muR05_muF10Weight = leaves->mcEventWeight;
	leaves->muR05_muF05Weight = leaves->mcEventWeight;
	leaves->muR20_muF20Weight = leaves->mcEventWeight;
	leaves->fsr_muRfac05 =      leaves->mcEventWeight;
	leaves->fsr_muRfac20 =      leaves->mcEventWeight;
	leaves->alphasUp =          leaves->mcEventWeight;
	leaves->alphasDown =        leaves->mcEventWeight;
	leaves->Var3cDown =         leaves->mcEventWeight;
	leaves->Var3cUp =           leaves->mcEventWeight;
      }
    }


    // MET
    auto metCont = handler.met();
    double metPhi = -999.;
    double met = -999.;
      
    xAOD::MissingETContainer::const_iterator met_it = metCont->find("Final");
    if (met_it != metCont->end())
      {
	leaves->met = (*met_it)->met();
	leaves->met_phi = (*met_it)->phi();
	met = (*met_it)->met();
	metPhi = (*met_it)->phi();	
      }
    else {
      Error( "CalibrationAlg::execute()", "Failed to retrieve MET Final" );
      return EL::StatusCode::FAILURE;
    }

    auto metNoMuonCont = handler.metNoMuon();
    xAOD::MissingETContainer::const_iterator metNoMuon_it = metNoMuonCont->find("Final");
    if (metNoMuon_it != metNoMuonCont->end())
      {
	leaves->metNoMuon = (*metNoMuon_it)->met();
	leaves->metNoMuon_phi = (*metNoMuon_it)->phi();	    
      }
    else {
      Error( "CalibrationAlg::execute()", "Failed to retrieve MET_noMuon Final" );
      return EL::StatusCode::FAILURE;
    }    
    
    leaves->METSigSoftTrk_seed   = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METSigSoftTrk", syst.m_systset) );
    leaves->METSig_seed          = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METSig", syst.m_systset) );
    leaves->METSigPow3_seed      = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METSigPow3", syst.m_systset) );
    leaves->METSigSoftSub_seed   = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METSigSoftSub", syst.m_systset) );
    leaves->METoverPtMean_seed   = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METoverPtMean", syst.m_systset) );
    leaves->METoverPtMean12_seed = eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("METoverPtMean12", syst.m_systset) );
    // have to retrieve it as double
    leaves->Nbjet_seed           = (int) eventInfo->auxdataConst< double >( EventParametersAlg::DecorationName("Nbjet", syst.m_systset) );
    
    double metSignificance = -999;
    if(SusyTauGlobals::s_susyDef->GetMETSig(*metCont, metSignificance) != EL::StatusCode::SUCCESS) {
      cout << "Failed to compute MET significance" << endl;
    }
    leaves->METSigSoftTrk   = metSignificance;
    
    leaves->METSig          = Calculators::CalculateMetSig(&handler);
    leaves->METSigPow3      = Calculators::CalculateMetSigPow3(&handler);
    leaves->METSigSoftSub   = Calculators::CalculateMetSigSoftSub(&handler);
    leaves->METoverPtMean   = Calculators::CalculateMetOverPtMean(&handler); 
    leaves->METoverPtMean12 = Calculators::CalculateMetOverPtMean12(&handler); 

    leaves->LeptonVeto = handler.muons().size()+handler.electrons().size()==0;
    

    //Added by oricken, to be validated
    int iJet = -1;
    // jets
    double mv2c10 = -999., mv2c20 = -999.;

    double sumMTJet = 0.;

    double mindphi_allj = TMath::Pi();
    int index_allj = -1;
    
    // retrieve HLT jets for future trigger-level jet smearing??!
    const xAOD::JetContainer * HLT_jets = nullptr;
    string HLT_jet_name = leaves->RunNumber>=325713 ? "HLT_xAOD__JetContainer_a4tcemsubjesISFS" : "HLT_xAOD__JetContainer_a4tcemsubjesFS";

    // no HLT jet in SUSY5...
    if(m_event->contains<xAOD::JetContainer>(HLT_jet_name)) {
      CHECK( m_event->retrieve( HLT_jets, HLT_jet_name.c_str() ) );
    }
    
    // attempt to flag semi-leptonic b-jets
    xAOD::MuonContainer* muons_baseline = nullptr;
    xAOD::ElectronContainer* electrons_baseline = nullptr;
    CHECK( m_event->retrieve( muons_baseline, CalibrationAlg::ContainerName( Phys::ContainerType::Muon, syst.m_systset ) ) );
    CHECK( m_event->retrieve( electrons_baseline, CalibrationAlg::ContainerName( Phys::ContainerType::Electron, syst.m_systset ) ) );

    leaves->jet_n = jetCont.size();

    for( auto jet : jetCont ) {

      leaves->jet_pt.push_back(jet->pt());
      leaves->jet_eta.push_back(jet->eta());
      leaves->jet_phi.push_back(jet->phi());
      leaves->jet_m.push_back(jet->m());
      
      leaves->jet_isBjet.push_back( jet->auxdataConst< char >("bjet") );
      if ( jet->auxdataConst< char >("bjet") ) leaves->jet_n_btag++;

      jet->btagging()->MVx_discriminant( "MV2c10", mv2c10 );
      leaves->jet_mv2c10.push_back( mv2c10 );
      jet->btagging()->MVx_discriminant( "MV2c20", mv2c20 );
      leaves->jet_mv2c20.push_back( mv2c20 );
      leaves->jet_isBjetMatched.push_back(jet->auxdataConst<char>("isBjetMatched"));
      leaves->jet_onlineBtag.push_back(jet->auxdataConst<float>("onlineBtag"));
      leaves->jet_isBadTight.push_back( jet->auxdataConst< char >("bad_tight") );
      leaves->jet_jvt.push_back( jet->auxdataConst<float>("Jvt") );     
      leaves->jet_width.push_back(jet->auxdataConst<float>("Width"));
      //leaves->jet_n90.push_back(jet->auxdataConst<float>("N90Constituents"));
      leaves->jet_TileStatus.push_back(jet->auxdataConst<unsigned int>("TileStatus"));

      // special treatment for LBA52
      unsigned int LBA52_status = 0;  
      // the special BRL stops at Run 307856, as LBA52 is know to be dead for later runs
      if(leaves->badLBTileDQ || leaves->RunNumber >= 307856)
	LBA52_status = SusyTauGlobals::s_jetTileCorrectionTool->GetLBA52status(jet->jetP4(xAOD::JetConstitScaleMomentum).eta(),
									       jet->jetP4(xAOD::JetConstitScaleMomentum).phi(),0.4);
      leaves->jet_LBA52status.push_back(LBA52_status);

      // semi-leptonic b-jets
      bool jet_eleMatch = false;
      for(auto el : *electrons_baseline) 
	if( el->auxdataConst<char>("baseline") == 1 )
	  if(jet->p4().DeltaR(el->p4()) < 0.4) {
	    jet_eleMatch = true;
	    break;
	  }
      
      bool jet_muMatch = false;
      for(auto mu : *muons_baseline) 
	if( mu->auxdataConst<char>("baseline") == 1 )
	  if(jet->p4().DeltaR(mu->p4()) < 0.4) {
	    jet_muMatch = true;
	    break;
	  }
      
      leaves->jet_eleMatch.push_back(jet_eleMatch);
      leaves->jet_muMatch.push_back(jet_muMatch);
      
      if ( m_isMC )
	{
	  leaves->jet_hadronTruthID.push_back(jet->auxdataConst< int >("HadronConeExclTruthLabelID"));
	  leaves->jet_partonTruthID.push_back(jet->auxdataConst< int >("PartonTruthLabelID"));
	  leaves->jet_BjetSF.push_back(jet->auxdataConst<double>("effscalefact"));
	  leaves->jet_JVTSF.push_back(jet->auxdataConst<float>("jvtscalefact"));
	}
      else
	{
	  leaves->jet_hadronTruthID.push_back(-1);
	  leaves->jet_partonTruthID.push_back(-1);
	  leaves->jet_BjetSF.push_back( 1. );
	  leaves->jet_JVTSF.push_back( 1. );
	}

      double delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(jet->phi()-metPhi)-TMath::Pi())-TMath::Pi());
      double jet_mtMet = TMath::Sqrt(2*jet->pt()*met*(1-TMath::Cos(delPhiMet)));
      leaves->jet_delPhiMet.push_back(delPhiMet);
      leaves->jet_mtMet.push_back(jet_mtMet);
      sumMTJet += jet_mtMet;

      ///////////////////////////////  
      iJet++;

      if(delPhiMet < mindphi_allj) {
	mindphi_allj = delPhiMet;
	index_allj = iJet;
      }
      
      // truth matching
      if ( m_isMC && !m_jetSmear && m_event->contains<xAOD::JetContainer>( "AntiKt4TruthJets" ) )
	{
	  const xAOD::JetContainer jetTruth = handler.jets_truth();

	  bool found = false;
	  for ( auto truth_jet : jetTruth )
	    {
	      if (jet->p4().DeltaR(truth_jet->p4())<0.2) {
		leaves->jet_resolution.push_back( jet->pt()/truth_jet->pt() );
		found = true;
		break;
	      }
	    }
	  if(found == false) leaves->jet_resolution.push_back(-999.);
	}
      else leaves->jet_resolution.push_back(-999.);

      // offline/HLT matching (can access HLT_jet->pt()/truth_jet->pt() via jet_resolution * jet_HLT_resolution)
      bool HLT_match = false;
      if(HLT_jets)
	for ( auto HLT_jet : *HLT_jets ) {
	  if (jet->p4().DeltaR(HLT_jet->p4())<0.2) {
	    leaves->jet_HLT_resolution.push_back( HLT_jet->pt()/jet->pt() );
	    HLT_match = true;
	    break;
	  }
	}
      if(HLT_match == false) leaves->jet_HLT_resolution.push_back(-999.);

    }

    const xAOD::JetContainer alljets = handler.allJets();

    leaves->jvt_weight = m_isMC ? SusyTauGlobals::s_susyDef->JVT_SF(&alljets) : 1.;

    // jet weight systematics
    if (  m_isMC && syst.isNominal() )
      {
	for ( auto & sys: leaves->jet_effSF_total_syst )
	  {
	    double weight = 1.0;

	    ContainerHandler handler_syst;
	    CHECK( handler_syst.RetrieveContainers( sys.first ) );
	    
	    auto jetCont_syst = handler_syst.jets();
	    for ( auto jet_syst : jetCont_syst )
	      {
		// Btagging systematics
		if(sys.first.name().find("FT") != string::npos) {
		  //sys.second.push_back( jet_syst->auxdataConst<double>("effscalefact") );
		  weight *= jet_syst->auxdataConst<double>("effscalefact");
		}
		// JVT systematics
		else if(sys.first.name().find("Jvt") != string::npos) {
		  //sys.second.push_back( jet_syst->auxdataConst<float>("jvtscalefact") );
		  // this should no longer be used!!! as non-signal jets contribute to JVT SF
		  //weight *= jet_syst->auxdataConst<float>("jvtscalefact");
		}
		else {
		  cout << "Unexpected jet weight systematics. Aborting..." << endl; 
		  return EL::StatusCode::FAILURE;
		}
	      }

	    if(sys.first.name().find("FT") != string::npos) {
	      // technically identical to SusyTauGlobals::s_susyDef->BtagSFsys(&jetCont_syst, sys.first)
	      leaves->jet_effSF_total_syst[sys.first] = weight;
	    }
	    else if(sys.first.name().find("Jvt") != string::npos) {
	      const xAOD::JetContainer alljet = handler_syst.allJets();
	      leaves->jet_effSF_total_syst[sys.first] = SusyTauGlobals::s_susyDef->JVT_SFsys(&alljet, sys.first);
	    }
	  }
      }
    
    leaves->sumMTJet = sumMTJet;

    if( index_allj>=0 && leaves->jet_TileStatus.at(index_allj)!=0 ) leaves->JetTileVeto = true;
    if( index_allj>=0 && (leaves->jet_TileStatus.at(index_allj)!=0 || leaves->jet_LBA52status.at(index_allj)!=0) ) leaves->JetTileVetoLBA52 = true;

      
    // taus
    leaves->tau_n = tauCont.size();

    double tau_weight = 1.;
    double tau_medium_weight = 1.;
    double sumMT=0.;
    int tau_number=0;
    int tau_nTruthMatched=0;

    const xAOD::EmTauRoIContainer* LVL1EmTauRoIs = nullptr;
    CHECK( m_event->retrieve( LVL1EmTauRoIs, "LVL1EmTauRoIs" ) );
    
    for ( auto tau : tauCont ) {
      
      leaves->tau_pt.push_back(tau->pt());
      leaves->tau_eta.push_back(tau->eta());
      leaves->tau_phi.push_back(tau->phi());	
      leaves->tau_m.push_back(tau->m());
      leaves->tau_charge.push_back(tau->charge());
      leaves->tau_nTracks.push_back((int)tau->nTracks());
      leaves->tau_nPi0.push_back( tau->nPi0PFOs()  );
      leaves->tau_nCharged.push_back( tau->nChargedPFOs()  );

      leaves->tau_TileStatus.push_back(tau->auxdataConst<unsigned int>("TileStatus"));

      // special treatment for LBA52
      unsigned int LBA52_status = 0;  
      // the special BRL stops at Run 307856, as LBA52 is know to be dead for later runs
      if(leaves->badLBTileDQ || leaves->RunNumber >= 307856)
	LBA52_status = SusyTauGlobals::s_jetTileCorrectionTool->GetLBA52status(tau->eta(), tau->phi(), 0.2);
      leaves->tau_LBA52status.push_back(LBA52_status);

      int iDecayMode = -1;
      if (!tau->panTauDetail(xAOD::TauJetParameters::PanTau_DecayMode, iDecayMode))
 	cout << "Failed to retrieve tau Pantau decay mode" << endl;
      leaves->tau_PantauDecayMode.push_back(iDecayMode);
      
      // retrieve "jet seed" (but EM, no LCTopo)
      bool match = false;
      for (auto jet : alljets)
	if(jet->p4().DeltaR(tau->p4()) < 0.2) {
	  leaves->tau_width.push_back(jet->auxdataConst<float>("Width"));
	  //leaves->tau_n90.push_back(jet->auxdataConst<float>("N90Constituents"));
	  leaves->tau_jetpt.push_back(jet->pt());
	  if( m_isMC ) {
	    leaves->tau_jetHadronTruthID.push_back(jet->auxdataConst< int >("HadronConeExclTruthLabelID"));
	    leaves->tau_jetPartonTruthID.push_back(jet->auxdataConst< int >("PartonTruthLabelID"));
	  }
	  else {
	    leaves->tau_jetHadronTruthID.push_back(-1);
	    leaves->tau_jetPartonTruthID.push_back(-1);  
	  }
	  match = true;
	  break;
	}
      if(!match) {
	leaves->tau_width.push_back(-1.);
	//leaves->tau_n90.push_back(-1.);
	leaves->tau_jetpt.push_back(-1.);
	leaves->tau_jetHadronTruthID.push_back(-1);
	leaves->tau_jetPartonTruthID.push_back(-1);
      }
      
      tau_number++;

      double delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(tau->phi()-metPhi)-TMath::Pi())-TMath::Pi());
      double mTTauMet = TMath::Sqrt(2*tau->pt()*met*(1-TMath::Cos(delPhiMet)));
      
      leaves->tau_delPhiMet.push_back(delPhiMet);    
      leaves->tau_mtMet.push_back( mTTauMet );

      if (tau_number<3 && tauCont.size()>1)
	{
	  sumMT+=mTTauMet;
	}

      // WARNING !!! changed this flag - before, only checked if delPhiMet<mindphi_allj
      if( (delPhiMet<mindphi_allj || delPhiMet<0.2) && tau->auxdataConst<unsigned int>("TileStatus")!=0 ) leaves->TauTileVeto = true;
      if( (delPhiMet<mindphi_allj || delPhiMet<0.2) && (tau->auxdataConst<unsigned int>("TileStatus")!=0 || LBA52_status!=0) ) leaves->TauTileVetoLBA52 = true;


      leaves->tau_BDTJetScoreSigTrans.push_back(tau->discriminant(xAOD::TauJetParameters::BDTJetScoreSigTrans));
      leaves->tau_JetBDTLoose.push_back(tau->isTau(xAOD::TauJetParameters::JetBDTSigLoose));
      leaves->tau_JetBDTMedium.push_back(tau->isTau(xAOD::TauJetParameters::JetBDTSigMedium));
      leaves->tau_JetBDTTight.push_back(tau->isTau(xAOD::TauJetParameters::JetBDTSigTight));     
      leaves->tau_RNNJetScoreSigTrans.push_back(tau->discriminant(xAOD::TauJetParameters::RNNJetScoreSigTrans));
      leaves->tau_JetRNNLoose.push_back(tau->isTau(xAOD::TauJetParameters::JetRNNSigLoose));
      leaves->tau_JetRNNMedium.push_back(tau->isTau(xAOD::TauJetParameters::JetRNNSigMedium));
      leaves->tau_JetRNNTight.push_back(tau->isTau(xAOD::TauJetParameters::JetRNNSigTight));
      
      // not in R21 EDM?
      leaves->tau_BDTEleScoreSigTrans.push_back( tau->auxdataConst<float>("BDTEleScoreSigTrans") );

      /*	
      // tau trigger matching
      for(uint i=0;i<tau_L1_names.size();i++) 
	leaves->tau_match_L1[tau_L1_names[i]].push_back( IsTauTrigMatched_L1(tau_L1_names[i], tau, LVL1EmTauRoIs) ); 

      for(uint i=0;i<tau_HLT_names.size();i++)
	leaves->tau_match_HLT[tau_HLT_names[i]].push_back( IsTauTrigMatched_HLT(tau_HLT_names[i], tau) ); 
      */

      // tau trigger matching and SF not working properly in SUSYTools - to be fixed
      if(SusyTauGlobals::s_susyDef->IsTrigMatched(tau,"HLT_tau35_medium1_tracktwo_xe70_L1XE45")) {
	leaves->tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70.push_back(SusyTauGlobals::s_susyDef->GetTauTriggerEfficiencySF(*tau, "tau35_medium1_tracktwo"));
      }
      else {
	leaves->tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70.push_back(0);
      }
	
      if ( m_isMC && tau->auxdataConst< char >("IsTruthMatched") )
	{
	const xAOD::TruthParticle* truth = xAOD::TauHelpers::getLink<xAOD::TruthParticle>(tau, "truthParticleLink" );	
	if(truth) {	  
	  if ( truth->isTau() && truth->auxdataConst<char>("IsHadronicTau") ){
	      leaves->tau_isTruthMatchedTau.push_back( true );
	      tau_nTruthMatched++;
	  }
	  else
	      leaves->tau_isTruthMatchedTau.push_back( false );
	  
	  if(truth->isAvailable< unsigned int >( "classifierParticleOrigin" ))
	    leaves->tau_truthOrigin.push_back( truth->auxdataConst< unsigned int >( "classifierParticleOrigin" ) );
	  else
	    leaves->tau_truthOrigin.push_back( 0 );

	  if(truth->isAvailable< unsigned int >( "classifierParticleType" ))
	    leaves->tau_truthType.push_back( truth->auxdataConst< unsigned int >( "classifierParticleType" ) );
	  else
	    leaves->tau_truthType.push_back( 0 );

	  if (truth->isAvailable<double>("pt_vis") && truth->auxdataConst<double>("pt_vis")!=0.)
	    leaves->tau_resolution.push_back(tau->pt()/truth->auxdataConst<double>("pt_vis"));
	  else
	    leaves->tau_resolution.push_back(-999.);
	}	
	// protection, in case the link is broken
	else
	  {
	    leaves->tau_isTruthMatchedTau.push_back( false );
	    leaves->tau_resolution.push_back(-999.);
	    leaves->tau_truthOrigin.push_back( 0 );
	    leaves->tau_truthType.push_back( 0 );
	  }
      }
      else
      {
	  leaves->tau_isTruthMatchedTau.push_back( false );
	  leaves->tau_resolution.push_back(-999.);
	  leaves->tau_truthOrigin.push_back( 0 );
	  leaves->tau_truthType.push_back( 0 );
      }

      if ( m_isMC )
	{
	  leaves->tau_truthMatchedType.push_back( SusyTauGlobals::s_tauTruthMatch->getTruthParticleType( *tau ) );

	  leaves->tau_effSF.push_back(tau->auxdataConst<double>("effscalefact"));
	  tau_weight *= tau->auxdataConst<double>("effscalefact");

	  leaves->tau_medium_effSF.push_back(tau->auxdataConst<double>("effscalefact_medium"));
	  tau_medium_weight *= tau->auxdataConst<double>("effscalefact_medium");
	}
      else
	{
	  leaves->tau_truthMatchedType.push_back(0);
	  
	  leaves->tau_effSF.push_back( 1 );
	  leaves->tau_medium_effSF.push_back( 1 );
	}

      //Getting the leading track info
      double lead_pt=-999;
      double lead_eta=-999;
	
      for (uint i=0;i<tau->nTracks();++i) { 
	const xAOD::TauTrack* trk = tau->track(i);
	// do something with the tracks
	if(trk->pt()>lead_pt) {
	  lead_pt = trk->pt();
	  lead_eta = trk->eta();
	}
      }//Track loop
      leaves->tau_leadTrackPt.push_back(lead_pt);
      leaves->tau_leadTrackEta.push_back(lead_eta);	
    }

    // tau weight systematics
    if ( m_isMC && syst.isNominal() )
      {
	for ( auto & sys: leaves->tau_effSF_total_syst )
	  {
	    double weight = 1.0;
	    double weight_medium = 1.0;
	    
	    ContainerHandler handler_syst;
	    CHECK( handler_syst.RetrieveContainers( sys.first ) );
	    
	    auto tausCont_syst = handler_syst.taus();
	    for ( auto tau_syst : tausCont_syst )
	      {
		//sys.second.push_back( tau_syst->auxdataConst<double>("effscalefact") );
		weight *= tau_syst->auxdataConst<double>("effscalefact");
		
		//leaves->tau_medium_effSF_syst[sys.first].push_back( tau_syst->auxdataConst<double>("effscalefact_medium") );
		weight_medium *= tau_syst->auxdataConst<double>("effscalefact_medium");
	      }
	    leaves->tau_effSF_total_syst[sys.first] = weight;
	    leaves->tau_medium_effSF_total_syst[sys.first] = weight_medium;
	  }
      }
    
    leaves->tau_weight = tau_weight;
    leaves->tau_medium_weight = tau_medium_weight;

    leaves->sumMT = sumMT;
    
    if (tauCont.size()>1)
      leaves->Mt2 = asymm_mt2_lester_bisect::get_mT2(tauCont[0]->m(), tauCont[0]->pt()*TMath::Cos(tauCont[0]->phi()), tauCont[0]->pt()*TMath::Sin(tauCont[0]->phi()), 
						     tauCont[1]->m(), tauCont[1]->pt()*TMath::Cos(tauCont[1]->phi()), tauCont[1]->pt()*TMath::Sin(tauCont[1]->phi()), 
						     met*TMath::Cos(metPhi), met*TMath::Sin(metPhi), 0., 0., 0.);
    
    if(tauCont.size()>0 && muonCont.size()>0) {
      leaves->Mt2_taumu = asymm_mt2_lester_bisect::get_mT2(tauCont[0]->m(), tauCont[0]->pt()*TMath::Cos(tauCont[0]->phi()), tauCont[0]->pt()*TMath::Sin(tauCont[0]->phi()), 
							   muonCont[0]->m(), muonCont[0]->pt()*TMath::Cos(muonCont[0]->phi()), muonCont[0]->pt()*TMath::Sin(muonCont[0]->phi()),
							   met*TMath::Cos(metPhi), met*TMath::Sin(metPhi), 0., 0., 0.);

      leaves->Mtaumu = (tauCont[0]->p4()+muonCont[0]->p4()).M();      
    }

    if(tauCont.size()>0 && eleCont.size()>0) {
      leaves->Mt2_tauel = asymm_mt2_lester_bisect::get_mT2(tauCont[0]->m(), tauCont[0]->pt()*TMath::Cos(tauCont[0]->phi()), tauCont[0]->pt()*TMath::Sin(tauCont[0]->phi()), 
							   eleCont[0]->m(), eleCont[0]->pt()*TMath::Cos(eleCont[0]->phi()), eleCont[0]->pt()*TMath::Sin(eleCont[0]->phi()),
							   met*TMath::Cos(metPhi), met*TMath::Sin(metPhi), 0., 0., 0.);

      leaves->Mtauel = (tauCont[0]->p4()+eleCont[0]->p4()).M();      
    }

    leaves->tau_nTruthMatched = tau_nTruthMatched;
    
    leaves->sumMTTauJet = sumMTJet + sumMT;



    //electrons
    leaves->ele_n = eleCont.size();

    double ele_weight = 1.;

    for (auto ele : eleCont) {
      leaves->ele_pt.push_back(ele->pt());
      leaves->ele_eta.push_back(ele->eta());
      leaves->ele_phi.push_back(ele->phi());
      leaves->ele_m.push_back(ele->m());
      leaves->ele_charge.push_back(ele->charge());

      double delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(ele->phi()-metPhi)-TMath::Pi())-TMath::Pi());
      double ele_mtMet = TMath::Sqrt(2*ele->pt()*met*(1-TMath::Cos(delPhiMet)));
      leaves->ele_mtMet.push_back(ele_mtMet);

      if ( m_isMC )
	{
	  const xAOD::TruthParticle* tru_ele;
	  if (ele->auxdataConst < ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink"))
	    {
	      tru_ele=*(ele->auxdataConst < ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink"));
	      leaves->ele_resolution.push_back(ele->pt()/tru_ele->pt());
	    }
	  else leaves->ele_resolution.push_back(-999.);

	  leaves->ele_effSF.push_back(ele->auxdataConst<double>("effscalefact"));
          ele_weight *= ele->auxdataConst<double>("effscalefact");
	}
      else {
	leaves->ele_resolution.push_back(-999.);
	leaves->ele_effSF.push_back(1);
      }
      
      leaves->ele_isSignal.push_back( ele->auxdataConst<char>("signal") );
    }

    leaves->ele_weight = ele_weight;

    // electron weight systematics
    if ( m_isMC && syst.isNominal() )
      {
	for ( auto & sys: leaves->ele_effSF_total_syst )
	  {
	    double weight = 1.0;

	    ContainerHandler handler_syst;
	    CHECK( handler_syst.RetrieveContainers( sys.first ) );
	    
	    auto eleCont_syst = handler_syst.signalElectrons();
	    for ( auto ele_syst : eleCont_syst )
	      {
		//sys.second.push_back( ele_syst->auxdataConst<double>("effscalefact") );
		weight *= ele_syst->auxdataConst<double>("effscalefact");
	      }
	    leaves->ele_effSF_total_syst[sys.first] = weight;
	  }
      }
    

    //muons
    leaves->mu_n = muonCont.size();

    double mu_weight = 1.;
      
    for (auto mu : muonCont)
      {
	leaves->mu_pt.push_back(mu->pt());
	leaves->mu_eta.push_back(mu->eta());
	leaves->mu_phi.push_back(mu->phi());
	leaves->mu_m.push_back(mu->m());
	leaves->mu_charge.push_back(mu->charge());
	leaves->mu_author.push_back(mu->author());
	leaves->mu_muonType.push_back(mu->muonType());

        double delPhiMet = TMath::Abs(TMath::Abs(TMath::Abs(mu->phi()-metPhi)-TMath::Pi())-TMath::Pi());
        double mu_mtMet = TMath::Sqrt(2*mu->pt()*met*(1-TMath::Cos(delPhiMet)));
        leaves->mu_mtMet.push_back(mu_mtMet);

	if ( m_isMC )
	  {
	    const xAOD::TruthParticle* tru_mu;
	    if (mu->auxdataConst < ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink"))
	      {
		tru_mu=*(mu->auxdataConst < ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink"));
		leaves->mu_resolution.push_back(mu->pt()/tru_mu->pt());
	      }  
	    else leaves->mu_resolution.push_back(-999.);

	    leaves->mu_effSF.push_back(mu->auxdataConst<double>("effscalefact"));
	    mu_weight *= mu->auxdataConst<double>("effscalefact");
	  }
	else {
	  leaves->mu_resolution.push_back(-999.);
	  leaves->mu_effSF.push_back( 1. );
	}
	
	//Decoration checks
	leaves->mu_isBad.push_back( mu->auxdataConst<char>("bad") );
	leaves->mu_isSignalExp.push_back( mu->auxdataConst<char>("signal") );
      }
    leaves->mu_weight = mu_weight;

    // muon weight systematics
    if ( m_isMC && syst.isNominal() )
      {
	for ( auto & sys: leaves->mu_effSF_total_syst )
	  {
	    double weight = 1.0;

	    ContainerHandler handler_syst;
	    CHECK( handler_syst.RetrieveContainers( sys.first ) );
	    
	    auto muCont_syst = handler_syst.signalMuons();
	    for ( auto muon_syst : muCont_syst )
	      {
		//sys.second.push_back( muon_syst->auxdataConst<double>("effscalefact") );
		weight *= muon_syst->auxdataConst<double>("effscalefact");
	      }
	    leaves->mu_effSF_total_syst[sys.first] = weight;
	  }
      }
    
    
    if ( m_isMC ) {      
      const xAOD::MissingETContainer* metTruth = handler.met_truth();      
      xAOD::MissingETContainer::const_iterator met_itr = metTruth->begin();
      if (met_itr != metTruth->end())
	{
	  leaves->met_truth = (*met_itr)->met();
	  leaves->met_truth_phi = (*met_itr)->phi();
	}
    }
    
    leaves->meff = Calculators::CalculateMeff(&handler);
    leaves->ht =  Calculators::CalculateHT(&handler);


    // Fill the tree
    m_systMap[syst.m_systset]->Fill();

    //Clear Event
    ClearLeaves(leaves);


    } // end of JetSmearing loop


  }
    
       
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode NtupleVectorWriter :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode NtupleVectorWriter :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
    
  TFile *outputFile = wk()->getOutputFile (outputName);
  outputFile->cd();

  if ( m_cutflow )
    {
      TH1F *cutflow_copy=(TH1F*)m_cutflow->GetHist()->Clone();
      TH1F *cutflow_copy_raw=(TH1F*)m_cutflow->GetHistRaw()->Clone();
      cutflow_copy->SetDirectory (outputFile);
      cutflow_copy_raw->SetDirectory (outputFile);
    }
  
  m_metaData->SetDirectory( outputFile );

  return SusyTauAlgBase::histFinalize();
}


void NtupleVectorWriter::ClearLeaves(TreeLeaves * leaves)
{
  
  //Clear event related variables
 
  leaves->EventNumber    = 0;
  leaves->RunNumber      = 0;
  leaves->treatAsYear    = -999;
  leaves->actualIntXing  = -999;
  leaves->avgIntXing     = -999; 
  leaves->LumiBlock   = 0;
  leaves->isDebugStream = false;
  leaves->badLBTileDQ = false;

  leaves->nvtx = -999;
  leaves->PVz = -999.;
  leaves->mcEventWeight  = -999; 
  leaves->pileupweight   = -999;
  leaves->mcEventWeights.clear();
  //leaves->TauSpinnerWeight = -999;
  leaves->tau_weight = -999;
  leaves->tau_medium_weight = -999;
  leaves->ele_weight = -999;
  leaves->mu_weight = -999;
  leaves->sampleID =-999;

  leaves->bjet_weight = -999;
  leaves->jvt_weight = -999;
  leaves->LeptonVeto = false;
  leaves->JetTileVeto = false;
  leaves->TauTileVeto = false;
  leaves->JetTileVetoLBA52 = false;
  leaves->TauTileVetoLBA52 = false;
  leaves->isBadBatman = false;
  //leaves->isBadTile = false;

  //Clear tau variables
  leaves->tau_n=-999;
  leaves->tau_nTruthMatched=-999;
  leaves-> tau_pt.clear();
  leaves-> tau_eta.clear();
  leaves-> tau_phi.clear();
  leaves-> tau_m.clear();
  leaves-> tau_charge.clear();
  leaves-> tau_nTracks.clear();
  leaves-> tau_width.clear();
  //leaves-> tau_n90.clear();
  leaves-> tau_jetpt.clear();
  leaves-> tau_jetHadronTruthID.clear();
  leaves-> tau_jetPartonTruthID.clear();
  leaves-> tau_BDTJetScoreSigTrans.clear();
  leaves-> tau_JetBDTLoose.clear();
  leaves-> tau_JetBDTMedium.clear();
  leaves-> tau_JetBDTTight.clear();
  leaves-> tau_RNNJetScoreSigTrans.clear();
  leaves-> tau_JetRNNLoose.clear();
  leaves-> tau_JetRNNMedium.clear();
  leaves-> tau_JetRNNTight.clear();
  leaves-> tau_BDTEleScoreSigTrans.clear();
  leaves-> tau_TileStatus.clear();
  leaves-> tau_LBA52status.clear();
  leaves-> tau_PantauDecayMode.clear();
  leaves-> tau_isTruthMatchedTau.clear();
  leaves-> tau_truthMatchedType.clear(); 
  leaves-> tau_truthOrigin.clear();
  leaves-> tau_truthType.clear();

  leaves-> tau_effSF.clear();
  //for ( auto & sys: leaves->tau_effSF_syst ) sys.second.clear();
  for ( auto & sys: leaves->tau_effSF_total_syst ) sys.second = 1.;

  leaves-> tau_medium_effSF.clear();
  //for ( auto & sys: leaves->tau_medium_effSF_syst ) sys.second.clear();
  for ( auto & sys: leaves->tau_medium_effSF_total_syst ) sys.second = 1.;

  /*
  for(uint i=0;i<tau_L1_names.size();i++)
    leaves->tau_match_L1[tau_L1_names[i]].clear();
    
  for(uint i=0;i<tau_HLT_names.size();i++)
    leaves->tau_match_HLT[tau_HLT_names[i]].clear();
  */

  leaves-> tau_leadTrackPt.clear();
  leaves-> tau_leadTrackEta.clear();
  leaves-> tau_delPhiMet.clear();
  leaves-> tau_mtMet.clear();
  leaves-> tau_nPi0.clear();
  leaves-> tau_nCharged.clear();
                                        
  leaves->sumMT=0.;
  leaves->sumMTJet=0.;
  leaves->sumMTTauJet=0.;
  leaves->Mt2=-999.;
  leaves->Mt2_taumu=-999.;
  leaves->Mt2_tauel=-999.;
  leaves->Mtaumu=-999.;
  leaves->Mtauel=-999.;

  leaves->tau_resolution.clear();

  //Clear jet variables
  leaves->jet_n=-999;
  leaves->jet_n_btag=0;
  leaves->jet_pt.clear();
  leaves->jet_eta.clear();
  leaves->jet_phi.clear();
  leaves->jet_m.clear();
  leaves->jet_mtMet.clear();
  leaves->jet_mv2c10.clear();
  leaves->jet_mv2c20.clear();
  leaves->jet_isBjet.clear();
  leaves->jet_isBadTight.clear();
  leaves->jet_BjetSF.clear();
  leaves->jet_JVTSF.clear();
  leaves->jet_jvt.clear();

  //for ( auto & sys: leaves->jet_effSF_syst ) sys.second.clear();
  for ( auto & sys: leaves->jet_effSF_total_syst ) sys.second = 1.;

  leaves->jet_delPhiMet.clear();
  leaves->jet_hadronTruthID.clear();
  leaves->jet_partonTruthID.clear();
  leaves->jet_width.clear();
  //leaves->jet_n90.clear();
  leaves->jet_TileStatus.clear();
  leaves->jet_LBA52status.clear();
  leaves->jet_isBjetMatched.clear();
  leaves->jet_eleMatch.clear();
  leaves->jet_muMatch.clear();
  leaves->jet_resolution.clear();
  leaves->jet_HLT_resolution.clear();
  leaves->jet_onlineBtag.clear();

  //Clear ele variables
  leaves->ele_n=-999;
  leaves->ele_pt.clear();
  leaves->ele_eta.clear();
  leaves->ele_phi.clear();
  leaves->ele_m.clear();
  leaves->ele_mtMet.clear();
  leaves->ele_charge.clear();
  leaves->ele_isSignal.clear();
  leaves->ele_effSF.clear();

  //for ( auto & sys: leaves->ele_effSF_syst ) sys.second.clear();
  for ( auto & sys: leaves->ele_effSF_total_syst ) sys.second = 1.;

  leaves->ele_resolution.clear();

  //Clear Muon variables
  leaves->mu_n =-999;
  leaves->mu_pt.clear();
  leaves->mu_eta.clear();
  leaves->mu_phi.clear();
  leaves->mu_m.clear();
  leaves->mu_mtMet.clear();
  leaves->mu_charge.clear();
  leaves->mu_author.clear();
  leaves->mu_muonType.clear();
  leaves->mu_isBad.clear();
  leaves->mu_isSignalExp.clear();
  leaves->mu_effSF.clear();

  //for ( auto & sys: leaves->mu_effSF_syst ) sys.second.clear();
  for ( auto & sys: leaves->mu_effSF_total_syst ) sys.second = 1.;

  leaves->mu_resolution.clear();

  leaves->HLT_xe35 = false;
  leaves->HLT_xe60_mht = false;
  leaves->HLT_xe70_mht = false;
  leaves->HLT_xe80_mht_L1XE50 = false;
  leaves->HLT_xe90_mht_L1XE50 = false;
  leaves->HLT_xe100_mht_L1XE50 = false;
  leaves->HLT_xe110_mht_L1XE50 = false;
  leaves->HLT_xe110_pufit_L1XE50 = false;
  leaves->HLT_xe110_pufit_L1XE55 = false;
  leaves->HLT_xe110_pufit_xe70_L1XE50 = false;
  leaves->HLT_xe110_pufit_xe65_L1XE50 = false;
  leaves->IsMETTrigPassed = false;
  leaves->IsMETTrigPassed_noL1J400 = false;
  leaves->HLT_jX_prescale = -999.;
  leaves->HLT_j80_bmv2c2060_split_xe60 = false;
  leaves->HLT_mu50 = false;
  leaves->L1_2J50_XE40 = false;
  leaves->L1_XE50 = false;
  leaves->HLT_noalg_L1J400 = false;
  leaves->HLT_mu24_ivarmedium = false;
  leaves->HLT_mu26_ivarmedium = false;
  leaves->HLT_mu26_imedium = false;
  leaves->HLT_mu24_ivarloose = false;
  leaves->passDitauMetGRL = false;
  leaves->passBjetGRL = false;
  leaves->passBjetGRLTight = false;
  leaves->Bjet_PileupWeight = 0.;
  leaves->Bjet_PileupWeight_up = 0.;
  leaves->Bjet_PileupWeight_down = 0.;

  leaves->HLT_j60_gsc100_bmv2c1050_split_xe80_mht = false;
  leaves->HLT_2j25_gsc45_bmv2c1070_split_xe80_mht = false;
  leaves->HLT_j80_bmv2c1050_split_xe60 = false;

  leaves->L1_XE45  = false; 
  leaves->L1_XE55  = false;
  leaves->L1_XE60  = false;
  leaves->L1_TAU12  = false;
  leaves->L1_2J15_XE55  = false;
  leaves->L1_TAU40_2TAU12IM_XE40  = false;
  leaves->L1_TAU20IM_2TAU12IM_J25_2J20_3J12  = false;
  leaves->L1_TAU20IM_2J20_XE45  = false;
  leaves->L1_TAU20IM_2TAU12IM  = false;

  leaves->HLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45 = false;
  leaves->HLT_tau35_medium1_tracktwo_xe70_L1XE45 = false;
  leaves->HLT_tau35_medium1_tracktwoEF_xe70_L1XE45 = false;
  leaves->HLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45 = false;

  leaves->HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50 = false;
  leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50  = false;
  leaves->HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50  = false;
  leaves->HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50  = false;
  leaves->HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50  = false;

  leaves->HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo  = false;
  leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo  = false;
  leaves->HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM  = false;

  leaves->HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12 = false;
  leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = false;
  leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = false;
  leaves->HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = false;
  leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = false;
  leaves->HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = false;
  leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = false;
  leaves->HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = false;


  leaves->muonTriggerSF_HLT_mu50 = 0.;
  leaves->muonTriggerSF_HLT_mu24_ivarloose = 0.;
  leaves->muonTriggerSF_HLT_mu24_ivarmedium = 0.;
  leaves->muonTriggerSF_HLT_mu26_ivarmedium = 0.;
  leaves->muonTriggerSF_HLT_mu26_imedium = 0.;
  leaves->muonTriggerSF_HLT_mu24_ivarloose_OR_HLT_mu50 = 0.;
  leaves->muonTriggerSF_HLT_mu24_ivarmedium_OR_HLT_mu50 = 0.;
  leaves->muonTriggerSF_HLT_mu26_ivarmedium_OR_HLT_mu50 = 0.;
  leaves->muonTriggerSF_HLT_mu26_imedium_OR_HLT_mu50 = 0.;
  leaves->tau_TriggerSF_HLT_tau35_medium1_tracktwo_xe70.clear(); 

  leaves->muR10_muF20Weight = -999.;
  leaves->muR10_muF05Weight = -999.;
  leaves->muR20_muF10Weight = -999.;
  leaves->muR05_muF10Weight = -999.;
  leaves->muR05_muF05Weight = -999.;
  leaves->muR20_muF20Weight = -999.;
  leaves->fsr_muRfac05 =      -999.;
  leaves->fsr_muRfac20 =      -999.;
  leaves->alphasUp =          -999.;
  leaves->alphasDown =        -999.;
  leaves->Var3cDown =         -999.;
  leaves->Var3cUp =           -999.;


  leaves->met = -999.;
  leaves->met_phi = -999.;
  leaves->metNoMuon = -999.;
  leaves->metNoMuon_phi  = -999.;
  leaves->METSigSoftTrk = -999.;
  leaves->METSig = -999.;
  leaves->METSigPow3 = -999.;
  leaves->METSigSoftSub = -999.;
  leaves->METoverPtMean = -999;
  leaves->METoverPtMean12 = -999;
  leaves->METSigSoftTrk_seed = -999.;
  leaves->METSig_seed = -999.;
  leaves->METSigPow3_seed = -999.;
  leaves->METSigSoftSub_seed = -999.;
  leaves->METoverPtMean_seed = -999;
  leaves->METoverPtMean12_seed = -999;
  leaves->Nbjet_seed = -1;
  leaves->ht = -999.;
  leaves->meff = -999.;
  leaves->truthHT = -999.;
  leaves->truthMET = -999.;
  leaves->met_truth = -999.;
  leaves->met_truth_phi = -999.;
}


bool NtupleVectorWriter::IsTauTrigMatched_HLT(string trigger, const xAOD::TauJet* tau, const xAOD::EmTauRoIContainer* L1taus) 
{  
  unsigned int condition = TrigDefs::Physics | TrigDefs::allowResurrectedDecision;
  
  // HACK NEEDED FOR TAU40
  bool tau40 = (trigger.find("tau40")!=string::npos);
  if(tau40) {
    // require that the tau matches L1_TAU25IM RoI
    if (!IsTauTrigMatched_L1("L1_TAU25IM", tau, L1taus) ) return false;
    // require that the tau matches tau35 HLT tau with pt > 40 GeV
    boost::replace_all(trigger, "tau40", "tau35");
  }

  if(SusyTauGlobals::s_susyDef->IsTrigPassed(trigger, condition)) {
    auto features = SusyTauGlobals::s_susyDef->GetTriggerFeatures(trigger, condition );
    auto tau_features = features.containerFeature<xAOD::TauJetContainer>("TrigTauRecMerged");
    if (tau_features.size() == 0) cout << "WARNING!!! " << trigger << " has no features of type xAOD::TauJetContainer" << endl; 
    
    for (auto &tauContainer : tau_features) {
      if (tauContainer.cptr()) {
	for (auto tauHLT : *tauContainer.cptr()) {
	  if(tauHLT->p4().DeltaR(tau->p4()) < 0.2) {

	    // tau40 hack
	    if(tau40 && tauHLT->pt()<40000.) continue;

	    return true;
	  }
	}
      }
    }
  }
  
  return false;
}

bool NtupleVectorWriter::IsTauTrigMatched_L1(string trigger, const xAOD::TauJet* tau, const xAOD::EmTauRoIContainer* L1taus) 
{
  // HACK NEEDED FOR L1_TAU25IM
  bool tau25 = (trigger.find("TAU25")!=string::npos);
  if(tau25) {
    // require that the tau matches L1_TAU20M RoI with pt > 25 GeV
    boost::replace_all(trigger, "TAU25", "TAU20");
  }

 if(SusyTauGlobals::s_susyDef->IsTrigPassed(trigger, (TrigDefs::Physics | TrigDefs::allowResurrectedDecision) )) {
 
    string thresh = trigger;
    boost::replace_all(thresh, "L1_TAU", "");
    boost::replace_all(thresh, "IM", "");
    float ptcut = atof( thresh.c_str() );

    if(tau25) ptcut = 25.;

    bool do_isolation = (trigger.find("IM") != string::npos);
   
    for (auto L1tau : *L1taus) {
      
      if(L1tau->roiType() != xAOD::EmTauRoI::TauRoIWord) continue;

      if(L1tau->tauClus() < ptcut*1000.) continue;

      if(do_isolation && (L1tau->tauClus() < 60000.)) {
	if(L1tau->emIsol() > 2000. + 0.1*L1tau->tauClus()) continue;
      }

      double dEta = L1tau->eta() - tau->eta();
      double dPhi = L1tau->phi() - tau->phi();
      if (dPhi > M_PI) dPhi -= 2. * M_PI;
      else if (dPhi < -M_PI) dPhi += 2. * M_PI;
      double dR = sqrt(dEta * dEta + dPhi * dPhi);
      if(dR<0.3) return true;
    }
  }
 
 return false;
}
