#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <SusyTauAnalysisXAOD/SkimWriter.h>
#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SusyTauAnalysisXAOD/ContainerHandler.h>
#include <SusyTauAnalysisXAOD/Cutflows/SusyOneTauBaselineCutflow.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include <TFile.h>

#include <algorithm>

static const char* APP_NAME = "SkimWriter";

// this is needed to distribute the algorithm to the workers
ClassImp(SkimWriter)

SkimWriter::SkimWriter (  ) : SusyTauAlgBase(),
    outputName(  )
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

SkimWriter::SkimWriter ( std::string skim_name, 
			 std::string cutflow_name, std::vector<std::string> cutflow_config ) : 
    SusyTauAlgBase( cutflow_name, cutflow_config),
    outputName( skim_name )
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}


EL::StatusCode SkimWriter :: setupJob (EL::Job& job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.
    
    SusyTauAlgBase::setupJob (job);

    auto opts = SusyTauGlobals::s_config.GetOptions();
    containerNames = opts["skimContainers"].as<std::vector< std::string > >();
    containerMcNames = opts["skimMcContainers"].as<std::vector< std::string > >();
    
    // define an xAOD output
    EL::OutputStream output(outputName, "xAOD");
    job.outputAdd (output);
    // define an xAOD metadata output
    EL::OutputStream outputMeta("metadata_" + outputName);
    job.outputAdd (outputMeta);
    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode SkimWriter :: histInitialize ()
{
  if( SusyTauAlgBase::histInitialize() != EL::StatusCode::SUCCESS ) {
    Error("SkimWriter::histInitialize()","SusyTauAlgBase::histInitialize() failed.");
      return EL::StatusCode::FAILURE;
    }
    
    // No TrigNavigation in SUSY11
    if ( wk()->metaData()->castInteger( "derivationNum" ) != 11 )
    {
	containerNames.push_back( "TrigNavigation" );
    }

    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode SkimWriter :: changeInput (bool firstFile)
{
  // is this needed?
  //TFile *outputFile = wk()->getOutputFile (outputName);
    TFile *outputMetaFile = wk()->getOutputFile ("metadata_" + outputName);
    

  if ( firstFile )
  {
      m_event = wk()->xaodEvent();
      m_store = wk()->xaodStore();
      
      m_metaData = new TTree ( "skim_metadata", "skim_metadata" );
      m_metaData->SetDirectory (outputMetaFile);
      
      m_metaData->Branch("SampleID", &sampleID);    
      m_metaData->Branch("EventSum", &EventSum);    
      m_metaData->Branch("EventWeightSum", &EventWeightSum);
      m_metaData->Branch("EventWeightSqSum", &EventWeightSqSum);
      
      m_trig_metatool = new xAODMaker::TriggerMenuMetaDataTool("SusyTauTrigMetaTool");
  }


    //Read the CutBookkeeper container
    const xAOD::CutBookkeeperContainer* completeCBC = nullptr;
    if (m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
	// First, let's find the smallest cycle number,
	// i.e., the original first processing step/cycle
	int minCycle = 10000;
	for ( auto cbk : *completeCBC ) {
	    if ( minCycle > cbk->cycle() ) { minCycle = cbk->cycle(); }
	}
	
	// Now, let's actually find the right one that contains all the needed info...
	const xAOD::CutBookkeeper* allEventsCBK = nullptr;
	for ( auto cbk :  *completeCBC ) {
	    if ( minCycle == cbk->cycle() && cbk->name() == "AllExecutedEvents" ) {
		allEventsCBK = cbk;
		break;
	    }
	}
	
	if(allEventsCBK) {
	    if ( wk()->metaData()->castBool( "isMC" ) )
		sampleID = wk()->metaData()->castInteger( "sampleID" );
	    else
		sampleID = 0;
	    EventSum = allEventsCBK->nAcceptedEvents();
	    EventWeightSum = allEventsCBK->sumOfEventWeights();
	    EventWeightSqSum = allEventsCBK->sumOfEventWeightsSquared();
	}
	m_metaData->Fill();
    }
    else
    {
	TFile *file = wk()->inputFile();
	TTree * meta_orig = (TTree*)file->Get("skim_metadata");
	
	int orig_sampleID;
	double orig_EventSum, orig_EventWeightSum, orig_EventWeightSqSum;
	
	meta_orig->SetBranchAddress("SampleID", &orig_sampleID);
	meta_orig->SetBranchAddress("EventSum", &orig_EventSum);
	meta_orig->SetBranchAddress("EventWeightSum", &orig_EventWeightSum);
	meta_orig->SetBranchAddress("EventWeightSqSum", &orig_EventWeightSqSum);

	for ( int i = 0; i < meta_orig->GetEntries(); i++ )
	{
	    meta_orig->GetEntry(i);
	    sampleID         =  orig_sampleID;
	    EventSum         = orig_EventSum;
            EventWeightSum   = orig_EventWeightSum;
	    EventWeightSqSum = orig_EventWeightSqSum;
	    m_metaData->Fill();
	}
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode SkimWriter :: initialize ()
{
    if( SusyTauAlgBase::initialize() != EL::StatusCode::SUCCESS ) {
      Error("SkimWriter::initialize()","SusyTauAlgBase::initialize() failed.");
      return EL::StatusCode::FAILURE;
    }

    // output xAOD
    TFile *file = wk()->getOutputFile ( outputName );
    CHECK ( m_event->writeTo(file) );
	    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode SkimWriter :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.
    
    ContainerHandler handler;
    
    // Check the trigger if requested 
    for ( auto syst : m_systematics ) {
	handler.RetrieveContainers( syst.m_systset );
        auto output = m_cutflow->ExecuteCuts( handler, 1. );

	const xAOD::EventInfo* eventInfo = nullptr;
	CHECK( m_event->retrieve( eventInfo, "EventInfo") );
	
	if ( output.first )
	{	    
	    for (auto container : containerNames )
	    {
	      CHECK( m_event->copy(container) );
	    }
	    if ( wk()->metaData()->castBool("isMC") )
	    {
		for (auto container : containerMcNames )
		{
		  CHECK( m_event->copy(container) );
		}
	    }
	    m_event->fill();
	    break;
	}

	
	// Skip further systematics
	//break;
	//}
	//else
	//{
	//std::cout << "Failed to select event " << output.second << std::endl;
	//}
    }
    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode SkimWriter :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode SkimWriter :: histFinalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.
    
    
    TFile *file = wk()->getOutputFile (outputName );

    CHECK( m_event->finishWritingTo( file ) );

    delete m_trig_metatool;

    SusyTauAlgBase :: histFinalize ();
    
    return EL::StatusCode::SUCCESS;
}
