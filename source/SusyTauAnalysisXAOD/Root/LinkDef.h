#include <SusyTauAnalysisXAOD/MakeTriggerDecision.h>
#include <SusyTauAnalysisXAOD/JetSmearingAlg.h>
#include <SusyTauAnalysisXAOD/EventParametersAlg.h>
#include <SusyTauAnalysisXAOD/CalibrationAlg.h>
#include <SusyTauAnalysisXAOD/SusyTauAlgBase.h>
#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SusyTauAnalysisXAOD/NtupleVectorWriter.h>
#include <SusyTauAnalysisXAOD/SkimWriter.h>
#include <SusyTauAnalysisXAOD/TauSmearingTool.h>

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif

#ifdef __CINT__
#pragma link C++ class SusyTauAlgBase+;
#pragma link C++ class CalibrationAlg+;
#pragma link C++ class EventParametersAlg+;
#pragma link C++ class NtupleVectorWriter+;
#pragma link C++ class SkimWriter+;
#pragma link C++ class SusyTauConfig::SystematicSettings+;
#pragma link C++ class JetSmearingAlg+;
#pragma link C++ class TauSmearingTool+;
#pragma link C++ class MakeTriggerDecision+;
#endif
