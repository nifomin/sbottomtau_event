#include "SusyTauAnalysisXAOD/SusyTauSystVariation.h"

SusyTauSystVariation::SusyTauSystVariation( CP::SystematicSet systset,
                                            string fullname, string suffix,
                                            bool affKin, bool affWgt)
    : m_systset( systset ),
      m_fullname( fullname ),
      m_suffix( suffix ),
      m_affectsKinematics( affKin ),
      m_affectsWeights( affWgt )
{
}

