#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <SusyTauAnalysisXAOD/MakeTriggerDecision.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SusyTauAnalysisXAOD/ContainerHandler.h>
#include <SusyTauAnalysisXAOD/SusyTauSystVariation.h>
#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include <EventLoop/OutputStream.h>
#include "TTree.h"
#include <iostream>
#include "xAODMetaData/FileMetaData.h"

static const char* APP_NAME = "MakeTriggerDecision";

// this is needed to distribute the algorithm to the workers
ClassImp(MakeTriggerDecision)


//MakeTriggerDecision :: MakeTriggerDecision ()
//{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.  Note that you should only put
// the most basic initialization here, since this method will be
// called on both the submission and the worker node.  Most of your
// initialization code will go into histInitialize() and
// initialize().
//}



EL::StatusCode MakeTriggerDecision :: setupJob (EL::Job& job)
{
  
  if ( SusyTauAlgBase :: setupJob (job) != EL::StatusCode::SUCCESS ) {
    return EL::StatusCode::FAILURE;
  }


  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: histInitialize ()
{

  if ( SusyTauAlgBase::histInitialize() != EL::StatusCode::SUCCESS ) {
    return EL::StatusCode::FAILURE;
  }

  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



  
EL::StatusCode MakeTriggerDecision :: changeInput (bool /*firstFile*/)
{

  xAOD::TEvent* m_event = wk()->xaodEvent();
  
  const xAOD::EventInfo* eventInfo = nullptr;
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  
  m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
  
  if(m_isMC != wk()->metaData()->castBool( "isMC" )) {
    Error("changeInput()", "Flag isMC inconsistent. Exiting." );
    return EL::StatusCode::FAILURE;
  }
  

  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: initialize ()
{
  
  if ( SusyTauAlgBase::initialize() != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;


  m_grl2 = new GoodRunsListSelectionTool("GoodRunsListSelectionTool2");
  
  std::vector<std::string> vecStringGRL2;
  vecStringGRL2.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_BjetHLT.xml"); 	
  vecStringGRL2.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_BjetHLT_Normal2017.xml");
  vecStringGRL2.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190708/data18_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");

  CHECK(m_grl2->setProperty( "GoodRunsListVec", vecStringGRL2));
  CHECK(m_grl2->setProperty("PassThrough", true)); // if true (default) will ignore result of GRL and will just pass all events
//  CHECK(m_grl2->initialize());


  std::vector<std::string> vecStringGRL3;
  vecStringGRL3.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_BjetHLT_Tight.xml");

  m_grl3 = new GoodRunsListSelectionTool("GoodRunsListSelectionTool3");
  CHECK(m_grl3->setProperty( "GoodRunsListVec", vecStringGRL3));
  CHECK(m_grl3->setProperty("PassThrough", true));
//  CHECK(m_grl3->initialize());

  m_prwTool2 = new CP::PileupReweightingTool("prwTool2");
  std::vector<std::string> listOfLumicalcFiles2;  
  std::vector<std::string> prwfiles2;

  if(m_isMC)
    {
      xAOD::TEvent* event = wk()->xaodEvent();
      const xAOD::FileMetaData* fmd = nullptr;
      CHECK( event->retrieveMetaInput( fmd, "FileMetaData" ) );

      std::string amiTag("");
      fmd->value(xAOD::FileMetaData::amiTag, amiTag);
      if ( amiTag.find("r9364")!=string::npos ) mcCampaign = "mc16a";
      else if ( amiTag.find("r10201")!=string::npos ) mcCampaign = "mc16d";
      else if ( amiTag.find("r10724")!=string::npos ) mcCampaign = "mc16e";
      else
	{
	  std::cout<<"Unknown amiTag"<<std::endl;
	  return EL::StatusCode::FAILURE;
	}

      std::string simType;
      if(wk()->metaData()->castBool( "isFullsim" )){simType = "FS";}
      else{simType = "AFII";}
      int sampleID = wk()->metaData()->castBool( "isMC" ) ? wk()->metaData()->castInteger( "sampleID" ) : 0;
      std::string path_to_prws = "dev/PileupReweighting/share/";
      path_to_prws += "DSID" + std::to_string(sampleID/1000) + "xxx/pileup_" + mcCampaign + "_dsid" + std::to_string(sampleID) + "_" + simType + ".root";
//      path_to_prws = "dev/PileupReweighting/share/DSID436xxx/pileup_mc16a_dsid436531_AFII.root";
      prwfiles2.push_back(path_to_prws);
      std::string GRLpath = std::string(getenv("WorkDir_DIR")) + "/../../grls/";
      if(mcCampaign=="mc16a")
	{
	  listOfLumicalcFiles2.push_back(GRLpath +"PHYS_StandardGRL_All_Good_25ns_BjetHLT_297730-311481_OflLumi-13TeV-009.root");
	}
      else if(mcCampaign=="mc16d")
	{
          prwfiles2.push_back(GRLpath +"physics_25ns_BjetHLT_Normal2017.actualMu.OflLumi-13TeV-010.root");
	  listOfLumicalcFiles2.push_back(GRLpath +"physics_25ns_BjetHLT_Normal2017.lumicalc.OflLumi-13TeV-010.root");
	}
      else if(mcCampaign=="mc16e")
	{
          prwfiles2.push_back(GRLpath +"purw.actualMu.2018.root");
          listOfLumicalcFiles2.push_back(GRLpath +"ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root");
	}
    }

  CHECK(asg::setProperty(m_prwTool2,"LumiCalcFiles",listOfLumicalcFiles2));
  CHECK(asg::setProperty(m_prwTool2,"ConfigFiles", prwfiles2 ));
  CHECK(asg::setProperty(m_prwTool2,"DataScaleFactor",1/1.03));
  CHECK(asg::setProperty(m_prwTool2,"DataScaleFactorUP",1./0.99));
  CHECK(asg::setProperty(m_prwTool2,"DataScaleFactorDOWN",1./1.07));
  CHECK(m_prwTool2->initialize());

  //  bTagTool = new BTaggingSelectionTool("BTaggingSelectionTool2");
  //   ANA_CHECK( bTagTool->setProperty("MaxEta", 2.4) );
  //   ANA_CHECK( bTagTool->setProperty("MinPt", 20000.) );
  //  ANA_CHECK( bTagTool->setProperty("JetAuthor", "AntiKt4EMTopoJets") );
  //  ANA_CHECK( bTagTool->setProperty("TaggerName", "MV2c10") );
  //  ANA_CHECK( bTagTool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root" ) );
  //  ANA_CHECK( bTagTool->setProperty("OperatingPoint", "HybBEff_60") );
  //   ANA_CHECK( bTagTool->setProperty("OperatingPoint", "FixedCutBEff_77") );
  //  ANA_CHECK( bTagTool->initialize() );

  //  bTagEffTool = new BTaggingEfficiencyTool("BTaggingEfficiencyTool2");
  //  ANA_CHECK( bTagEffTool->setProperty("JetAuthor", "AntiKt4EMTopoJets") );
  //  ANA_CHECK( bTagEffTool->setProperty("TaggerName", "MV2c10") );
  //  ANA_CHECK( bTagEffTool->setProperty("ScaleFactorFileName", "13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root") );
  //  ANA_CHECK( bTagEffTool->setProperty("OperatingPoint", "HybBEff_60") );
  //   ANA_CHECK( bTagEffTool->setProperty("OperatingPoint", "FixedCutBEff_77") );
  //  ANA_CHECK( bTagEffTool->setProperty("ConeFlavourLabel", true) );
  //  ANA_CHECK( bTagEffTool->initialize() );


  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent* m_event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = nullptr;
  CHECK( m_event->retrieve(eventInfo, "EventInfo") );


  //for(auto weight: SusyTauGlobals::s_weightTool->getWeightNames())
  //{
  //std::cout<<weight<<std::endl;
  //}


  
  //std::cout<<"TEST"<<std::endl;  
  //auto chainGroup3 =  SusyTauGlobals::s_susyDef->GetTrigChainGroup("HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo.*");
  //for(auto &trig : chainGroup3->getListOfTriggers()) {
  //std::cout<<trig<<std::endl;
  //}
  if(!m_isMC)
    {
/*
      eventInfo->auxdecor<bool>("passBjetGRL") = m_grl2->passRunLB(eventInfo->runNumber(), eventInfo->lumiBlock());
      eventInfo->auxdecor<float>("BjetPileupWeight") = 1.;
      eventInfo->auxdecor<bool>("passBjetGRLTight") = m_grl3->passRunLB(eventInfo->runNumber(), eventInfo->lumiBlock());
*/
    }
  else
    {
      eventInfo->auxdecor<bool>("passBjetGRL") = 1;
      eventInfo->auxdecor<bool>("passBjetGRLTight") = 1;

      CP::SystematicSet s; 
      m_prwTool2->applySystematicVariation( s );
      eventInfo->auxdecor<float>("BjetPileupWeight") = m_prwTool2->getCombinedWeight( *eventInfo );
      s.clear(); s.insert( CP::SystematicVariation("PRW_DATASF",1) );
      m_prwTool2->applySystematicVariation( s );
      eventInfo->auxdecor<float>("BjetPileupWeight_up") = m_prwTool2->getCombinedWeight( *eventInfo );
      s.clear(); s.insert(CP::SystematicVariation("PRW_DATASF",-1) );
      m_prwTool2->applySystematicVariation( s );
      eventInfo->auxdecor<float>("BjetPileupWeight_down") = m_prwTool2->getCombinedWeight( *eventInfo );

    }


  if(wk()->metaData()->castInteger("derivationNum")!= 3) return EL::StatusCode::SUCCESS;


  if(mcCampaign == "mc16a" || (eventInfo->runNumber() > 297729 && eventInfo->runNumber() < 311482  ))
    {
      auto baseTrigger = SusyTauGlobals::s_susyDef->GetTrigChainGroup("HLT_j80_bmv2c2060_split_xe60_L12J50_XE40");
      Trig::FeatureContainer fcc = SusyTauGlobals::s_susyDef->GetTriggerFeatures("HLT_j80_bmv2c2060_split_xe60_L12J50_XE40", TrigDefs::Physics );
      Trig::FeatureContainer::combination_const_iterator comb   (fcc.getCombinations().begin());       
      Trig::FeatureContainer::combination_const_iterator combEnd(fcc.getCombinations().end());
      for( ; comb!=combEnd ; ++comb) {
	std::vector< Trig::Feature<xAOD::JetContainer> > jetCollections  = comb->containerFeature<xAOD::JetContainer>("SplitJet");
	std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");
	if(jetCollections.size() != bjetCollections.size()) {
	  std::cout << "ERROR Problem in container size, jets: " << jetCollections.size() << " bjets: "<< bjetCollections.size() << std::endl;	  
	  continue;
	}
 	for(unsigned int i = 0; i<jetCollections.size();i++) 	  
	  { 	    
	    const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(i));
	    const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(i)); 	    	

	    //if(baseTrigger->isPassed()){std::cout<<"MV2C20: "<<hlt_btag->auxdataConst<double>("MV2c20_discriminant")<<std::endl;}
                CHECK(matchJets(35000,hlt_jet->p4(),true,hlt_btag->auxdataConst<double>("MV2c20_discriminant")));

	    if(baseTrigger->isPassed() && hlt_jet->pt() > 80000 && hlt_btag->auxdataConst<double>("MV2c20_discriminant") >-0.0224729 ) 	      
	      {

		CHECK(matchJets(20000,hlt_jet->p4(),false,-2));
	      }
	  }
      }
    }

  if(mcCampaign == "mc16d" || (eventInfo->runNumber() > 325712 && eventInfo->runNumber() < 340454  ))
    {

      auto baseTrigger2 = SusyTauGlobals::s_susyDef->GetTrigChainGroup("HLT_j80_bmv2c1050_split_xe60_L12J50_XE40");
      Trig::FeatureContainer fcc2 = SusyTauGlobals::s_susyDef->GetTriggerFeatures("HLT_j80_bmv2c1050_split_xe60_L12J50_XE40", TrigDefs::Physics );
      Trig::FeatureContainer::combination_const_iterator comb2   (fcc2.getCombinations().begin());
      Trig::FeatureContainer::combination_const_iterator combEnd2(fcc2.getCombinations().end());
      for( ; comb2!=combEnd2 ; ++comb2) {
        std::vector< Trig::Feature<xAOD::JetContainer> > jetCollections  = comb2->containerFeature<xAOD::JetContainer>("SplitJet");
        std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb2->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");
        if(jetCollections.size() != bjetCollections.size()) {
          std::cout << "ERROR Problem in container size, jets: " << jetCollections.size() << " bjets: "<< bjetCollections.size() << std::endl;
          continue;
        }
        for(unsigned int i = 0; i<jetCollections.size();i++)
          {
            const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(i));
            const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(i));
                CHECK(matchJets(35000,hlt_jet->p4(),true,hlt_btag->auxdataConst<double>("MV2c10_discriminant")));

            if(baseTrigger2->isPassed() && hlt_jet->pt() > 80000 && hlt_btag->auxdataConst<double>("MV2c10_discriminant") >0.948 )
              {

                CHECK(matchJets(20000,hlt_jet->p4(),false,-2));
              }
          }
      }
    }


  if(mcCampaign == "mc16e" || (eventInfo->runNumber() > 340455 ))
    {
      auto baseTrigger2 = SusyTauGlobals::s_susyDef->GetTrigChainGroup("HLT_j80_bmv2c1050_split_xe60_L12J50_XE40");
      Trig::FeatureContainer fcc2 = SusyTauGlobals::s_susyDef->GetTriggerFeatures("HLT_j80_bmv2c1050_split_xe60_L12J50_XE40", TrigDefs::Physics );
      Trig::FeatureContainer::combination_const_iterator comb2   (fcc2.getCombinations().begin());
      Trig::FeatureContainer::combination_const_iterator combEnd2(fcc2.getCombinations().end());
      for( ; comb2!=combEnd2 ; ++comb2) {
        std::vector< Trig::Feature<xAOD::JetContainer> > jetCollections  = comb2->containerFeature<xAOD::JetContainer>("SplitJet");
        std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb2->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");
        if(jetCollections.size() != bjetCollections.size()) {
          std::cout << "ERROR Problem in container size, jets: " << jetCollections.size() << " bjets: "<< bjetCollections.size() << std::endl;
          continue;
        }
        for(unsigned int i = 0; i<jetCollections.size();i++)
          {
            const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(i));
            const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(i));
                CHECK(matchJets(35000,hlt_jet->p4(),true,hlt_btag->auxdataConst<double>("MV2c10_discriminant")));

            if(baseTrigger2->isPassed() && hlt_jet->pt() > 80000 && hlt_btag->auxdataConst<double>("MV2c10_discriminant") >0.948 )
              {

                CHECK(matchJets(20000,hlt_jet->p4(),false,-2));
              }
          }
      }



    }
    



  // HybridEff_60 bjets and bjet SFs here...
  //  for ( auto syst : m_systematics )
  //    {
  //      if(!syst.isNominal()) continue;
  //      ContainerHandler handler;
  //      handler.GetContainers(syst.m_systset,-1);
  //      const xAOD::JetContainer jetCont = handler.jets();
  //      for(const xAOD::Jet* jet_itr : jetCont)
  //	{
  //	  float sf = 1.;
  //	  if(bTagTool->accept(*jet_itr))
  //	    {
  //	      jet_itr->auxdecor<char>( "isEff60Bjet" ) = true;
  //	      if(m_isMC)
  //		{
  //		  bTagEffTool->getScaleFactor(*jet_itr,sf);
  //		  jet_itr->auxdecor<float>( "bTag60SF" ) = sf;
  //		}
  //	    }
  //	  else
  //	    {
  //	      jet_itr->auxdecor<char>( "isEff60Bjet" ) = false;
  //	      if(m_isMC)
  //		{
  //		  bTagEffTool->getInefficiencyScaleFactor(*jet_itr,sf);
  //		  jet_itr->auxdecor<float>( "bTag60SF" ) = sf;
  //		}
  //	    }
  //	}
  //    }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: finalize ()
{

  if(m_grl2) {
    delete m_grl2;
    m_grl2 = 0;
  }
  if(m_grl3) {
    delete m_grl3;
    m_grl3 = 0;
  }
  if(m_prwTool2) {
    delete m_prwTool2;
    m_prwTool2 = 0;
  }

  //  if(bTagTool){
  //   delete bTagTool;
  //   bTagTool = 0;
  //  }

  //  if(bTagEffTool){
  //   delete bTagEffTool;
  //   bTagEffTool = 0;
  //  }


  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MakeTriggerDecision :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  
  //  return SusyTauAlgBase::histFinalize();
  return EL::StatusCode::SUCCESS;


}



EL::StatusCode MakeTriggerDecision :: matchJets (double ptcut, const TLorentzVector bjet, bool isAlt, double btag) 
{   
  //check if deltaR(jet, Bjet) < 0.4, set "Matched" to jet if it is. Doing it this way so there is freedom to use different btags later on.
  for ( auto syst : m_systematics ) 
    {         
      //      if(!syst.isNominal()) continue;         
      ContainerHandler handler;     
      CHECK(handler.GetContainers(syst.m_systset,-1));     
      const xAOD::JetContainer jetCont = handler.jets();     
      xAOD::JetContainer::const_iterator jet_itr = jetCont.begin();     
      xAOD::JetContainer::const_iterator jet_end = jetCont.end();     
      for( ; jet_itr != jet_end; ++jet_itr )       
	{ 	
	  if((*jet_itr)->pt() > ptcut) 	  
	    { 	    
	      if((*jet_itr)->p4().DeltaR(bjet)<0.2) //changed to 0.2 following recommendations  
		{
		  //matching with online jets/online trigger bjets
		  if(isAlt)
                    {
                      ( *jet_itr )->auxdecor< float >( "onlineBtag" ) = btag;
                    }
		  else if(!isAlt)
		    {
		      ( *jet_itr )->auxdecor< char >( "isBjetMatched" ) = true;
		    }
		}
	    }       
	}  
    }     
  return EL::StatusCode::SUCCESS; 
}



