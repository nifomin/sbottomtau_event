#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <SusyTauAnalysisXAOD/EventParametersAlg.h>
#include <SusyTauAnalysisXAOD/Calculators/mt2.h>
#include <SusyTauAnalysisXAOD/Calculators/mct.h>
#include <fstream>

static const char* APP_NAME = "EventParametersAlg";

std::map< std::string, std::map< CP::SystematicSet, std::string > > EventParametersAlg :: s_parameters_syst = {};

EventParametersAlg :: EventParametersAlg ():
    SusyTauAlgBase()
{
}

EL::StatusCode EventParametersAlg :: initialize ()
{
  CHECK( SusyTauAlgBase::initialize() );
  
  for ( auto & syst : m_systematics )
    {
      if ( syst.affectsKinematics() && ( syst.affectsType() == Phys::ContainerType::Jet || syst.affectsType() == Phys::ContainerType::Tau ) )
	s_parameters_syst["HT"][syst.m_systset] = std::string("HT") + syst.fullname();
      if ( syst.affectsKinematics() )
	{
	  s_parameters_syst["METSigSoftTrk"][syst.m_systset] = std::string("METSigSoftTrk") + syst.fullname();
	  s_parameters_syst["METSig"][syst.m_systset] = std::string("METSig") + syst.fullname();
	  s_parameters_syst["METSigPow3"][syst.m_systset] = std::string("METSigPow3") + syst.fullname();
	  s_parameters_syst["METSigSoftSub"][syst.m_systset] = std::string("METSigSoftSub") + syst.fullname();
	  s_parameters_syst["METoverPtMean"][syst.m_systset] = std::string("METoverPtMean") + syst.fullname();
	  s_parameters_syst["METoverPtMean12"][syst.m_systset] = std::string("METoverPtMean12") + syst.fullname();
	  s_parameters_syst["Nbjet"][syst.m_systset] = std::string("Nbjet") + syst.fullname();
	  s_parameters_syst["METMEFF"][syst.m_systset] = std::string("METMEFF") + syst.fullname();
	  s_parameters_syst["MEFF"][syst.m_systset] = std::string("MEFF") + syst.fullname();
	  s_parameters_syst["MT2"][syst.m_systset] = std::string("MT2") + syst.fullname();
	  s_parameters_syst["MCT"][syst.m_systset] = std::string("MCT") + syst.fullname();
	  s_parameters_syst["MT_tau1met"][syst.m_systset] = std::string("MT_tau1met") + syst.fullname();
	  s_parameters_syst["MT_tau2met"][syst.m_systset] = std::string("MT_tau2met") + syst.fullname();
	  s_parameters_syst["MT_tau1tau2"][syst.m_systset] = std::string("MT_tau1tau2") + syst.fullname();
	  s_parameters_syst["DelPhi_jet1met"][syst.m_systset] = std::string("DelPhi_jet1met") + syst.fullname();
	  s_parameters_syst["DelPhi_jet2met"][syst.m_systset] = std::string("DelPhi_jet2met") + syst.fullname();
	  s_parameters_syst["DelPhi_tau1met"][syst.m_systset] = std::string("DelPhi_tau1met") + syst.fullname();
	}
    }


  const int Ntrig = 19;
  int threshold[Ntrig] = { 420, 400, 380, 360, 320, 300, 260, 200, 175, 150, 110, 100, 85, 60, 55, 45, 35, 25, 15 };

  for(int i=0; i<Ntrig; i++)
    m_HLT_jX.push_back(Form("HLT_j%i",threshold[i]));

  m_isMC = wk()->metaData()->castBool("isMC") ;


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventParametersAlg :: execute ()
{
    const xAOD::EventInfo* eventInfo = nullptr;
    CHECK( m_event->retrieve( eventInfo, "EventInfo") );
    
    eventInfo->auxdecor< double >( "HLT_jX_prescale" ) = ( m_isMC || (wk()->metaData()->castInteger("derivationNum")!=11) ) ? 1. : CalculatePrescaleWeight(eventInfo->runNumber());
    
    // Actually add the observables as decorations 
    for ( auto syst : m_systematics )
    {    
        CHECK ( m_handler.RetrieveContainers( syst.m_systset ) );
	SystAwareCalculator( &EventParametersAlg::CalculateMetSigSoftTrk, std::string("METSigSoftTrk"),   syst ); 
	SystAwareCalculator( &EventParametersAlg::CalculateMetSig,        std::string("METSig"),   syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMetSigPow3,    std::string("METSigPow3"), syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMetSigSoftSub, std::string("METSigSoftSub"),   syst );

	// NEW
	SystAwareCalculator( &EventParametersAlg::CalculateMetOverPtMean, std::string("METoverPtMean"),   syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMetOverPtMean12, std::string("METoverPtMean12"),   syst );
	SystAwareCalculator( &EventParametersAlg::CalculateNbjet, std::string("Nbjet"),   syst );


	SystAwareCalculator( &EventParametersAlg::CalculateMetMeff,   std::string("METMEFF"),   syst );
	SystAwareCalculator( &EventParametersAlg::CalculateHT,   std::string("HT"),   syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMT2,  std::string("MT2"),  syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMeff, std::string("MEFF"), syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMCT,  std::string("MCT"),  syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMT_tau1met,  std::string("MT_tau1met"),  syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMT_tau2met,  std::string("MT_tau2met"),  syst );
	SystAwareCalculator( &EventParametersAlg::CalculateMT_tau1tau2, std::string("MT_tau1tau2"), syst );
	SystAwareCalculator( &EventParametersAlg::CalculateDelPhi_jet1met, std::string("DelPhi_jet1met"), syst );
	SystAwareCalculator( &EventParametersAlg::CalculateDelPhi_jet2met, std::string("DelPhi_jet2met"), syst );
        SystAwareCalculator( &EventParametersAlg::CalculateDelPhi_tau1met, std::string("DelPhi_tau1met"), syst );
    }
    return EL::StatusCode::SUCCESS;
}


double EventParametersAlg :: CalculateMetSigSoftTrk()
{
  auto metCont = m_handler.met();
  
  double metSignificance = -999;
  if ( SusyTauGlobals::s_susyDef->GetMETSig(*metCont, metSignificance) != EL::StatusCode::SUCCESS) 
    cout << "Failed to compute MET significance" << endl;

  return metSignificance;
}

double EventParametersAlg :: CalculateMetSig()
{  
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
  
  double met = (*metcst_it)->met()/1000;	//Convert to GeV
  double sumet = (*metcst_it)->sumet()/1000;	//Convert to GeV
  
  return met/std::sqrt(sumet);			//In sqrt(GeV) 
}


double EventParametersAlg :: CalculateMetSigPow3()
{
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
  
  double met = (*metcst_it)->met()/1000.;
  double sumet = (*metcst_it)->sumet()/1000.;
  
  return met/std::pow(sumet,1./3.); 
}


double EventParametersAlg :: CalculateMetSigSoftSub()
{
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
  
  double met = (*metcst_it)->met()/1000;	//Convert to GeV
  double sumet = (*metcst_it)->sumet()/1000;	//Convert to GeV
  
  // subtract magical 8 GeV contribution from the soft term
  return (met - 8.)/std::sqrt(sumet);			//In sqrt(GeV)  
}


// suggested for seed selection
double EventParametersAlg :: CalculateMetOverPtMean()
{  
    double ptmean = 0.;

    const xAOD::TauJetContainer tauCont = m_handler.taus();
    for ( auto tau : tauCont )
	ptmean += tau->pt();
    
    const xAOD::JetContainer jetCont = m_handler.jets();
    for ( auto jet : jetCont )
	ptmean += jet->pt();

    if(ptmean>0.)
      ptmean /= (tauCont.size() + jetCont.size());
    
    auto metCont = m_handler.met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met();

    return ptmean>0. ? met / ptmean : 0.; 
}


// older suggestion?
double EventParametersAlg :: CalculateMetOverPtMean12()
{  
    const xAOD::TauJetContainer tauCont = m_handler.taus();
    const xAOD::JetContainer jetCont = m_handler.jets();

    if(tauCont.size() + jetCont.size() < 2) return -1.; 

    double pt1=0., pt2=0.;
    
    for ( auto jet : jetCont ) {
      if(jet->pt()>pt1) {
	pt2 = pt1;
	pt1 = jet->pt(); 
      }
      else if(jet->pt()>pt2) {
	pt2 = jet->pt(); 
      }
    }

    for ( auto tau : tauCont ) {
      if(tau->pt()>pt1) {
	pt2 = pt1;
	pt1 = tau->pt(); 
      }
      else if(tau->pt()>pt2) {
	pt2 = tau->pt(); 
      }
    }

    double ptmean = (pt1 + pt2)/2.;

    auto metCont = m_handler.met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    
    double met = (*metcst_it)->met();

    return ptmean>0. ? met / ptmean : 0.; 
}


int EventParametersAlg :: CalculateNbjet()
{
  auto jetCont = m_handler.jets();

  int Nbjet = 0;

  for( auto jet : jetCont )
    if ( jet->auxdata< char >("bjet") ) Nbjet++;
  
  return Nbjet;
}



double EventParametersAlg :: CalculateMetMeff()
{
  
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
  
  double met = (*metcst_it)->met();
  double meff = CalculateMeff();
  
  return met/meff;
  
}


double EventParametersAlg :: CalculateHT()
{
  
    double ht = 0.;
    // ht calculation
    const xAOD::TauJetContainer tauCont = m_handler.taus();
    for ( auto tau : tauCont )
	ht += tau->pt();
    
    const xAOD::JetContainer jetCont = m_handler.jets();
    for ( auto jet : jetCont )
	ht += jet->pt();
    
    return ht;
  
}



double EventParametersAlg :: CalculateMT2()
{
  
    const xAOD::TauJetContainer tauCont = m_handler.taus();
    auto metCont = m_handler.met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    return Calculators::ComputeMaxMT2( tauCont, **metcst_it, 0., 0. );
}

double EventParametersAlg :: CalculateMeff()
{
    double meff = CalculateHT();
    
    auto metCont = m_handler.met();
    xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");
    meff += (*metcst_it)->met();
    return meff;
}
	
double EventParametersAlg :: CalculateMCT()
{
  return Calculators::ComputeMaxMCT( m_handler.taus() );
}

double EventParametersAlg :: CalculateMT_tau1met()
{
  
 const xAOD::TauJetContainer tauCont = m_handler.taus();
 if( tauCont.size() == 0 ) return -1;
 auto metCont = m_handler.met();
 xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

 double tau_pt = tauCont[0]->pt();
 double tau_phi  = tauCont[0]->phi();

 double met = (*metcst_it)->met();
 double met_phi = (*metcst_it)->phi();

 return std::sqrt(2*tau_pt*met*(1 - std::cos(deltaPhi(tau_phi, met_phi))));
  
}


double EventParametersAlg :: CalculateMT_tau2met()
{
  
 const xAOD::TauJetContainer tauCont = m_handler.taus();
 if( tauCont.size() < 2 ) return -1;
 auto metCont = m_handler.met();
 xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

 double tau_pt = tauCont[1]->pt();
 double tau_phi  = tauCont[1]->phi();

 double met = (*metcst_it)->met();
 double met_phi = (*metcst_it)->phi();

 return std::sqrt(2*tau_pt*met*(1 - std::cos(deltaPhi(tau_phi, met_phi))));
  
}

double EventParametersAlg :: CalculateMT_tau1tau2()
{
  
  const xAOD::TauJetContainer tauCont = m_handler.taus();
  if (tauCont.size() < 2) return -1;

  TLorentzVector tau1;
  tau1.SetPtEtaPhiM(tauCont[0]->pt(), tauCont[0]->eta(), tauCont[0]->phi(), tauCont[0]->pt()/std::cosh(tauCont[0]->eta()));

  TLorentzVector tau2;
  tau2.SetPtEtaPhiM(tauCont[1]->pt(), tauCont[1]->eta(), tauCont[1]->phi(), tauCont[1]->pt()/std::cosh(tauCont[1]->eta()));

  TLorentzVector MT_tautau;
  MT_tautau = tau1+tau2;
  
  return MT_tautau.M();
  
}


double EventParametersAlg :: CalculateDelPhi_jet1met()
{
  
  const xAOD::JetContainer jetCont = m_handler.jets();
  if(jetCont.size() < 1) return -1;
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

  double jet_phi = jetCont[0]->phi();
  double met_phi = (*metcst_it)->phi();
  
  return deltaPhi(jet_phi, met_phi);
  
}


double EventParametersAlg :: CalculateDelPhi_jet2met()
{
  
  const xAOD::JetContainer jetCont = m_handler.jets();
  if(jetCont.size() < 2) return -1;
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

  double jet_phi = jetCont[1]->phi();
  double met_phi = (*metcst_it)->phi();
  
  return deltaPhi(jet_phi, met_phi);
  
}

double EventParametersAlg :: CalculateDelPhi_tau1met()
{
  const xAOD::TauJetContainer tauCont = m_handler.taus();
  if( tauCont.size() == 0 ) return -1;
  auto metCont = m_handler.met();
  xAOD::MissingETContainer::const_iterator metcst_it = metCont->find("Final");

  double tau_phi  = tauCont[0]->phi();
  double met_phi = (*metcst_it)->phi();

  return deltaPhi(tau_phi, met_phi);
}


double EventParametersAlg :: CalculatePrescaleWeight(int RunNumber)
{
  double prescale_weight = 0.;

  std::vector<std::pair<std::string,bool > > triggersPass;
  
  for(auto HLT_jX : m_HLT_jX) {
    // check whether each single-jet trigger has fired
    triggersPass.push_back( std::make_pair (HLT_jX, checkTrigger(HLT_jX)));
  }

  // retrieve HLT jets
  const xAOD::JetContainer * hlt_jet = nullptr;
  // HLT jet container name changed in 2017
  string hlt_jet_name = RunNumber>=325713 ? "HLT_xAOD__JetContainer_a4tcemsubjesISFS" : "HLT_xAOD__JetContainer_a4tcemsubjesFS";
  
  if( ! m_event->retrieve( hlt_jet, hlt_jet_name.c_str() ).isSuccess() ) {
    Error("EventParametersAlg :: CalculatePrescaleWeight()", Form("Failed to retrieve %s", hlt_jet_name.c_str()));
    return -1;
  }
  
  // retrieve the prescale weight for the event
  if(hlt_jet->size()>0) {
    prescale_weight = SusyTauGlobals::s_prescaleTool->getTriggerPrescale(triggersPass, hlt_jet->at(0)->pt()/1000., std::to_string(RunNumber));
  }
  
  return prescale_weight;
}


double EventParametersAlg :: deltaPhi(double phi1, double phi2) {
    
double dPhi=std::fabs(phi1-phi2);
  if (dPhi>M_PI) dPhi=2*M_PI-dPhi;
  return dPhi;
  
}
