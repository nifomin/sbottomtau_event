#include "SusyTauAnalysisXAOD/SusyTauGlobals.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SampleHandler/MetaObject.h"
#include "xAODMetaData/FileMetaData.h"
#include "PathResolver/PathResolver.h"

static const char* APP_NAME = "SusyTauGlobals";

// Must be filled by the submission script
SusyTauConfig::SusyTauConfigParser SusyTauGlobals::s_config;

// SUSY object definitions
ST::SUSYObjDef_xAOD * SusyTauGlobals::s_susyDef;

GoodRunsListSelectionTool * SusyTauGlobals::s_grl;
GoodRunsListSelectionTool * SusyTauGlobals::s_grl_tile;
GoodRunsListSelectionTool * SusyTauGlobals::s_grl_ditauMET;

TauAnalysisTools::TauTruthMatchingTool * SusyTauGlobals::s_tauTruthMatch;
asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> SusyTauGlobals::s_tauSelTool;
TauAnalysisTools::TauEfficiencyCorrectionsTool* SusyTauGlobals::s_tauEffTool;

//TauSpinnerTools::xAODEventReaderTool* SusyTauGlobals::s_tauSpinnerReader; 
//TauSpinnerTools::StandardModelTool* SusyTauGlobals::s_tauSpinnerWeightTool;

// A globally accessble TEvent and TStore
xAOD::TEvent * SusyTauGlobals::s_event;
xAOD::TStore * SusyTauGlobals::s_store;

//Pileup Reweighting (and de-prescaling)
CP::PileupReweightingTool * SusyTauGlobals::s_prwtool;
// dead tile modules
CP::JetTileCorrectionTool* SusyTauGlobals::s_jetTileCorrectionTool;
// now use tool from JetSmearing package
JetSmearing::PreScaleTool* SusyTauGlobals::s_prescaleTool;

JetCleaningTool * SusyTauGlobals::s_jetCleaningTool;




// WARNING: should consider using photons in MET calculation
bool SusyTauGlobals::doPhotons = false;
bool SusyTauGlobals::isMC = false;

EL::StatusCode SusyTauGlobals::InitialiseSusyTools( const SH::MetaObject * metadata,  xAOD::TEvent* event )
{
  SusyTauGlobals::isMC = metadata->castBool("isMC");
  bool isMC = metadata->castBool("isMC");
  bool isFullSim = isMC ? metadata->castBool( "isFullsim" ) : false;

 
  SusyTauGlobals::s_susyDef = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD" );

  CHECK( SusyTauGlobals::s_susyDef->setProperty("DataSource", static_cast<ST::ISUSYObjDef_xAODTool::DataSource>(isMC ? (isFullSim ? ST::ISUSYObjDef_xAODTool::FullSim : ST::ISUSYObjDef_xAODTool::AtlfastII ) : ST::ISUSYObjDef_xAODTool::Data )) );

  std::string project_name = metadata->castString("project_name");
  std::string mcCampaign("");  
  //std::vector<std::string> prwfiles;

  if(isMC)
    {
      const xAOD::FileMetaData* fmd = nullptr; 
      CHECK( event->retrieveMetaInput( fmd, "FileMetaData" ) );

      std::string amiTag("");
      fmd->value(xAOD::FileMetaData::amiTag, amiTag);

      if ( amiTag.find("r9364")!=string::npos ) mcCampaign = "mc16a";       
      else if ( amiTag.find("r10201")!=string::npos ) mcCampaign = "mc16d";
      else if ( amiTag.find("r10724")!=string::npos ) mcCampaign = "mc16e";
      else {
	std::cout << "Unknown amiTag " << amiTag << std::endl;
	return EL::StatusCode::FAILURE;
      }

      // CHECK( SusyTauGlobals::s_susyDef->setProperty("PRWConfigFiles", prwfiles) );
      
      // enables autoconfig, overrides prw config files
      CHECK( SusyTauGlobals::s_susyDef->setBoolProperty("AutoconfigurePRWTool", true) );
      
      // patch for Znunu MC16e with broken metadata
      if( (mcCampaign == "mc16e") && (metadata->castString("HFFilter") != std::string("none")) ) {
	CHECK( SusyTauGlobals::s_susyDef->setProperty("AutoconfigurePRWToolHFFilter", metadata->castString("HFFilter") ) );
      }

    }
  
  std::vector<std::string> lumifiles;
//  std::string GRLpath = "/sbottomtau_event/grls/";
  std::string GRLpath = std::string(getenv("WorkDir_DIR")) + "/../../grls/";
  if(mcCampaign=="mc16a" || project_name=="data15_13TeV" || project_name=="data16_13TeV")
    {
      // 2015 data, 3219.44 pb-1
      lumifiles.push_back( GRLpath + "ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-010.root" );
      // 2016 data, 32989.6 pb-1
      lumifiles.push_back( GRLpath + "ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-010.root" );
    }
  else if(mcCampaign=="mc16d" || project_name=="data17_13TeV")
    {
      // 2017 data (fast reprocessing, not T0), 44385.7 pb-1
      lumifiles.push_back( GRLpath + "ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root" );
    }
  else if(mcCampaign=="mc16e" || project_name=="data18_13TeV")
    {
      // 2018 data, 58450.1 pb-1
      lumifiles.push_back( GRLpath + "ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root" );
    }
    
  CHECK( SusyTauGlobals::s_susyDef->setProperty("PRWLumiCalcFiles", lumifiles) );

  CHECK( SusyTauGlobals::s_susyDef->setProperty("PRWActualMu2017File", "GoodRunsLists/data17_13TeV/20190708/purw.actualMu.2017.root") );
  CHECK( SusyTauGlobals::s_susyDef->setProperty("PRWActualMu2018File", "GoodRunsLists/data18_13TeV/20190708/purw.actualMu.2018.root") );

  // use default SUSYTools config file, else constantly need to keep in sync...
  // all parameters we need to change can be configured via setProperty, else, request it
  // CHECK( SusyTauGlobals::s_susyDef->setProperty("ConfigFile", PathResolverFindCalibFile("SusyTauAnalysisXAOD/SUSYTools_Default.conf") );
    
  CHECK( SusyTauGlobals::s_susyDef->setProperty("TauId", metadata->castString("tauID")) );  
  CHECK( SusyTauGlobals::s_susyDef->setProperty("TauBaselineId", metadata->castString("tauID")) );

  // use local tau config
  if( metadata->castBool( "localTauConfig" ) ) {
    CHECK( SusyTauGlobals::s_susyDef->setProperty("TauIdConfigPathBaseline", "SusyTauAnalysisXAOD/tau_selection_" + metadata->castString( "tauID" ) + ".conf") );
    CHECK( SusyTauGlobals::s_susyDef->setProperty("TauIdConfigPath", "SusyTauAnalysisXAOD/tau_selection_" + metadata->castString( "tauID" ) + ".conf") );
  }  

  CHECK( SusyTauGlobals::s_susyDef->setBoolProperty("DoTauOR", true ) );

  if( metadata->castString( "jetCont" ) == std::string("AntiKt4EMPFlowJets") ) {
    std::cout << "Switching to PFlow jets" << std::endl;
    CHECK( SusyTauGlobals::s_susyDef->setProperty("JetInputType", xAOD::JetInput::EMPFlow) );

    // use new FTAG scheme
    CHECK( SusyTauGlobals::s_susyDef->setProperty("BtagTagger", "DL1r") );
    CHECK( SusyTauGlobals::s_susyDef->setProperty("BtagTimeStamp", "201903") );
  }

  //SusyTauGlobals::s_susyDef->msg().setLevel( MSG::VERBOSE );

  CHECK( SusyTauGlobals::s_susyDef->initialize() );

  SusyTauGlobals::s_jetCleaningTool = new JetCleaningTool("SusyTauTightCleaningTool");
  CHECK( SusyTauGlobals::s_jetCleaningTool->setProperty("CutLevel", "TightBad") );
  CHECK( SusyTauGlobals::s_jetCleaningTool->initialize() );

  SusyTauGlobals::s_jetTileCorrectionTool = new CP::JetTileCorrectionTool("JetTileCorrectionTool");
  CHECK( SusyTauGlobals::s_jetTileCorrectionTool->initialize() );

  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauGlobals::InitialiseTauTruthTools(const SH::MetaObject * metadata)
{
  SusyTauGlobals::s_tauTruthMatch = new TauAnalysisTools::TauTruthMatchingTool("TauTruthMatchingTool");
  SusyTauGlobals::s_tauTruthMatch->msg().setLevel( MSG::INFO );
  CHECK( SusyTauGlobals::s_tauTruthMatch->initialize() );

  string inputfile = metadata->castBool( "localTauConfig" ) ? "SusyTauAnalysisXAOD/tau_selection_Medium.conf" : "SUSYTools/tau_selection_medium.conf";
  if (!SusyTauGlobals::s_tauSelTool.isUserConfigured()) {
    SusyTauGlobals::s_tauSelTool.setTypeAndName("TauAnalysisTools::TauSelectionTool/MediumRNN");
    CHECK( SusyTauGlobals::s_tauSelTool.setProperty("ConfigPath", inputfile) );
    CHECK( SusyTauGlobals::s_tauSelTool.retrieve() );
  }

  SusyTauGlobals::s_tauEffTool = new TauAnalysisTools::TauEfficiencyCorrectionsTool("TauEffTool_medium");
  CHECK( SusyTauGlobals::s_tauEffTool->setProperty("TauSelectionTool", SusyTauGlobals::s_tauSelTool.getHandle()) );
  CHECK( SusyTauGlobals::s_tauEffTool->initialize() );

  /*
  // WARNING: we may have to use TauSpinner on diboson as well...
  if( metadata->castBool("isMC") && metadata->castBool("isSherpa") && metadata->castString("sampleType")=="ttbar" ) {

    SusyTauGlobals::s_tauSpinnerReader = new TauSpinnerTools::xAODEventReaderTool("xAODEventReaderTool");
    CHECK( SusyTauGlobals::s_tauSpinnerReader->setProperty("IsSherpa", true) );
    CHECK( SusyTauGlobals::s_tauSpinnerReader->setProperty("IsSherpaTtbarDilep", true) );
    CHECK( SusyTauGlobals::s_tauSpinnerReader->initialize() );
    
    SusyTauGlobals::s_tauSpinnerWeightTool = new TauSpinnerTools::StandardModelTool("StandardModelTool");
    CHECK( SusyTauGlobals::s_tauSpinnerWeightTool->setProperty("CMSENE", 13000.) );
    CHECK( SusyTauGlobals::s_tauSpinnerWeightTool->setProperty("PDFname", "NNPDF30_nnlo_as_0118.LHGRID") );

    // Make Event Reader tool handle and pass it to weighter
    ToolHandle<TauSpinnerTools::IEventReaderToolBase> readerHandle(SusyTauGlobals::s_tauSpinnerReader);
    CHECK( SusyTauGlobals::s_tauSpinnerWeightTool->setProperty("EventReaderTool", readerHandle) );    
    CHECK( SusyTauGlobals::s_tauSpinnerWeightTool->initialize() );
  }
  */

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauGlobals::InitialiseGRL()
{
  if( SusyTauGlobals::isMC==false ) {
    // main GRL, for data only
    SusyTauGlobals::s_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    
    std::vector<std::string> mygrls;
    std::string GRLpath = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/";
    // Run2 legacy GRLs
    mygrls.push_back( GRLpath + "data15_13TeV/20190708/data15_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns.xml" );
    mygrls.push_back( GRLpath + "data16_13TeV/20190708/data16_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_WITH_IGNORES.xml" );
    mygrls.push_back( GRLpath + "data17_13TeV/20190708/data17_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml" );
    mygrls.push_back( GRLpath + "data18_13TeV/20190708/data18_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml" );

    CHECK( SusyTauGlobals::s_grl->setProperty( "GoodRunsListVec", mygrls) );
    CHECK( SusyTauGlobals::s_grl->setProperty( "PassThrough", false) );
    CHECK( SusyTauGlobals::s_grl->initialize() );
    
    Info( "SusyTauGlobals::InitialiseGRL()", "GRL tool initialized... " );   
    for(size_t i=0; i<mygrls.size(); i++)
      std::cout << "Using GRL " << mygrls[i] << std::endl; 
  }
  
  // special hand-made GRL for low-threshold ditau+MET trigger in 2017 (deactivated for a few LBs at the start of run), to be used for data and MC
  SusyTauGlobals::s_grl_ditauMET = new GoodRunsListSelectionTool("GRLTool_ditauMET");
  std::vector<std::string> grl_ditauMET;
  grl_ditauMET.push_back( PathResolverFindCalibFile("SusyTauAnalysisXAOD/data17_ditauMET_GRL.xml" ) );  
  CHECK( SusyTauGlobals::s_grl_ditauMET->setProperty( "GoodRunsListVec", grl_ditauMET) );
  CHECK( SusyTauGlobals::s_grl_ditauMET->setProperty( "PassThrough", false) );
  CHECK( SusyTauGlobals::s_grl_ditauMET->initialize() );
  
  Info( "SusyTauGlobals::InitialiseGRL()", "GRL tool for ditau+MET trigger initialized... " );
  std::cout << "Using GRL " << grl_ditauMET.at(0) << std::endl; 

  // special hand-made BadRunsList to handle LBA52 failure, to be used for data and MC
  SusyTauGlobals::s_grl_tile = new GoodRunsListSelectionTool("BadRunsListSelectionTool");
  std::vector<std::string> mybrls_tile;
  mybrls_tile.push_back( PathResolverFindCalibFile("SusyTauAnalysisXAOD/data16_tileDQ_LBA52_BRL.xml" ) );
  CHECK( SusyTauGlobals::s_grl_tile->setProperty( "GoodRunsListVec", mybrls_tile) );
  CHECK( SusyTauGlobals::s_grl_tile->initialize() );

  Info( "SusyTauGlobals::InitialiseGRL()", "GRL tool for LBA52 initialized... " );   
  std::cout << "Using BadRunsList " << mybrls_tile[0] << std::endl; 

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauGlobals::InitialiseTriggerTools()
{
  SusyTauGlobals::s_prescaleTool = new JetSmearing::PreScaleTool("PrescaleTool");
  
  CHECK( SusyTauGlobals::s_prescaleTool->setProperty("HistoPath", PathResolverFindCalibFile("JetSmearing/PreScales/prescale_histos_combined_2015-2018_v102-pro22-04.root")) );
  CHECK( SusyTauGlobals::s_prescaleTool->initialize() );

  return EL::StatusCode::SUCCESS;
}
