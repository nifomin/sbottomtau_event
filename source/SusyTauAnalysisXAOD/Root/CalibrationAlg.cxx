#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SusyTauAnalysisXAOD/CalibrationAlg.h>
#include "xAODEventInfo/EventInfo.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include <iomanip>

static const char* APP_NAME = "CalibrationAlg";

// this is needed to distribute the algorithm to the workers
ClassImp(CalibrationAlg)

//  - Calibrated container names, these are the ones that should be read from the analysis algorithms
std::map< Phys::ContainerType, std::string > CalibrationAlg :: s_contNames_calibrated = { { Phys::ContainerType::Jet, "CalibratedJets" },
											  { Phys::ContainerType::Electron, "CalibratedElectrons" },
											  { Phys::ContainerType::Muon, "CalibratedMuons" },
											  { Phys::ContainerType::Photon, "CalibratedPhotons" },
											  { Phys::ContainerType::Tau, "CalibratedTaus" },
											  { Phys::ContainerType::MissingET, "CalibratedMET" } };

//  - Names of the different objects, here for pretty printing
std::map< Phys::ContainerType, std::string > CalibrationAlg :: s_objNames = { { Phys::ContainerType::Jet, "Jet" },
									      { Phys::ContainerType::Electron, "Electron" },
									      { Phys::ContainerType::Muon, "Muon" },
									      { Phys::ContainerType::Photon, "Photon" },
									      { Phys::ContainerType::Tau, "Tau" },
									      { Phys::ContainerType::BTag, "BTag" },
									      { Phys::ContainerType::MissingET, "Missing ET" }, 
									      { Phys::ContainerType::AntiKt4TruthJets, "AntiKt4TruthJets" },
									      { Phys::ContainerType::TruthElectrons, "TruthElectrons" },
									      { Phys::ContainerType::TruthMuons, "TruthMuons" },
									      { Phys::ContainerType::TruthTaus, "TruthTaus" },
									      { Phys::ContainerType::MET_Truth, "MET_Truth" } };

//  - Calibrated container names for systematic variations, these are the ones read from analysis algorithms looping over systematics
std::map< Phys::ContainerType, std::map< CP::SystematicSet, std::string > > CalibrationAlg :: s_contNames_syst = {};

std::map< Phys::ContainerType, std::vector< SusyTauSystVariation > > CalibrationAlg :: s_eff_syst = { { Phys::ContainerType::Jet, std::vector< SusyTauSystVariation >() },
												      { Phys::ContainerType::Electron, std::vector< SusyTauSystVariation >() },
												      { Phys::ContainerType::Muon, std::vector< SusyTauSystVariation >() },
												      { Phys::ContainerType::Photon, std::vector< SusyTauSystVariation >() },
												      { Phys::ContainerType::Tau, std::vector< SusyTauSystVariation >() } };

CalibrationAlg :: CalibrationAlg ():
    SusyTauAlgBase(),
    m_name("CalibrationAlgorithm")
{
}

EL::StatusCode CalibrationAlg :: setupJob (EL::Job& job)
{
    if ( SusyTauAlgBase::setupJob(job) != EL::StatusCode::SUCCESS )
	return EL::StatusCode::FAILURE;
    
    auto opts = SusyTauGlobals::s_config.GetOptions();
    if(opts.count("jetCont"))
    {s_contNames_original[Phys::ContainerType::Jet] = opts["jetCont"].as<std::string>();}
    if(opts.count("tauCont"))
    {s_contNames_original[Phys::ContainerType::Tau] = opts["tauCont"].as<std::string>();}
    if(opts.count("electronCont"))
    {s_contNames_original[Phys::ContainerType::Electron] = opts["electronCont"].as<std::string>();}
    if(opts.count("muonCont"))
    {s_contNames_original[Phys::ContainerType::Muon] = opts["muonCont"].as<std::string>();}
    if(opts.count("photonCont"))
    {s_contNames_original[Phys::ContainerType::Photon] = opts["photonCont"].as<std::string>();}
    
    if(opts.count("jetCont"))
    {s_contNames_calibrated[Phys::ContainerType::Jet] = std::string("STCalib") + opts["jetCont"].as<std::string>();}
    if(opts.count("tauCont"))
    {s_contNames_calibrated[Phys::ContainerType::Tau] = std::string("STCalib") + opts["tauCont"].as<std::string>();}
    if(opts.count("electronCont"))
    {s_contNames_calibrated[Phys::ContainerType::Electron] = std::string("STCalib") + opts["electronCont"].as<std::string>();}
    if(opts.count("muonCont"))
    {s_contNames_calibrated[Phys::ContainerType::Muon] = std::string("STCalib") + opts["muonCont"].as<std::string>();}
    if(opts.count("photonCont"))
    {s_contNames_calibrated[Phys::ContainerType::Photon] = std::string("STCalib") + opts["photonCont"].as<std::string>();}
    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode CalibrationAlg :: histInitialize ()
{
    if ( SusyTauAlgBase::histInitialize() != EL::StatusCode::SUCCESS )
	return EL::StatusCode::FAILURE;
   
    h_runnumber = new TH1I("h_runnumber", "Processed events per run number",130000,270000,400000);
    wk()->addOutput(h_runnumber);
 
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode CalibrationAlg :: changeInput (bool firstFile)
{
    if ( firstFile )
    {
	m_event = wk()->xaodEvent();
	m_store = wk()->xaodStore();
	
	SusyTauGlobals::s_event = wk()->xaodEvent();
	SusyTauGlobals::s_store = wk()->xaodStore();
    }
    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode CalibrationAlg :: initialize ()
{
    if ( SusyTauAlgBase::initialize() != EL::StatusCode::SUCCESS )
	return EL::StatusCode::FAILURE;
    
    for ( auto sysInfo : m_systematics )
    {
	if ( sysInfo.isNominal() )
	    continue;
	// Kinematics are affected, we need a collection for all objects to take OR into account
	if ( sysInfo.affectsKinematics() )
	{
	    s_contNames_syst[Phys::ContainerType::Jet][sysInfo.m_systset]      = s_contNames_calibrated[Phys::ContainerType::Jet]      + sysInfo.suffix();
	    s_contNames_syst[Phys::ContainerType::Tau][sysInfo.m_systset]      = s_contNames_calibrated[Phys::ContainerType::Tau]      + sysInfo.suffix();
	    s_contNames_syst[Phys::ContainerType::Electron][sysInfo.m_systset] = s_contNames_calibrated[Phys::ContainerType::Electron] + sysInfo.suffix();
	    s_contNames_syst[Phys::ContainerType::Muon][sysInfo.m_systset]     = s_contNames_calibrated[Phys::ContainerType::Muon]     + sysInfo.suffix();
	    s_contNames_syst[Phys::ContainerType::Photon][sysInfo.m_systset]   = s_contNames_calibrated[Phys::ContainerType::Photon]   + sysInfo.suffix();
	    // MET is assumed to be affected by ALL systematics that affect objects.
	    //  This needs to be refined obviously...
	    s_contNames_syst[Phys::ContainerType::MissingET][sysInfo.m_systset]=  s_contNames_calibrated[Phys::ContainerType::MissingET] + sysInfo.suffix();
	}
	else // Just SFs affected, just need to adjust those, OR unaffected
	  {
	    /*
	    std::cout << "==========" << std::endl;
	    std::cout << sysInfo.suffix()<< std::endl;
	    std::cout << sysInfo.affectsType() << std::endl;
	    */

	    s_contNames_syst[static_cast<Phys::ContainerType>(sysInfo.affectsType())][sysInfo.m_systset]
	      = s_contNames_calibrated[static_cast<Phys::ContainerType>(sysInfo.affectsType())] + sysInfo.suffix();
	    s_eff_syst[sysInfo.affectsType()].push_back(sysInfo);
	  }
    }

    if ( !s_contNames_syst.empty() )
    {
	std::cout << "== Collections considered for systematic variations     ==" << std::endl;
	std::cout << "== (one single collection for weight variations)        ==" << std::endl;
	std::cout << "== (a full set of collections for kinematic variations) ==" << std::endl;

	for ( auto cat : s_contNames_syst )
	{
	    for ( auto syst : cat.second )
		std::cout << "\tsyst " << std::setw(45) << std::left << syst.first.name() << " with collection\t" << syst.second << std::endl;
	}
	std::cout << "== End list of collections ==" << std::endl;
    }

    if ( !s_contNames_syst.empty() )
    {
	std::cout << "== Weigth-based systematic variations considered ==" << std::endl;
	for ( auto cat : s_eff_syst )
	{
	    //std::cout << "- " << s_objNames.at(static_cast<Phys::ContainerType>(cat.first) ) << " weight based systematics:"<< std::endl;
	    for ( auto syst : cat.second )
		std::cout << "\t" << std::setw(45) << std::left << syst.fullname() << std::endl;
	}
	std::cout << "== End of weight-based variations ==" << std::endl;
    }

    
    if ( wk()->metaData()->castBool( "isMC" ) )
      {
	if(SusyTauGlobals::InitialiseTauTruthTools( wk()->metaData() ) != EL::StatusCode::SUCCESS) {
	  Error("CalibrationAlg::initialize", "Failed to initialize tau truth tools. Exiting." );
	  return EL::StatusCode::FAILURE;
	}
	
	auto systematics = SusyTauGlobals::s_susyDef->getSystInfoList();
	for( auto& sysInfo : systematics )
	  {
	    if ( sysInfo.systset.name() == std::string("PRW_DATASF__1up") )
	      {
		m_prw_up = sysInfo;
	      }
	    if ( sysInfo.systset.name() == std::string("PRW_DATASF__1down") )
	      {
		m_prw_down = sysInfo;
	      }
	  }
      }
   
 
    return EL::StatusCode::SUCCESS;
}

/// TODO: Error checking via exceptions is needed in this method.
EL::StatusCode CalibrationAlg :: execute()
{
  const xAOD::EventInfo* eventInfo = nullptr;
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  
  // keep track of number of processed events
  if(!eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION))
    {
      h_runnumber->Fill(eventInfo->runNumber());
    }
  
  // to avoid crashes for events with no primary vertex
  if (SusyTauGlobals::s_susyDef->GetPrimVtx() == nullptr) {
    wk()->skipEvent();
    return EL::StatusCode::SUCCESS;
  }
  
  if ( wk()->metaData()->castBool( "isMC" ) )
    {	
      CHECK( SusyTauGlobals::s_susyDef->ApplyPRWTool() );
      
      if ( SusyTauGlobals::s_susyDef->applySystematicVariation(m_prw_up.m_systset) == CP::SystematicCode::Ok ){
	eventInfo->auxdecor<float>("PileupWeightUp") = SusyTauGlobals::s_susyDef->GetPileupWeight();
      }
      else
	{
	  Error("CalibrationAlg::Execute", "Failed to set PRW systeamatics. Exiting." );
	  return EL::StatusCode::FAILURE;
	}
      if ( SusyTauGlobals::s_susyDef->applySystematicVariation(m_prw_down.m_systset) == CP::SystematicCode::Ok ){
	eventInfo->auxdecor<float>("PileupWeightDown") = SusyTauGlobals::s_susyDef->GetPileupWeight();
      }
      else
	{
	  Error("CalibrationAlg::Execute", "Failed to set PRW tools systeamatics. Exiting." );
	  return EL::StatusCode::FAILURE;
	}
      if ( SusyTauGlobals::s_susyDef->resetSystematics() != CP::SystematicCode::Ok ){
	Error("CalibrationAlg::Execute", "Failed to reset SUSY tools systeamatics. Exiting." );
	return EL::StatusCode::FAILURE;
      }
    }	   
  
    // Create nominal collections
    if ( SusyTauGlobals::s_susyDef->resetSystematics() != CP::SystematicCode::Ok ) {
	Error("CalibrationAlg::Execute", "Failed to reset SUSY tools systeamatics. Exiting." );
	return EL::StatusCode::FAILURE;
    }
    auto jetCont  = CalibrateContainer<xAOD::JetContainer>      ( s_contNames_original[Phys::ContainerType::Jet]     , s_contNames_calibrated[Phys::ContainerType::Jet] );
    auto tauCont  = CalibrateContainer<xAOD::TauJetContainer>   ( s_contNames_original[Phys::ContainerType::Tau]     , s_contNames_calibrated[Phys::ContainerType::Tau] );
    auto muonCont = CalibrateContainer<xAOD::MuonContainer>     ( s_contNames_original[Phys::ContainerType::Muon]    , s_contNames_calibrated[Phys::ContainerType::Muon] );
    auto eleCont  = CalibrateContainer<xAOD::ElectronContainer> ( s_contNames_original[Phys::ContainerType::Electron], s_contNames_calibrated[Phys::ContainerType::Electron] );
    xAOD::PhotonContainer* phoCont = nullptr;
    if (SusyTauGlobals::doPhotons) 
      phoCont  = CalibrateContainer<xAOD::PhotonContainer>   ( s_contNames_original[Phys::ContainerType::Photon]  , s_contNames_calibrated[Phys::ContainerType::Photon] );
    
    // No OR without PV, assume the event is killed if PV not present anyway
    if ( SusyTauGlobals::s_susyDef->GetPrimVtx() != nullptr )
      {
	CHECK( SusyTauGlobals::s_susyDef->OverlapRemoval( eleCont, muonCont, jetCont, nullptr, tauCont ) );
      }

    MakeMissingETContainer( s_contNames_calibrated[Phys::ContainerType::MissingET], jetCont, eleCont, muonCont,  phoCont, tauCont ); 

    MakeMissingETnoMuonContainer( s_contNames_calibrated[Phys::ContainerType::MissingET] + string("_noMuon"), jetCont, eleCont, muonCont, phoCont, tauCont ); 

    for ( auto & sysInfo : m_systematics )
    {
	if ( SusyTauGlobals::s_susyDef->applySystematicVariation(sysInfo.m_systset) != CP::SystematicCode::Ok ){
	    Error("CalibrationAlg::Execute", "Failed to set SUSY tools systematics. Exiting." );
	    return EL::StatusCode::FAILURE;
	}

	// apply systematic variation to our own tools which have systematics 
	if( wk()->metaData()->castBool( "isMC" ) )
	  if( SusyTauGlobals::s_tauEffTool->applySystematicVariation(sysInfo.m_systset) != CP::SystematicCode::Ok ){
	    Error("CalibrationAlg::Execute", "Failed to set tau efficiency tools systematics. Exiting." );
	    return EL::StatusCode::FAILURE;
	  }

	// Kinematics are affected, we need a collection for all objects to take OR into account
	if ( sysInfo.affectsKinematics() )
	{
	    auto jet  = ( sysInfo.affectsType() == Phys::ContainerType::Jet )
		? CalibrateContainer<xAOD::JetContainer> ( s_contNames_original[Phys::ContainerType::Jet], s_contNames_syst[Phys::ContainerType::Jet][sysInfo.m_systset] )
 		: ShallowContainer( jetCont, s_contNames_syst[Phys::ContainerType::Jet][sysInfo.m_systset] );
	    auto tau  = ( sysInfo.affectsType() == Phys::ContainerType::Tau )
		? CalibrateContainer<xAOD::TauJetContainer> ( s_contNames_original[Phys::ContainerType::Tau], s_contNames_syst[Phys::ContainerType::Tau][sysInfo.m_systset] )
 		: ShallowContainer( tauCont, s_contNames_syst[Phys::ContainerType::Tau][sysInfo.m_systset] );
	    auto electron  = ( sysInfo.affectsType() == Phys::ContainerType::Electron )
		? CalibrateContainer<xAOD::ElectronContainer> ( s_contNames_original[Phys::ContainerType::Electron], s_contNames_syst[Phys::ContainerType::Electron][sysInfo.m_systset] )
 		: ShallowContainer( eleCont, s_contNames_syst[Phys::ContainerType::Electron][sysInfo.m_systset] );
	    auto muon  = ( sysInfo.affectsType() == Phys::ContainerType::Muon )
		? CalibrateContainer<xAOD::MuonContainer> ( s_contNames_original[Phys::ContainerType::Muon], s_contNames_syst[Phys::ContainerType::Muon][sysInfo.m_systset] )
 		: ShallowContainer( muonCont, s_contNames_syst[Phys::ContainerType::Muon][sysInfo.m_systset] );
	    xAOD::PhotonContainer* photon  = nullptr;
	    if ( SusyTauGlobals::doPhotons )
		photon = ( sysInfo.affectsType() == Phys::ContainerType::Photon )
		    ? CalibrateContainer<xAOD::PhotonContainer> ( s_contNames_original[Phys::ContainerType::Photon], s_contNames_syst[Phys::ContainerType::Photon][sysInfo.m_systset] )
		    : ShallowContainer( phoCont, s_contNames_syst[Phys::ContainerType::Photon][sysInfo.m_systset] );

	    MakeMissingETContainer( s_contNames_syst[Phys::ContainerType::MissingET][sysInfo.m_systset], jet, electron, muon, photon, tau );

	    MakeMissingETnoMuonContainer( s_contNames_syst[Phys::ContainerType::MissingET][sysInfo.m_systset] + string("_noMuon"), jet, electron, muon, photon, tau ); 

	    // No OR without PV, assume the event is killed if PV not present anyway
	    if ( SusyTauGlobals::s_susyDef->GetPrimVtx() != nullptr )
	      {
		CHECK( SusyTauGlobals::s_susyDef->OverlapRemoval( electron, muon, jet, nullptr, tau ) );
	      }
	}
	else
	{
	    if ( sysInfo.affectsType() == Phys::ContainerType::Jet )
		DecorateContainer<xAOD::JetContainer> ( jetCont, s_contNames_syst[Phys::ContainerType::Jet][sysInfo.m_systset], sysInfo.m_systset );
	    if ( sysInfo.affectsType() == Phys::ContainerType::Tau )
		DecorateContainer<xAOD::TauJetContainer> ( tauCont, s_contNames_syst[Phys::ContainerType::Tau][sysInfo.m_systset], sysInfo.m_systset );
	    if ( sysInfo.affectsType() == Phys::ContainerType::Muon )
		DecorateContainer<xAOD::MuonContainer> ( muonCont, s_contNames_syst[Phys::ContainerType::Muon][sysInfo.m_systset], sysInfo.m_systset );
	    if ( sysInfo.affectsType() == Phys::ContainerType::Electron )
		DecorateContainer<xAOD::ElectronContainer> ( eleCont, s_contNames_syst[Phys::ContainerType::Electron][sysInfo.m_systset], sysInfo.m_systset );
	}
    }

    if ( SusyTauGlobals::s_susyDef->resetSystematics() != CP::SystematicCode::Ok ){
	Error("CalibrationAlg::Execute", "Failed to reset SUSY tools systematics. Exiting." );
	return EL::StatusCode::FAILURE;
    }

    if( wk()->metaData()->castBool( "isMC" ) )
      if( SusyTauGlobals::s_tauEffTool->applySystematicVariation(SusyTauSystVariation().m_systset) != CP::SystematicCode::Ok ){ 
	Error("CalibrationAlg::Execute", "Failed to reset tau efficiency tools systematics. Exiting." );
	return EL::StatusCode::FAILURE;
      }


    return EL::StatusCode::SUCCESS;
}

EL::StatusCode CalibrationAlg :: histFinalize ()
{    
    Info("CalibrationAlg::finalise()", "Deleting the susy tools" );

    delete SusyTauGlobals::s_susyDef;
//     delete SusyTauGlobals::s_prwtool;
    delete SusyTauGlobals::s_jetTileCorrectionTool;

    SusyTauGlobals::s_susyDef = 0;
//     SusyTauGlobals::s_prwtool = 0;
    SusyTauGlobals::s_jetTileCorrectionTool = 0;
    
    if ( wk()->metaData()->castBool( "isMC" ) ) 
      {
	delete SusyTauGlobals::s_tauTruthMatch;
	delete SusyTauGlobals::s_tauEffTool;

	/*	  
	// print TauSpinnerTool info
	// hm, maybe TauSpinner will have to be run on diboson as well...
	if( wk()->metaData()->castBool("isSherpa") && wk()->metaData()->castString("sampleType")=="ttbar" ) {
	  
	  SusyTauGlobals::s_tauSpinnerReader->summary(); 
	  SusyTauGlobals::s_tauSpinnerWeightTool->summary();
	}

	delete SusyTauGlobals::s_tauSpinnerReader;
	delete SusyTauGlobals::s_tauSpinnerWeightTool;	
	SusyTauGlobals::s_tauSpinnerReader = 0;
	SusyTauGlobals::s_tauSpinnerWeightTool = 0;
	*/
      }
    else {
      delete SusyTauGlobals::s_grl;
      SusyTauGlobals::s_grl = 0;
      delete SusyTauGlobals::s_prescaleTool;
      SusyTauGlobals::s_prescaleTool = 0;
    }
    delete SusyTauGlobals::s_grl_ditauMET;
    SusyTauGlobals::s_grl_ditauMET = 0;
    delete SusyTauGlobals::s_grl_tile;
    SusyTauGlobals::s_grl_tile = 0;

    if ( SusyTauAlgBase::histFinalize() != EL::StatusCode::SUCCESS )
      Error("CalibrationAlg::histFinalize", "SusyTauAlgBase::histFinalize() failed." );

    return EL::StatusCode::SUCCESS;
}

xAOD::MissingETContainer * CalibrationAlg :: MakeMissingETContainer ( std::string calibrated_name,
								      const xAOD::JetContainer* jet,
								      const xAOD::ElectronContainer* elec,
								      const xAOD::MuonContainer* muon,
								      const xAOD::PhotonContainer* photon,
								      const xAOD::TauJetContainer* tau )
{
    xAOD::MissingETContainer *met = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
    met->setStore(met_aux);
    
    if( ! SusyTauGlobals::s_susyDef->GetMET( *met, jet, elec, muon, photon, tau ).isSuccess() )
    {
        // should throw an exception here	
    }    
    if( !m_store->record( met, calibrated_name ).isSuccess() )
    {
	// should throw an exception here
    }
    if( !m_store->record( met_aux, calibrated_name+"Aux." ).isSuccess() )
    {
        // should throw an exception here
    }
    
    return met;    
}

xAOD::MissingETContainer * CalibrationAlg :: MakeMissingETnoMuonContainer ( std::string calibrated_name,
									    const xAOD::JetContainer* jet,
									    const xAOD::ElectronContainer* elec,
                                                                            const xAOD::MuonContainer* muon,
									    const xAOD::PhotonContainer* photon,
									    const xAOD::TauJetContainer* tau )
{
    xAOD::MissingETContainer *met = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
    met->setStore(met_aux);

    if( ! SusyTauGlobals::s_susyDef->GetMET( *met, jet, elec, muon, photon, tau, true, true, muon ).isSuccess() )
    {
        // should throw an exception here
    }
    if( !m_store->record( met, calibrated_name ).isSuccess() )
    {
        // should throw an exception here
    }
    if( !m_store->record( met_aux, calibrated_name+"Aux." ).isSuccess() )
    {
        // should throw an exception here
    }

    return met;
}


