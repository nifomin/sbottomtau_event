#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "SusyTauAnalysisXAOD/SusyTauAlgBase.h"
#include "SusyTauAnalysisXAOD/Cutflows/SusyOneTauBaselineCutflow.h"
#include "SusyTauAnalysisXAOD/Cutflows/SusyDiTauBaselineCutflow.h"
#include "SusyTauAnalysisXAOD/Cutflows/SusyTauVanillaBaselineCutflow.h"

SusyTauAlgBase :: SusyTauAlgBase ():
    m_cutflow_name(),
    m_cutflow_vector(),
    m_cutflow(nullptr)
{
}

SusyTauAlgBase :: SusyTauAlgBase ( std::string cutflow_name ):
    m_cutflow_name(cutflow_name),
    m_cutflow_vector(),
    m_cutflow(nullptr)
{
}

SusyTauAlgBase :: SusyTauAlgBase ( std::string cutflow_name, std::vector<std::string> cutflow_config ):
    m_cutflow_name(cutflow_name),
    m_cutflow_vector(cutflow_config),
    m_cutflow(nullptr)
{
}

EL::StatusCode SusyTauAlgBase :: setupJob (EL::Job& /*job*/)
{
    m_systSettings = SusyTauGlobals::s_config.GetSystConfig();

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauAlgBase :: histInitialize ()
{
    
    if ( m_cutflow_name.length() )
    {
	if ( m_cutflow_vector.size() )
	    m_cutflow = createCutflow( m_cutflow_name, m_cutflow_vector );
	else
	    m_cutflow = lookupCutflow( m_cutflow_name );
    }
    
    if ( m_cutflow )
    {
	wk()->addOutput( m_cutflow->GetHist() );
	wk()->addOutput( m_cutflow->GetHistRaw() );
    }
    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauAlgBase :: initialize ()
{
    m_event = wk()->xaodEvent();
    m_store = wk()->xaodStore();

    if ( ! SusyTauGlobals::s_susyDef )
	if ( SusyTauGlobals::InitialiseSusyTools( wk()->metaData(), m_event ) != EL::StatusCode::SUCCESS )
	{
	    std::cout << "Failure in SUSYTools initialisation. Terminating!" << std::endl;
	    return EL::StatusCode::FAILURE;
	}
    
    if ( (!wk()->metaData()->castBool( "isMC" ) && !SusyTauGlobals::s_grl) || !SusyTauGlobals::s_grl_ditauMET || !SusyTauGlobals::s_grl_tile)
      if ( SusyTauGlobals::InitialiseGRL() != EL::StatusCode::SUCCESS )
	{
	  std::cout << "Failure in GRL initialisation. Terminating!" << std::endl;
	  return EL::StatusCode::FAILURE;
	}
    
//     if ( !SusyTauGlobals::s_prwtool )
//       if ( SusyTauGlobals::InitialisePRWTools( wk()->metaData() ) != EL::StatusCode::SUCCESS )
//       {
// 	std::cout << "Failure in PRW Tool initialisation. Terminating!" << std::endl;
// 	return EL::StatusCode::FAILURE;
//       }


    if( !wk()->metaData()->castBool( "isMC" ) && !SusyTauGlobals::s_prescaleTool ) 
      if ( SusyTauGlobals::InitialiseTriggerTools( ) != EL::StatusCode::SUCCESS ) {
	std::cout << "Failure in trigger tool initialisation. Terminating!" << std::endl;
	return EL::StatusCode::FAILURE; 
      }
    
    m_systematics = SusyTauConfig::RetrieveSystematicList( m_systSettings, wk()->metaData()->castBool( "isFullsim" )  );
    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode SusyTauAlgBase :: histFinalize ()
{
    if ( m_cutflow )
    {
	m_cutflow->PrintCutFlow();
	delete m_cutflow;
    }
    return EL::StatusCode::SUCCESS;
}

CutflowBase *SusyTauAlgBase :: lookupCutflow ( std::string cutflowName )
{
  CutflowBase *cutflow = nullptr;
  cout << "Value of cutflow in lookupCutflow of SusyTauEventProperties: " << cutflowName << endl;
  if( cutflowName == "SusyOneTauBaselineCutflow" ){
    cutflow = new SusyOneTauBaselineCutflow();
    cout << "Loaded SusyOneTauBaselineCutflow!" << endl;
  }
  if( cutflowName == "SusyDiTauBaselineCutflow" ){
    cutflow = new SusyDiTauBaselineCutflow();
    cout << "Loaded SusyDiTauBaselineCutflow!" << endl;
  }
  return cutflow;
}

CutflowBase *SusyTauAlgBase :: createCutflow ( std::string cutflowName, std::vector< std::string > cutflow)
{
  CutflowBase *ret_cutflow = nullptr;
  ret_cutflow = new SusyTauVanillaBaselineCutflow( cutflowName, cutflow );
  return ret_cutflow;
}
