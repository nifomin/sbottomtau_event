#include "SusyTauAnalysisXAOD/TauSmearingTool.h"

using namespace std;

namespace TauSmearing {

  TauSmearingTool::TauSmearingTool(const std::string& name) : asg::AsgTool( name ) {

    //default setting of 1000 smears per seed event
    declareProperty("NumberOfSmearedEvents",m_nSmear = 1000);

    m_ptBinning = 20*1000;
  }

  TauSmearingTool::~TauSmearingTool() {

    if (m_Rvector.size()) {
      for (unsigned int i=0; i<m_Rvector.size(); i++) {
	if (m_Rvector[i].size())
	  for (unsigned int j=0; j<m_Rvector[i].size(); j++) 
	    delete m_Rvector[i][j];
	m_Rvector[i].clear();
      }
      m_Rvector.clear();
    }
    if (m_Rmap.size()) {
      for (unsigned int i=0; i<m_Rmap.size(); i++) 
	delete m_Rmap[i];
      m_Rmap.clear();
    }

  }
  
  StatusCode TauSmearingTool::initialize() {
    
    std::cout << std::endl ;
    std::cout << "Setting up Tau Smearing Tool with parameters :" << std::endl;
    std::cout << "NumberOfSmearedEvents = " << m_nSmear << " ||  ";
    std::cout << "Tau Response Map true jet pt binning = " << m_ptBinning << std::endl << std::endl ;      
    
    return StatusCode::SUCCESS;
  }


  void TauSmearingTool::AddResponseMaps( const TH2F* Rmap ) {
    
    if (Rmap) {
      TH2F* rMapClone = (TH2F*)Rmap->Clone("TauSmearingTool::rMapClone");
      m_Rmap.push_back(rMapClone);
      std::vector<TH1F*> vector_1DHistograms ; // Creating a buffer of TH1F's
      
      const unsigned int rMapBinNumber = m_Rmap.size() - 1; // Get the Rmap bin number to loop over ,last added.
      for (int i=1; i<=m_Rmap[rMapBinNumber]->GetNbinsX(); i++) {
	
	std::stringstream ss;
	ss << i;
	std::string name = "response_sys" + ss.str();
	const TH1F* h_temp = (TH1F*)m_Rmap[rMapBinNumber]->ProjectionY(name.c_str(), i, i);
	name = "TauSmearingTool::projectionHist_" + ss.str();
	TH1F* h = (TH1F*)h_temp->Clone(name.c_str());

	vector_1DHistograms.push_back(h); /// Getting the vector of TH1F's   
      }
      m_Rvector.push_back(vector_1DHistograms); 
    }

  }

  const TH1F* TauSmearingTool::MakeSmearingHist(const double& pt, const unsigned int& i){
    //This is only valid for pt binning = 20 GeV
    const int ptBin = this->getPtBin(pt);
    return m_Rvector[i][ptBin];
  }


  //void TauSmearingTool::DoSmearing(std::vector<std::unique_ptr<SmearData> >& smrMc, const xAOD::TauJetContainer& taus, const xAOD::JetContainer& alljets) {
  void TauSmearingTool::DoSmearing(std::vector<std::unique_ptr<SmearData> >& smrMc, const xAOD::TauJetContainer& taus) {

    const unsigned int nTaus = taus.size();

    smrMc.clear();

    for (unsigned int i = 0; i < m_nSmear; i++) {
       std::unique_ptr<SmearData> smearDataPtr( new SmearData(taus) );
       smrMc.push_back( std::move(smearDataPtr) );
    }

    std::vector<const TH1F*> vectorOfResponseHistograms(nTaus);

    /*
    vector<TLorentzVector> seedjet;
    
    for (unsigned int i = 0; i < nTaus; i++) {
      
      // missing aux data for seed jet... can't use tau->jet()
      bool match = false;
      double dRmin = 999.;

      for (auto jet : alljets) {
	double dR = jet->p4().DeltaR(taus[i]->p4());
	if( dR < dRmin ) dRmin = dR;
	// matching within dR=0.25 instead of 0.2 to recover a few marginal cases
	if ( dR < 0.25 ) {	  
	  seedjet.push_back(jet->p4());	  
	  vectorOfResponseHistograms[i] = MakeSmearingHist(jet->pt(), 0);
	  match = true;
	  break;
	}
      }
      // matching sometimes fails for low-pt taus (not sure why)
      if(match==false) {
	cout << "TauSmearingTool::DoSmearing() failed to match tau to seed jet. Using tau pt instead of seed jet pt..." << endl;
	cout << "dRmin = " << dRmin << " tau pt = " << taus[i]->pt()/1000. << endl;
	seedjet.push_back(taus[i]->p4());	  
	vectorOfResponseHistograms[i] = MakeSmearingHist(taus[i]->pt(), 0);
      }
    }
    */

    // no longer use seed jet pt...
    vector<float> Rmean (nTaus);

    for (unsigned int i = 0; i < nTaus; i++) {

      Rmean[i] = h_TauPtOverSeedJetPt->GetBinContent( (int)(taus[i]->pt()/m_ptBinning) + 1 );
      
      //vectorOfResponseHistograms[i] = MakeSmearingHist(taus[i]->pt(), 0); 

      // NEW: resolution binned in truth R=0.2 jet
      // therefore, evaluate resolution at tau pt / <R> = truth jet pt
      // and later on, enforce average response = 1 by dividing random number by <R>

      vectorOfResponseHistograms[i] = MakeSmearingHist(taus[i]->pt() / Rmean[i] , 0); 

    }

    for (unsigned int i = 0; i < m_nSmear; i++) {

      SmearData& smrData = *smrMc[i];

      for (unsigned int j = 0; j < nTaus; j++) {

	const xAOD::TauJet* thisTau = taus[j];
	
	// only smear taus that pass kinematic selection
	if ( !this->passSelection(*thisTau) ) continue;

	double Random = 1.;

	// protection against empty slices
	//if(vectorOfResponseHistograms[j]->GetEntries()!=0) Random = vectorOfResponseHistograms[j]->GetRandom();

	// NEW !!! correct for mean response
	if( vectorOfResponseHistograms[j]->GetEntries()!=0 && Rmean[j]!=0. )
	  Random = vectorOfResponseHistograms[j]->GetRandom() / Rmean[j];

	//cout << "tau pt = " << taus[j]->pt()/1000 << " Rmean = " << Rmean[j] << endl;

	//TLorentzVector theLorentzVector(seedjet[j].Px()*Random, seedjet[j].Py()*Random, seedjet[j].Pz()*Random, seedjet[j].E()*Random );
	TLorentzVector theLorentzVector;
	theLorentzVector.SetPtEtaPhiE( taus[j]->pt()*Random, taus[j]->eta(), taus[j]->phi(), taus[j]->e()*Random ); 

	xAOD::TauJetContainer* smearedTauContainer = smrData.tauContainer;
	xAOD::TauJet* smearedTau = smearedTauContainer->at(j);
	this->setTau4Vector(*smearedTau,theLorentzVector);

      }//taus loop

    }//smearing loop
    
  }//doSmearing



  void TauSmearingTool::setTau4Vector(xAOD::TauJet& theTau, const TLorentzVector& theLorentzVector){

    theTau.setP4( theLorentzVector.Pt(), theLorentzVector.Eta(), theLorentzVector.Phi(), theLorentzVector.M() );
    // calo TES
    theTau.setP4( xAOD::TauJetParameters::TauEnergyScale, theLorentzVector.Pt(), theLorentzVector.Eta(), theLorentzVector.Phi(), theLorentzVector.M() );
    // MVA TES - will be picked up by TAT
    theTau.auxdata<float>("ptFinalCalib") = theLorentzVector.Pt();
    theTau.auxdata<float>("etaFinalCalib") = theLorentzVector.Eta();
    theTau.auxdata<float>("phiFinalCalib") = theLorentzVector.Phi();
    theTau.auxdata<float>("mFinalCalib") = theLorentzVector.M();
  }

  bool TauSmearingTool::passSelection(const xAOD::TauJet& theTau){
    
    if ( theTau.auxdata< char >("smeartau") == false ) return false;    
    return true;

  }

}
