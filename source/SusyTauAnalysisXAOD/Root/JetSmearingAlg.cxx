#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <SusyTauAnalysisXAOD/JetSmearingAlg.h>
#include "SusyTauAnalysisXAOD/EventParametersAlg.h"
#include <SusyTauAnalysisXAOD/SusyTauSystVariation.h>
#include "TFile.h"
#include <SusyTauAnalysisXAOD/CalibrationAlg.h>
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMetaData/FileMetaData.h"
#include "PathResolver/PathResolver.h"

static const char* APP_NAME = "JetSmearingAlg";

// this is needed to distribute the algorithm to the workers
ClassImp(JetSmearingAlg)

const bool debug = false;

EL::StatusCode JetSmearingAlg :: setupJob (EL::Job& job)
{
  if ( SusyTauAlgBase :: setupJob (job) != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;
    
  auto opts = SusyTauGlobals::s_config.GetOptions();
  m_numSmear = opts["numSmear"].as<unsigned int>();   
  m_doTauSmearing = opts["tauSmear"].as<bool>();   

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetSmearingAlg :: histInitialize ()
{
  if ( SusyTauAlgBase::histInitialize() != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetSmearingAlg::initialize()
{
  if ( SusyTauAlgBase::initialize() != EL::StatusCode::SUCCESS )
    return EL::StatusCode::FAILURE;

  // jet smearing
  m_JetSmearingTool = new JetSmearing::JetMCSmearingTool("JetSmearingTool");

  CHECK( m_JetSmearingTool->setProperty("NumberOfSmearedEvents",m_numSmear) );
  CHECK( m_JetSmearingTool->setProperty("DoPhiSmearing", true) );  
  CHECK( m_JetSmearingTool->initialize() );
  
  // set appropriate response maps
  std::string campaign("");  

  const xAOD::FileMetaData* fmd = nullptr; 
  CHECK( m_event->retrieveMetaInput( fmd, "FileMetaData" ) );

  if( wk()->metaData()->castBool("isMC") ) {
    std::string amiTag("");
    fmd->value(xAOD::FileMetaData::amiTag, amiTag);
    
    if ( amiTag.find("r9364")!=string::npos ) campaign = "MC16a";       
    else if ( amiTag.find("r10201")!=string::npos ) campaign = "MC16d";
    else if ( amiTag.find("r10724")!=string::npos ) campaign = "MC16e";
    else {
      std::cout<<"Unknown amiTag"<<std::endl;
      return EL::StatusCode::FAILURE;
    }
  }
  else {
    string project_name = wk()->metaData()->castString("project_name"); 
    if(project_name=="data15_13TeV" || project_name=="data16_13TeV") campaign = "MC16a";
    else if(project_name=="data17_13TeV") campaign = "MC16d";
    else if(project_name=="data18_13TeV") campaign = "MC16e";
    else {
      ATH_MSG_ERROR( "Unknown project_name " <<  project_name );
      return EL::StatusCode::FAILURE;
    } 
  }
  
  std::string pathToBVetoFile, pathToBTagFile;

  if(campaign=="MC16a") {
    pathToBVetoFile = PathResolverFindCalibFile("JetSmearing/Moriond19/R_map2019_bveto_EJES_p3627_r9364.root");
    pathToBTagFile = PathResolverFindCalibFile("JetSmearing/Moriond19/R_map2019_btag_EJES_p3627_r9364.root");
  }
  // only MC16d available
  else {
    pathToBVetoFile = PathResolverFindCalibFile("JetSmearing/Moriond19/R_map2019_bveto_EJES_p3627_r10201.root");
    pathToBTagFile = PathResolverFindCalibFile("JetSmearing/Moriond19/R_map2019_btag_EJES_p3627_r10201.root");
  }

  TFile* lightJetFile = TFile::Open(pathToBVetoFile.data());
  TFile* bJetFile = TFile::Open(pathToBTagFile.data());
  TH2F* lightJetResponse = (TH2F*)lightJetFile->Get("responseEJES");
  TH2F* bJetResponse = (TH2F*)bJetFile->Get("responseEJES");
  
  ATH_MSG_INFO( "Using following response maps:" );  
  ATH_MSG_INFO( pathToBVetoFile );  
  ATH_MSG_INFO( pathToBTagFile );  
  
  m_JetSmearingTool->SetResponseMaps(lightJetResponse,bJetResponse);

  // load phi smearing corrections
  TFile* file_phismear = TFile::Open( PathResolverFindCalibFile("JetSmearing/Moriond19/dijet_balance_Medium_metsig_M8_corrections.root").data() );
  const TF1* f_phiData; 
  const TF1* f_phiPseudoData;
  if(campaign=="MC16a") {
    f_phiData = (TF1*) file_phismear->Get("dPhi/Data1516");
    f_phiPseudoData = (TF1*) file_phismear->Get("dPhi/PseudoData1516");
  }
  else if(campaign=="MC16d") {
    f_phiData = (TF1*) file_phismear->Get("dPhi/Data17");
    f_phiPseudoData = (TF1*) file_phismear->Get("dPhi/PseudoData17");
  }
  else {
    f_phiData = (TF1*) file_phismear->Get("dPhi/Data18");
    f_phiPseudoData = (TF1*) file_phismear->Get("dPhi/PseudoData18");
  }
  m_JetSmearingTool->SetPhiCorrections(f_phiData, f_phiPseudoData);


  // tau smearing
  if(m_doTauSmearing == true) {
    
    m_TauSmearingTool = new TauSmearing::TauSmearingTool("TauSmearingTool");
    
    CHECK( m_TauSmearingTool->setProperty("NumberOfSmearedEvents",m_numSmear) );
    CHECK( m_TauSmearingTool->initialize() );
    
    std::string pathToTauFile = PathResolverFindCalibFile("SusyTauAnalysisXAOD/FakeTauResponse_02ConeTruth.root");
    //std::string pathToTauFile = PathResolverFindCalibFile("SusyTauAnalysisXAOD/FakeTauResponse.root");
    
    TFile* tauFile = new TFile(pathToTauFile.data());
    TH2F* fakeTauResponse = (TH2F*) tauFile->Get("FakeTauResponse");
    
    m_TauSmearingTool->SetResponseMaps(fakeTauResponse);
    
    TH1F* h_TauPtOverSeedJetPt = (TH1F*) tauFile->Get("TauPtOverSeedJetPt");   
    
    m_TauSmearingTool->SetOutOfConeHist( h_TauPtOverSeedJetPt );
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetSmearingAlg :: execute ()
{
  ContainerHandler handler;
  const xAOD::EventInfo *eventInfo = nullptr;
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  
  SusyTauSystVariation syst;
  CHECK( handler.RetrieveContainers( syst.m_systset ) );

  // call skipEvent, else smeared jet container won't exist for events failing cutflow
  if ( m_cutflow )
    {
      auto output = m_cutflow->ExecuteCuts( handler, 1. );

      if ( ! output.first ) {
	// make sure you include the bad jet veto in the cutflow
	// else bad jets with pt > 6.5 TeV will make the jet smearing code crash
	wk()->skipEvent(); 
	return EL::StatusCode::SUCCESS;
      }
  }

  const xAOD::ElectronContainer NominalElecCont = handler.electrons();
  const xAOD::MuonContainer NominalMuonCont = handler.muons();

  // calibrated jets to be used as input 
  const xAOD::JetContainer * jetCont = nullptr;
  CHECK( SusyTauGlobals::s_event->retrieve( jetCont, CalibrationAlg::ContainerName( Phys::ContainerType::Jet, syst.m_systset ) ) );

  // flag the jets to be smeared
  for (auto jet : *jetCont) {

    // baseline requires pt > 20 GeV and pass JVT, no eta cut - TO BE STUDIED
    if(jet->auxdataConst<char>("baseline")==true) jet->auxdecor<char>("smearjet") = true;
    else jet->auxdecor<char>("smearjet") = false;
    //jet->auxdecor<char>("smearjet") = true;

    if (debug) cout << "original jet pt " << jet->pt() << " eta " << jet->eta() << endl;
  }

  // smear jets - only jets with "smearjet" set to true will be smeared
  std::vector< std::unique_ptr< SmearData > > smearedEventsJet;
  m_JetSmearingTool->DoSmearing(smearedEventsJet,*jetCont);

  std::vector< std::unique_ptr< TauSmearing::SmearData > > smearedEventsTau;

  if(m_doTauSmearing == true) {

    // calibrated taus to be used as input 
    const xAOD::TauJetContainer * tauCont = nullptr;
    
    CHECK( SusyTauGlobals::s_event->retrieve( tauCont, CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst.m_systset ) ) );
    
    // needed to find seed jet, as tau->jet() does not work (LC jets not kept)
    //const xAOD::JetContainer alljets = handler.allJets();
    
    //  only taus with "smeartau" set to true will be smeared
    for (auto tau : *tauCont) {
      
      // WARNING: to be studied (lower down pt cut to avoid depleting low pt region)
      if(tau->auxdataConst<char>("baseline")==true) tau->auxdecor<char>("smeartau") = true;
      else tau->auxdecor<char>("smeartau") = false;    
      //tau->auxdecor<char>("smeartau") = true;
      
      if (debug) cout << "original tau pt " << tau->pt() << " eta " << tau->eta() << endl;
    }
    
    //m_TauSmearingTool->DoSmearing(smearedEventsTau, *tauCont, alljets);
    m_TauSmearingTool->DoSmearing(smearedEventsTau, *tauCont);
    
    if(smearedEventsJet.size() != smearedEventsTau.size()) {
      cout << "Error: inconsistent number of iterations for jets (" << smearedEventsJet.size() << ") and taus (" << smearedEventsTau.size() << "). Aborting." << endl;
      return EL::StatusCode::FAILURE ;
    }
  }

  if (debug) {
    auto OrigMetCont = handler.met();
    xAOD::MissingETContainer::const_iterator metcst_it = OrigMetCont->find("Final");
    double Origmet = (*metcst_it)->met();
    cout << "Original met: " << Origmet << endl;
  }

  unsigned int iteration = 0;

  for (auto& SmearedEvent : smearedEventsJet){

    xAOD::JetContainer* theJetContainer =  SmearedEvent->jetContainer;
    xAOD::ShallowAuxContainer* theAuxJetContainer = SmearedEvent->jetAuxContainer;

    if (debug) {
      xAOD::JetContainer::iterator firstJet = theJetContainer->begin();
      xAOD::JetContainer::iterator lastJet = theJetContainer->end();
      std::cout << " New smeared event" << std::endl;
      for (; firstJet != lastJet; ++firstJet) std::cout << "  smeared jet pt is " << (*firstJet)->pt() << std::endl;
      std::cout << "" << std::endl;
    }
    
    // update/reset decorations: 'selected' needs to be updated before overlap removal
    for ( auto jet : *theJetContainer ) {

      // the reset is done internally by the OR tool
      //jet->auxdata<char>("passOR") = true;
      
      jet->auxdata<char>("bjet") = false;
      SusyTauGlobals::s_susyDef->IsBJet( *jet );
      
      jet->auxdata<char>("bjet_loose") = false;
      SusyTauGlobals::s_susyDef->IsBJetLoose( *jet );

      // recompute JVT before passing jets to MET recalculation
      SusyTauGlobals::s_susyDef->JetPassJVT( *jet, true );  
      
      // JVT no longer part of baseline requirements, but used to prioritize jets in the OR procedure
      if( jet->pt()/1000. > 20. ) {
	jet->auxdata<char>("baseline") = true;
	if(jet->auxdataConst<char>("passJvt")) jet->auxdata<char>("selected") = 2;
	else jet->auxdata<char>("selected") = 1;
      }
      else {
	jet->auxdata<char>("baseline") = false;
	jet->auxdata<char>("selected") = 0;
      }    
      
      jet->auxdata<char>("bad") = false;
      jet->auxdata<char>("signal") = false;
    }

    string jetContName = CalibrationAlg::ContainerName( Phys::ContainerType::Jet, syst.m_systset, iteration );
    
    CHECK( m_store->record( theJetContainer, jetContName ) );
    CHECK( m_store->record( theAuxJetContainer, jetContName +"Aux.") );

    if (debug) cout << "Recorded jet collection under name " << jetContName << endl;

    // Nullify the pointers as the SmearedEvent object assumes it still has ownership over these
    SmearedEvent->jetContainer = nullptr;
    SmearedEvent->jetAuxContainer = nullptr;

    xAOD::TauJetContainer* smearTauContainer = nullptr;

    if(m_doTauSmearing == true) {
      
      smearTauContainer =  smearedEventsTau[iteration]->tauContainer;
      xAOD::ShallowAuxContainer* smearAuxTauContainer = smearedEventsTau[iteration]->tauAuxContainer;
      
      if (debug) {
	xAOD::TauJetContainer::iterator firstTau = smearTauContainer->begin();
	xAOD::TauJetContainer::iterator lastTau = smearTauContainer->end();
	for (; firstTau != lastTau; ++firstTau) std::cout << "  smeared tau pt is " << (*firstTau)->pt() << std::endl;
	std::cout << "" << std::endl;
      }
      
      // apply tau selection and update decorations
      for ( auto tau : *smearTauContainer ) {
	
	CHECK( SusyTauGlobals::s_susyDef->FillTau( *tau ) );
	
	tau->auxdata<char>("signal") = false;
	SusyTauGlobals::s_susyDef->IsSignalTau( *tau , 20000., 2.5);
      }    
      
      if (debug) {
	xAOD::TauJetContainer::iterator firstTau = smearTauContainer->begin();
	xAOD::TauJetContainer::iterator lastTau = smearTauContainer->end();
	for (; firstTau != lastTau; ++firstTau) std::cout << "  smeared tau pt AFTER TAT CALL is " << (*firstTau)->pt() << std::endl;
	std::cout << "" << std::endl;
      }
      
      string tauContName = CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst.m_systset, iteration );
      
      CHECK( m_store->record( smearTauContainer, tauContName ) );
      CHECK( m_store->record( smearAuxTauContainer, tauContName +"Aux.") );
      
      if (debug) cout << "Recorded tau collection under name " << tauContName << endl;
    }
    
    // pick up smeared or unsmeared taus
    xAOD::TauJetContainer theTauContainer = m_doTauSmearing ? *smearTauContainer : handler.taus();

    CHECK( SusyTauGlobals::s_susyDef->OverlapRemoval( &NominalElecCont, &NominalMuonCont, theJetContainer, nullptr, &theTauContainer, nullptr ) );
    
    // update remaining jet decorations after OR
    for ( auto jet : *theJetContainer ) {
      SusyTauGlobals::s_susyDef->IsBadJet( *jet );
      SusyTauGlobals::s_susyDef->IsSignalJet( *jet, 20000., 2.8 );
    }


    // recompute MET after jet and tau smearing
    xAOD::MissingETContainer *met = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
    met->setStore(met_aux);
    
    CHECK( SusyTauGlobals::s_susyDef->GetMET( *met, theJetContainer, &NominalElecCont, &NominalMuonCont, 0, &theTauContainer ) );
    
    string metContName = CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst.m_systset, iteration );
    
    CHECK( m_store->record( met, metContName ) );
    CHECK( m_store->record( met_aux, metContName +"Aux.") );
    
    if (debug) cout << "  Smeared MET: " << (*(met->find("Final")))->met() << endl;
    if (debug) cout << "  Recorded under label " << metContName <<endl;
    
    
    // not sure we need this...
    xAOD::MissingETContainer *metNoMuon = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* metNoMuon_aux = new xAOD::MissingETAuxContainer;  
    metNoMuon->setStore(metNoMuon_aux);  
    
    CHECK( SusyTauGlobals::s_susyDef->GetMET( *metNoMuon, theJetContainer, &NominalElecCont, &NominalMuonCont, 0, &theTauContainer, true, true, &NominalMuonCont ) );
    
    CHECK( m_store->record( metNoMuon, metContName + string("_noMuon") ) );
    CHECK( m_store->record( metNoMuon_aux, metContName + string("_noMuonAux.") ) );

    iteration ++;
  }  

  //if (debug)  m_store->print();
  return EL::StatusCode::SUCCESS;
}


// delete SmearingTool earlier, to avoid crashes on empty files
EL::StatusCode JetSmearingAlg::finalize()
{
  delete m_JetSmearingTool;
  m_JetSmearingTool = 0;

  if( m_doTauSmearing == true) {
    delete m_TauSmearingTool;
    m_TauSmearingTool = 0;
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetSmearingAlg::histFinalize()
{
  return SusyTauAlgBase::histFinalize();
}

