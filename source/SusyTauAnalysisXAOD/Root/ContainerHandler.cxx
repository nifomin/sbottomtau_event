#include "SusyTauAnalysisXAOD/ContainerHandler.h"
#include "SusyTauAnalysisXAOD/CalibrationAlg.h"
#include "SusyTauAnalysisXAOD/JetSmearingAlg.h"
#include <algorithm> 

static const char* APP_NAME = "ContainerHandler";

EL::StatusCode ContainerHandler :: RetrieveContainers( CP::SystematicSet syst )
{
    _jets = 0;
    _taus = 0;
    _muons = 0;
    _electrons = 0;
    _photons = 0;
    _met = 0;
    _metNoMuon = 0;

    _jets_truth = 0;
    _taus_truth = 0;
    _muons_truth = 0;
    _electrons_truth = 0;
    _met_truth = 0;
    
    CHECK( SusyTauGlobals::s_event->retrieve( _jets, CalibrationAlg::ContainerName( Phys::ContainerType::Jet, syst ) ) );
    CHECK( SusyTauGlobals::s_event->retrieve( _taus, CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst ) ) );
    CHECK( SusyTauGlobals::s_event->retrieve( _muons, CalibrationAlg::ContainerName( Phys::ContainerType::Muon, syst ) ) );
    CHECK( SusyTauGlobals::s_event->retrieve( _electrons, CalibrationAlg::ContainerName( Phys::ContainerType::Electron, syst ) ) );
    if (SusyTauGlobals::doPhotons) CHECK( SusyTauGlobals::s_event->retrieve( _photons, CalibrationAlg::ContainerName( Phys::ContainerType::Photon, syst ) ) );
    CHECK( SusyTauGlobals::s_event->retrieve( _met, CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst ) ) );
    CHECK( SusyTauGlobals::s_event->retrieve( _metNoMuon, CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst ) + string("_noMuon") ) );

    if (SusyTauGlobals::isMC){
      CHECK( SusyTauGlobals::s_event->retrieve( _jets_truth, "AntiKt4TruthJets") );
      CHECK( SusyTauGlobals::s_event->retrieve( _taus_truth, "TruthTaus") );
      CHECK( SusyTauGlobals::s_event->retrieve( _muons_truth, "TruthMuons" ) );
      CHECK( SusyTauGlobals::s_event->retrieve( _electrons_truth, "TruthElectrons" ) );
      CHECK( SusyTauGlobals::s_event->retrieve( _met_truth, "MET_Truth" ) );
    }    

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode ContainerHandler::RetrieveSmearContainers( CP::SystematicSet syst, int smearIteration, bool smearTau )
{
  _jets = 0;
  _taus = 0;
  _muons = 0;
  _electrons = 0;
  _photons = 0;
  _met = 0;
  _metNoMuon = 0;
 
  if (!SusyTauGlobals::s_event->contains<xAOD::JetContainer>(CalibrationAlg::ContainerName( Phys::ContainerType::Jet, syst, smearIteration )))
    return EL::StatusCode::FAILURE;
  
  CHECK( SusyTauGlobals::s_event->retrieve( _jets, CalibrationAlg::ContainerName( Phys::ContainerType::Jet, syst, smearIteration ) ) );
  
  if(smearTau == true) {
    if (!SusyTauGlobals::s_event->contains<xAOD::TauJetContainer>(CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst, smearIteration )))
      return EL::StatusCode::FAILURE; 
    
    CHECK( SusyTauGlobals::s_event->retrieve( _taus, CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst, smearIteration ) ) );
  }
  else
   CHECK( SusyTauGlobals::s_event->retrieve( _taus, CalibrationAlg::ContainerName( Phys::ContainerType::Tau, syst ) ) );

  CHECK( SusyTauGlobals::s_event->retrieve( _muons, CalibrationAlg::ContainerName( Phys::ContainerType::Muon, syst ) ) );
  CHECK( SusyTauGlobals::s_event->retrieve( _electrons, CalibrationAlg::ContainerName( Phys::ContainerType::Electron, syst ) ) );
  if (SusyTauGlobals::doPhotons) CHECK( SusyTauGlobals::s_event->retrieve( _photons, CalibrationAlg::ContainerName( Phys::ContainerType::Photon, syst ) ) );
 
  if (!SusyTauGlobals::s_event->contains<xAOD::MissingETContainer>(CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst, smearIteration ))) 
    return EL::StatusCode::FAILURE;
  
  CHECK( SusyTauGlobals::s_event->retrieve( _met, CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst, smearIteration ) ) );
  CHECK( SusyTauGlobals::s_event->retrieve( _metNoMuon, CalibrationAlg::ContainerName( Phys::ContainerType::MissingET, syst, smearIteration ) + string("_noMuon") ) );
  
  return EL::StatusCode::SUCCESS;  
}


EL::StatusCode ContainerHandler::GetContainers( CP::SystematicSet syst , int iteration, bool smearTau )
{
  if(iteration < 0) {
    CHECK( RetrieveContainers( syst ) );
  }
  else {
    CHECK( RetrieveSmearContainers( syst, iteration, smearTau ) );
  }
  
  return EL::StatusCode::SUCCESS;
}


// Methods that return the final collection with baseline selection applied
//   The selection below is arbitrary, used as an illustration
xAOD::JetContainer ContainerHandler :: jets()
{
    xAOD::JetContainer good_jets(SG::VIEW_ELEMENTS);
    
    for ( auto jet_itr = _jets->begin(); jet_itr != _jets->end(); ++jet_itr )
    {
	if( (*jet_itr)->auxdata<char>("baseline")==1
	    && (*jet_itr)->auxdata<char>("signal") == 1
	    && (*jet_itr)->auxdata<char>("passOR")==1 ) 
	{
	    good_jets.push_back( (*jet_itr) );
	}
    }
    
    std::sort( good_jets.begin(), good_jets.end(),
	       [](const xAOD::Jet * a, const xAOD::Jet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);
   
    return good_jets;
}

xAOD::JetContainer ContainerHandler :: baselineJets()
{
    xAOD::JetContainer good_jets(SG::VIEW_ELEMENTS);
    
    for ( auto jet_itr = _jets->begin(); jet_itr != _jets->end(); ++jet_itr )
    {
	if( (*jet_itr)->auxdata<char>("baseline")==1
	    && (*jet_itr)->auxdata<char>("passOR")==1 )
	{
	    good_jets.push_back( (*jet_itr) );
	}
    }
    
    std::sort( good_jets.begin(), good_jets.end(),
	       [](const xAOD::Jet * a, const xAOD::Jet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);
   
    return good_jets;
}

xAOD::JetContainer ContainerHandler :: allJets()
{
    xAOD::JetContainer good_jets(SG::VIEW_ELEMENTS);
    
    for ( auto jet_itr = _jets->begin(); jet_itr != _jets->end(); ++jet_itr )
    {
      good_jets.push_back( (*jet_itr) );
    }
    
    std::sort( good_jets.begin(), good_jets.end(),
	       [](const xAOD::Jet * a, const xAOD::Jet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);
   
    return good_jets;
}

xAOD::TauJetContainer ContainerHandler :: taus()
{
    xAOD::TauJetContainer good_taus(SG::VIEW_ELEMENTS);
    
    for ( auto tau_itr = _taus->begin(); tau_itr != _taus->end(); ++tau_itr )
      {
	if( (*tau_itr)->auxdata<char>("baseline") == 1 
	    && (*tau_itr)->auxdata<char>("signal") == 1 
	    && (*tau_itr)->auxdata<char>("passOR")==1 )
	  {
	    good_taus.push_back( (*tau_itr) );
	  }
      }
    
    std::sort( good_taus.begin(), good_taus.end(),
	       [](const xAOD::TauJet * a, const xAOD::TauJet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_taus;
}

xAOD::TauJetContainer ContainerHandler :: baselineTaus()
{
    xAOD::TauJetContainer good_taus(SG::VIEW_ELEMENTS);
    
    for ( auto tau_itr = _taus->begin(); tau_itr != _taus->end(); ++tau_itr )
    {
	if( (*tau_itr)->auxdata<char>("baseline") == 1 
	    && (*tau_itr)->auxdata<char>("passOR")==1 )
	    {
	      good_taus.push_back( (*tau_itr) );
	    }
    }
    
    std::sort( good_taus.begin(), good_taus.end(),
	       [](const xAOD::TauJet * a, const xAOD::TauJet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_taus;
}

xAOD::MuonContainer ContainerHandler :: muons()
{
    xAOD::MuonContainer good_muons(SG::VIEW_ELEMENTS);
    
    for ( auto muon_itr = _muons->begin(); muon_itr != _muons->end(); ++muon_itr )
      {
	if ( (*muon_itr)->auxdata<char>("baseline") == 1 
	    && (*muon_itr)->auxdata<char>("passOR")==1 )
	  good_muons.push_back( (*muon_itr) );
      }
    
    std::sort( good_muons.begin(), good_muons.end(),
	       [](const xAOD::Muon * a, const xAOD::Muon * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_muons;
}

xAOD::MuonContainer ContainerHandler :: signalMuons()
{
    xAOD::MuonContainer good_muons(SG::VIEW_ELEMENTS);
    
    for ( auto muon_itr = _muons->begin(); muon_itr != _muons->end(); ++muon_itr )
      {
	if ( (*muon_itr)->auxdata<char>("signal") == 1 
	    && (*muon_itr)->auxdata<char>("passOR")==1 )
	  good_muons.push_back( (*muon_itr) );
      }
    
    std::sort( good_muons.begin(), good_muons.end(),
	       [](const xAOD::Muon * a, const xAOD::Muon * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_muons;
}

xAOD::ElectronContainer ContainerHandler :: electrons()
{
    xAOD::ElectronContainer good_electrons(SG::VIEW_ELEMENTS);
    
    for ( auto elec_itr = _electrons->begin(); elec_itr != _electrons->end(); ++elec_itr )
    {
	if ( (*elec_itr)->auxdata<char>("baseline") == 1
	    && (*elec_itr)->auxdata<char>("passOR")==1 )
	{
	    good_electrons.push_back( (*elec_itr) );
	}
    }

    std::sort( good_electrons.begin(), good_electrons.end(),
	       [](const xAOD::Electron * a, const xAOD::Electron * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_electrons;
}

xAOD::ElectronContainer ContainerHandler :: signalElectrons()
{
    xAOD::ElectronContainer good_electrons(SG::VIEW_ELEMENTS);
    
    for ( auto elec_itr = _electrons->begin(); elec_itr != _electrons->end(); ++elec_itr )
    {
	if ( (*elec_itr)->auxdata<char>("signal") == 1
	    && (*elec_itr)->auxdata<char>("passOR")==1 )
	{
	    good_electrons.push_back( (*elec_itr) );
	}
    }

    std::sort( good_electrons.begin(), good_electrons.end(),
	       [](const xAOD::Electron * a, const xAOD::Electron * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	);

    return good_electrons;
}

xAOD::PhotonContainer ContainerHandler :: photons()
{
    xAOD::PhotonContainer good_photons(SG::VIEW_ELEMENTS);
    
    if (SusyTauGlobals::doPhotons){
      for ( auto photon_itr = _photons->begin(); photon_itr != _photons->end(); ++photon_itr )
      {
	  if( (*photon_itr)->auxdata<char>("baseline")==1
	      && (*photon_itr)->pt() > 20000.
	      && ( fabs( (*photon_itr)->eta()) < 2.5) )
	  {
	      good_photons.push_back( (*photon_itr) );
	  }
      }

      std::sort( good_photons.begin(), good_photons.end(),
		[](const xAOD::Photon * a, const xAOD::Photon * b) -> bool
		{
		    return a->pt() > b->pt();
		}
	  );
    }
    return good_photons;
}

xAOD::MissingETContainer * ContainerHandler :: met()
{
    return _met;
}

xAOD::MissingETContainer * ContainerHandler :: metNoMuon()
{
    return _metNoMuon;
}

xAOD::JetContainer ContainerHandler :: jets_truth()
{
    xAOD::JetContainer good_jets(SG::VIEW_ELEMENTS);

    xAOD::JetContainer * TJet = const_cast<xAOD::JetContainer*>(_jets_truth);
    
    for ( auto jet_itr = TJet->begin(); jet_itr != TJet->end(); ++jet_itr )
    {
      good_jets.push_back( (*jet_itr) );
    }
    
    std::sort( good_jets.begin(), good_jets.end(),
	       [](const xAOD::Jet * a, const xAOD::Jet * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	       );
   
    return good_jets;
}

xAOD::TruthParticleContainer ContainerHandler :: taus_truth()
{
    xAOD::TruthParticleContainer good_taus(SG::VIEW_ELEMENTS);
    
    xAOD::TruthParticleContainer * TTau = const_cast<xAOD::TruthParticleContainer*>(_taus_truth);

    for (auto tau_itr = TTau->begin(); tau_itr != TTau->end(); ++tau_itr )
    {
      if( (*tau_itr)->auxdataConst<char>("IsHadronicTau") )
	  {
	    good_taus.push_back( (*tau_itr) );
	  }
    }
    
    std::sort( good_taus.begin(), good_taus.end(),
	       [](const xAOD::TruthParticle * a, const xAOD::TruthParticle * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	       );

    return good_taus;
}


xAOD::TruthParticleContainer ContainerHandler :: muons_truth()
{
    xAOD::TruthParticleContainer good_muons(SG::VIEW_ELEMENTS);

    xAOD::TruthParticleContainer * TMuon = const_cast<xAOD::TruthParticleContainer*>(_muons_truth);
    
    for ( auto muon_itr = TMuon->begin(); muon_itr != TMuon->end(); ++muon_itr )
      {
	good_muons.push_back( (*muon_itr) );
      }
    
    std::sort( good_muons.begin(), good_muons.end(),
	       [](const xAOD::TruthParticle * a, const xAOD::TruthParticle * b) -> bool
	       {
		   return a->pt() > b->pt();
	       }
	       );

    return good_muons;
}

xAOD::TruthParticleContainer ContainerHandler :: electrons_truth()
{

    xAOD::TruthParticleContainer good_electrons(SG::VIEW_ELEMENTS);

    xAOD::TruthParticleContainer * TEle = const_cast<xAOD::TruthParticleContainer*>(_electrons_truth);
    
    for ( auto elec_itr = TEle->begin(); elec_itr != TEle->end(); ++elec_itr )
    {
      good_electrons.push_back( (*elec_itr) );
    }

    std::sort( good_electrons.begin(), good_electrons.end(),
	       [](const xAOD::TruthParticle * a, const xAOD::TruthParticle * b) -> bool
	       {
		 return a->pt() > b->pt();
	       }
	       );

    return good_electrons;
}

const xAOD::MissingETContainer * ContainerHandler :: met_truth()
{
    return _met_truth; 
}
