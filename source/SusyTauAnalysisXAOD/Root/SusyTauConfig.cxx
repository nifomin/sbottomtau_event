#include <SusyTauAnalysisXAOD/SusyTauConfig.h>
#include <SusyTauAnalysisXAOD/SusyTauGlobals.h>
#include <SampleHandler/Sample.h>
#include <SampleHandler/MetaObject.h>
#include <SampleHandler/ToolsMeta.h>
#include <SampleHandler/ToolsSplit.h>
#include <SampleHandler/fetch.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <SampleHandler/DiskListLocal.h>
#include <SampleHandler/DiskListXRD.h>
#include <SampleHandler/DiskList.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <PathResolver/PathResolver.h>
#include <string>
#include <exception>
#include <stdlib.h>
#include "TRegexp.h"

SusyTauConfig::SusyTauConfigParser::SusyTauConfigParser():
  m_allOptions("All options"),
  m_commonOptions("Common options"),
  m_analysisOptions("Analysis options"),
  m_systOptions("Systematic options"),
  m_batchOptions("Batch submission options"),
  m_gridOptions("Grid submission options"),
  m_containerOptions("Container"),
  m_variables(),
  m_isGrid(false) 
  {
  m_commonOptions.add_options()
    ("help", "produce help message")
    ("configFile",     boost::program_options::value<std::string>(), "Configuration file for options")
    ("inputPath",      boost::program_options::value<std::string>(), "path to sample location")
    ("xrootdServer",   boost::program_options::value<std::string>(), "server name, switches to DiskListXRD for SampleHandler")
    ("samplePattern",  boost::program_options::value<std::string>(), "sample pattern for selecting samples")
    ("nEvents",        boost::program_options::value<int>()->default_value(-1), "number of events to process")
    ("baselineNtuple", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "write baseline ntuples")
    ("plateauNtuple",  boost::program_options::value<bool>()->default_value(true)->implicit_value(true), "write XEplateau ntuples")
    ("sbottomNtuple",  boost::program_options::value<bool>()->default_value(true)->implicit_value(true), "write sbottom plateau ntuples")
    ("subDir",         boost::program_options::value<std::string>()->default_value("submitDir"), "submission directory")
    ("isSignal",       boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "assume sample type to be signal (false: use heuristic, true: override)");

  m_analysisOptions.add_options()
    ("tauID", boost::program_options::value<std::string>()->default_value("Loose"), "baseline tau ID: Loose/Medium/Tight")
    ("localTauConfig", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "use own tau config")
    ("jetCont", boost::program_options::value<std::string>()->default_value("AntiKt4EMTopoJets"), "jet container")
    ("jetSmear",boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "Activate jet smearing")
    ("tauSmear",boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "Activate tau smearing")
    ("numSmear",boost::program_options::value<unsigned int>()->default_value(200), "number of jet smearing iterations")
    ("bjetTrig",boost::program_options::value<bool>()->default_value(false)->implicit_value(true),"b-jet trigger matching")
    ("jetSystNp",boost::program_options::value<int>()->default_value(1),"Jet systematics parameter set");

  m_systOptions.add_options()
    ("enableJetSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable jet systematics")
    ("enableTauSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable tau systematics")
    ("enableMuonSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable muon systematics")
    ("enableElecSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable electron systematics")
    ("enablePhotSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable photon systematics")
    ("enableMETSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable MET systematics")
    ("enableAllSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable all systematics")
    ("enableWeightSyst", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "enable weight systematics")
    ("skipNominal", boost::program_options::value<bool>()->default_value(false)->implicit_value(true), "skip NOMINAL tree");

  m_batchOptions.add_options()
    ("shellInit", boost::program_options::value<std::string>()->default_value(""), "shell setup for batch jobs")
    ("filesPerJob", boost::program_options::value<int>()->default_value(5), "number of files per job")
    ("queue", boost::program_options::value<std::string>(), "queue to use in submission")
    ("extraOptions", boost::program_options::value<std::string>(), "extra PBS parameters for submission");

  m_gridOptions.add_options()
    ("outputPrefix", boost::program_options::value<std::string>(), "Prefix for output DS name. Example: skimV1 will create output DS user.nickname.skimV1")
    ("gridSampleList",boost::program_options::value< std::vector<std::string> >(), "List of grid samples to process, normally read in via file")
    ("gridSampleFile", boost::program_options::value<std::string>()->default_value("SusyTauAnalysisXAOD/config/grid_samples.cfg"), "Location of the sample list file")
    ("gridNFilesPerJob", boost::program_options::value<int>()->default_value(-1), "Number of files per job");

  m_containerOptions.add_options()
    ("jetCont", boost::program_options::value<std::string>(), "Jets container")
    ("tauCont", boost::program_options::value<std::string>(), "Taus container")
    ("muonCont", boost::program_options::value<std::string>(), "Muons container")
    ("electronCont", boost::program_options::value<std::string>(), "Electron container")
    ("photonCont", boost::program_options::value<std::string>(), "Photons container")
    ("skimContainers", boost::program_options::value< std::vector<std::string> >(), "Containers to keep for skim")
    ("skimMcContainers", boost::program_options::value< std::vector<std::string> >(), "Additional containers to keep for MC skim");

  m_commonOptions.add( m_systOptions );
  m_commonOptions.add( m_analysisOptions );

  m_allOptions.add( m_commonOptions );
  m_allOptions.add( m_containerOptions );
}

void SusyTauConfig::SusyTauConfigParser::enableBatchOptions() {
  m_commonOptions.add( m_batchOptions );
}

void SusyTauConfig::SusyTauConfigParser::enableGridOptions() {
  m_commonOptions.add( m_gridOptions );
  m_isGrid = true;
}

SH::SampleHandler SusyTauConfig::SusyTauConfigParser::GetSampleHandler() {
  SH::SampleHandler sh; // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SampleHandler

  if ( ! m_isGrid ) {
    // setup scanner
    auto scanner = SH::ScanDir()
                  .sampleDepth(0)
                  .samplePostfix("_tid*_*");

    if ( m_variables.count( "samplePattern" ) )
      scanner.samplePattern( m_variables["samplePattern"].as<std::string>() );
    
    // setup file list
    SH::DiskList* list;
    if (m_variables.count("xrootdServer")) {
      list = new SH::DiskListXRD(m_variables["xrootdServer"].as<std::string>(), m_variables["inputPath"].as<std::string>(), true);
    } else {
      list = new SH::DiskListLocal(m_variables["inputPath"].as<std::string>());
    }
    
    // scan
    scanner.scan( sh, *list );
    delete list;
    
  } else {
    for ( auto sampleName : m_variables["gridSampleList"].as< std::vector<std::string> >() )
      SH::scanRucio (sh, sampleName);
  }

  sh.setMetaString( "nc_tree", "CollectionTree" );
  try {
    ConfigureSamples( sh );
  } catch (exception& e) {
    cout << "Autoconfiguration failed due to error: " << e.what() << endl;
    cout << "Terminating..." << std::endl;
    exit(1);
  }
  std::cout<<"Samples found:"<<endl;
  sh.print();

  return sh;
}

EL::Job SusyTauConfig::SusyTauConfigParser::GetJob() {
  EL::Job job;
  job.options()->setDouble (EL::Job::optMaxEvents, m_variables["nEvents"].as< int >());
  job.options()->setDouble (EL::Job::optCacheSize, 20*1024*1024);
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
  job.useXAOD ();

  return job;
}

SusyTauConfig::SystematicSettings SusyTauConfig::SusyTauConfigParser::GetSystConfig() {
  SusyTauConfig::SystematicSettings config;

  if ( m_variables["enableAllSyst"].as<bool>() ) {
    config.enableJet();
    config.enableTau();
    config.enableMuon();
    config.enableElectron();
    config.enableMET();
    // don't turn on photon systematics
  } else {
    if ( m_variables["enableJetSyst"].as<bool>() )
      config.enableJet();
    if ( m_variables["enableTauSyst"].as<bool>() )
      config.enableTau();
    if ( m_variables["enableMuonSyst"].as<bool>() )
      config.enableMuon();
    if ( m_variables["enableElecSyst"].as<bool>() )
      config.enableElectron();
    if ( m_variables["enablePhotSyst"].as<bool>() )
      config.enablePhoton();
    if ( m_variables["enableMETSyst"].as<bool>() )
      config.enableMET();
    if ( m_variables["enableWeightSyst"].as<bool>() )
      config.enableWeight();
  }

  return config;
}

std::string SusyTauConfig::SusyTauConfigParser::GetParametersPBS() {
  std::string parameters;
  if ( m_variables.count( "queue" ) ) {
    parameters += "-q " + m_variables["queue"].as<std::string>();
  }

  if ( m_variables.count( "extraOptions" ) ) {
    parameters += " " + m_variables["extraOptions"].as<std::string>();
  }

  return parameters;
}

bool SusyTauConfig::SusyTauConfigParser::ParseCommandLine( int ac, char* av[]) {
  boost::program_options::store(boost::program_options::parse_command_line(ac, av, m_commonOptions), m_variables);

  if ( m_variables.count("configFile") ) {
    std::ifstream configFile ( m_variables["configFile"].as<std::string>() , std::ifstream::in );
    boost::program_options::store(boost::program_options::parse_config_file(configFile,m_commonOptions), m_variables);
    configFile.close();
  }

  {
    std::ifstream contConfig ( PathResolverFindCalibFile("SusyTauAnalysisXAOD/config/container.cfg") , std::ifstream::in );
    boost::program_options::store(boost::program_options::parse_config_file(contConfig,m_containerOptions), m_variables);
    contConfig.close();
  }

  if ( m_variables.count("gridSampleFile") ) {
    std::ifstream sampleConfig ( PathResolverFindCalibFile( "SusyTauAnalysisXAOD/config/" + m_variables["gridSampleFile"].as<std::string>() ) , std::ifstream::in );
    boost::program_options::store(boost::program_options::parse_config_file(sampleConfig,m_gridOptions), m_variables);
    sampleConfig.close();
  }

  boost::program_options::notify(m_variables);

  if ( m_variables.count("help") ) {
    std::cout << m_commonOptions << std::endl;
    return false;
  }

  if ( ! m_isGrid ) {
    if ( ! m_variables.count( "inputPath" ) ) {
      std::cout << "Please provide the path to the samples to read from (--inputPath)" << std::endl;
      return false;
    }
  } else {
    if ( ! m_variables.count("gridSampleFile") && ! m_variables.count("gridSampleList") ) {
      std::cout << "Please provide a config file with grid samples (--gridSampleFile) to run over or a sample list (--gridSampleList)" << std::endl;
      return false;
    }
    if ( ! m_variables.count( "outputPrefix" ) ) {
      std::cout << "Please provide the prefix to append to the output samples (--outputPrefix)" << std::endl;
      return false;
    }
  }

  return true;
}

std::vector<SusyTauSystVariation> SusyTauConfig::RetrieveSystematicList( SusyTauConfig::SystematicSettings & settings, bool isFullSim, bool addNominal ) {
  std::vector<SusyTauSystVariation> systList;
  if ( addNominal )
    systList.push_back( SusyTauSystVariation() );

  auto systematics = SusyTauGlobals::s_susyDef->getSystInfoList();

  for( auto& sysInfo : systematics ) {
    // Filter out unwanted systematics. For now, hardcoded, easy to make it cleaner.

    TRegexp trig_regex("[tT][rR][iI][gG]");
    int length;
    if ( trig_regex.Index( sysInfo.systset.name(), &length ) != -1 && sysInfo.systset.name().find("MUON") == string::npos ) {
      //cout << "SusyTauConfig::RetrieveSystematicList: Found a trigger systematics " << sysInfo.systset.name() << ". Skipping it..." << endl;
      continue;
    } else if( sysInfo.systset.name().find("TILECORR") != string::npos ) {
      //cout << "SusyTauConfig::RetrieveSystematicList: Found a dead tile correction systematics " << sysInfo.systset.name() << " (not used). Skipping it..." << endl;
      continue;
    } else if( (sysInfo.systset.name().find("PH_EFF") != string::npos || sysInfo.systset.name().find("PRW_DATASF") != string::npos) && !settings.doPhoton() ) {
      //cout << "SusyTauConfig::RetrieveSystematicList: Found a photon efficiency systematics " << sysInfo.systset.name() << " (not used). Skipping it..." << endl;
      continue;
    } else if( sysInfo.systset.name().find("JET_LargeR") != string::npos
               || sysInfo.systset.name().find("JET_MassRes") != string::npos
               || sysInfo.systset.name().find("JET_CombMass") != string::npos
               || sysInfo.systset.name().find("JET_Rtrk") != string::npos
               || (sysInfo.systset.name().find("JET_") != string::npos && sysInfo.systset.name().find("R10") != string::npos) ) {
      //cout << "SusyTauConfig::RetrieveSystematicList: Found a large-R jet systematics " << sysInfo.systset.name() << " (not used). Skipping it..." << endl;
      continue;
    } else if( sysInfo.systset.name().find("AF2") != string::npos && isFullSim ) {
      continue;
    } else if( sysInfo.systset.name().find("EL_CHARGEID") != string::npos || sysInfo.systset.name().find("EL_EFF_ChargeID") != string::npos ) {
      continue;
    }

    if ( settings.doTau() && sysInfo.affectsType == ST::SystObjType::Tau )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doJet() && ( sysInfo.affectsType == ST::SystObjType::Jet || sysInfo.affectsType == ST::SystObjType::BTag ) )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doMuon() && sysInfo.affectsType == ST::SystObjType::Muon )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doElectron() && sysInfo.affectsType == ST::SystObjType::Electron )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doPhoton() && sysInfo.affectsType == ST::SystObjType::Photon )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doMET() && ( sysInfo.affectsType == ST::SystObjType::MET_TST || sysInfo.affectsType == ST::SystObjType::MET_CST || sysInfo.affectsType == ST::SystObjType::MET_Track ) )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
    else if ( settings.doWeight() && sysInfo.affectsWeights == true )
      systList.push_back( SusyTauSystVariation( sysInfo ) );
  }

  if( settings.doTau() ) {
    ST::SystInfo ST_tau_tes_loosebdt_up;
    ST_tau_tes_loosebdt_up.systset.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_SME_TES_LOOSEBDT",  1));
    ST_tau_tes_loosebdt_up.affectsKinematics = true;
    ST_tau_tes_loosebdt_up.affectsWeights = false;
    ST_tau_tes_loosebdt_up.affectsType = ST::SystObjType::Tau;
    systList.push_back( SusyTauSystVariation( ST_tau_tes_loosebdt_up ) );

    ST::SystInfo ST_tau_tes_loosebdt_down;
    ST_tau_tes_loosebdt_down.systset.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_SME_TES_LOOSEBDT",  -1));
    ST_tau_tes_loosebdt_down.affectsKinematics = true;
    ST_tau_tes_loosebdt_down.affectsWeights = false;
    ST_tau_tes_loosebdt_down.affectsType = ST::SystObjType::Tau;
    systList.push_back( SusyTauSystVariation( ST_tau_tes_loosebdt_down ) );

    if ( !isFullSim ) {
      ST::SystInfo ST_tau_tes_fast_up;
      ST_tau_tes_fast_up.systset.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_SME_TES_AFII",  1));
      ST_tau_tes_fast_up.affectsKinematics = true;
      ST_tau_tes_fast_up.affectsWeights = false;
      ST_tau_tes_fast_up.affectsType = ST::SystObjType::Tau;
      systList.push_back( SusyTauSystVariation( ST_tau_tes_fast_up   ) );

      ST::SystInfo ST_tau_tes_fast_down;
      ST_tau_tes_fast_down.systset.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_SME_TES_AFII", -1));
      ST_tau_tes_fast_down.affectsKinematics = true;
      ST_tau_tes_fast_down.affectsWeights = false;
      ST_tau_tes_fast_down.affectsType = ST::SystObjType::Tau;
      systList.push_back( SusyTauSystVariation( ST_tau_tes_fast_down ) );
    }
  }

  if( (settings.doTau() || settings.doWeight()) && !isFullSim ) {
    ST::SystInfo ST_tau_eff;
    ST_tau_eff.affectsKinematics = false;
    ST_tau_eff.affectsWeights = true;
    ST_tau_eff.affectsType = ST::SystObjType::Tau;

    CP::SystematicSet afii_ele_up;
    afii_ele_up.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_ELEOLR_AFII",  1) );
    ST_tau_eff.systset = afii_ele_up;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );

    CP::SystematicSet afii_ele_dn;
    afii_ele_dn.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_ELEOLR_AFII",  -1) );
    ST_tau_eff.systset = afii_ele_dn;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );

    // no AF2 systematics for ELEOLR for reco taus matched to true electrons

    CP::SystematicSet afii_jetid_up;
    afii_jetid_up.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_JETID_AFII",  1) );
    ST_tau_eff.systset = afii_jetid_up;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );

    CP::SystematicSet afii_jetid_dn;
    afii_jetid_dn.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_JETID_AFII",  -1) );
    ST_tau_eff.systset = afii_jetid_dn;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );

    CP::SystematicSet afii_reco_up;
    afii_reco_up.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_RECO_AFII",  1) );
    ST_tau_eff.systset = afii_reco_up;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );

    CP::SystematicSet afii_reco_dn;
    afii_reco_dn.insert( CP::SystematicVariation("TAUS_TRUEHADTAU_EFF_RECO_AFII",  -1) );
    ST_tau_eff.systset = afii_reco_dn;
    systList.push_back( SusyTauSystVariation( ST_tau_eff ) );
  }


  // printed in more details in CalibrationAlg, NtupleVectorWriter, ...
  /*
  for( auto& sysInfo : systList )
  {
  	std::cout << sysInfo.m_systset.name() << std::endl;
  }
  */
  return systList;
}

void SusyTauConfig::SusyTauConfigParser::ConfigureSamples( SH::SampleHandler & sh ) {

  sh.setMetaString("tauID", SusyTauGlobals::s_config.GetOptions()["tauID"].as<std::string>() );

  for ( SH::SampleHandler::iterator iter = sh.begin(); iter != sh.end(); ++ iter) {
    SH::Sample * sample = *iter;

    const TString sample_name( sample->name().c_str() );
    int length;

    TRegexp data_regex("^data");
    TRegexp mc_regex("^mc");
    TRegexp mc_regex2("e[0-9]*_");

    TRegexp fullsim_regex("e[0-9]*_s");
    TRegexp fastsim_regex("e[0-9]*_a");

    TRegexp full_regex(".AOD.");
    TRegexp deriv_regex(".DAOD_SUSY");
    TRegexp private_data_regex("^user.*data.*");
    TRegexp private_data_regex2(".00[0-9][0-9][0-9][0-9][0-9][0-9].");
    TRegexp private_mc_regex("^user.*mc.*");

    TRegexp deriv_3_regex(".DAOD_SUSY3");
    TRegexp deriv_5_regex(".DAOD_SUSY5");
    TRegexp deriv_11_regex(".DAOD_SUSY11");
    TRegexp debugstream_regex("debugrec_hlt");

    TRegexp signal_regex("GMSB");
    TRegexp signal_regex2("2stepStau");
    TRegexp signal_regex3("BB_onestepN2hN1");
    TRegexp sherpa_regex("Sh");

    TRegexp sampleid_regex(".[0-9][0-9][0-9][0-9][0-9][0-9].");

    if ( deriv_regex.Index( sample_name, &length ) != -1 || private_data_regex.Index( sample_name, &length ) != -1 || private_mc_regex.Index( sample_name, &length ) != -1)
      sample->meta()->setBool("isDerived", true );
    else if ( full_regex.Index( sample_name, &length ) != -1 )
      sample->meta()->setBool("isDerived", false );

    if  ( deriv_3_regex.Index( sample_name, &length ) != -1 )
      sample->meta()->setInteger("derivationNum", 3 );
    else if ( deriv_5_regex.Index( sample_name, &length ) != -1 )
      sample->meta()->setInteger("derivationNum", 5 );
    else if ( deriv_11_regex.Index( sample_name, &length ) != -1 )
      sample->meta()->setInteger("derivationNum", 11 );
    else
      sample->meta()->setInteger("derivationNum", -1 );

    if ( mc_regex.Index( sample_name, &length ) != -1 || mc_regex2.Index( sample_name, &length ) != -1 || private_mc_regex.Index( sample_name, &length ) != -1 ) {

      sample->meta()->setBool("isMC", true );
      sample->meta()->setBool("isDebugStream", false );

      if ( fullsim_regex.Index( sample_name, &length ) != -1 )
        sample->meta()->setBool("isFullsim", true );
      else if ( fastsim_regex.Index( sample_name, &length ) != -1 )
        sample->meta()->setBool("isFullsim", false );

      if ( ( m_variables["isSignal"].as<bool>() ) || signal_regex.Index( sample_name, &length ) != -1 || signal_regex2.Index( sample_name, &length ) != -1 || signal_regex3.Index( sample_name, &length ) != -1 )
        sample->meta()->setBool("isSignal", true );
      else
        sample->meta()->setBool("isSignal", false );
      

      sample->meta()->setBool("isSherpa", sherpa_regex.Index( sample_name, &length ) != -1 );

      int loc = sampleid_regex.Index( sample_name, &length );
      int sampleID = 0;
      if ( loc != -1 )
        sampleID = std::stoi( sample->name().substr(loc+1,length-2) );

      sample->meta()->setInteger( "sampleID", sampleID );
      sample->meta()->setString( "sampleType",  sample->meta()->castBool("isSignal") ? "signal" : SampleType( sampleID ) );

      // buggy DSID in MC16e Znunu Sherpa metadata
      if( sampleID >= 366010 && sampleID <= 366035 ) {
        if(sample->name().find("BFilter")!=string::npos) sample->meta()->setString("HFFilter", "BFilter");
        else if(sample->name().find("CVetoBVeto")!=string::npos) sample->meta()->setString("HFFilter", "CVetoBVeto");
        else if(sample->name().find("CFilterBVeto")!=string::npos) sample->meta()->setString("HFFilter", "CFilterBVeto");
        else {
          std::cout << "WARNING! Failed to parse HFFilter for Znunu, PRW will crash..." << std::endl;
          sample->meta()->setString("HFFilter", "none");
        }
      } else sample->meta()->setString("HFFilter", "none");

      if(sample->name().find("r9364")!=string::npos) sample->meta()->setString("MC16", "MC16a");
      else if(sample->name().find("r10201")!=string::npos) sample->meta()->setString("MC16", "MC16d");
      else if(sample->name().find("r10724")!=string::npos) sample->meta()->setString("MC16", "MC16e");
      else {
        cout << "WARNING! Failed to determine MC16 campaign from rtag." << endl;
      }
      sample->meta()->setString( "project_name", sample->name().substr(0,10) );
    } else if ( data_regex.Index( sample_name, &length ) != -1 || private_data_regex.Index( sample_name, &length ) != -1 || private_data_regex2.Index( sample_name, &length ) != -1 ) {
      sample->meta()->setBool("isMC", false );
      sample->meta()->setBool("isSignal", false );
      sample->meta()->setBool("isSherpa", false );
      sample->meta()->setString("HFFilter", "none");
      sample->meta()->setString( "sampleType",  "data" );
      sample->meta()->setString( "project_name", sample->name().substr(0,12) );
      sample->meta()->setString("MC16", "none");

      if ( debugstream_regex.Index( sample_name, &length ) != -1 )
        sample->meta()->setBool("isDebugStream", true );
      else
        sample->meta()->setBool("isDebugStream", false );
    } else
      std::cout << "\t\t - UNKNOWN sample type, neither data nor MC!" << std::endl;


    std::cout << "Sample " << sample->name() << " autoconfigured:" << std::endl;
    std::cout << std::setw(35) << std::left << "\t- Is this a derivation?"<< sample->meta()->castBool("isDerived") << std::endl;
    if ( sample->meta()->castBool("isDerived") ) {
      if ( sample->meta()->castInteger("derivationNum") > 0 )
        std::cout << "\t\t - Derivation is SUSY" << sample->meta()->castInteger("derivationNum") << std::endl;
      else
        std::cout << "\t\t - UNKNOWN derivation!" << std::endl;
    }
    std::cout << std::setw(35) << std::left << "\t- Is this MC?"<< sample->meta()->castBool("isMC") << std::endl;
    if ( sample->meta()->castBool("isMC") ) {
      std::cout << std::setw(35) << std::left << "\t\t- Is this full sim sample?"<< sample->meta()->castBool("isFullsim") << std::endl;
      std::cout << std::setw(35) << std::left << "\t\t- Is this a signal sample?"<< sample->meta()->castBool("isSignal") << std::endl;
    }
    std::cout << std::setw(35) << std::left << "\t- Sample ID" << sample->meta()->castInteger("sampleID") << std::endl;
    std::cout << std::setw(35) << std::left << "\t- Sample type" << sample->meta()->castString("sampleType") << std::endl;


    sample->meta()->setInteger("jetSystNp", SusyTauGlobals::s_config.GetOptions()["jetSystNp"].as<int>() );

    sample->meta()->setBool("localTauConfig", SusyTauGlobals::s_config.GetOptions()["localTauConfig"].as<bool>() );

    sample->meta()->setString("jetCont", SusyTauGlobals::s_config.GetOptions()["jetCont"].as<std::string>() );
  }
}

int SusyTauConfig::GetDerivNum( SH::SampleHandler & sh ) {
  int format = 0;

  for ( SH::SampleHandler::iterator iter = sh.begin(); iter != sh.end(); ++ iter) {
    SH::Sample * sample = *iter;
    if(format==0) {
      format = sample->meta()->castInteger("derivationNum");
    } else if( format != sample->meta()->castInteger("derivationNum") ) {
      throw std::runtime_error( std::string("Can't mix SUSY") + std::to_string(sample->meta()->castInteger("derivationNum"))
                                + std::string(" and SUSY") + std::to_string(format) + std::string(" inupts.") );
      return 0;
    }
  }

  return format;
}

std::string SusyTauConfig::SampleType( int process_id ) {

  if ( (process_id >= 410470 && process_id <= 410471 )  ||  (process_id >= 407342 && process_id <= 407347 ) || process_id==345935 )
    return "ttbar";

  if ( (process_id >= 410644 && process_id <= 410647) ||  (process_id >= 410658 && process_id <= 410659) ||  (process_id >= 407018 && process_id <= 407021)
       || (process_id >= 411193 && process_id <= 411198) || (process_id >= 411181 && process_id <= 411186) )
    return "singletop";

  if ( ( process_id >= 410155 && process_id <= 410157 ) ||  ( process_id >= 410218 && process_id <= 410220 ) ||  ( process_id >= 410276 && process_id <= 410278 ))
    return "ttbarV";

  if ( ( process_id >= 364170 && process_id <= 364183 ) )
    return "wenu";

  if ( ( process_id >= 364156 && process_id <= 364169 ) )
    return "wmunu";

  if ( ( process_id >= 364184 && process_id <= 364197 ) )
    return "wtaunu";

  if ( ( process_id >= 364114 && process_id <= 364127 ) )
    return "zee";

  if ( ( process_id >= 364100 && process_id <= 364113 ) )
    return "zmumu";

  if ( ( process_id >= 364128 && process_id <= 364141 ) )
    return "ztautau";

  if ( ( process_id >= 364142 && process_id <= 364155 ) || ( process_id >= 364222 && process_id <= 364223 )  || ( process_id >= 366010 && process_id <= 366035 ))
    return "znunu";

  if ( ( process_id >= 364198  && process_id <= 364215 ) || ( process_id >= 364280  && process_id <= 364282 ) || ( process_id >= 364358  && process_id <= 364363 ))
    return "zlowmass";

  if ( ( process_id >= 308092  && process_id <= 308098 ) )
    return "V2jets";

  if ( ( process_id >= 363355 && process_id <= 363360 ) || process_id == 363489 || process_id == 345723 || ( process_id >= 364302 && process_id <= 364305 ) || process_id == 364286 || process_id == 345715 || process_id == 345718 || ( process_id >= 345705 && process_id <= 345706 ) || ( process_id >= 364283  && process_id <= 364290  ))
    return "diboson";

  if ( ( process_id >= 364242 && process_id <=364255 ) || process_id == 363494 || ( process_id >= 407311 && process_id <= 407315 ) )
    return "triboson";

  if ( ( process_id == 342284 || process_id == 342285 ) || (process_id >=345073 && process_id <=345076) || (process_id >=345120 && process_id <=345123) )
    return "Higgs";

  if ( process_id >= 372820 && process_id <= 372899 )
    return "GMSB";

  if ( (process_id >= 373200 && process_id <= 373299) || (process_id >= 373400 && process_id <= 373404) || (process_id >= 393951 && process_id <= 394014) )
    return "2StepStau";

  if ( process_id >= 310759 && process_id <= 310767 )
    return "monoH";

  if ( process_id ==410080 || process_id ==410081 || process_id == 304014 || process_id == 407321 || process_id == 410560 || process_id == 410408 || (process_id >=345940 && process_id <=345942) )
    return "ttX";

  if ( (process_id >= 390130 && process_id <=390317) || (process_id >=391834 && process_id <=391916) || (process_id >=391924 && process_id <=391932)  || (process_id >=436483  && process_id <= 436531)
       || (process_id >= 437333 && process_id <= 437334) )
    return "sbottom";

  if (process_id >= 361600 && process_id <= 361611)
    return "diboson_alt";

  if ( (process_id >= 361510 && process_id <= 361514) || (process_id >= 361638 && process_id <= 361642) )
    return "ztautau_alt";

  if (process_id >= 363123 && process_id <= 363146)
    return "zmumu_alt";

  if (process_id >= 361515 && process_id <= 361519)
    return "znunu_alt";

  if (process_id >= 363624 && process_id <= 363647)
    return "wmunu_alt";

  if (process_id >= 363648 && process_id <= 363671)
    return "wtaunu_alt";

  if ( (process_id >= 410654 && process_id <= 410657) || process_id == 412002 || (process_id >= 411032 && process_id <= 411037)
       || (process_id >= 411187 && process_id <= 411192) || (process_id >= 411199 && process_id <= 411204) || (process_id >= 412004 && process_id <= 412005) )
    return "singletop_alt";

  if ( (process_id >= 410557 && process_id <= 410559) || (process_id >= 410464 && process_id <= 410466) || (process_id >= 410480 && process_id <= 410482)
       || (process_id >= 407348 && process_id <= 407350) || (process_id >= 407354 && process_id <= 407359) )
    return "ttbar_alt";

  throw std::runtime_error( std::string("Unknown sample type for DSID ") + std::to_string(process_id)  );
  return "Unknown";
}

