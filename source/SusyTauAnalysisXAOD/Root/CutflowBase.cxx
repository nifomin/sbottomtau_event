#include "SusyTauAnalysisXAOD/CutflowBase.h"
#include "EventLoop/StatusCode.h"

TH1F * CutflowBase::GetHist()
{
    if ( m_hist )
	return m_hist;
    
    m_hist = new TH1F( (m_name + "_cutflow").c_str(), (m_name + " Cutflow").c_str(),
		       m_cuts.size(), -0.5, m_cuts.size()-0.5 );
    m_hist_raw = new TH1F( (m_name + "_cutflow_unweighted").c_str(), (m_name + " Cutflow (Unweighted entries)").c_str(),
		       m_cuts.size(), -0.5, m_cuts.size()-0.5 );

    for ( unsigned int i = 1; i <= m_cuts.size(); i++ )
    {
	m_hist->GetXaxis()->SetBinLabel( i, m_cuts[i-1].first.c_str() );
	m_hist_raw->GetXaxis()->SetBinLabel( i, m_cuts[i-1].first.c_str() );
    }
    
    return m_hist;
}

TH1F * CutflowBase::GetHistRaw()
{
    if ( m_hist_raw )
	return m_hist_raw;
    
    m_hist = new TH1F( (m_name + "_cutflow").c_str(), (m_name + " Cutflow").c_str(),
		       m_cuts.size(), -0.5, m_cuts.size()-0.5 );
    m_hist_raw = new TH1F( (m_name + "_cutflow_unweighted").c_str(), (m_name + " Cutflow (Unweighted entries)").c_str(),
		       m_cuts.size(), -0.5, m_cuts.size()-0.5 );

    for ( unsigned int i = 1; i <= m_cuts.size(); i++ )
    {
	m_hist->GetXaxis()->SetBinLabel( i, m_cuts[i-1].first.c_str() );
	m_hist_raw->GetXaxis()->SetBinLabel( i, m_cuts[i-1].first.c_str() );
    }
    
    return m_hist_raw;
}

void CutflowBase::PrintCutFlow()
{
    std::cout << "Cutflow for " << m_name << std::endl;
    for ( auto cut : m_cuts )
	std::cout << "\t" << cut.first << ":  " << cut.second << std::endl;
}

void CutflowBase::RegisterCut( std::string cutname )
{
    m_cuts.push_back( std::pair<std::string, double>{cutname, 0} );
}
    
EL::StatusCode CutflowBase::FillCut( std::string cutname, double weight )
{
    for ( unsigned int i = 0; i < m_cuts.size(); i++ )
    {
	if ( m_cuts[i].first == cutname )
	{
	    return FillCut( i, weight );
	}
    }
    return EL::StatusCode::FAILURE;
}

EL::StatusCode CutflowBase::FillCut( unsigned int pos, double weight )
{
    if ( pos >= m_cuts.size() )
	return EL::StatusCode::FAILURE;
    
    m_cuts[pos].second += 1.0;
    if ( m_hist )
    {
	m_hist->Fill( pos, weight );
	m_hist_raw->Fill( pos, 1.0  );
    }

    return EL::StatusCode::SUCCESS;
}

int CutflowBase::GetCutPos( std::string cutname )
{
    for ( unsigned int i = 0; i < m_cuts.size(); i++ )
    {
	if ( m_cuts[i].first == cutname )
	{
	    return i;
	}
    }
    return -1;
}
