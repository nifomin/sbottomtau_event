// $Id$

// Local include(s):
#include "SusyTauAnalysisXAOD/FileMetaDataTool.h"

namespace xAODMaker {

    FileMetaDataTool::FileMetaDataTool( const std::string& name )
	: asg::AsgMetadataTool( name ),
	  m_md(), m_mdAux() {

	declareProperty( "InputKey", m_inputKey = "CutBookkeepers" );
	declareProperty( "OutputKey", m_outputKey = "CutBookkeepers" );

#ifdef ASGTOOL_ATHENA
	declareInterface< ::IMetaDataTool >( this );
#endif // ASGTOOL_ATHENA
    }

    StatusCode FileMetaDataTool::initialize() {

	// Greet the user:
	ATH_MSG_DEBUG( "Initialising xAODMaker::FileMetaDataTool" );
	ATH_MSG_DEBUG( "  InputKey  = " << m_inputKey );
	ATH_MSG_DEBUG( "  OutputKey = " << m_outputKey );

	// Return gracefully:
	return StatusCode::SUCCESS;
    }

    StatusCode FileMetaDataTool::beginInputFile() {

	// If the input file doesn't have any file-level metadata, then
	// finish right away:
	if( ! inputMetaStore()->contains< xAOD::CutBookkeeperContainer >( m_inputKey ) ) {
	    return StatusCode::SUCCESS;
	}

	// Retrieve the input object:
	const xAOD::CutBookkeeperContainer* input = 0;
	ATH_CHECK( inputMetaStore()->retrieve( input, m_inputKey ) );

	// Create the output objects if they don't exist yet:
	if( ( ! m_md.get() ) && ( ! m_mdAux.get() ) ) {
	    ATH_MSG_DEBUG( "Creating output objects" );
	    m_md.reset( new xAOD::CutBookkeeperContainer() );
	    m_mdAux.reset( new xAOD::CutBookkeeperAuxContainer() );
	    m_md->setStore( m_mdAux.get() );

	    // Copy the payload of the input object:
	    *( m_md.get() ) = *input;
	}

	// Make sure that the objects are compatible:
	if( *( m_md.get() ) != *input ) {
	    ATH_MSG_ERROR( "Processing input files with differing conditions" );
	    ATH_MSG_ERROR( "Consistent xAOD::FileMetaData can't be provided for "
			   "the output" );
	    return StatusCode::FAILURE;
	}

	// Return gracefully:
	return StatusCode::SUCCESS;
    }

    StatusCode FileMetaDataTool::metaDataStop() {

	// Don't be offended if the metadata already exists in the output:
	if( outputMetaStore()->contains< xAOD::CutBookkeeperContainer >( m_outputKey ) ) {
	    ATH_MSG_DEBUG( "xAOD::FileMetaData already in the output" );
	    return StatusCode::SUCCESS;
	}

	// Record the metadata, if any was found on the input:
	if( m_md.get() && m_mdAux.get() ) {
	    ATH_MSG_DEBUG( "Recoding file level metadata" );
	    ATH_CHECK( outputMetaStore()->record( m_md.release(), m_outputKey ) );
	    ATH_CHECK( outputMetaStore()->record( m_mdAux.release(),
						  m_outputKey + "Aux." ) );
	}

	// Return gracefully:
	return StatusCode::SUCCESS;
    }

} // namespace xAODMaker
