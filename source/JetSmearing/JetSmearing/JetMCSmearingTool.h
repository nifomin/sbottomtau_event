#ifndef _INC_JET_MC_SMEARING_TOOL_
#define _INC_JET_MC_SMEARING_TOOL_
#include "SmearData.h"
#include "IJetMCSmearingTool.h"
#include "JetSmearing/JetMCSmearingParameters.h"
#include "xAODJet/Jet.h"

#include "AsgTools/AsgTool.h"

#include "TH2F.h"
#include "TF1.h"
#include <vector>

namespace JetSmearing {

  enum SmearingType {optimal, high, low, none };
  
  class JetMCSmearingTool : public virtual IJetMCSmearingTool, public asg::AsgTool {
	
    /** Athena constructor */
    ASG_TOOL_CLASS( JetMCSmearingTool, JetSmearing::IJetMCSmearingTool )

  public:

    /** Rootcore constructor */
    JetMCSmearingTool(const std::string& name);
    ~JetMCSmearingTool() ;

    virtual StatusCode initialize();

    /** Set pointers to response maps */
    void SetResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj) { this->AddResponseMaps(Rmap_lj, Rmap_bj) ; }

    /** Set pointers to correction files */
    void SetPhiCorrections(const TF1* data, const TF1* pseudo) { this->AddPhiCorrections(data,pseudo) ; }

    /** Add systematic response map or pair of maps */
    void AddSystematicResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj ) { this->AddResponseMaps(Rmap_lj, Rmap_bj) ; }

    /** Do the jet smearing */
    void DoSmearing(std::vector<std::unique_ptr<SmearData >>& smrMc, const xAOD::JetContainer& jets);

    /** Function to decorate the jet as a 'smearjet' **/
    //void DecorateSmeared(const xAOD::Jet& jets);

  private:
    /** Vector of light jet optimal response map + additional systematic variation response maps */
    std::vector<TH2F*> m_Rmap;
    /** Vector of b-tagged jet optimal response map + additional systematic variation response maps */
    std::vector<TH2F*> m_bJet_Rmap;
    /** For each light jet response map we create N 1D response histograms via projections, and store them in this vector */
    std::vector< std::vector<TH1F*> > m_Rvector; 
    /** For each b-tagged jet response map we create N 1D response histograms via projections, and store them in this vector */
    std::vector< std::vector<TH1F*> > m_bJet_Rvector; 
    /** Flag to toggle usage of b-jet smearing - default is true */
    bool m_doBJetSmearing;
    /** Number of times to smear each event */
    unsigned int m_nSmear;
    /** Pt Binning of response histograms */
    double m_ptBinning;
    /** Toggle whether to use tail weights scheme to study systematic uncertainties */
    bool m_doTailWeights;
    /** Toggle whether to smear Gaussian core of jet response */
    bool m_doSecondarySmearing;
    /** String version of Type of secondary Gaussian smearing - optimal or systematic (high or low) */
    std::string m_gaussianCoreSmearingTypeString;
    /** String version of Type of adjustment to make to the mean of the jet response - optimal (does nothing - so same as none), none or high/low */
    std::string  m_meanShiftTypeString;
    /** Type of secondary Gaussian smearing - optimal or systematic (high or low) */
    SmearingType m_gaussianCoreSmearingType;
    /** Type of adjustment to make to the mean of the jet response - optimal (does nothing - so same as none), none or high/low */
    SmearingType m_meanShiftType;
    /** Toggle whether to enable smearing of phi component of the jet response */
    bool m_doPhiSmearing;


    /** object to store the phi parameters **/
    JetMCSmearingParameters m_thePhiParameters;
    /** objec to store the gaussian core parameters **/
    JetMCSmearingParameters m_theCoreParameters;



    /** GeV value */
    const double m_GeV = 1000;
     /** Add response map or pair of maps */
    void AddResponseMaps(const TH2F* Rmap_lj , const TH2F* Rmap_bj );

     /** Add phi corrections  */
    void AddPhiCorrections(const TF1* data , const TF1* pseudo );

    /** Create 1D smearing histogram for light jets */
    const TH1F* MakeSmearingHist(const double& pt, const unsigned int& i); 

    /** Create 1D smearing histogram for b-tagged jets */
    const TH1F* MakeBJetSmearingHist(const double& pt, const unsigned int& i); 
    
    /** Calculate pt bin, in 2-D smearing histogram, for a jet with momentum pt */
    inline int getPtBin(const double& pt) { return static_cast<int>(pt/m_ptBinning) ; }

    /** Check if a jet is b-tagged */
    bool isJetBTagged(const xAOD::Jet& theJet);

    /** Calculate amount to smear Gaussian core of the jet response by */
    double calculateGaussianCoreSmearingValue(const JetMCSmearingParameters& theParameters, const double& jetPt, const double& meanValue , const bool& isPhiSmearing = false);

    /** Rotate Jet 4-vector around z-axis */
    void rotateZ(xAOD::Jet& theJet, const double& angle, TLorentzVector& theLorentzVector);

    /** Set the 4-vector of an xAOD:Jet */
    void setJet4Vector(xAOD::Jet& theJet, const TLorentzVector& theLorentzVector);

    /** Do kinematic selection of jets to smear */
    bool passSelection(const xAOD::Jet& theJet);

    /** Map String to Smearing Type */
    SmearingType mapStringToSmearingType(const std::string& theString);

  };

}
#endif
