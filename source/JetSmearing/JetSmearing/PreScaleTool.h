#ifndef PRESCALETOOL_H
#define PRESCALETOOL_H

#include "TH1F.h"
#include "TFile.h"
#include "TSystem.h"
#include "TKey.h"
#include <map>

#include <iostream>
#include "xAODJet/Jet.h"
#include "xAODEgamma/Photon.h"
#include "AsgTools/AsgTool.h"
#include "JetSmearing/IPreScaleTool.h"

namespace JetSmearing{

class PreScaleTool : public virtual IPreScaleTool,  public asg::AsgTool
{
  ASG_TOOL_CLASS( PreScaleTool, JetSmearing::IPreScaleTool )
 public:
  
  double getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , xAOD::Jet theJet, int runnumber);
  double getTriggerPrescale(std::vector<std::pair<std::string,bool > >  trigPass , double pT_1jet, std::string runnumber);

  PreScaleTool(const std::string& name);
  ~PreScaleTool() ;
  
  virtual StatusCode initialize();
  
  
 private:
  std::string m_histoPath;
  TFile* infile;
  std::map< std::string, TH1F*> _hmap;

};
}
#endif
 
