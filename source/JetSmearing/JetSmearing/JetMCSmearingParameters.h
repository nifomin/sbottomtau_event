#ifndef JETMCSMEARINGPARAMETERS_H
#define JETMCSMEARINGPARAMETERS_H

class  JetMCSmearingParameters {

 public:
  JetMCSmearingParameters();
  ~JetMCSmearingParameters ();
  void setParametersData(double N, double S, double C);
  void setParametersPseudo(double N, double S, double C);


  inline double getN_Data() const {return N_data;} 
  inline double getS_Data() const {return S_data;} 
  inline double getC_Data() const {return C_data;} 
  inline double getN_Smr() const {return N_smr;} 
  inline double getS_Smr() const {return S_smr;} 
  inline double getC_Smr() const {return C_smr;} 

 protected:
  double N_data;
  double S_data;
  double C_data;
  double N_smr;
  double S_smr;
  double C_smr;

};
#endif


