// Gaudi/Athena include(s):
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "JetSmearing/JetMCSmearingTool.h"
#include "JetSmearing/PreScaleTool.h"
#include "JetSmearing/SmearData.h"
#include "JetSmearing/IJetMCSmearingTool.h"
#include "JetSmearing/JetMCSmearingParametersBase.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "../ExampleSmrAthena.h"



DECLARE_NAMESPACE_TOOL_FACTORY( JetSmearing, JetMCSmearingTool )
DECLARE_NAMESPACE_TOOL_FACTORY( JetSmearing, PreScaleTool )
DECLARE_ALGORITHM_FACTORY(ExampleSmrAthena)

DECLARE_FACTORY_ENTRIES( JetSmearing )
{
  DECLARE_NAMESPACE_TOOL( JetSmearing, JetMCSmearingTool )
  DECLARE_NAMESPACE_TOOL( JetSmearing, PreScaleTool )
  DECLARE_ALGORITHM(ExampleSmrAthena)

}

