#ifndef JETSMEARING_MYALG_H
#define JETSMEARING_MYALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "JetSmearing/IJetMCSmearingTool.h"
#include "JetSmearing/IPreScaleTool.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

#include "xAODEventInfo/EventInfo.h" 

class ExampleSmrAthena: public ::AthAlgorithm { 
 public: 
  ExampleSmrAthena( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ExampleSmrAthena(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 
  ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;
  ToolHandle<JetSmearing::IJetMCSmearingTool> m_JetSmearing;
  ToolHandle<JetSmearing::IPreScaleTool> m_PreScaleTool;
  std::string m_bveto_map;
  std::string m_btag_map;
  
}; 

#endif
