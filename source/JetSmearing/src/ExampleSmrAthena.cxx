#include "ExampleSmrAthena.h"
#include <TH2F.h>
#include <TFile.h>

ExampleSmrAthena::ExampleSmrAthena( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration
  declareProperty( "SUSYTools",   m_SUSYTools      );
  declareProperty( "JetSmearing",   m_JetSmearing      );
  declareProperty( "PreScaleTool",   m_PreScaleTool      );
  declareProperty( "bveto_map",   m_bveto_map     );
  declareProperty( "btag_map",   m_btag_map      );
}


ExampleSmrAthena::~ExampleSmrAthena() {}


StatusCode ExampleSmrAthena::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //if (m_SUSYTools.retrieve().isFailure())
  //  {
  //   ATH_MSG_ERROR("Failed to retrieve tool: " << m_SUSYTools->name());
      //return StatusCode::FAILURE;
  // }
  
  //ATH_MSG_INFO("Retrieved tool: " << m_SUSYTools->name() );

  if (m_JetSmearing.retrieve().isFailure())
    {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_JetSmearing->name());
      return StatusCode::FAILURE;
    }
  
  ATH_MSG_INFO("Retrieved tool: " << m_JetSmearing->name() );


  if (m_PreScaleTool.retrieve().isFailure())
    {
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_PreScaleTool->name());
      return StatusCode::FAILURE;
    }
  
  ATH_MSG_INFO("Retrieved tool: " << m_PreScaleTool->name() );

  
 
 
  CHECK(m_JetSmearing->initialize());
   // get response maps for jets and bjets
  std::string tempname = m_bveto_map;//"JetSmearing/share/MC15/R_map2015_bveto_OP77_EJES_p2419SUSY11_MarchHADD.root";
  std::string tempnameB = m_btag_map;//"JetSmearing/share/MC15/R_map2015_btag_OP77_EJES_p2419SUSY11_MarchHADD.root";
  TFile* LFile = new TFile(tempname.c_str());
  TFile* BFile = new TFile(tempnameB.c_str());
  ATH_MSG_INFO("Getting File (L-jets) = " << tempname.c_str() << "Getting File (b-jets) = " << tempnameB.c_str());
  tempname = "responseEJES_p2419SUSY11";
  TH2F* tempMap = (TH2F*)LFile->Get(tempname.c_str());
  TH2F* myMap = (TH2F*)tempMap->Clone("map");
  tempnameB = "responseEJES_p2419SUSY11";
  TH2F* tempMapB = (TH2F*)BFile->Get(tempnameB.c_str());
  TH2F* myMapB = (TH2F*)tempMapB->Clone("Bmap");
  ATH_MSG_INFO("Getting Map (L-jets) = " << tempname.c_str() << "Getting Map (b-jets) = " << tempnameB.c_str());
  m_JetSmearing->SetResponseMaps(myMap,myMapB);

  CHECK(m_PreScaleTool->initialize());

 


  return StatusCode::SUCCESS;
}

StatusCode ExampleSmrAthena::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode ExampleSmrAthena::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  
  const xAOD::EventInfo* eventInfo = 0;
  CHECK(evtStore()->retrieve( eventInfo , "EventInfo") );
  ATH_MSG_INFO("Runnumber = " << eventInfo->runNumber());


  //get jets
  const xAOD::JetContainer* jets (0);
  std::string m_JetsName("AntiKt4EMTopoJets");
  CHECK( evtStore()->retrieve( jets, m_JetsName) );
  
  xAOD::JetContainer* m_jets(0); 
  xAOD::ShallowAuxContainer* m_jets_aux(0);
  CHECK(m_SUSYTools->GetJets(m_jets,m_jets_aux));


  //decorate jets intended to be smeared
  for( const auto &jet : *m_jets ) {
    jet->auxdata<char>("smearjet") = false;
    if(/*jet->auxdata< char >("signal")==1 &&  jet->auxdata< char >("passOR")==1 && */jet->auxdata< char >("baseline")==1) {
      if(jet->p4().Pt()>20000 && fabs(jet->p4().Eta())<2.8)
	jet->auxdata<char>("smearjet") = true;
    }
  }


  // get hlt jets for prescale tool
  const xAOD::JetContainer * hlt_jet (0);
  TString mc_name="HLT_xAOD__JetContainer_a4tcemsubjesFS";
  if( ! evtStore()->retrieve( hlt_jet, mc_name.Data()).isSuccess() )
    Error("execute()", Form("failed to retrieve %s", mc_name.Data()));

  
  std::string single_jet_triggers [16] = {"HLT_j400","HLT_j380","HLT_j360","HLT_j320","HLT_j300","HLT_j260","HLT_j200","HLT_j175",
						"HLT_j150","HLT_j110","HLT_j100","HLT_j85","HLT_j60","HLT_j55","HLT_j25","HLT_j15"};

  std::vector<std::pair<std::string,bool > > triggersPass;
  for(const std::string &sjt : single_jet_triggers)
    {
      triggersPass.push_back( std::make_pair (sjt,  m_SUSYTools->IsTrigPassed(sjt)));
    }

  //use the prescale tool to get the correct prescale weight using the single jet triggers
  double m_TriggerWeight = 0.;
  int runnumber=eventInfo->runNumber();
  if(hlt_jet->size()>0){
    m_TriggerWeight = m_PreScaleTool->getTriggerPrescale(triggersPass,*(hlt_jet->at(0)),runnumber);
  }
  ATH_MSG_INFO("Trigger weight = " << m_TriggerWeight << " Pt= " << hlt_jet->at(0)->p4().Pt());



  /// here you should do your seed selection test e.g. met/sqrt(sumet)<0.5 
  bool m_passSeedSelection = true;
  
  if(!m_passSeedSelection)
    return StatusCode::SUCCESS;

  std::vector<std::unique_ptr<SmearData > > smrMc;
  std::vector<xAOD::Jet*> smrJetVec;
  m_JetSmearing->DoSmearing(smrMc,*m_jets);
  
  std::vector<std::unique_ptr<SmearData > >::iterator firstSmear = smrMc.begin();
  std::vector<std::unique_ptr<SmearData > >::iterator lastSmear = smrMc.end();
  
  // m_SMRmetContainerAux = new xAOD::MissingETAuxContainer;
  // m_SMRmetContainer->setStore(m_SMRmetContainerAux);
  // CHECK( store->record(m_SMRmetContainer,"SMRMet" ));
  // CHECK( store->record(m_SMRmetContainerAux,"SMRMetAux." ));

  for(; firstSmear != lastSmear; ++firstSmear){          
    smrJetVec.clear();
    smrJetVec.shrink_to_fit();
    //m_SMRmetContainer->clear();
    // CHECK(m_objTool->GetMET(*m_SMRmetContainer ,(*firstSmear)->jetContainer,m_electrons_copy,m_muons_copy,m_photons_copy,0,true));
      
    xAOD::JetContainer::iterator firstJet = (*firstSmear)->jetContainer->begin();
    xAOD::JetContainer::iterator lastJet = (*firstSmear)->jetContainer->end();
    
    
    for(; firstJet != lastJet; ++firstJet)
      {                    
	if ( (*firstJet)->auxdata< char >("smearjet") == true )
	  {
	    smrJetVec.push_back(*firstJet);
	    ATH_MSG_INFO("smrJet Pt = " << (*firstJet)->pt());
	  }
      }
    //std::sort(smrJetVec.begin(),smrJetVec.end(),ptsorter);
  }
  
  return StatusCode::SUCCESS;
}


