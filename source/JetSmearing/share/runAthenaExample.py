#Author: Will Buttinger
#modified by Calum Macdonald

#This is the configuration file for an athena job using SUSYTools to calibrate physics objects
#You would use this joboption by copying it and substituting the TestAlg for your own algorithm
#and subtituting your own input files


import AthenaPoolCnvSvc.ReadAthenaPool #read xAOD files


theApp.EvtMax = 1000 #set to -1 to run on all events


inputFile = "/atlas/macdonald/atlas1/DATA16/p2667/test/data16_13TeV.00300687.physics_Main.merge.DAOD_SUSY11.f708_m1606_p2667/data16_13TeV/DAOD_SUSY11.08622732._000245.pool.root.1" #os.environ['ASG_TEST_FILE_DATA'] #test input file
#inputFile = os.environ['ASG_TEST_FILE_MC']
svcMgr.EventSelector.InputCollections = [ inputFile ] #specify input files here, takes a list


print  CfgMgr.JetSmearing__JetMCSmearingTool

ToolSvc += CfgMgr.JetSmearing__JetMCSmearingTool("JetMCSmearingTool")
ToolSvc += CfgMgr.JetSmearing__PreScaleTool("PreScaleTool")
ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD("SUSYTools")
ToolSvc.SUSYTools.ConfigFile = "SUSYTools/SUSYTools_Default.conf" #look in the data directory of SUSYTools for other config files
ToolSvc.SUSYTools.PRWConfigFiles = [
    "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15ab_defaults.NotRecommended.prw.root", 
    "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root"
    ]
#ToolSvc.SUSYTools.PRWLumiCalcFiles = ["/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/ilumicalc_histograms_None_276262-284154_IBLOFF.root"]
ToolSvc.SUSYTools.PRWLumiCalcFiles = ["/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/ilumicalc_histograms_None_276262-284154.root"]

ToolSvc.SUSYTools.DataSource = 1  #configure to run on data




algseq = CfgMgr.AthSequencer("AthAlgSeq") #The main alg sequence

#this bit is used in the ATN nightly tests, for appending event rates to previous results
try:
    from TrigValTools.Utils import getPreviousNightlyPath
    myPath = getPreviousNightlyPath()
except ImportError:
    myPath=""


algseq += CfgMgr.ExampleSmrAthena("SMRAlg")
from PathResolver import PathResolver
ToolSvc.PreScaleTool.HistoPath="../source/JetSmearing/share/PreScales/prescale_histos_combined_2015-2016_v83-pro20-15.root"
ToolSvc.JetMCSmearingTool.NumberOfSmearedEvents=100
ToolSvc.JetMCSmearingTool.DoPhiSmearing=True

algseq.SMRAlg.JetSmearing = ToolSvc.JetMCSmearingTool
algseq.SMRAlg.PreScaleTool = ToolSvc.PreScaleTool
algseq.SMRAlg.SUSYTools = ToolSvc.SUSYTools
algseq.SMRAlg.bveto_map = "../source/JetSmearing/share/MC15/R_map2015_bveto_OP77_EJES_p2419SUSY11_MarchHADD.root"
algseq.SMRAlg.btag_map  = "../source/JetSmearing/share/MC15/R_map2015_btag_OP77_EJES_p2419SUSY11_MarchHADD.root"


#That completes the minimum configuration. The rest is extra....
svcMgr.MessageSvc.Format = "% F%50W%S%7W%R%T %0W%M" #Creates more space for displaying tool names
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=100) #message every 100 events processed

#this is for the output of histograms
svcMgr+=CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='monitoring.data.root' OPT='RECREATE'"]
