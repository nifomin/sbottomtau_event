pwd
test_submit --inputPath=$1 --baselineNtuple --bjetTrig --localTauConfig --enableAllSyst
#normalise_samples submitDir/data-miniVectorNtuple_SbotPlateau/mc16_13TeV.436531.MGPy8EG_A14N23LO_BB_onestepN2hN1_850_180_50_1tau.deriv.DAOD_SUSY3.e6967_a875_r9364_p3712.root test.root 25000
normalise_samples submitDir/data-miniVectorNtuple_SbotPlateau/*.root test.root 25000
add_bjet_trig_sf test.root
flatten_ntuples test.root flat_test.root
divide_ntuples_rename flat_test.root $2
